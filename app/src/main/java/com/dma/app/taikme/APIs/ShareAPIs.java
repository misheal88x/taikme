package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ShareAPIs {

    @FormUrlEncoded
    @POST("share/{id}/{type}")
    Call<BaseResponse> share(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("id") String id,
            @Path("type") String type,
            @Field("sharing_text") String sharing_text
    );
}
