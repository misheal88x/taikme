package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CategoryObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("parent_id") private int parent_id = 0;
    @SerializedName("en_name") private String en_name = "";
    @SerializedName("ar_name") private String ar_name = "";
    @SerializedName("is_highlighted") private String is_highlighted = "";
    @SerializedName("image") private String image = "";
    @SerializedName("thumb_image") private String thumb_image = "";
    @SerializedName("icon") private String icon = "";
    @SerializedName("sub_categories") private List<CategoryObject> sub_categories = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name;
    }

    public String getAr_name() {
        return ar_name;
    }

    public void setAr_name(String ar_name) {
        this.ar_name = ar_name;
    }

    public String getIs_highlighted() {
        return is_highlighted;
    }

    public void setIs_highlighted(String is_highlighted) {
        this.is_highlighted = is_highlighted;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<CategoryObject> getSub_categories() {
        return sub_categories;
    }

    public void setSub_categories(List<CategoryObject> sub_categories) {
        this.sub_categories = sub_categories;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }
}
