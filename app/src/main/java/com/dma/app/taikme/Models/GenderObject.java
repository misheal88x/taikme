package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class GenderObject {
    @SerializedName("MALE") private int male = 0;
    @SerializedName("FEMALE") private int female = 0;
    @SerializedName("OTHER") private int other = 0;
    @SerializedName("ALL") private int all = 0;

    public int getMale() {
        return male;
    }

    public void setMale(int male) {
        this.male = male;
    }

    public int getFemale() {
        return female;
    }

    public void setFemale(int female) {
        this.female = female;
    }

    public int getOther() {
        return other;
    }

    public void setOoth(int both) {
        this.other = both;
    }

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
