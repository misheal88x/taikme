package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.dma.app.taikme.Models.NearByPlaceObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class CreatePostLocationAdapter extends  RecyclerView.Adapter<CreatePostLocationAdapter.ViewHolder> {
    private Context context;
    private List<NearByPlaceObject> list;
    private int selected_place = 0;
    private int selected_location_id = 0;

    public CreatePostLocationAdapter(Context context,List<NearByPlaceObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView img_profile;
        private TextView tv_name,tv_address;
        private SimpleRatingBar rb_rate;
        private ImageView btn_accept,btn_cancel;
        public ViewHolder(View view) {
            super(view);
            img_profile = view.findViewById(R.id.item_create_post_location_image);
            tv_name = view.findViewById(R.id.item_create_post_location_user_name);
            tv_address = view.findViewById(R.id.item_create_post_location_address);
            btn_accept = view.findViewById(R.id.item_create_post_location_check);
            btn_cancel = view.findViewById(R.id.item_create_post_location_cancel);
            rb_rate = view.findViewById(R.id.item_create_post_location_rate);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_create_post_location, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final NearByPlaceObject o = list.get(position);

        holder.tv_name.setText(o.getName());
        holder.tv_address.setText(o.getLocation_address());
        holder.rb_rate.setRating(Float.valueOf(o.getBusiness_avg_review()));
        BaseFunctions.setGlideImage(context,holder.img_profile,o.getBusiness_thumb_image());
        if (o.getIs_selected()){
            holder.btn_accept.setVisibility(View.GONE);
            holder.btn_cancel.setVisibility(View.VISIBLE);
        }else {
            holder.btn_accept.setVisibility(View.VISIBLE);
            holder.btn_cancel.setVisibility(View.GONE);
        }
        holder.btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_place = o.getId();
                selected_location_id = o.getBusiness_location_id();
                o.setIs_selected(true);
                list.set(position,o);
                notifyItemChanged(position);
                for (int i = 0; i <list.size() ; i++) {
                    if (i!=position){
                        NearByPlaceObject n = list.get(i);
                        n.setIs_selected(false);
                        list.set(i,n);
                        notifyItemChanged(i);
                    }
                }
            }
        });

        holder.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_place = 0;
                selected_location_id = 0;
                o.setIs_selected(false);
                list.set(position,o);
                notifyItemChanged(position);
                for (int i = 0; i < list.size(); i++) {
                    if (i!=position){
                        NearByPlaceObject n = list.get(i);
                        n.setIs_selected(false);
                        list.set(i,n);
                        notifyItemChanged(i);
                    }
                }
            }
        });
    }

    public int get_selected_place(){
        return selected_place;
    }

    public int get_selected_location_id(){
        return selected_location_id;
    }
}
