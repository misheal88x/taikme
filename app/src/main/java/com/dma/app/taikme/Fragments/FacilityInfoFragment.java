package com.dma.app.taikme.Fragments;

import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Emitter;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Cancellable;

import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.dma.app.taikme.APIsClass.BusinessLocationsAPIsClass;
import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.APIsClass.OffersAPIsClass;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.APIsClass.ReviewsAPIClass;
import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Activities.PickPlaceMapActivity;
import com.dma.app.taikme.Activities.RecordVideoActivity;
import com.dma.app.taikme.Activities.ViewVideoActivity;
import com.dma.app.taikme.Adapters.FacilityInfoLocationsAdapter;
import com.dma.app.taikme.Adapters.FacilityInfoMediaAdapter;
import com.dma.app.taikme.Adapters.FacilityInfoOffersAdapter;
import com.dma.app.taikme.Adapters.FacilityInfoReviewsAdapter;
import com.dma.app.taikme.Adapters.ProfileMediaAdapter;
import com.dma.app.taikme.Dialogs.AddLocationDialog;
import com.dma.app.taikme.Dialogs.AddOfferDialog;
import com.dma.app.taikme.Dialogs.AmplifyDialog;
import com.dma.app.taikme.Dialogs.ChooseSelectionDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Interfaces.ISelection;
import com.dma.app.taikme.Models.BusinessLocatonNewObject;
import com.dma.app.taikme.Models.DumpPostObject;
import com.dma.app.taikme.Models.OfferObject;
import com.dma.app.taikme.Models.OffersResponse;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Models.PostObject;
import com.dma.app.taikme.Models.ProfileMediaObject;
import com.dma.app.taikme.Models.ProfileMediaResponse;
import com.dma.app.taikme.Models.ProfileNewResponse;
import com.dma.app.taikme.Models.ReviewNewObject;
import com.dma.app.taikme.Models.ReviewObject;
import com.dma.app.taikme.Models.ReviewsNewResponse;
import com.dma.app.taikme.Models.ReviewsResponse;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.github.tcking.giraffecompressor.GiraffeCompressor;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class FacilityInfoFragment extends BaseFragment implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate{

    private TextView tv_name,tv_nickname,tv_rate,tv_followers,tv_desc,tv_recommends,tv_recommends_value;
    private CircleImageView img_profile;
    private SimpleRatingBar rb_rate;
    private LinearLayout btn_media,btn_offers,btn_reviews,btn_locations;
    private AVLoadingIndicatorView pb_loading,pb_more;
    private RecyclerView rv_section;
    private LinearLayoutManager layoutManager;
    private FacilityInfoMediaAdapter mediaAdapter;
    private FacilityInfoOffersAdapter offersAdapter;
    private FacilityInfoLocationsAdapter locationsAdapter;
    private FacilityInfoReviewsAdapter reviewsAdapter;
    private List<OfferObject> listOfOffers;
    private List<ReviewNewObject> listOfReviews;
    private List<BusinessLocatonNewObject> listOfLocations;
    private List<ProfileMediaObject> listOfMedia;
    private RelativeLayout lyt_offer,lyt_location;
    private LinearLayout lyt_review,lyt_media;
    private ImageView btn_speaker,btn_audience,btn_events,btn_follow,btn_call,btn_three_dots;
    private ImageView icon_media,icon_reviews,icon_offers,icon_location;
    private TextView txt_media,txt_reviews,txt_offers,txt_location;
    private TextView tv_section_desc;
    //Review layout contents
    private Button btn_add_review;
    private SimpleRatingBar rb_add_review;
    private EditText edt_add_review;
    private String selected_review_rate = "";

    private RelativeLayout root,private_layout;
    private NestedScrollView scrollView;
    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;
    private int selected_list = 1;
    private int place_id = 0;
    private boolean mine = false;
    private int is_followed = 0;
    private String profile_image = "";
    private String username = "";
    private String address = "";
    private float rating = 0f;

    private String selected_review_value = "";

    private AddLocationDialog location_dialog;

    private ImageView btn_create_media_image,btn_create_media_video;
    private EditText edt_create_media_caption;
    private Button btn_create_media_upload;
    private String imagePath = "";
    private String videoPath = "";
    private String newPath = "";
    private File imageFile,videoFile;
    private int media_type = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_facility_info,container,false);
    }

    @Override
    public void init_views() {
        super.init_views();
        //RelativeLayout
        root = base.findViewById(R.id.facility_info_layout);
        private_layout = base.findViewById(R.id.private_layout);
        //NestedScrollView
        scrollView = base.findViewById(R.id.facility_info_scroll);
        //TextView
        tv_name = base.findViewById(R.id.facility_info_name);
        tv_nickname = base.findViewById(R.id.facility_info_nickname);
        tv_rate = base.findViewById(R.id.facility_info_rate_txt);
        tv_followers = base.findViewById(R.id.facility_info_followers);
        tv_desc = base.findViewById(R.id.facility_info_desc);
        tv_recommends = base.findViewById(R.id.facility_info_recommends);
        tv_recommends_value = base.findViewById(R.id.facility_info_recommends_value);
        txt_media = base.findViewById(R.id.facility_info_media_txt);
        txt_reviews = base.findViewById(R.id.facility_info_reviews_txt);
        txt_offers = base.findViewById(R.id.facility_info_offers_txt);
        txt_location = base.findViewById(R.id.facility_info_locations_txt);
        img_profile = base.findViewById(R.id.facility_info_profile_image);
        tv_section_desc = base.findViewById(R.id.profile_section_desc);
        //Simple Rating Bar
        rb_rate = base.findViewById(R.id.facility_info_rate_rate);
        rb_add_review = base.findViewById(R.id.facility_info_review_rate);
        //Relative Layout
        lyt_review = base.findViewById(R.id.facility_info_review_layout);
        lyt_offer = base.findViewById(R.id.facility_info_offer_layout);
        lyt_location = base.findViewById(R.id.facility_info_location_layout);
        lyt_media = base.findViewById(R.id.facility_info_media_layout);
        //AVLoadingIndicator
        pb_loading = base.findViewById(R.id.facility_info_loading_section);
        pb_more = base.findViewById(R.id.facility_info_more);
        //RecyclerView
        rv_section = base.findViewById(R.id.facility_info_section_recycler);
        //ImageView
        btn_speaker = base.findViewById(R.id.facility_info_speaker);
        btn_audience = base.findViewById(R.id.facility_info_people);
        btn_follow = base.findViewById(R.id.facility_info_follow);
        btn_call = base.findViewById(R.id.facility_info_call);
        btn_events = base.findViewById(R.id.facility_info_events_btn);
        btn_media = base.findViewById(R.id.facility_info_media_lyt);
        btn_offers = base.findViewById(R.id.facility_info_offers_lyt);
        btn_reviews = base.findViewById(R.id.facility_info_reviews_lyt);
        btn_locations = base.findViewById(R.id.facility_info_locations_lyt);
        icon_media = base.findViewById(R.id.facility_info_media_icon);
        icon_reviews = base.findViewById(R.id.facility_info_reviews_icon);
        icon_offers = base.findViewById(R.id.facility_info_offers_icon);
        icon_location = base.findViewById(R.id.facility_info_locations_icon);
        btn_three_dots = base.findViewById(R.id.facility_info_three_dots);
        btn_create_media_image = base.findViewById(R.id.facility_info_media_image);
        btn_create_media_video = base.findViewById(R.id.facility_info_media_video);
        //Buttom
        btn_add_review = base.findViewById(R.id.facility_info_review_btn);
        btn_create_media_upload = base.findViewById(R.id.facility_info_media_upload);
        //EditText
        edt_add_review = base.findViewById(R.id.facility_info_review_txt);
        edt_create_media_caption = base.findViewById(R.id.facility_info_media_caption);

    }

    @Override
    public void init_events() {
        btn_three_dots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final BottomSheetDialog dialog = new BottomSheetDialog(base);
                dialog.setContentView(R.layout.dialog_bottom_post_more);
                dialog.setCanceledOnTouchOutside(true);
                TextView txt1 = dialog.findViewById(R.id.txt1);
                TextView txt2 = dialog.findViewById(R.id.txt2);
                TextView txt3 = dialog.findViewById(R.id.txt3);
                View v1 = dialog.findViewById(R.id.view1);
                View v2 = dialog.findViewById(R.id.view2);
                if (is_followed == 0){
                    txt2.setText(getResources().getString(R.string.bottom_dialog_post_follow_business));
                }else {
                    txt2.setText(getResources().getString(R.string.bottom_dialog_post_unfollow_business));
                }
                txt1.setVisibility(View.GONE);
                txt3.setVisibility(View.GONE);
                v1.setVisibility(View.GONE);
                v2.setVisibility(View.GONE);

                txt2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callFollowAPI(String.valueOf(place_id),is_followed==0?"yes":"no");
                        dialog.cancel();
                    }
                });
                dialog.show();
            }
        });

        btn_reviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeTab(2);
                selected_list = 3;
                continue_paginate = true;
                if (!mine){
                    lyt_review.setVisibility(View.VISIBLE);
                }
                tv_section_desc.setText(getResources().getString(R.string.facility_info_no_reviews));
                tv_section_desc.setVisibility(View.GONE);
                lyt_location.setVisibility(View.GONE);
                lyt_media.setVisibility(View.GONE);
                lyt_offer.setVisibility(View.GONE);
                listOfReviews = new ArrayList<>();
                currentPage = 1;
                reviewsAdapter = new FacilityInfoReviewsAdapter(base,listOfReviews);
                rv_section.setAdapter(reviewsAdapter);
                if (place_id == 0 || place_id==-1){
                    callReviewsAPI(currentPage,0,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                }else {
                    callReviewsAPI(currentPage,0,String.valueOf(place_id));
                }
            }
        });
        btn_media.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeTab(0);
                selected_list = 1;
                continue_paginate = true;
                lyt_review.setVisibility(View.GONE);
                lyt_location.setVisibility(View.GONE);
                if (mine) {
                    lyt_media.setVisibility(View.VISIBLE);
                }
                tv_section_desc.setText(getResources().getString(R.string.facility_info_no_media));
                tv_section_desc.setVisibility(View.GONE);
                lyt_offer.setVisibility(View.GONE);
                listOfMedia = new ArrayList<>();
                currentPage = 1;
                mediaAdapter = new FacilityInfoMediaAdapter(base,listOfMedia,
                        mine?"with":"without");
                rv_section.setAdapter(mediaAdapter);
                if (place_id == 0 || place_id == -1){
                    callMediaAPI(currentPage,0,
                            String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                }else {
                    callMediaAPI(currentPage,0,
                            String.valueOf(place_id));
                }

            }
        });
        btn_offers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeTab(1);
                selected_list = 2;
                continue_paginate = true;
                lyt_review.setVisibility(View.GONE);
                lyt_location.setVisibility(View.GONE);
                lyt_media.setVisibility(View.GONE);
                if (mine) {
                    lyt_offer.setVisibility(View.VISIBLE);
                }
                tv_section_desc.setText(getResources().getString(R.string.facility_info_no_offers));
                tv_section_desc.setVisibility(View.GONE);
                listOfOffers = new ArrayList<>();
                currentPage = 1;
                offersAdapter = new FacilityInfoOffersAdapter(base,listOfOffers);
                rv_section.setAdapter(offersAdapter);
                if (place_id == -1 || place_id == 0){
                    callOffersAPI(currentPage,0,String.valueOf(
                            SharedPrefManager.getInstance(base).getUser().getId()));
                }else {
                    callOffersAPI(currentPage,0,String.valueOf(place_id));
                }
            }
        });

        btn_locations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activeTab(3);
                selected_list = 4;
                continue_paginate = true;
                lyt_review.setVisibility(View.GONE);
                if (mine) {
                    lyt_location.setVisibility(View.VISIBLE);
                }
                tv_section_desc.setText(getResources().getString(R.string.facility_info_no_locations));
                tv_section_desc.setVisibility(View.GONE);
                lyt_media.setVisibility(View.GONE);
                lyt_offer.setVisibility(View.GONE);
                listOfLocations = new ArrayList<>();
                currentPage = 1;
                locationsAdapter = new FacilityInfoLocationsAdapter(base,listOfLocations);
                rv_section.setAdapter(locationsAdapter);
                if (place_id == -1 || place_id == 0){
                    callLocationsAPI(String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                }else {
                    callLocationsAPI(String.valueOf(place_id));
                }
            }
        });
        btn_speaker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AmplifyDialog dialog = new AmplifyDialog(base,"","");
                dialog.show();
            }
        });
        btn_follow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callFollowAPI(String.valueOf(place_id),is_followed==0?"yes":"no");
            }
        });
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        switch (selected_list){
                            case 1:{
                                if (listOfMedia.size()>=per_page){
                                    if (continue_paginate){
                                        currentPage++;
                                        if (place_id==-1 || place_id==0){
                                            callMediaAPI(currentPage,1,
                                                    String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                        }else {
                                            callMediaAPI(currentPage,1,
                                                    String.valueOf(place_id));
                                        }
                                    }
                                }
                            }break;
                            case 2:{
                                if (listOfOffers.size()>=per_page){
                                    if (continue_paginate){
                                        currentPage++;
                                        if (place_id == 0 || place_id == -1){
                                            callOffersAPI(currentPage,1,
                                                    String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                        }else {
                                            callOffersAPI(currentPage,1,
                                                    String.valueOf(place_id));
                                        }

                                    }
                                }
                            }break;
                            case 3:{if (listOfReviews.size()>=per_page){
                                if (continue_paginate){
                                    currentPage++;
                                    if (place_id == -1 || place_id == 0){
                                        callReviewsAPI(currentPage,1,String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                    }else {
                                        callReviewsAPI(currentPage,1,String.valueOf(place_id));
                                    }
                                }
                            }}break;
                            case 4:{}break;
                        }
                    }
                }
            }
        });
        btn_audience.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                YourAudienceFragment yourAudienceFragment = new YourAudienceFragment();
                base.open_fragment(yourAudienceFragment);
            }
        });
        btn_events.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventsFragment fragment = new EventsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type","facility");
                fragment.setArguments(bundle);
                base.open_fragment(fragment);
            }
        });
        lyt_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddOfferDialog dialog = new AddOfferDialog(base);
                dialog.show();
            }
        });
        lyt_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                location_dialog = new AddLocationDialog(base, new IMove() {
                    @Override
                    public void move() {
                        final LocationManager manager = (LocationManager) base.getSystemService( Context.LOCATION_SERVICE );
                        if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                            buildAlertMessageNoGps();
                        }else {
                            Intent intent = new Intent(base,PickPlaceMapActivity.class);
                            startActivityForResult(intent,100);
                        }
                    }

                    @Override
                    public void move(int position) {

                    }
                });
                location_dialog.show();
            }
        });
        rb_add_review.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                selected_review_rate = String.valueOf(rating);
            }
        });
        btn_add_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_review_rate.equals("")){
                    Toast.makeText(base, getResources().getString(R.string.facility_info_add_review_no_rate), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (edt_add_review.getText().toString().equals("")){
                    Toast.makeText(base, getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                    return;
                }

                callAddReviewAPI(place_id==-1?String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()):
                        String.valueOf(place_id),selected_review_rate,edt_add_review.getText().toString());
            }
        });


        btn_create_media_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseSelectionDialog dialog = new ChooseSelectionDialog(base, "camera", new ISelection() {
                    @Override
                    public void onTakeCameraClicked() {
                        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                        StrictMode.setVmPolicy(builder.build());
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        imageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                "test.jpg");
                        Uri tempUri = Uri.fromFile(imageFile);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT,tempUri);
                        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,1);
                        startActivityForResult(intent,110);
                    }

                    @Override
                    public void onTakeVideoClicked() {

                    }

                    @Override
                    public void onCameraGalleryClicked() {
                        BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.dma.app.taikme.provider")
                                .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                                .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                                .build();
                        singleSelectionPicker.show(getChildFragmentManager(),"picker");
                    }

                    @Override
                    public void onVideoGalleryClicked() {

                    }
                });
                dialog.show();
            }
        });
        btn_create_media_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseSelectionDialog dialog = new ChooseSelectionDialog(base, "video", new ISelection() {
                    @Override
                    public void onTakeCameraClicked() {

                    }

                    @Override
                    public void onTakeVideoClicked() {
                        Intent intent = new Intent(base, RecordVideoActivity.class);
                        startActivityForResult(intent,102);
                    }

                    @Override
                    public void onCameraGalleryClicked() {

                    }

                    @Override
                    public void onVideoGalleryClicked() {
                        Intent intent = new Intent();
                        intent.setType("video/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent,"Select Video"),101);
                    }
                });
                dialog.show();
            }
        });

        btn_create_media_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_create_media_caption.getText().toString().equals("")){
                    Toast.makeText(base, getResources().getString(R.string.create_post_no_caption), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (media_type == 0){
                    Toast.makeText(base, getString(R.string.create_post_no_media), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (media_type == 1){
                    if (imagePath.equals("")||imagePath==null){
                        Toast.makeText(base, getResources().getString(R.string.create_event_no_image), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(base, getResources().getString(R.string.create_post_creating), Toast.LENGTH_SHORT).show();
                    BaseFunctions.uploadPost(base,edt_create_media_caption.getText().toString(),PostObject.TYPE_PHOTO,edt_create_media_caption.getText().toString(),
                            "","",imagePath);
                    edt_create_media_caption.setText("");
                    /*
                    Intent intent = new Intent(base, UploadStoryService.class);
                    intent.putExtra("service_type","post");
                    intent.putExtra("topic_id","1");
                    intent.putExtra("caption",edt_caption.getText().toString());
                    intent.putExtra("post_type",PostObject.TYPE_PHOTO);
                    intent.putExtra("filePath",imagePath);
                    base.startService(intent);
                    **/
                    //base.open_fragment(new HomeFragment());
                }else if (media_type == 2){
                    if (newPath.equals("")||newPath==null){
                        Toast.makeText(base, getResources().getString(R.string.create_post_no_video), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Toast.makeText(base, getResources().getString(R.string.create_post_creating), Toast.LENGTH_SHORT).show();
                    BaseFunctions.uploadPost(base,edt_create_media_caption.getText().toString(),PostObject.TYPE_VIDEO,edt_create_media_caption.getText().toString(),
                            "","",newPath);
                    edt_create_media_caption.setText("");
                    //base.open_fragment(new HomeFragment());
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        try{
            place_id = this.getArguments().getInt("place_id");
            if (place_id == 0){
                btn_follow.setVisibility(View.GONE);
                btn_call.setVisibility(View.GONE);
                btn_speaker.setVisibility(View.GONE);
                btn_three_dots.setVisibility(View.GONE);
                mine = true;
            }else if (place_id == SharedPrefManager.getInstance(base).getUser().getId()){
                btn_follow.setVisibility(View.GONE);
                btn_call.setVisibility(View.GONE);
                btn_speaker.setVisibility(View.GONE);
                btn_three_dots.setVisibility(View.GONE);
                mine = true;
            } else {
                btn_speaker.setVisibility(View.GONE);
                btn_audience.setVisibility(View.GONE);
                btn_events.setVisibility(View.GONE);
                mine = false;
            }
        }catch (Exception e){
            place_id = -1;
            btn_follow.setVisibility(View.GONE);
            btn_call.setVisibility(View.GONE);
            mine = true;
        }
        init_recycler();
        if (place_id == -1 || place_id == 0){
            callProfileAPI(String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
        }else {
            callProfileAPI(String.valueOf(place_id));
        }

        //restore_data();
    }

    private void buildAlertMessageNoGps(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(base);
        builder.setMessage(getResources().getString(R.string.gps_disabled))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.gps_turn_on), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    private void init_recycler(){
        layoutManager = new LinearLayoutManager(base,RecyclerView.VERTICAL,false);
        rv_section.setLayoutManager(layoutManager);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case 100: {
                if (resultCode == RESULT_OK) {
                    String lat = data.getStringExtra("lat");
                    String lng = data.getStringExtra("lng");
                    location_dialog.setLatLng(lat, lng);
                }
            }
            break;

            case 110: {
                if (resultCode == RESULT_OK) {
                    if (imageFile.exists()) {
                        compressImage(imageFile);
                    } else {
                        Toast.makeText(base, "Image file does not exists", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            case 101: {
                if (resultCode == RESULT_OK) {
                    Uri selectedImageUri = data.getData();
                    videoPath = getPath(base, selectedImageUri);
                    File old_file = new File(videoPath);
                    int file_size = Integer.parseInt(String.valueOf(old_file.length() / 1024));
                    if (file_size > 3000) {
                        Toast.makeText(base, getResources().getString(R.string.create_post_large), Toast.LENGTH_LONG).show();
                    } else {
                        Intent intent = new Intent(base, ViewVideoActivity.class);
                        intent.putExtra("video_path", videoPath);
                        startActivityForResult(intent, 103);
                    }
                }
            }
            break;
            case 102: {
                if (resultCode == RESULT_OK) {
                    videoPath = data.getStringExtra("video_path");
                    if (videoPath != null) {
                        File file = new File(videoPath);
                        if (file.exists()) {
                            Intent intent = new Intent(base, ViewVideoActivity.class);
                            intent.putExtra("video_path", videoPath);
                            startActivityForResult(intent, 103);
                        } else {
                            Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            case 103: {
                if (resultCode == RESULT_OK) {
                    videoPath = data.getStringExtra("video_path");
                    File file = new File(videoPath);
                    final ProgressDialog progressDialog = new ProgressDialog(base);
                    progressDialog.setMessage(getResources().getString(R.string.create_status_prepare_video));
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    newPath = String.format("/sdcard/%d.mp4", System.currentTimeMillis());
                    File newFile = new File(newPath);
                    GiraffeCompressor.init(base);
                    GiraffeCompressor.create("ffmpeg")
                            .input(file)
                            .output(newFile)
                            .bitRate(690000)
                            .resizeFactor(1.0f)
                            .ready()
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Emitter<GiraffeCompressor.Result>() {
                                @Override
                                public void setSubscription(Subscription s) {

                                }

                                @Override
                                public void setCancellation(Cancellable c) {

                                }

                                @Override
                                public long requested() {
                                    return 0;
                                }

                                @Override
                                public void onCompleted() {

                                }

                                @Override
                                public void onError(Throwable e) {

                                }

                                @Override
                                public void onNext(GiraffeCompressor.Result s) {
                                    progressDialog.cancel();
                                    media_type = 2;
                                }
                            });
                }
            }
        }
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {
                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static MultipartBody.Part getMultiPartBody(String key, String mMediaUrl) {
        if (mMediaUrl != null) {
            File file = new File(mMediaUrl);
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
            return MultipartBody.Part.createFormData(key, file.getName(), requestFile);
        } else {
            return MultipartBody.Part.createFormData(key, "");
        }
    }

    private void callReviewsAPI(final int page, final int type,final  String id){
        if (type == 0){
            pb_loading.smoothToShow();
        }else {
            pb_more.smoothToShow();
        }
        ReviewsAPIClass.getReviews(
                base,
                BaseFunctions.getDeviceId(base),
                "user",
                id,
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            ReviewsNewResponse success = new Gson().fromJson(j,ReviewsNewResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (ReviewNewObject po : success.getData()){
                                        listOfReviews.add(po);
                                        reviewsAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callReviewsAPI(page,type,id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callMediaAPI(final int page,final int type,final String id){
        if (type == 0){
            pb_loading.smoothToShow();
        }else {
            pb_more.smoothToShow();
        }
        UserAPIsClass.profileMedia(
                base,
                BaseFunctions.getDeviceId(base),
                id,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            ProfileMediaResponse success = new Gson().fromJson(j,ProfileMediaResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (ProfileMediaObject o : success.getData()){
                                        listOfMedia.add(o);
                                        mediaAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callMediaAPI( page, type, id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callLocationsAPI(final String id){
        pb_loading.smoothToShow();
        BusinessLocationsAPIsClass.getAll(
                base,
                BaseFunctions.getDeviceId(base),
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            BusinessLocatonNewObject[] success = new Gson().fromJson(j,BusinessLocatonNewObject[].class);
                            if (success.length>0){
                                pb_loading.smoothToHide();
                                tv_section_desc.setVisibility(View.GONE);
                                for (BusinessLocatonNewObject po : success){
                                    listOfLocations.add(po);
                                    locationsAdapter.notifyDataSetChanged();
                                }
                            }else {
                                tv_section_desc.setVisibility(View.VISIBLE);
                                pb_loading.smoothToHide();
                            }
                        }else {
                            tv_section_desc.setVisibility(View.VISIBLE);
                            pb_loading.smoothToHide();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        pb_loading.smoothToHide();
                        tv_section_desc.setVisibility(View.VISIBLE);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callLocationsAPI(id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void callOffersAPI(final int page,final int type,final String id){
        if (type == 0){
            pb_loading.smoothToShow();
        }else {
            pb_more.smoothToShow();
        }
        OffersAPIsClass.get_offers(
                base,
                BaseFunctions.getDeviceId(base),
                id,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            OffersResponse success = new Gson().fromJson(j,OffersResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (OfferObject o : success.getData()){
                                        listOfOffers.add(o);
                                        offersAdapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callOffersAPI( page, type, id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            pb_more.smoothToHide();
        }else {
            tv_section_desc.setVisibility(View.VISIBLE);
            pb_loading.smoothToHide();
        }
    }
    private void process_data(int type){
        if (type == 0){
            pb_loading.setVisibility(View.GONE);
            tv_section_desc.setVisibility(View.GONE);
        }else {
            pb_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            pb_loading.smoothToHide();
            tv_section_desc.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }

    private void callFollowAPI(final String id,final String value){
        FollowersAPIsClass.followNew(
                base,
                BaseFunctions.getDeviceId(base),
                "user",
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("yes")) {
                                    Toast.makeText(base, getResources().getString(R.string.follow_place_true), Toast.LENGTH_SHORT).show();
                                    is_followed = 1;
                                }else {
                                    Toast.makeText(base, getResources().getString(R.string.follow_place_false), Toast.LENGTH_SHORT).show();
                                    is_followed = 0;
                                }
                            }else {
                                Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callFollowAPI(id,value);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void activeTab(int pos){
        switch (pos){
            //Media
            case 0:{
                txt_media.setTextColor(getResources().getColor(R.color.colorPrimary));
                txt_offers.setTextColor(getResources().getColor(R.color.text_color_grey));
                txt_reviews.setTextColor(getResources().getColor(R.color.text_color_grey));
                txt_location.setTextColor(getResources().getColor(R.color.text_color_grey));

                icon_media.setImageResource(R.drawable.ic_circle_play_primary);
                icon_offers.setImageResource(R.drawable.ic_circle_percent_grey);
                icon_reviews.setImageResource(R.drawable.ic_circle_star_grey);
                icon_location.setImageResource(R.drawable.ic_circle_location_grey);
            }break;
            //Offers
            case 1:{
                txt_media.setTextColor(getResources().getColor(R.color.text_color_grey));
                txt_offers.setTextColor(getResources().getColor(R.color.colorPrimary));
                txt_reviews.setTextColor(getResources().getColor(R.color.text_color_grey));
                txt_location.setTextColor(getResources().getColor(R.color.text_color_grey));

                icon_media.setImageResource(R.drawable.ic_circle_play_grey);
                icon_offers.setImageResource(R.drawable.ic_circle_percent_primary);
                icon_reviews.setImageResource(R.drawable.ic_circle_star_grey);
                icon_location.setImageResource(R.drawable.ic_circle_location_grey);
            }break;
            //Reviews
            case 2:{
                txt_media.setTextColor(getResources().getColor(R.color.text_color_grey));
                txt_offers.setTextColor(getResources().getColor(R.color.text_color_grey));
                txt_reviews.setTextColor(getResources().getColor(R.color.colorPrimary));
                txt_location.setTextColor(getResources().getColor(R.color.text_color_grey));

                icon_media.setImageResource(R.drawable.ic_circle_play_grey);
                icon_offers.setImageResource(R.drawable.ic_circle_percent_grey);
                icon_reviews.setImageResource(R.drawable.ic_circle_star_primary);
                icon_location.setImageResource(R.drawable.ic_circle_location_grey);
            }break;
            //Locations
            case 3:{
                txt_media.setTextColor(getResources().getColor(R.color.text_color_grey));
                txt_offers.setTextColor(getResources().getColor(R.color.text_color_grey));
                txt_reviews.setTextColor(getResources().getColor(R.color.text_color_grey));
                txt_location.setTextColor(getResources().getColor(R.color.colorPrimary));

                icon_media.setImageResource(R.drawable.ic_circle_play_grey);
                icon_offers.setImageResource(R.drawable.ic_circle_percent_grey);
                icon_reviews.setImageResource(R.drawable.ic_circle_star_grey);
                icon_location.setImageResource(R.drawable.ic_circle_location_primary);
            }break;
        }
    }

    private void callAddReviewAPI(String id,String rate,String comment){
        ReviewsAPIClass.addReview(
                base,
                BaseFunctions.getDeviceId(base),
                "user",
                id,
                comment,
                rate,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null){
                            rb_add_review.setRating(0);
                            selected_review_rate = "";
                            edt_add_review.setText("");
                            Toast.makeText(base, getResources().getString(R.string.facility_info_add_review_success), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(base, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }

    private void callProfileAPI(final String id){
        scrollView.setVisibility(View.GONE);
        UserAPIsClass.getProfile(
                base,
                BaseFunctions.getDeviceId(base),
                id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null) {
                            String j = new Gson().toJson(json);
                            ProfileNewResponse success = new Gson().fromJson(j, ProfileNewResponse.class);
                            //Profile image
                            BaseFunctions.setGlideImage(base,img_profile,success.getImage());
                            profile_image = success.getThumb_image();
                            //Name
                            tv_name.setText(success.getName());
                            username = success.getName();
                            //Description
                            tv_nickname.setText(success.getSignature()!=null?success.getSignature():"");
                            //Rate
                            rb_rate.setRating(Float.valueOf(success.getReviews_rating()));
                            tv_rate.setText(Math.round(Float.valueOf(success.getReviews_rating()))+" "+
                                    getResources().getString(R.string.facility_info_out_of)+" 5 "+
                                    getResources().getString(R.string.facility_info_from)+" "+
                                    success.getVotes_count()+" "+
                                    getResources().getString(R.string.facility_info_votes));
                            rating = Float.valueOf(success.getReviews_rating());
                            //Followers
                            tv_followers.setText(success.getFollowers_count()+
                                    getResources().getString(R.string.facility_info_followers));
                            //Seating - Atmosphere - music genre - Kids area - Parking - Handicapped entrance
                            String seating = "";
                            for (StartupItemObject o : SharedPrefManager.getInstance(base).getStartUp().getBusiness_seating()){
                                if (o.getId().equals(String.valueOf(success.getSeating()))){
                                    seating = o.getName();
                                    break;
                                }
                            }
                            String atmosphere = "";
                            for (StartupItemObject o : SharedPrefManager.getInstance(base).getStartUp().getBusiness_atmosphere()){
                                if (o.getId().equals(String.valueOf(success.getAtmosphere()))){
                                    atmosphere = o.getName();
                                    break;
                                }
                            }
                            String music = "";
                            for (StartupItemObject o : SharedPrefManager.getInstance(base).getStartUp().getMusic_gender_type()){
                                if (o.getId().equals(String.valueOf(success.getFirst_music_genre()))){
                                    music = o.getName();
                                    break;
                                }
                            }
                            String cuisine = "";
                            for (StartupItemObject o : SharedPrefManager.getInstance(base).getStartUp().getCuisine_definitions()){
                                if (o.getId().equals(String.valueOf(success.getFirst_best_cuisines()))){
                                    cuisine = o.getName();
                                    break;
                                }
                            }
                            String kids = "";
                            if (success.getKids_area() == 1){
                                kids = getResources().getString(R.string.facility_info_kids_area);
                            }
                            String parking = "";
                            if (success.getParking() == 1){
                                parking = getResources().getString(R.string.facility_info_parking);
                            }
                            String entrance = "";
                            if (success.getHandicapped_entrance() == 1){
                                entrance = getResources().getString(R.string.facility_info_handicapped);
                            }
                            String desc = seating.equals("")?"":" - "+seating;
                            desc += atmosphere.equals("")?"":" - "+atmosphere;
                            desc += music.equals("")?"":" - "+music;
                            desc += cuisine.equals("")?"":" - "+cuisine;
                            desc += kids.equals("")?"":" - "+kids;
                            desc += parking.equals("")?"":" - "+parking;
                            desc += entrance.equals("")?"":" - "+entrance;
                            if (desc.length()>0) {
                                if (desc.charAt(0) == ' ' && desc.charAt(1) == '-') {
                                    desc = desc.replaceFirst("-", "");
                                }
                            }
                            tv_desc.setText(desc);
                            //Following
                            is_followed = success.getIs_following();
                            //Address
                            address = success.getCountry_name()+
                                    ((success.getCity_name()!=null&&!success.getCity_name().equals(""))?" - "+
                            success.getCity_name():"");

                            if (mine){
                                scrollView.setVisibility(View.VISIBLE);
                                activeTab(0);
                                selected_list = 1;
                                continue_paginate = true;
                                lyt_review.setVisibility(View.GONE);
                                lyt_location.setVisibility(View.GONE);
                                if (mine) {
                                    lyt_media.setVisibility(View.VISIBLE);
                                }
                                tv_section_desc.setText(getResources().getString(R.string.facility_info_no_media));
                                tv_section_desc.setVisibility(View.GONE);
                                lyt_offer.setVisibility(View.GONE);
                                listOfMedia = new ArrayList<>();
                                currentPage = 1;
                                mediaAdapter = new FacilityInfoMediaAdapter(base,listOfMedia,
                                        mine?"with":"without");
                                rv_section.setAdapter(mediaAdapter);
                                if (place_id == 0 || place_id == -1){
                                    callMediaAPI(currentPage,0,
                                            String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                }else {
                                    callMediaAPI(currentPage,0,
                                            String.valueOf(place_id));
                                }
                            }else {
                                if (success.getDevice_setting().getIs_profile_public() == 0){
                                    private_layout.setVisibility(View.VISIBLE);
                                }else {
                                    if (success.getDevice_setting().getAllow_people_to_follow() == 0){
                                        btn_three_dots.setVisibility(View.GONE);
                                        btn_follow.setVisibility(View.GONE);
                                    }
                                    scrollView.setVisibility(View.VISIBLE);
                                    activeTab(0);
                                    selected_list = 1;
                                    continue_paginate = true;
                                    lyt_review.setVisibility(View.GONE);
                                    lyt_location.setVisibility(View.GONE);
                                    if (mine) {
                                        lyt_media.setVisibility(View.VISIBLE);
                                    }
                                    tv_section_desc.setText(getResources().getString(R.string.facility_info_no_media));
                                    tv_section_desc.setVisibility(View.GONE);
                                    lyt_offer.setVisibility(View.GONE);
                                    listOfMedia = new ArrayList<>();
                                    currentPage = 1;
                                    mediaAdapter = new FacilityInfoMediaAdapter(base,listOfMedia,
                                            mine?"with":"without");
                                    rv_section.setAdapter(mediaAdapter);
                                    if (place_id == 0 || place_id == -1){
                                        callMediaAPI(currentPage,0,
                                                String.valueOf(SharedPrefManager.getInstance(base).getUser().getId()));
                                    }else {
                                        callMediaAPI(currentPage,0,
                                                String.valueOf(place_id));
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callProfileAPI(id);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {
        Glide.with(base).load(imageFile).into(ivImage);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        media_type = 1;
        imagePath = uri.getPath();
        compressImage(new File(uri.getPath()));
    }

    private void compressImage(final File imageFile){
        base.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Compressor(base)
                        .setQuality(75)
                        .setDestinationDirectoryPath("/sdcard/")
                        .compressToFileAsFlowable(imageFile)
                        .subscribeOn(Schedulers.io())
                        .subscribe(new Consumer<File>() {
                            @Override
                            public void accept(File file) {
                                if (file.exists()){
                                    Log.i("compressed_image", "accept: "+file.getPath());
                                    Bitmap bitmap = BitmapFactory.decodeFile(file.getPath());
                                    imagePath = file.getPath();
                                    media_type = 1;
                                }else {
                                    Toast.makeText(base, "Please select another image", Toast.LENGTH_SHORT).show();
                                }
                                Log.i("media_typeee", "onClick: "+media_type);
                            }
                        }, new Consumer<Throwable>() {
                            @Override
                            public void accept(Throwable throwable) {
                                throwable.printStackTrace();
                                Log.i("media_typeee", "onClick: "+throwable.getMessage());
                            }
                        });
            }
        });
    }
}
