package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.PlacesAPIs;
import com.dma.app.taikme.APIs.ReviewsAPIs;
import com.dma.app.taikme.APIs.UserAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class PlacesAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void addPlace(
            final Context context,
            String device_id,
            String device_token,
            String en_name,
            String ar_name,
            String price,
            String lat,
            String lng,
            String en_location,
            String ar_location,
            String radius,
            String image,
            String category_id,
            String en_description,
            String ar_description,
            IResponse onRespose1,
            IFailure onFailure1){
        onResponse = onRespose1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        //Image
        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("image",image);
        //En Name
        RequestBody en_name_request = BaseFunctions.uploadFileStringConverter(en_name);
        //Ar Name
        RequestBody ar_name_request = BaseFunctions.uploadFileStringConverter(ar_name);
        //Price
        RequestBody price_request = BaseFunctions.uploadFileStringConverter(price);
        //Lat
        RequestBody lat_request = BaseFunctions.uploadFileStringConverter(lat);
        //Lng
        RequestBody lng_request = BaseFunctions.uploadFileStringConverter(lng);
        //En Location
        RequestBody en_location_request = BaseFunctions.uploadFileStringConverter(en_location);
        //Ar Location
        RequestBody ar_location_request = BaseFunctions.uploadFileStringConverter(ar_location);
        //Radius
        RequestBody radius_request = BaseFunctions.uploadFileStringConverter(radius);
        //Category
        RequestBody category_request = BaseFunctions.uploadFileStringConverter(category_id);
        //En Desc
        RequestBody en_desc_request = BaseFunctions.uploadFileStringConverter(en_description);
        //Ar Desc
        RequestBody ar_desc_request = BaseFunctions.uploadFileStringConverter(ar_description);


        Retrofit retrofit = configureRetrofitWithBearer(context);
        PlacesAPIs api = retrofit.create(PlacesAPIs.class);
        Call<BaseResponse> call = api.addPlace(
                "application/json",
                device_id,
                device_token,
                en_name_request,
                ar_name_request,
                price_request,
                lng_request,
                lat_request,
                en_location_request,
                ar_location_request,
                radius_request,
                body,
                category_request,
                en_desc_request,
                ar_desc_request);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getVisitedPlaces(final Context context,
                                       String device_id,
                                       String id,
                                       int page,
                                        final int type,
                                       IResponse onResponse1,
                                       final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        PlacesAPIs api = retrofit.create(PlacesAPIs.class);
        Call<BaseResponse> call = api.get_visited_places("application/json",
                device_id,
                id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

                onFailure.onFailure();
            }
        });
    }

    public static void getReviewedPlaces(final Context context,
                                        String device_id,
                                        String id,
                                         int page,
                                         final int type,
                                        IResponse onResponse1,
                                        final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        PlacesAPIs api = retrofit.create(PlacesAPIs.class);
        Call<BaseResponse> call = api.get_reviewed_places("application/json",
                device_id,
                id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void getTrendingPlaces(final Context context,
                                         String device_id,
                                         int page,
                                         final int type,
                                         IResponse onResponse1,
                                         final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        if (type == 0){
            dialog = new NewProgressDialog(context);
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(context);
        PlacesAPIs api = retrofit.create(PlacesAPIs.class);
        Call<BaseResponse> call = api.get_trending_places("application/json",
                device_id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if(type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if(type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }
    public static void updatePlace(
            final Context context,
            String device_id,
            String device_token,
            String en_name,
            String ar_name,
            String price,
            String lat,
            String lng,
            String en_location,
            String ar_location,
            String radius,
            String image,
            String category_id,
            String en_description,
            String ar_description,
            String place_id,
            IResponse onRespose1,
            IFailure onFailure1){
        onResponse = onRespose1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        //Image
        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("image",image);
        //En Name
        RequestBody en_name_request = BaseFunctions.uploadFileStringConverter(en_name);
        //Ar Name
        RequestBody ar_name_request = BaseFunctions.uploadFileStringConverter(ar_name);
        //Price
        RequestBody price_request = BaseFunctions.uploadFileStringConverter(price);
        //Lat
        RequestBody lat_request = BaseFunctions.uploadFileStringConverter(lat);
        //Lng
        RequestBody lng_request = BaseFunctions.uploadFileStringConverter(lng);
        //En Location
        RequestBody en_location_request = BaseFunctions.uploadFileStringConverter(en_location);
        //Ar Location
        RequestBody ar_location_request = BaseFunctions.uploadFileStringConverter(ar_location);
        //Radius
        RequestBody radius_request = BaseFunctions.uploadFileStringConverter(radius);
        //Category
        RequestBody category_request = BaseFunctions.uploadFileStringConverter(category_id);
        //En Desc
        RequestBody en_desc_request = BaseFunctions.uploadFileStringConverter(en_description);
        //Ar Desc
        RequestBody ar_desc_request = BaseFunctions.uploadFileStringConverter(ar_description);
        //Place id
        RequestBody place_id_request = BaseFunctions.uploadFileStringConverter(place_id);


        Retrofit retrofit = configureRetrofitWithBearer(context);
        PlacesAPIs api = retrofit.create(PlacesAPIs.class);
        Call<BaseResponse> call = api.updatePlace(
                "application/json",
                device_id,
                device_token,
                en_name_request,
                ar_name_request,
                price_request,
                lng_request,
                lat_request,
                en_location_request,
                ar_location_request,
                radius_request,
                body,
                category_request,
                en_desc_request,
                ar_desc_request,
                place_id_request);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getNearPlaces(final Context context,
                                         String device_id,
                                         String lat,
                                         String lng,
                                         int distance,
                                         IResponse onResponse1,
                                         final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        Retrofit retrofit = configureRetrofitWithBearer(context);
        PlacesAPIs api = retrofit.create(PlacesAPIs.class);
        Call<BaseResponse> call = api.get_near_places("application/json",
                device_id,
                lat,lng,distance);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
