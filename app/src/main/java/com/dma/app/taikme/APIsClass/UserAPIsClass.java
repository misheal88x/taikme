package com.dma.app.taikme.APIsClass;

import android.content.Context;
import android.util.Log;

import com.dma.app.taikme.APIs.UserAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Field;
import retrofit2.http.Part;

public class UserAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void updateCover(final Context context,
                                   String device_id,
                                   String image,
                                   IResponse onResponse1,
                                   final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("image",image);

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_cover("application/json",
                device_id,
                body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
    public static void updateProfileImage(final Context context,
                                   String device_id,
                                   String image,
                                   IResponse onResponse1,
                                   final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("image",image);

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_profile_image("application/json",
                device_id,
                body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
    public static void updateFCMToken(final Context context,
                                          String device_id,
                                          String token,
                                          IResponse onResponse1,
                                          final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_fcm("application/json",
                device_id,
                token);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void updateSettings(final Context context,
                                      String device_id,
                                      int visible_to_search,
                                      int allow_people_to_follow,
                                      int is_profile_public,
                                      int stories_visible,
                                      int people_can_comment_on_posts,
                                      int people_can_vote_posts,
                                      int comments_notifications,
                                      int votes_notifications,
                                      String view_language,
                                      IResponse onResponse1,
                                      final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_settings("application/json",
                device_id,
                visible_to_search,
                allow_people_to_follow,
                is_profile_public,
                stories_visible,
                people_can_comment_on_posts,
                people_can_vote_posts,
                comments_notifications,
                votes_notifications,
                view_language);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void getProfile(final Context context,
                                          String device_id,
                                        String user_id,
                                          IResponse onResponse1,
                                          final IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.get_profile("application/json",
                device_id,
                user_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void removeProfileImage(final Context context,
                                      String device_id,
                                      IResponse onResponse1,
                                      final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.remove_profile_image("application/json",
                device_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void updateName(final Context context,
                                          String device_id,
                                          String name,
                                          IResponse onResponse1,
                                          final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_name("application/json",
                device_id,name);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void sendVerificationCode(final Context context,
                                  String device_id,
                                  IResponse onResponse1,
                                  final IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.send_verification_code("application/json",
                device_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void confirmAccount(final Context context,
                                            String device_id,
                                            String code,
                                            IResponse onResponse1,
                                            final IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.confirmAccount("application/json",
                device_id,
                code);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void updateAccount(final Context context,
                                            String device_id,
                                            String user_type,
                                            String name,
                                            String mobile_number,
                                            String email,
                                            String website,
                                            String category,
                                            String sub_category,
                                            String seating,
                                            String atmosphere,
                                            String first_music_genre,
                                            String second_music_genre,
                                            String third_music_genre,
                                            String cuisine,
                                            String signature,
                                            String kids_area,
                                            String parking,
                                            String handicapped_entrance,
                                            String takeaway,
                                            String rest_rooms,
                                            String price_range,
                                            String better_for,
                                            String hours,
                                            String image,
                                            IResponse onResponse1,
                                            final IFailure onFailure1) {

        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        Log.i("fjbgbfg", "updateAccount: user_type "+user_type);
        Log.i("fjbgbfg", "updateAccount: name "+name);
        Log.i("fjbgbfg", "updateAccount: mobile_number "+mobile_number);
        Log.i("fjbgbfg", "updateAccount: email "+email);
        Log.i("fjbgbfg", "updateAccount: website "+website);
        Log.i("fjbgbfg", "updateAccount: category "+category);
        Log.i("fjbgbfg", "updateAccount: sub_category "+sub_category);
        Log.i("fjbgbfg", "updateAccount: seating "+seating);
        Log.i("fjbgbfg", "updateAccount: atmosphere "+atmosphere);
        Log.i("fjbgbfg", "updateAccount: first_music_genre "+first_music_genre);
        Log.i("fjbgbfg", "updateAccount: second_music_genre "+second_music_genre);
        Log.i("fjbgbfg", "updateAccount: third_music_genre "+third_music_genre);
        Log.i("fjbgbfg", "updateAccount: cuisine "+cuisine);
        Log.i("fjbgbfg", "updateAccount: signature "+signature);
        Log.i("fjbgbfg", "updateAccount: kids_area "+kids_area);
        Log.i("fjbgbfg", "updateAccount: parking "+parking);
        Log.i("fjbgbfg", "updateAccount: handicapped_entrance "+handicapped_entrance);
        Log.i("fjbgbfg", "updateAccount: takeaway "+takeaway);
        Log.i("fjbgbfg", "updateAccount: rest_rooms "+rest_rooms);
        Log.i("fjbgbfg", "updateAccount: price_range "+price_range);
        Log.i("fjbgbfg", "updateAccount: better_for "+better_for);
        Log.i("fjbgbfg", "updateAccount: hours "+hours);
        Log.i("fjbgbfg", "updateAccount: image "+image);
        //Usertype
        RequestBody usertype_request = BaseFunctions.uploadFileStringConverter(user_type);
        //Name
        RequestBody name_request = BaseFunctions.uploadFileStringConverter(name);
        //Mobile number
        RequestBody mobile_request = BaseFunctions.uploadFileStringConverter(mobile_number);
        //Email
        RequestBody email_request = BaseFunctions.uploadFileStringConverter(email);
        //Website
        RequestBody website_request = BaseFunctions.uploadFileStringConverter(website);
        //Category
        RequestBody category_request = BaseFunctions.uploadFileStringConverter(category);
        //Sub category
        RequestBody sub_category_request = BaseFunctions.uploadFileStringConverter(sub_category);
        //Seating
        RequestBody seating_request = BaseFunctions.uploadFileStringConverter(seating);
        //Atmosphere
        RequestBody atmosphere_request = BaseFunctions.uploadFileStringConverter(atmosphere);
        //First music genre
        RequestBody first_music_request = BaseFunctions.uploadFileStringConverter(first_music_genre);
        //Second music genre
        RequestBody second_music_request = BaseFunctions.uploadFileStringConverter(second_music_genre);
        //Third music genre
        RequestBody third_music_request = BaseFunctions.uploadFileStringConverter(third_music_genre);
        //Cuisine
        RequestBody cuisine_request = BaseFunctions.uploadFileStringConverter(cuisine);
        //Signature
        RequestBody signature_request = BaseFunctions.uploadFileStringConverter(signature);
        //Kids area
        RequestBody kids_request = BaseFunctions.uploadFileStringConverter(kids_area);
        //Parking
        RequestBody parking_request = BaseFunctions.uploadFileStringConverter(parking);
        //Handicapped entrance
        RequestBody handicapped_request = BaseFunctions.uploadFileStringConverter(handicapped_entrance);
        //Take away
        RequestBody take_away_request = BaseFunctions.uploadFileStringConverter(takeaway);
        //Rest rooms
        RequestBody rest_rooms_request = BaseFunctions.uploadFileStringConverter(rest_rooms);
        //Price range
        RequestBody price_range_request = BaseFunctions.uploadFileStringConverter(price_range);
        //Better for
        RequestBody better_for_request = BaseFunctions.uploadFileStringConverter(better_for);
        //hours
        RequestBody hours_request = BaseFunctions.uploadFileStringConverter(hours);
        //Image
        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("file_image", image);


        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.update_account("application/json",
                device_id,
                usertype_request,
                name_request,
                mobile_request,
                email_request,
                website_request,
                category_request,
                sub_category_request,
                seating_request,
                atmosphere_request,
                first_music_request,
                second_music_request,
                third_music_request,
                cuisine_request,
                signature_request,
                kids_request,
                parking_request,
                handicapped_request,
                take_away_request,
                rest_rooms_request,
                price_range_request,
                better_for_request,
                hours_request,
                body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response, onResponse, context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void changeMobileNumber(final Context context,
                                  String device_id,
                                  String mobile_number,
                                  IResponse onResponse1,
                                  final IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.change_mobile_number("application/json",
                device_id,
                mobile_number);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
    public static void changePassword(final Context context,
                                          String device_id,
                                          String password,
                                          IResponse onResponse1,
                                          final IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.change_password("application/json",
                device_id,
                password);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
    public static void profileMedia(final Context context,
                                      String device_id,
                                      String id,
                                      int page,
                                      IResponse onResponse1,
                                      final IFailure onFailure1){

        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        UserAPIs api = retrofit.create(UserAPIs.class);
        Call<BaseResponse> call = api.profile_media("application/json",
                device_id,
                id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
