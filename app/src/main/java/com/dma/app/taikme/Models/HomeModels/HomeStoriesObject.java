package com.dma.app.taikme.Models.HomeModels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HomeStoriesObject {
    @SerializedName("my_stories") private List<HomeStoryObject> my_stories = new ArrayList<>();
    @SerializedName("others") private List<HomeStoryObject> others = new ArrayList<>();

    public List<HomeStoryObject> getMy_stories() {
        return my_stories;
    }

    public void setMy_stories(List<HomeStoryObject> my_stories) {
        this.my_stories = my_stories;
    }

    public List<HomeStoryObject> getOthers() {
        return others;
    }

    public void setOthers(List<HomeStoryObject> others) {
        this.others = others;
    }
}
