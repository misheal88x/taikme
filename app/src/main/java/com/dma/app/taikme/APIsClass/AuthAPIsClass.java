package com.dma.app.taikme.APIsClass;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.dma.app.taikme.APIs.AuthAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Models.FacebookObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Field;
import retrofit2.http.Part;

public class AuthAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void NormalRegister(final Context context,
                                      String device_id,
                                      String mobile_number,
                                      String password,
                                      String name,
                                      String user_type,
                                      String age,
                                      String gender,
                                      String birthday,
                                      String country_name,
                                      String city_name,
                                      String first_best_cuisines,
                                      String second_best_cuisines,
                                      String third_best_cuisines,
                                      String prefer,
                                      String bio,
                                      String website,
                                      String atmosphere,
                                      String first_music_genre,
                                      String second_music_genre,
                                      String third_music_genre,
                                      String cuisine,
                                      String kids_area,
                                      String handicapped_entrance,
                                      String takeaway,
                                      String rest_rooms,
                                      String price_range,
                                      String better_for,
                                      String signature,
                                      String seating,
                                      String parking,
                                      String category,
                                      String sub_category,
                                      String hours,
                                      String going_on,
                                      String image,
                                      IResponse onREeponse1,
                                      final IFailure onFailure1){
        onResponse = onREeponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();


        //Image
        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("file_image",image);
        //Mobile number
        RequestBody mobile_request = BaseFunctions.uploadFileStringConverter(mobile_number);
        //Password
        RequestBody password_request = BaseFunctions.uploadFileStringConverter(password);
        //Name
        RequestBody name_request = BaseFunctions.uploadFileStringConverter(name);
        //User type
        RequestBody type_request = BaseFunctions.uploadFileStringConverter(user_type);
        //Email
        RequestBody email_request = BaseFunctions.uploadFileStringConverter(mobile_number+"@taikme.com");
        //Age
        RequestBody age_request = BaseFunctions.uploadFileStringConverter(age);
        //Gender
        RequestBody gender_request = BaseFunctions.uploadFileStringConverter(gender);
        //Birthday
        RequestBody birth_request = BaseFunctions.uploadFileStringConverter(birthday);
        //Country name
        RequestBody country_name_request = BaseFunctions.uploadFileStringConverter(country_name);
        //City name
        RequestBody city_name_request = BaseFunctions.uploadFileStringConverter(city_name);
        //First best cuisine
        RequestBody first_cuisine_request = BaseFunctions.uploadFileStringConverter(first_best_cuisines);
        //Second best cuisine
        RequestBody second_cuisine_request = BaseFunctions.uploadFileStringConverter(second_best_cuisines);
        //Third best cuisine
        RequestBody third_cuisine_request = BaseFunctions.uploadFileStringConverter(third_best_cuisines);
        //Prefer
        RequestBody prefer_request = BaseFunctions.uploadFileStringConverter(prefer);
        //Bio
        RequestBody bio_request = BaseFunctions.uploadFileStringConverter(bio);
        //Website
        RequestBody website_request = BaseFunctions.uploadFileStringConverter(website);
        //Atmosphere
        RequestBody atmosphere_request = BaseFunctions.uploadFileStringConverter(atmosphere);
        //First music genre
        RequestBody first_music_request = BaseFunctions.uploadFileStringConverter(first_music_genre);
        //Second music genre
        RequestBody second_music_request = BaseFunctions.uploadFileStringConverter(second_music_genre);
        //Third music genre
        RequestBody third_music_request = BaseFunctions.uploadFileStringConverter(third_music_genre);
        //Cuisine
        RequestBody cuisine_request = BaseFunctions.uploadFileStringConverter(cuisine);
        //Kids area
        RequestBody kids_request = BaseFunctions.uploadFileStringConverter(kids_area);
        //Handicapped entrance
        RequestBody handicapped_request = BaseFunctions.uploadFileStringConverter(handicapped_entrance);
        //Take away
        RequestBody take_away_request = BaseFunctions.uploadFileStringConverter(takeaway);
        //Rest rooms
        RequestBody rest_rooms_request = BaseFunctions.uploadFileStringConverter(rest_rooms);
        //Price range
        RequestBody price_range_request = BaseFunctions.uploadFileStringConverter(price_range);
        //Better for
        RequestBody better_for_request = BaseFunctions.uploadFileStringConverter(better_for);
        //Signature
        RequestBody signature_request = BaseFunctions.uploadFileStringConverter(signature);
        //Seating
        RequestBody seating_request = BaseFunctions.uploadFileStringConverter(seating);
        //Parking
        RequestBody parking_request = BaseFunctions.uploadFileStringConverter(parking);
        //Category
        RequestBody category_request = BaseFunctions.uploadFileStringConverter(category);
        //Sub category
        RequestBody sub_category_request = BaseFunctions.uploadFileStringConverter(sub_category);
        //hours
        RequestBody hours_request = BaseFunctions.uploadFileStringConverter(hours);
        //going on
        RequestBody going_on_request = BaseFunctions.uploadFileStringConverter(going_on);

        Retrofit retrofit = configureRetrofitWithoutBearer();
        AuthAPIs api = retrofit.create(AuthAPIs.class);
        Call<BaseResponse> call = api.normalResgister("application/json",
                device_id,mobile_request,password_request,name_request,email_request,type_request,
                age_request,gender_request,birth_request,country_name_request,city_name_request,
                first_cuisine_request,second_cuisine_request,third_cuisine_request,
                prefer_request,bio_request,website_request,atmosphere_request,first_music_request,
                second_music_request,third_music_request,cuisine_request,kids_request,handicapped_request,take_away_request
                ,rest_rooms_request,price_range_request,better_for_request,signature_request,seating_request
                ,parking_request,category_request,sub_category_request,hours_request,going_on_request,body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void userLogin(final Context context,
                                      String device_id,
                                      String username,
                                      String password,
                                      IResponse onReeponse1,
                                      final IFailure onFailure1){
        onResponse = onReeponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        AuthAPIs api = retrofit.create(AuthAPIs.class);
        Call<BaseResponse> call = api.userLogin("application/json",
                device_id,username,password);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void social_register(final Context context,
                                 String device_id,
                                       String access_token,
                                       String oauth2_provider,
                                       String email,
                                       String name,
                                 String image,
                                 IResponse onReeponse1,
                                 final IFailure onFailure1){
        onResponse = onReeponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        AuthAPIs api = retrofit.create(AuthAPIs.class);
        Call<BaseResponse> call = api.social_register("application/json",
                device_id,access_token,oauth2_provider,email,name,image);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void social_verify(final Context context,
                                       String device_id,
                                       String access_token,
                                       String oauth2_provider,
                                       IResponse onReeponse1,
                                       final IFailure onFailure1){
        onResponse = onReeponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithoutBearer();
        AuthAPIs api = retrofit.create(AuthAPIs.class);
        Call<BaseResponse> call = api.social_verify("application/json",
                device_id,access_token,oauth2_provider);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void get_facebook_data(final Context context,
                                  String access_token,
                                  final IResponse onReeponse1,
                                  final IFailure onFailure1){
        onResponse = onReeponse1;
        onFailure = onFailure1;
        Retrofit retrofit = configureRetrofitWithoutBearerFacebook();
        AuthAPIs api = retrofit.create(AuthAPIs.class);
        Call<FacebookObject> call = api.get_infos("id,name,email",access_token);
        call.enqueue(new Callback<FacebookObject>() {
            @Override
            public void onResponse(Call<FacebookObject> call, Response<FacebookObject> response) {
                if (response.isSuccessful()){
                    onResponse.onResponse(response.body());
                }else {
                    onResponse.onResponse();
                }
            }

            @Override
            public void onFailure(Call<FacebookObject> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void update_social_data(final Context context,
                                          String device_id,
                                          String mobile_number,
                                          String password,
                                          String name,
                                          String user_type,
                                          String age,
                                          String gender,
                                          String birthday,
                                          String country_name,
                                          String city_name,
                                          String first_best_cuisines,
                                          String second_best_cuisines,
                                          String third_best_cuisines,
                                          String prefer,
                                          String bio,
                                          String website,
                                          String atmosphere,
                                          String first_music_genre,
                                          String second_music_genre,
                                          String third_music_genre,
                                          String cuisine,
                                          String kids_area,
                                          String handicapped_entrance,
                                          String takeaway,
                                          String rest_rooms,
                                          String price_range,
                                          String better_for,
                                          String signature,
                                          String seating,
                                          String parking,
                                          String category,
                                          String sub_category,
                                          String hours,
                                          String going_on,
                                          final IResponse onReeponse1,
                                          final IFailure onFailure1){
        onResponse = onReeponse1;
        onFailure = onFailure1;
        Log.i("update_data", "update_social_data: device_id "+device_id);
        Log.i("update_data", "update_social_data: mobile_number "+mobile_number);
        Log.i("update_data", "update_social_data: password "+password);
        Log.i("update_data", "update_social_data: name "+name);
        Log.i("update_data", "update_social_data: user_type "+user_type);
        Log.i("update_data", "update_social_data: age "+age);
        Log.i("update_data", "update_social_data: gender "+gender);
        Log.i("update_data", "update_social_data: birthday "+birthday);
        Log.i("update_data", "update_social_data: country_name "+country_name);
        Log.i("update_data", "update_social_data: city_name "+city_name);
        Log.i("update_data", "update_social_data: first_best_cuisines "+first_best_cuisines);
        Log.i("update_data", "update_social_data: second_best_cuisines "+second_best_cuisines);
        Log.i("update_data", "update_social_data: third_best_cuisines "+third_best_cuisines);
        Log.i("update_data", "update_social_data: prefer "+prefer);
        Log.i("update_data", "update_social_data: bio "+bio);
        Log.i("update_data", "update_social_data: website "+website);
        Log.i("update_data", "update_social_data: atmosphere "+atmosphere);
        Log.i("update_data", "update_social_data: first_music_genre "+first_music_genre);
        Log.i("update_data", "update_social_data: second_music_genre "+second_music_genre);
        Log.i("update_data", "update_social_data: third_music_genre "+third_music_genre);
        Log.i("update_data", "update_social_data: cuisine "+cuisine);
        Log.i("update_data", "update_social_data: kids_area "+kids_area);
        Log.i("update_data", "update_social_data: handicapped_entrance "+handicapped_entrance);
        Log.i("update_data", "update_social_data: takeaway "+takeaway);
        Log.i("update_data", "update_social_data: rest_rooms "+rest_rooms);
        Log.i("update_data", "update_social_data: price_range "+price_range);
        Log.i("update_data", "update_social_data: better_for "+better_for);
        Log.i("update_data", "update_social_data: signature "+signature);
        Log.i("update_data", "update_social_data: seating "+seating);
        Log.i("update_data", "update_social_data: parking "+parking);
        Log.i("update_data", "update_social_data: category "+category);
        Log.i("update_data", "update_social_data: sub_category "+sub_category);
        Log.i("update_data", "update_social_data: hours "+hours);
        Log.i("update_data", "update_social_data: going_on "+going_on);
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        AuthAPIs api = retrofit.create(AuthAPIs.class);
        Call<BaseResponse> call = api.update_social_data("application/json",
                device_id,
                mobile_number,
                password,
                name,
                "",
                user_type,
                age,
                gender,
                birthday,
                country_name,
                city_name,
                first_best_cuisines,
                second_best_cuisines,
                third_best_cuisines,
                prefer,
                bio,
                website,
                atmosphere,
                first_music_genre,
                second_music_genre,
                third_music_genre,
                cuisine,
                kids_area,
                handicapped_entrance,
                takeaway,
                rest_rooms,
                price_range,
                better_for,
                signature,
                seating,
                parking,
                category,
                sub_category,
                hours,
                going_on);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
