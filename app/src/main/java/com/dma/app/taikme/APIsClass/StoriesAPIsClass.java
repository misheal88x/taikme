package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.PlacesAPIs;
import com.dma.app.taikme.APIs.ReviewsAPIs;
import com.dma.app.taikme.APIs.StoriesAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class StoriesAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void addStory(
            final Context context,
            String device_id,
            String type,
            String content,
            String filePath,
            IResponse onRespose1,
            IFailure onFailure1){
        onResponse = onRespose1;
        onFailure = onFailure1;

        //File
        MultipartBody.Part body = BaseFunctions.uploadFileImageConverter("file",filePath);
        //Type
        RequestBody type_request = BaseFunctions.uploadFileStringConverter(type);
        //Content
        RequestBody content_request = BaseFunctions.uploadFileStringConverter(content);

        Retrofit retrofit = configureRetrofitWithBearerLongTime(context);
        StoriesAPIs api = retrofit.create(StoriesAPIs.class);
        Call<BaseResponse> call = api.store(
                "application/json",
                device_id,
                type_request,
                content_request,
                body);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
    public static void deleteStory(final Context context,
                                    String device_id,
                                    String story_id,
                                    IResponse onResponse1,
                                    final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        StoriesAPIs api = retrofit.create(StoriesAPIs.class);
        Call<BaseResponse> call = api.delete("application/json",
                device_id,
                story_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

}
