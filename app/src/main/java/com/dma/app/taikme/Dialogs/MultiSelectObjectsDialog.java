package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.Adapters.MultiSelectAdapter;
import com.dma.app.taikme.Adapters.MultiSelectCategoriesAdapter;
import com.dma.app.taikme.Interfaces.IMultiSelect;
import com.dma.app.taikme.Interfaces.IMultiSelectObjects;
import com.dma.app.taikme.Models.CategoryObject;
import com.dma.app.taikme.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MultiSelectObjectsDialog extends AlertDialog {
    private Context context;
    private RecyclerView rv_list;
    private List<CategoryObject> source_list,selected_list;
    private MultiSelectCategoriesAdapter adapter;
    private LinearLayoutManager layoutManager;
    private TextView tv_ok;
    private IMultiSelectObjects iMultiSelect;

    public MultiSelectObjectsDialog(@NonNull Context context, List<CategoryObject> source_list,List<CategoryObject> selected_list, IMultiSelectObjects iMultiSelect) {
        super(context);
        this.context = context;
        this.source_list = source_list;
        this.selected_list = selected_list;
        this.iMultiSelect = iMultiSelect;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_multi_select);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //RecyclerView
        rv_list = findViewById(R.id.multi_select_recycler);
        //TextView
        tv_ok = findViewById(R.id.multi_select_ok);
    }

    private void init_events(){
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapter.getCheckedItems()!=null){
                    if (adapter.getCheckedItems().size()==0){
                        Toast.makeText(context, context.getResources().getString(R.string.multi_select_no_items), Toast.LENGTH_SHORT).show();
                        return;
                    }else {
                        iMultiSelect.onSelected(adapter.getCheckedItems());
                        cancel();
                    }
                }
            }
        });
    }

    private void init_dialog(){
        init_Recycler();
    }

    private void init_Recycler(){
        adapter = new MultiSelectCategoriesAdapter(context,source_list,selected_list);
        layoutManager = new LinearLayoutManager(context,RecyclerView.VERTICAL,false);
        rv_list.setLayoutManager(layoutManager);
        rv_list.setAdapter(adapter);
    }
}
