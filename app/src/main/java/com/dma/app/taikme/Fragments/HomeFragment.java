package com.dma.app.taikme.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.APIsClass.HidingAPIsClass;
import com.dma.app.taikme.APIsClass.HomeAPIsClass;
import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Activities.CreateStatusActivity;
import com.dma.app.taikme.Activities.ViewStoryActivity;
import com.dma.app.taikme.Adapters.GeneralHomeAdapter;
import com.dma.app.taikme.Adapters.HomePostsAdapter;
import com.dma.app.taikme.Adapters.HomeStatusAdapter;
import com.dma.app.taikme.Dialogs.HomeDialog;
import com.dma.app.taikme.Dialogs.ReportDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IPostHashTag;
import com.dma.app.taikme.Interfaces.IPostMore;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.HomeModels.HomeNewObject;
import com.dma.app.taikme.Models.HomeModels.HomePostObject;
import com.dma.app.taikme.Models.HomeModels.HomePostsObject;
import com.dma.app.taikme.Models.HomeModels.HomeStoryObject;
import com.dma.app.taikme.Models.UpdateFCMResponse;
import com.dma.app.taikme.Others.App;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.EndlessRecyclerViewScrollListener;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 19/11/2019.
 */

public class HomeFragment extends BaseFragment implements IMove,IPostMore,IPostHashTag {
    private RecyclerView rv_status,rv_posts;
    //private PullRefreshLayout refreshLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private HomeStatusAdapter statusAdapter;
    private HomePostsAdapter postsAdapter;
    private List<HomePostsObject> listOfPosts;
    private List<HomeStoryObject> listOfStories;
    private LinearLayoutManager postsLayoutManager;
    private LinearLayoutManager statusLayoutManager;
    private ImageView btn_launch_dialog;
    private RelativeLayout root;
    private NestedScrollView scrollView;
    private LinearLayout no_data;
    public int current_page = 1,current_hash_page = 1;
    public boolean continue_paginate = true;
    private String api_type = "home";
    private String clicked_hash = "";
    private AVLoadingIndicatorView more;
    private EndlessRecyclerViewScrollListener scrollListener;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home,container,false);
    }

    @Override
    public void init_views() {
        super.init_views();
        //RecyclerView
        rv_status = base.findViewById(R.id.home_status_recycler);
        rv_posts = base.findViewById(R.id.home_posts_recycler);
        //Pull Refresh Layout
        //refreshLayout = base.findViewById(R.id.swipeRefreshLayout);
        //refreshLayout.setRefreshStyle(PullRefreshLayout.STYLE_SMARTISAN);
        //refreshLayout.setColor(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout = base.findViewById(R.id.swipe_refresh_layout);
        //ImageView
        btn_launch_dialog = base.findViewById(R.id.home_launch_dialog_btn);
        //RelativeLayout
        root = base.findViewById(R.id.home_layout);
        //NestedScrollView
        scrollView = base.findViewById(R.id.scrollView);
        //LinearLayout
        no_data = base.findViewById(R.id.no_data_layout);
        //ProgressBar
        more = base.findViewById(R.id.more);
    }

    @Override
    public void init_events() {

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                current_page = 1;
                continue_paginate = true;
                listOfPosts = new ArrayList<>();
                postsAdapter = new HomePostsAdapter(base,listOfPosts,HomeFragment.this,HomeFragment.this);
                rv_posts.setAdapter(postsAdapter);
                listOfStories = new ArrayList<>();
                statusAdapter = new HomeStatusAdapter(base,listOfStories,HomeFragment.this);
                rv_status.setAdapter(statusAdapter);
                api_type = "home";
                callHomeAPI(current_page,0);
            }
        });
        btn_launch_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new HomeDialog(base, new IMove() {
                    @Override
                    public void move() {
                        PlansFragment plansFragment = new PlansFragment();
                        base.open_fragment(plansFragment);
                    }

                    @Override
                    public void move(int position) {

                    }
                }).show();
            }
        });


        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {

                            //if (listOfPosts.size()>=20){
                            if (continue_paginate) {
                                if (BaseFunctions.isOnline(base)) {
                                    if (!api_type.equals("home")) {
                                        current_page++;
                                        callHashTagAPI(current_page, 1, clicked_hash);
                                    } else {
                                        current_page++;
                                        callHomeAPI(current_page, 1);
                                    }
                                }
                            }
                            //  }
                    }
                }
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_status_recycler();
        init_posts_recycler();
        if (SharedPrefManager.getInstance(base).getUser()!=null){
            String token = FirebaseInstanceId.getInstance().getToken();
            Log.i("generated_fcm_token", "init_activity: "+token);
            //callUpdateTokenAPI(token);
        }
        if (BaseFunctions.isOnline(base)){
            api_type = "home";
            continue_paginate = true;
            callHomeAPI(current_page,0);
        }else {
            restore_data();
        }


    }

    private void init_status_recycler(){
        listOfStories = new ArrayList<>();
        statusAdapter = new HomeStatusAdapter(base,listOfStories,this);
        statusLayoutManager = new LinearLayoutManager(base,LinearLayoutManager.HORIZONTAL,false);
        rv_status.setLayoutManager(statusLayoutManager);
        BaseFunctions.runAnimation(rv_status,0,statusAdapter);
        rv_status.setAdapter(statusAdapter);
    }

    private void init_posts_recycler(){
        listOfPosts = new ArrayList<>();
        postsAdapter = new HomePostsAdapter(base,listOfPosts,this,this);


        /*
        postsLayoutManager = new LinearLayoutManager(base,RecyclerView.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
         */

        //postsLayoutManager = new CustomLayoutManager(base,LinearLayoutManager.VERTICAL,false);
        postsLayoutManager = new LinearLayoutManager(base);
        rv_posts.setNestedScrollingEnabled(false);
        rv_posts.setLayoutManager(postsLayoutManager);
        rv_posts.setAdapter(postsAdapter);
        rv_posts.getItemAnimator().setChangeDuration(0);
        rv_posts.setItemViewCacheSize(20);
        rv_posts.setDrawingCacheEnabled(true);
        rv_posts.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        ViewCompat.setNestedScrollingEnabled(rv_posts, false);
        rv_posts.setNestedScrollingEnabled(false);
        rv_posts.setHasFixedSize(true);
        /*
        scrollListener = new EndlessRecyclerViewScrollListener(postsLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                //if (listOfPosts.size()>=20){
                if (continue_paginate) {
                    if (BaseFunctions.isOnline(base)) {
                        if (!api_type.equals("home")) {
                            current_page++;
                            callHashTagAPI(current_page, 1, clicked_hash);
                        } else {
                            current_page++;
                            callHomeAPI(current_page, 1);
                        }
                    }
                }
                //  }
            }
        };
        rv_posts.addOnScrollListener(scrollListener);

         */
    }

    @Override
    public void move() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.RECORD_AUDIO},
                1);
    }

    @Override
    public void move(int position) {
        Intent intent = new Intent(base,ViewStoryActivity.class);
        intent.putExtra("stories",new Gson().toJson(listOfStories));
        intent.putExtra("position",position);
        intent.putExtra("total_num",listOfStories.size());
        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[3] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[4] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[5] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(base,CreateStatusActivity.class));
                } else {
                    Toast.makeText(base, getResources().getString(R.string.no_permissions), Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    private void restore_data(){
        /*
        HomeObject ho = SharedPrefManager.getInstance(base).getHome();
        listOfStories.clear();
        listOfPosts.clear();
        rv_status.setAdapter(new HomeStatusAdapter(base,listOfStories,this));
        rv_posts.setAdapter(new HomePostsAdapter(base,listOfPosts,this,this));
        no_data.setVisibility(View.VISIBLE);
        statusAdapter.notifyDataSetChanged();
        if (ho!=null){
            if (ho.getStories()!=null){
                if (ho.getStories().getMy_stories()!=null){
                    if (ho.getStories().getMy_stories().size()>0){
                        listOfStories.add(ho.getStories().getMy_stories().get(0));
                        statusAdapter.notifyDataSetChanged();
                    }else {
                        listOfStories.add(new StoryObject());
                        statusAdapter.notifyDataSetChanged();
                    }
                }else {
                    listOfStories.add(new StoryObject());
                    statusAdapter.notifyDataSetChanged();
                }
                if (ho.getStories().getStories()!=null){
                    if (ho.getStories().getStories().getData()!=null){
                        if (ho.getStories().getStories().getData().size()>0){
                            for (StoryObject so : ho.getStories().getStories().getData()){
                                listOfStories.add(so);
                                statusAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            }else {
                listOfStories.add(new StoryObject());
                statusAdapter.notifyDataSetChanged();
            }
            if (ho.getWall()!=null){
                if (ho.getWall().size()>0){
                    no_data.setVisibility(View.GONE);
                    for (PostFatherObject pfo : ho.getWall()){
                        listOfPosts.add(pfo);
                        postsAdapter.notifyDataSetChanged();
                    }
                }else {
                    no_data.setVisibility(View.VISIBLE);
                }
            }else {
                no_data.setVisibility(View.VISIBLE);
            }
        }else {
            no_data.setVisibility(View.VISIBLE);
        }

         */

    }

    public void callHomeAPI(final int page,final int type){
        Log.i("jdfjfd", "callHomeAPI: "+page);
        App.home_api_selected = "home";
        if (type == 0){
            swipeRefreshLayout.setRefreshing(true);
        }else {
            more.smoothToShow();
        }
        HomeAPIsClass.get_home(base,
                BaseFunctions.getDeviceId(base),
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        if (type == 0) {
                            swipeRefreshLayout.setRefreshing(false);
                        }else {
                            more.smoothToHide();

                        }
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (type == 0) {
                            swipeRefreshLayout.setRefreshing(false);
                        }else {
                            more.smoothToHide();
                        }
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            HomeNewObject success = new Gson().fromJson(j,HomeNewObject.class);
                            if (type == 0){
                                listOfPosts.clear();
                                listOfStories.clear();
                                rv_posts.setAdapter(new HomePostsAdapter(base,listOfPosts,HomeFragment.this,HomeFragment.this));
                                rv_status.setAdapter(new HomeStatusAdapter(base,listOfStories,HomeFragment.this));
                                if (success!=null){
                                    if (success.getStories()!=null){
                                        if (success.getStories().getMy_stories()!=null){
                                            if (success.getStories().getMy_stories().size()>0){
                                                listOfStories.add(success.getStories().getMy_stories().get(0));
                                                statusAdapter.notifyDataSetChanged();
                                            }else {
                                                listOfStories.add(new HomeStoryObject());
                                                statusAdapter.notifyDataSetChanged();
                                            }
                                        }else {
                                            listOfStories.add(new HomeStoryObject());
                                            statusAdapter.notifyDataSetChanged();
                                        }
                                        if (success.getStories()!=null){
                                            if (success.getStories().getOthers()!=null){
                                                if (success.getStories().getOthers().size()>0){
                                                    for (HomeStoryObject so : success.getStories().getOthers()){
                                                        if (listOfStories.size()>0){
                                                            boolean found = false;
                                                            for (HomeStoryObject hso : listOfStories){
                                                                if (hso.getUser_id() == so.getUser_id()){
                                                                    found = true;
                                                                    break;
                                                                }
                                                            }
                                                            if (!found){
                                                                listOfStories.add(so);
                                                                statusAdapter.notifyDataSetChanged();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }else {
                                        listOfStories.add(new HomeStoryObject());
                                        statusAdapter.notifyDataSetChanged();
                                    }
                                    if (success.getWall()!=null){
                                        if (success.getWall().size()>0){
                                            no_data.setVisibility(View.GONE);
                                            for (HomePostsObject pfo : success.getWall()){
                                                Log.i("hgyhgyhty", "onResponse: "+pfo.getPost_object().getId());
                                                listOfPosts.add(pfo);
                                                postsAdapter.notifyDataSetChanged();
                                            }
                                        }else {
                                            no_data.setVisibility(View.VISIBLE);
                                        }
                                    }else {
                                        no_data.setVisibility(View.VISIBLE);
                                    }
                                }else {
                                    no_data.setVisibility(View.VISIBLE);
                                }
                                //SharedPrefManager.getInstance(base).setHome(success);
                                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(base, new OnSuccessListener<InstanceIdResult>() {
                                    @Override
                                    public void onSuccess(InstanceIdResult instanceIdResult) {
                                        String token = instanceIdResult.getToken();
                                        Log.i("refreshed_token", "the token: " + token);
                                        if (SharedPrefManager.getInstance(base).getUser()!=null){
                                            if (!App.is_token_updated) {
                                                callUpdateTokenAPI(token);
                                            }
                                        }
                                    }
                                });
                                //String recent_token = FirebaseInstanceId.getInstance().getToken();
                            }else {
                                if (success.getWall()!=null){
                                    if (success.getWall().size()>0){
                                        for (HomePostsObject pfo : success.getWall()){
                                            listOfPosts.add(pfo);
                                        }
                                        //postsAdapter.notifyDataSetChanged();
                                        postsAdapter = new HomePostsAdapter(base,listOfPosts,HomeFragment.this,HomeFragment.this);
                                        rv_posts.setAdapter(postsAdapter);
                                        continue_paginate = true;
                                    }else {
                                        continue_paginate = false;
                                        Snackbar.make(root,"No more",Snackbar.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (type == 0) {
                            swipeRefreshLayout.setRefreshing(false);
                        }else {
                            more.smoothToHide();
                        }
                    }
                });
    }

    public void callHashTagAPI(final int page,final int type,final String text){
        continue_paginate = false;
        App.home_api_selected = "hash";
        if (type == 0){
            swipeRefreshLayout.setRefreshing(true);
        }else {
            more.smoothToShow();
        }
        HomeAPIsClass.get_hashtag(base,
                BaseFunctions.getDeviceId(base),
                text,
                page,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        if (type == 0) {
                            swipeRefreshLayout.setRefreshing(false);
                        }else {
                            more.smoothToHide();
                        }
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (type == 0) {
                            swipeRefreshLayout.setRefreshing(false);
                        }else {
                            more.smoothToHide();
                        }
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            HomeNewObject success = new Gson().fromJson(j,HomeNewObject.class);
                            if (type == 0){
                                listOfPosts.clear();
                                listOfStories.clear();
                                rv_posts.setAdapter(new HomePostsAdapter(base,listOfPosts,HomeFragment.this,HomeFragment.this));
                                rv_status.setAdapter(new HomeStatusAdapter(base,listOfStories,HomeFragment.this));
                                if (success!=null){
                                    if (success.getStories()!=null){
                                        if (success.getStories().getMy_stories()!=null){
                                            if (success.getStories().getMy_stories().size()>0){
                                                listOfStories.add(success.getStories().getMy_stories().get(0));
                                                statusAdapter.notifyDataSetChanged();
                                            }else {
                                                listOfStories.add(new HomeStoryObject());
                                                statusAdapter.notifyDataSetChanged();
                                            }
                                        }else {
                                            listOfStories.add(new HomeStoryObject());
                                            statusAdapter.notifyDataSetChanged();
                                        }
                                        if (success.getStories()!=null){
                                            if (success.getStories().getOthers()!=null){
                                                if (success.getStories().getOthers().size()>0){
                                                    for (HomeStoryObject so : success.getStories().getOthers()){
                                                        if (listOfStories.size()>0){
                                                            boolean found = false;
                                                            for (HomeStoryObject hso : listOfStories){
                                                                if (hso.getUser_id() == so.getUser_id()){
                                                                    found = true;
                                                                    break;
                                                                }
                                                            }
                                                            if (!found){
                                                                listOfStories.add(so);
                                                                statusAdapter.notifyDataSetChanged();
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }else {
                                        listOfStories.add(new HomeStoryObject());
                                        statusAdapter.notifyDataSetChanged();
                                    }
                                    if (success.getWall()!=null){
                                        if (success.getWall().size()>0){
                                            no_data.setVisibility(View.GONE);
                                            for (HomePostsObject pfo : success.getWall()){
                                                if (pfo.getMetadata().getIs_shared()==false) {
                                                    listOfPosts.add(pfo);
                                                    postsAdapter.notifyDataSetChanged();
                                                }
                                            }
                                        }else {
                                            no_data.setVisibility(View.VISIBLE);
                                        }
                                    }else {
                                        no_data.setVisibility(View.VISIBLE);
                                    }
                                }else {
                                    no_data.setVisibility(View.VISIBLE);
                                }
                                //SharedPrefManager.getInstance(base).setHome(success);
                            }else {
                                if (success.getWall()!=null){
                                    if (success.getWall().size()>0){
                                        for (HomePostsObject pfo : success.getWall()){
                                            listOfPosts.add(pfo);
                                        }
                                        postsAdapter = new HomePostsAdapter(base,listOfPosts,HomeFragment.this,HomeFragment.this);
                                        rv_posts.setAdapter(postsAdapter);
                                        continue_paginate = true;
                                    }else {
                                        continue_paginate = false;
                                    }
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        if (type == 0) {
                            swipeRefreshLayout.setRefreshing(false);
                        }else {
                            more.smoothToHide();
                        }
                    }
                });
    }

    private void callUpdateTokenAPI(String token) {
        App.is_token_updated = true;
        UserAPIsClass.updateFCMToken(
                base,
                BaseFunctions.getDeviceId(base),
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            UpdateFCMResponse success = new Gson().fromJson(json1,UpdateFCMResponse.class);
                            if (success.getStatus()){
                                Log.i("fcm_token_update", "onResponse: "+"success");
                            }else {
                                Log.i("fcm_token_update", "onResponse: "+"failed");
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("fcm_token_update", "onResponse: "+"no_internet");
                    }
                }
        );
    }


    @Override
    public void onMoreClicked(int type,final  int position,final int isFollowed) {
        switch (type){
                //Photo
            case 1:
                //Video
            case 2:
                //Rate
            case 4:

                //Check in
            case 6:
                //Share Photo
            case 9:
                //Share Photo
            case 10:
                //Share Rate
            case 11:
                //Share Check In
            case 12:
                //Share Ad
            case 15:
                //Share Offer
            case 16:{
                final BottomSheetDialog dialog = new BottomSheetDialog(base);
                dialog.setContentView(R.layout.dialog_bottom_post_more);
                dialog.setCanceledOnTouchOutside(true);
                TextView txt1 = dialog.findViewById(R.id.txt1);
                final TextView txt2 = dialog.findViewById(R.id.txt2);
                TextView txt3 = dialog.findViewById(R.id.txt3);
                View v1 = dialog.findViewById(R.id.view1);
                View v2 = dialog.findViewById(R.id.view2);
                final HomePostsObject po = listOfPosts.get(position);
                txt1.setText(getResources().getString(R.string.bottom_dialog_post_report));
                if (isFollowed == 0){
                    txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_follow)+
                            " "+"<b>"+po.getPost_object().getUser_name()+"</b>"));
                }else {
                    txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_unfollow)+
                            " "+"<b>"+po.getPost_object().getUser_name()+"</b>"));
                }

                txt3.setVisibility(View.GONE);
                v2.setVisibility(View.GONE);
                txt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportDialog rtp = new ReportDialog(base,String.valueOf(po.getPost_object().getId()),"post");
                        rtp.show();
                        dialog.cancel();
                    }
                });
                txt2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isFollowed==0){
                            txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_unfollow)+
                                    " "+"<b>"+po.getPost_object().getUser_name()+"</b>"));
                        }else {
                            txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_follow)+
                                    " "+"<b>"+po.getPost_object().getUser_name()+"</b>"));
                        }
                        callFollowAPI(String.valueOf(po.getPost_object().getUser_id()),
                                "user",isFollowed==0?"yes":"no",position,
                                getResources().getString(R.string.follow_person_true),
                                getResources().getString(R.string.follow_person_false));
                        dialog.cancel();
                    }
                });
                dialog.show();
            }break;

            case 5:
            case 13:
            {
                final BottomSheetDialog dialog = new BottomSheetDialog(base);
                dialog.setContentView(R.layout.dialog_bottom_post_more);
                dialog.setCanceledOnTouchOutside(true);
                TextView txt1 = dialog.findViewById(R.id.txt1);
                final TextView txt2 = dialog.findViewById(R.id.txt2);
                TextView txt3 = dialog.findViewById(R.id.txt3);
                View v1 = dialog.findViewById(R.id.view1);
                View v2 = dialog.findViewById(R.id.view2);
                final HomePostsObject po = listOfPosts.get(position);
                txt1.setText(getResources().getString(R.string.bottom_dialog_post_report));
                if (isFollowed == 0){
                    txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_follow)+
                            " "+"<b>"+po.getPost_object().getUser_name()+"</b>"));
                }else {
                    txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_unfollow)+
                            " "+"<b>"+po.getPost_object().getUser_name()+"</b>"));
                }

                txt3.setVisibility(View.GONE);
                v2.setVisibility(View.GONE);
                txt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportDialog rtp = new ReportDialog(base,String.valueOf(po.getPost_object().getId()),"post");
                        rtp.show();
                        dialog.cancel();
                    }
                });
                txt2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isFollowed==0){
                            txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_unfollow)+
                                    " "+"<b>"+po.getPost_object().getUser_name()+"</b>"));
                        }else {
                            txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_follow)+
                                    " "+"<b>"+po.getPost_object().getUser_name()+"</b>"));
                        }
                        callReviewFollowAPI(String.valueOf(po.getPost_object().getUser_id()),
                                "user",isFollowed==0?"yes":"no",position,
                                getResources().getString(R.string.follow_person_true),
                                getResources().getString(R.string.follow_person_false));
                        dialog.cancel();
                    }
                });
                dialog.show();
            }break;
                //Offer
            case 8:{
                final BottomSheetDialog dialog = new BottomSheetDialog(base);
                dialog.setContentView(R.layout.dialog_bottom_post_more);
                dialog.setCanceledOnTouchOutside(true);
                TextView txt1 = dialog.findViewById(R.id.txt1);
                final TextView txt2 = dialog.findViewById(R.id.txt2);
                TextView txt3 = dialog.findViewById(R.id.txt3);
                View v1 = dialog.findViewById(R.id.view1);
                View v2 = dialog.findViewById(R.id.view2);
                final HomePostsObject po = listOfPosts.get(position);
                txt1.setText(getResources().getString(R.string.bottom_dialog_post_report));
                if (isFollowed == 0){
                    txt2.setText(getResources().getString(R.string.bottom_dialog_post_follow_business));
                }else {
                    txt2.setText(getResources().getString(R.string.bottom_dialog_post_unfollow_business));
                }

                txt3.setVisibility(View.GONE);
                v2.setVisibility(View.GONE);
                txt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportDialog rpt = new ReportDialog(base,String.valueOf(po.getPost_object().getId()),"offer");
                        rpt.show();
                        dialog.cancel();
                    }
                });
                txt2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isFollowed==0){
                            txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_unfollow)+
                                    " "+"<b>"+po.getPost_object().getBusiness_name()+"</b>"));
                        }else {
                            txt2.setText(Html.fromHtml(getResources().getString(R.string.bottom_dialog_post_follow)+
                                    " "+"<b>"+po.getPost_object().getBusiness_name()+"</b>"));
                        }
                        callFollowAPI(String.valueOf(po.getPost_object().getBusiness_id()),
                                "user",isFollowed==0?"yes":"no",position,
                                getResources().getString(R.string.follow_business_false),
                                getResources().getString(R.string.follow_business_false));
                        dialog.cancel();
                    }
                });
                dialog.show();
            }break;
                //Ad
            case 7:{
                final BottomSheetDialog dialog = new BottomSheetDialog(base);
                dialog.setContentView(R.layout.dialog_bottom_post_more);
                dialog.setCanceledOnTouchOutside(true);
                TextView txt1 = dialog.findViewById(R.id.txt1);
                TextView txt2 = dialog.findViewById(R.id.txt2);
                TextView txt3 = dialog.findViewById(R.id.txt3);
                View v1 = dialog.findViewById(R.id.view1);
                View v2 = dialog.findViewById(R.id.view2);
                final HomePostsObject po = listOfPosts.get(position);
                txt1.setText(getResources().getString(R.string.bottom_dialog_post_report));
                txt2.setText(getResources().getString(R.string.bottom_dialog_post_hide));
                txt3.setText(getResources().getString(R.string.bottom_dialog_post_hide_all));
                txt1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ReportDialog rpt = new ReportDialog(base,String.valueOf(po.getPost_object().getId()),"post");
                        rpt.show();
                        dialog.cancel();
                    }
                });
                txt2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callHideAPI(position,"post","on",po.getPost_object().getBusiness_id(),po.getPost_object().getId());
                        dialog.cancel();
                    }
                });
                txt3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callHideAPI(position,"post","on",po.getPost_object().getBusiness_id(),0);
                        dialog.cancel();
                    }
                });
                dialog.show();
            }break;
        }
    }

    @Override
    public void onHashTagClicked(String text) {
        clicked_hash = text;
        api_type = "hash";
        current_page = 1;
        continue_paginate = true;
        callHashTagAPI(current_page,0,text);
    }

    private void callFollowAPI(String id,
                               String type,
                               final String value,
                               final int position,
                               final String follow_message,
                               final String unfollow_message){
        FollowersAPIsClass.followNew(
                base,
                BaseFunctions.getDeviceId(base),
                type,
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success) {
                                if (value.equals("yes")) {
                                    Toast.makeText(base, follow_message, Toast.LENGTH_SHORT).show();
                                    for (int i = 0; i < listOfPosts.size(); i++) {
                                        HomePostsObject o = listOfPosts.get(i);
                                        //if it is not a review post
                                        if (o.getMetadata().getObject_type() != 3) {
                                            HomePostObject oo = o.getPost_object();
                                            oo.setIs_following(1);
                                            o.setObject(oo);
                                            listOfPosts.set(i, o);
                                            if (i == position) {
                                                postsAdapter.notifyItemChanged(position);
                                            }
                                        }
                                    }

                                }
                                //HomePostsObject o = listOfPosts.get(position);
                                //HomePostObject oo = o.getPost_object();
                                //oo.setIs_business_following(1);
                                //o.setObject(oo);
                                //listOfPosts.set(position,o);
                                //postsAdapter.notifyItemChanged(position);

                            }else {
                                Toast.makeText(base, unfollow_message, Toast.LENGTH_SHORT).show();
                                for (int i = 0; i < listOfPosts.size(); i++) {
                                    HomePostsObject o = listOfPosts.get(i);
                                    //if it is not a review post
                                    if (o.getMetadata().getObject_type()!=3){
                                        HomePostObject oo = o.getPost_object();
                                        oo.setIs_following(0);
                                        o.setObject(oo);
                                        listOfPosts.set(i,o);
                                        if (i == position){
                                            postsAdapter.notifyItemChanged(position);
                                        }
                                    }
                                }
                                //HomePostsObject o = listOfPosts.get(position);
                                //HomePostObject oo = o.getPost_object();
                                //oo.setIs_business_following(0);
                                //o.setObject(oo);
                                //listOfPosts.set(position,o);
                                //postsAdapter.notifyItemChanged(position);

                            }
                        }else{
                                Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }

                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(base, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void callReviewFollowAPI(String id,
                               String type,
                               final String value,
                               final int position,
                               final String follow_message,
                               final String unfollow_message){
        FollowersAPIsClass.followNew(
                base,
                BaseFunctions.getDeviceId(base),
                type,
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("yes")){
                                    Toast.makeText(base, follow_message, Toast.LENGTH_SHORT).show();
                                    HomePostsObject o = listOfPosts.get(position);
                                    HomePostObject oo = o.getPost_object();
                                    oo.setIs_following(1);
                                    o.setObject(oo);
                                    listOfPosts.set(position,o);
                                    postsAdapter.notifyItemChanged(position);

                                }else {
                                    Toast.makeText(base, unfollow_message, Toast.LENGTH_SHORT).show();
                                    HomePostsObject o = listOfPosts.get(position);
                                    HomePostObject oo = o.getPost_object();
                                    oo.setIs_following(0);
                                    o.setObject(oo);
                                    listOfPosts.set(position,o);
                                    postsAdapter.notifyItemChanged(position);
                                }
                            }else {
                                Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(base, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
    private void callHideAPI(final int position,
                             String type,
                             String value,
                             int type_owner_id,
                             int type_id){
        HidingAPIsClass.hide(base,
                BaseFunctions.getDeviceId(base),
                type,
                value,
                type_owner_id,
                type_id,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        boolean sucess = new Gson().fromJson(j,Boolean.class);
                        if (sucess){
                            Toast.makeText(base, getResources().getString(R.string.hide_success), Toast.LENGTH_SHORT).show();
                            listOfPosts.remove(position);
                            postsAdapter.notifyDataSetChanged();
                        }else {
                            Toast.makeText(base, getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(base, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }



}
