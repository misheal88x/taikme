package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class FaqObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("en_title") private String en_title = "";
    @SerializedName("ar_title") private String ar_title = "";
    @SerializedName("en_content") private String en_content = "";
    @SerializedName("ar_content") private String ar_content = "";
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";

    public FaqObject() { }

    public FaqObject(int id,
                     String en_title,
                     String ar_title,
                     String en_content,
                     String ar_content,
                     String created_at,
                     String updated_at) {
        this.id = id;
        this.en_title = en_title;
        this.ar_title = ar_title;
        this.en_content = en_content;
        this.ar_content = ar_content;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
    }

    public String getAr_title() {
        return ar_title;
    }

    public void setAr_title(String ar_title) {
        this.ar_title = ar_title;
    }

    public String getEn_content() {
        return en_content;
    }

    public void setEn_content(String en_content) {
        this.en_content = en_content;
    }

    public String getAr_content() {
        return ar_content;
    }

    public void setAr_content(String ar_content) {
        this.ar_content = ar_content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
