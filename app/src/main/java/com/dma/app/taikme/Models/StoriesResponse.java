package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class StoriesResponse {
    @SerializedName("stories") private StoriesPagination stories = new StoriesPagination();
    @SerializedName("my_stories") private List<StoryObject> my_stories = new ArrayList<>();

    public StoriesPagination getStories() {
        return stories;
    }

    public void setStories(StoriesPagination people_stories) {
        this.stories = people_stories;
    }

    public List<StoryObject> getMy_stories() {
        return my_stories;
    }

    public void setMy_stories(List<StoryObject> my_stories) {
        this.my_stories = my_stories;
    }
}
