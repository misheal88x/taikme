package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class ProfilePlaceObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("image") private String image = "";
    @SerializedName("name") private String name = "";
    @SerializedName("rating") private float rating = 0f;
    @SerializedName("following_state") private boolean following_state = false;
    @SerializedName("object_type") private int object_type = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public boolean isFollowing_state() {
        return following_state;
    }

    public void setFollowing_state(boolean following_state) {
        this.following_state = following_state;
    }

    public int getObject_type() {
        return object_type;
    }

    public void setObject_type(int object_type) {
        this.object_type = object_type;
    }
}
