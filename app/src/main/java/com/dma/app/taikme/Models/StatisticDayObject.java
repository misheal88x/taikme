package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class StatisticDayObject {
    @SerializedName("cnt") private float cnt = 0;
    @SerializedName("dayName") private String dayName = "";

    public float getCnt() {
        return cnt;
    }

    public void setCnt(float cnt) {
        this.cnt = cnt;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }
}
