package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.R;

public class ViewVideoActivity extends BaseActivity {

    private VideoView vv_video;
    private ImageView btn_play;
    private TextView btn_upload;
    private Intent myIntent;
    private boolean isVideoStarted = false;
    private boolean is_video_ended = true;
    private int stopPosition = 0;
    private String videoPath = "";
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_view_video);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        videoPath = myIntent.getStringExtra("video_path");
        vv_video.setVideoPath(myIntent.getStringExtra("video_path"));
        vv_video.seekTo(1);
    }

    @Override
    public void init_views() {
        //VideoView
        vv_video = findViewById(R.id.video_status_view);
        //ImageView
        btn_play = findViewById(R.id.video_status_play);
        //TextView
        btn_upload = findViewById(R.id.video_status_upload);
        //Intent
        myIntent = getIntent();
    }

    @Override
    public void init_events() {
        vv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isVideoStarted){
                    vv_video.pause();
                    stopPosition = vv_video.getCurrentPosition();
                    btn_play.setVisibility(View.VISIBLE);
                    isVideoStarted = false;
                }else {
                    if (is_video_ended){
                        vv_video.start();
                        is_video_ended = false;
                    }else {
                        vv_video.seekTo(stopPosition);
                        vv_video.start();
                    }

                    btn_play.setVisibility(View.GONE);
                    isVideoStarted = true;
                }
            }
        });
        vv_video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                vv_video.setVideoPath(myIntent.getStringExtra("video_path"));
                is_video_ended = true;
                isVideoStarted = false;
                btn_play.setVisibility(View.VISIBLE);
            }
        });
        btn_upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("video_path",videoPath);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }
}
