package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PostsAPIs {
    @Multipart
    @POST("posts")
    Call<BaseResponse> store(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Part("post_type") RequestBody post_type,
            @Part("content") RequestBody content,
            @Part("caption") RequestBody caption,
            @Part("checkin_business_id") RequestBody checkin_business_id,
            @Part("checkin_business_location_id") RequestBody checkin_business_location_id);
    @Multipart
    @POST("posts/{post_id}/update")
    Call<BaseResponse> update(
            @Header("Accept") String accept,
            @Part("topic_id") RequestBody topic_id,
            @Part("post_type") RequestBody post_type,
            @Part("caption") RequestBody caption,
            @Path("post_id") String post_id,
            @Part MultipartBody.Part file);

    @POST("posts/{post_id}/delete")
    Call<BaseResponse> delete(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Header("device_token") String device_token,
            @Path("post_id") String post_id
    );
    @GET("posts")
    Call<BaseResponse> getPosts(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Header("device_token") String device_token,
            @Query("page") int page
    );
}
