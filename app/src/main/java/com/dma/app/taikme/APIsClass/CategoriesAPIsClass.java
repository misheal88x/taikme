package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.CategoriesAPIs;
import com.dma.app.taikme.APIs.FollowersAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class CategoriesAPIsClass extends BaseRetrofit {
    private static NewProgressDialog dialog;

    public static void getCategories(final Context context,
                              String device_id,
                              IResponse onResponse1,
                              final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        CategoriesAPIs api = retrofit.create(CategoriesAPIs.class);
        Call<BaseResponse> call = api.get_categories("application/json",
                device_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void getCategoryPlaces(final Context context,
                                     String device_id,
                                     String id,
                                         int page,
                                         final int type,
                                     IResponse onResponse1,
                                     final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        if (type == 0){
            dialog = new NewProgressDialog(context);
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(context);
        CategoriesAPIs api = retrofit.create(CategoriesAPIs.class);
        Call<BaseResponse> call = api.get_category_places("application/json",
                device_id,
                id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if(type == 0){
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if(type == 0){
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }
}
