package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.TaikMeAPIs;
import com.dma.app.taikme.APIs.VoteAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Field;

public class TaikMeAPIsClass extends BaseRetrofit {

    private static NewProgressDialog dialog;

    public static void taik(final Context context,
                            String device_id,
                            String feel,
                            String how_many,
                            String going_out_type,
                            String include_kids,
                            String trip_long,
                            String spending_per_person,
                            String traveling_type,
                            String within,
                            IResponse onResponse1,
                            final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        Retrofit retrofit = configureRetrofitWithBearer(context);
        TaikMeAPIs api = retrofit.create(TaikMeAPIs.class);
        Call<BaseResponse> call = api.taik("application/json",
                device_id,
                feel,
                how_many,
                going_out_type,
                include_kids,
                trip_long,
                spending_per_person,
                traveling_type,
                within);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
