package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.ITripLong;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class TripLongDialog extends AlertDialog {
    private Context context;

    private Spinner day_spinner,hour_spinner,minute_spinner;
    private CustomSpinnerAdapter day_adapter,hour_adapter,minute_adapter;
    private List<String> day_list_string,hour_list_string,minute_list_string;
    private String selected_day = "",selected_hours = "",selected_minutes = "";
    private TextView btn_select;
    private ITripLong iTripLong;
    public TripLongDialog(@NonNull Context context,ITripLong iTripLong) {
        super(context);
        this.context = context;
        this.iTripLong = iTripLong;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_trip_long);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //Spinner
        day_spinner = findViewById(R.id.days_spinner);
        hour_spinner = findViewById(R.id.hours_spinner);
        minute_spinner = findViewById(R.id.minutes_spinner);
        //TextView
        btn_select = findViewById(R.id.select_btn);
    }

    private void init_events(){
        day_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int  p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_day = day_list_string.get(p-1);
                }else {
                    selected_day = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        hour_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int  p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_hours = hour_list_string.get(p-1);
                }else {
                    selected_hours = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        minute_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int  p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_minutes = minute_list_string.get(p-1);
                }else {
                    selected_minutes = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_day.equals("")||selected_hours.equals("")||selected_minutes.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.trip_long_dialog_missing), Toast.LENGTH_SHORT).show();
                }
                iTripLong.onTimeSelected(selected_day,selected_hours,selected_minutes);
                cancel();
            }
        });
    }

    private void init_dialog(){
        init_spinners();
    }

    private void init_spinners(){
        day_list_string = new ArrayList<>();
        hour_list_string = new ArrayList<>();
        minute_list_string = new ArrayList<>();

        day_list_string.add(context.getResources().getString(R.string.trip_long_dialog_days));
        hour_list_string.add(context.getResources().getString(R.string.trip_long_dialog_hours));
        minute_list_string.add(context.getResources().getString(R.string.trip_long_dialog_minutes));

        for (int i = 0; i <=100 ; i++) {
            day_list_string.add(String.valueOf(i));
            hour_list_string.add(String.valueOf(i));
            minute_list_string.add(String.valueOf(i));
        }

        BaseFunctions.init_spinner(context,day_spinner,day_adapter,day_list_string);
        BaseFunctions.init_spinner(context,hour_spinner,hour_adapter,hour_list_string);
        BaseFunctions.init_spinner(context,minute_spinner,minute_adapter,minute_list_string);

    }
}
