package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ReviewsAPIs {
    @GET("places/{place_id}/reviews")
    Call<BaseResponse> get_place_reviews(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Header("device_token") String device_token,
            @Path("place_id") String place_id,
            @Query("page") int page);

    @FormUrlEncoded
    @POST("places/{place_id}/reviews")
    Call<BaseResponse> add_review(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Header("device_token") String device_token,
            @Path("place_id") String place_id,
            @Field("rating") float rating,
            @Field("review") String review);

    @FormUrlEncoded
    @POST("places/{place_id}/reviews/{review_id}/update")
    Call<BaseResponse> update_review(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Header("device_token") String device_token,
            @Path("place_id") String place_id,
            @Path("review_id") String review_id,
            @Field("rating") float rating,
            @Field("review") String review);

    @GET("reviews/{type}/{id}")
    Call<BaseResponse> getReviews(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("type") String type,
            @Path("id") String id,
            @Query("page") int page
    );

    @FormUrlEncoded
    @POST("reviews/{type}/{id}")
    Call<BaseResponse> addReview(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("type") String type,
            @Path("id") String id,
            @Field("review_text") String review_text,
            @Field("rating") String rating
    );

}
