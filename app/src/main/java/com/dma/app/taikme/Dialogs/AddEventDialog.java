package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.BusinessLocationsAPIsClass;
import com.dma.app.taikme.APIsClass.EventsAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.IAddEvent;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BusinessLocatonNewObject;
import com.dma.app.taikme.Models.LebanonCityObject;
import com.dma.app.taikme.Models.LocationObject;
import com.dma.app.taikme.Models.StartupObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class AddEventDialog extends AlertDialog {
    private Context context;
    private EditText edt_title;
    private Spinner locations_spinner;
    private TextView dates_spinner,hours_spinner;
    private CustomSpinnerAdapter locations_adapter;
    private List<String> locations_list_string;
    private List<BusinessLocatonNewObject> locations_list;
    private LinearLayout btn_pick_art;
    private TextView btn_start;
    private IAddEvent iMove;
    public static String imagePath = "";
    public static String date = "";
    public static String time = "";
    private int selected_location_id = -1;
    private RelativeLayout layout;
    public AddEventDialog(@NonNull Context context,IAddEvent iMove) {
        super(context);
        this.context = context;
        this.iMove = iMove;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_add_event);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //Spinner
        dates_spinner = findViewById(R.id.add_event_dates_spinner);
        hours_spinner = findViewById(R.id.add_event_hours_spinner);
        locations_spinner = findViewById(R.id.add_event_locations_spinner);
        //EditText
        edt_title = findViewById(R.id.dialog_add_event_name);
        //TextView
        btn_start = findViewById(R.id.add_event_set_btn);
        //LinearLayout
        btn_pick_art = findViewById(R.id.add_event_art_btn);
        //RelativeLayout
        layout = findViewById(R.id.layout);
    }

    private void init_events(){
        locations_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_location_id = locations_list.get(p).getId();
                }else {
                    selected_location_id = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_title.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.create_event_no_title), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_location_id == -1){
                    Toast.makeText(context, context.getResources().getString(R.string.create_event_no_location), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (date.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.create_event_no_date), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (time.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.create_event_no_time), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (imagePath.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.create_event_no_image), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAddEvntAPI();
            }
        });
        btn_pick_art.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.onImageClicked();
            }
        });
        dates_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.onDateClicked();
            }
        });

        hours_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.onTimeClicked();
            }
        });
    }

    private void init_dialog(){
        init_spinners();
    }

    private void init_spinners(){
        StartupObject so = SharedPrefManager.getInstance(context).getStartUp();
        String language =SharedPrefManager.getInstance(context).getSettings().getView_language();

        locations_list_string = new ArrayList<>();
        locations_list = new ArrayList<>();

        locations_list_string.add(context.getResources().getString(R.string.create_event_locations));
        locations_list.add(new BusinessLocatonNewObject());

        locations_adapter = new CustomSpinnerAdapter(context,R.layout.spinner_item,locations_list_string);
        locations_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        locations_spinner.setAdapter(locations_adapter);

        callLocationAPI();

    }

    public static void setImagePath(String path){
        imagePath = path;
    }

    private void callAddEvntAPI(){
        EventsAPIsClass.addEvent(context,
                BaseFunctions.getDeviceId(context),
                String.valueOf(selected_location_id),
                edt_title.getText().toString(),
                edt_title.getText().toString(),
                date + " " + time,
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(context, context.getResources().getString(R.string.create_event_success), Toast.LENGTH_SHORT).show();
                        BaseFunctions.playMusic(context,R.raw.added);
                        iMove.onEventAdded();
                        cancel();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callLocationAPI(){
        btn_start.setEnabled(false);
        BusinessLocationsAPIsClass.getAll(
                context,
                BaseFunctions.getDeviceId(context),
                "-1",
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        btn_start.setEnabled(true);
                        String j = new Gson().toJson(json);
                        BusinessLocatonNewObject[] success = new Gson().fromJson(j,BusinessLocatonNewObject[].class);
                        if (success.length>0){
                            for (BusinessLocatonNewObject o : success){
                                locations_list.add(o);
                                locations_list_string.add(o.getLocation_address());
                                locations_adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        btn_start.setEnabled(true);
                        Snackbar.make(layout, context.getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(context.getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callLocationAPI();
                                    }
                                }).setActionTextColor(context.getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }
}
