package com.dma.app.taikme.Interfaces;

import android.graphics.Typeface;

public interface AddtextFragmentListener {
    void onAddTextButtonClick(Typeface typeface, String text, int color);
}
