package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class StoryObject {

    public static final String TYPE_TEXT = "1";
    public static final String TYPE_IMAGE = "2";
    public static final String TYPE_VIDEO = "3";

    @SerializedName("id") private int id = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("content") private Object content;
    @SerializedName("content_type") private int content_type = 0;
    @SerializedName("scope_type") private int scope_type = 0;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("owner_name") private String user_name = "";
    @SerializedName("owner_profile_image") private String user_profile_image = null;
    @SerializedName("user_cover_image") private String user_cover_image = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public int getContent_type() {
        return content_type;
    }

    public void setContent_type(int content_type) {
        this.content_type = content_type;
    }

    public int getScope_type() {
        return scope_type;
    }

    public void setScope_type(int scope_type) {
        this.scope_type = scope_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getUser_cover_image() {
        return user_cover_image;
    }

    public void setUser_cover_image(String user_cover_image) {
        this.user_cover_image = user_cover_image;
    }
}
