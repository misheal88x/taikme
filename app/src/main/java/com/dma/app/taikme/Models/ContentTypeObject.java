package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class ContentTypeObject {
    @SerializedName("event") private int event = 0;
    @SerializedName("post") private int post = 0;
    @SerializedName("place") private int place = 0;
    @SerializedName("business") private int business = 0;
    @SerializedName("shared_content") private int shared_content = 0;

    public int getEvent() {
        return event;
    }

    public void setEvent(int event) {
        this.event = event;
    }

    public int getPost() {
        return post;
    }

    public void setPost(int post) {
        this.post = post;
    }

    public int getShared_content() {
        return shared_content;
    }

    public void setShared_content(int shared_content) {
        this.shared_content = shared_content;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public int getBusiness() {
        return business;
    }

    public void setBusiness(int business) {
        this.business = business;
    }
}
