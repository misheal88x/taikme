package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class CustomAudienceObject {
    //ok
    @SerializedName("id") private int id = 0;
    //ok
    @SerializedName("created_at") private String created_at = "";
    //New
    @SerializedName("business_id") private int business_id = 0;
    @SerializedName("location_id") private int location_id = 0;
    @SerializedName("en_name") private String en_name = "";
    @SerializedName("ar_name") private String ar_name = "";
    @SerializedName("gender") private String gender = "";
    @SerializedName("prefer") private String prefer = "";
    @SerializedName("character") private String character = "";
    @SerializedName("age_range_from") private int age_range_from = 0;
    @SerializedName("age_range_to") private int age_range_to = 0;
    @SerializedName("going_on") private String going_on = "";
    @SerializedName("best_cuisine") private String best_cuisine = "";
    @SerializedName("second_best_cuisine") private String second_best_cuisine = "";
    @SerializedName("third_best_cuisine") private String third_best_cuisine = "";
    @SerializedName("best_music_gender") private String best_music_gender = "";
    @SerializedName("second_best_music_gender") private String second_best_music_gender = "";
    @SerializedName("third_best_music_gender") private String third_best_music_gender = "";
    @SerializedName("follow_you") private String follow_you = "";
    @SerializedName("updated_at") private String updated_at = "";


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(int business_id) {
        this.business_id = business_id;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name;
    }

    public String getAr_name() {
        return ar_name;
    }

    public void setAr_name(String ar_name) {
        this.ar_name = ar_name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPrefer() {
        return prefer;
    }

    public void setPrefer(String prefer) {
        this.prefer = prefer;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public int getAge_range_from() {
        return age_range_from;
    }

    public void setAge_range_from(int age_range_from) {
        this.age_range_from = age_range_from;
    }

    public int getAge_range_to() {
        return age_range_to;
    }

    public void setAge_range_to(int age_range_to) {
        this.age_range_to = age_range_to;
    }

    public String getGoing_on() {
        return going_on;
    }

    public void setGoing_on(String going_on) {
        this.going_on = going_on;
    }

    public String getBest_cuisine() {
        return best_cuisine;
    }

    public void setBest_cuisine(String best_cuisine) {
        this.best_cuisine = best_cuisine;
    }

    public String getSecond_best_cuisine() {
        return second_best_cuisine;
    }

    public void setSecond_best_cuisine(String second_best_cuisine) {
        this.second_best_cuisine = second_best_cuisine;
    }

    public String getThird_best_cuisine() {
        return third_best_cuisine;
    }

    public void setThird_best_cuisine(String third_best_cuisine) {
        this.third_best_cuisine = third_best_cuisine;
    }

    public String getBest_music_gender() {
        return best_music_gender;
    }

    public void setBest_music_gender(String best_music_gender) {
        this.best_music_gender = best_music_gender;
    }

    public String getSecond_best_music_gender() {
        return second_best_music_gender;
    }

    public void setSecond_best_music_gender(String second_best_music_gender) {
        this.second_best_music_gender = second_best_music_gender;
    }

    public String getThird_best_music_gender() {
        return third_best_music_gender;
    }

    public void setThird_best_music_gender(String third_best_music_gender) {
        this.third_best_music_gender = third_best_music_gender;
    }

    public String getFollow_you() {
        return follow_you;
    }

    public void setFollow_you(String follow_you) {
        this.follow_you = follow_you;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
