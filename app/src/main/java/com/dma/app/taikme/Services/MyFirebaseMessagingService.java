package com.dma.app.taikme.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Activities.SplashActivity;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.UpdateFCMResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    final int MY_NOTIFICATION_ID = 1;
    final String NOTIFICATION_CHANNEL_ID = "10001";
    NotificationManager notificationManager;
    //Notification myNotification;
    NotificationCompat.Builder builder;
    final Context context = this;


    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        try {
            //String recent_token = FirebaseInstanceId.getInstance().getToken();
            SharedPrefManager.getInstance(getApplicationContext()).setFcmToken(s);
            Log.i("refreshed_token", "the token: " + s);
            if (SharedPrefManager.getInstance(getApplicationContext()).getUser()!=null){
                callUpdateTokenAPI(s);
            }
        }catch (Exception e){}
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> params = remoteMessage.getData();
            JSONObject jsonObject = new JSONObject(params);
            Log.i("JSON_OBJECT", "the : " + jsonObject.toString());
            sendNotification(jsonObject);
        }catch (Exception e){}
    }

    private void sendNotification(JSONObject jsonObject) {
        String content = "";
        String target_type = "";
        String target_id = "";
        String user_id = "";
        try {
            content = jsonObject.getString("displayed_message");
            target_type = jsonObject.getString("action_type");
            target_id = jsonObject.getString("target_id");
            user_id = jsonObject.getString("user_id");

        } catch (JSONException e) {
            e.printStackTrace();
        }
       // switch (target_type){
        //    case "0" : {
                Intent intent = new Intent(context, SplashActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(
                        context,
                        1,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                                | PendingIntent.FLAG_ONE_SHOT);
                BaseFunctions.showNotification(context,pendingIntent,content,builder,notificationManager,NOTIFICATION_CHANNEL_ID,MY_NOTIFICATION_ID);


        //    }break;
       // }
    }

    private void callUpdateTokenAPI(String token) {
        UserAPIsClass.updateFCMToken(
                getApplicationContext(),
                BaseFunctions.getDeviceId(getApplicationContext()),
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json != null) {
                            String json1 = new Gson().toJson(json);
                            UpdateFCMResponse success = new Gson().fromJson(json1,UpdateFCMResponse.class);
                            if (success.getStatus()){
                                Toast.makeText(context, "FCM updated", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(context, "FCM not updated", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, "FCM no internet", Toast.LENGTH_SHORT).show();

                    }
                }
        );
    }
}
