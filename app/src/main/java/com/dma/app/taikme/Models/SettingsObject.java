package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class SettingsObject {
    @SerializedName("visible_to_search") private int visible_to_search = 0;
    @SerializedName("allow_people_to_follow") private int allow_people_to_follow = 0;
    @SerializedName("is_profile_public") private int is_profile_public = 0;
    @SerializedName("people_can_comment_on_posts") private int people_can_comment_on_posts = 0;
    @SerializedName("people_can_vote_posts") private int people_can_vote_posts = 0;
    @SerializedName("comments_notifications") private int comments_notifications = 0;
    @SerializedName("votes_notifications") private int votes_notifications = 0;
    @SerializedName("view_language") private String view_language = "en";
    @SerializedName("is_stories_visible") private int is_stories_visible = 0;

    public int getVisible_to_search() {
        return visible_to_search;
    }

    public void setVisible_to_search(int visible_to_search) {
        this.visible_to_search = visible_to_search;
    }

    public int getAllow_people_to_follow() {
        return allow_people_to_follow;
    }

    public void setAllow_people_to_follow(int allow_people_to_follow) {
        this.allow_people_to_follow = allow_people_to_follow;
    }

    public int getIs_profile_public() {
        return is_profile_public;
    }

    public void setIs_profile_public(int is_profile_public) {
        this.is_profile_public = is_profile_public;
    }

    public int getPeople_can_comment_on_posts() {
        return people_can_comment_on_posts;
    }

    public void setPeople_can_comment_on_posts(int people_can_comment_on_posts) {
        this.people_can_comment_on_posts = people_can_comment_on_posts;
    }

    public int getPeople_can_vote_posts() {
        return people_can_vote_posts;
    }

    public void setPeople_can_vote_posts(int people_can_vote_posts) {
        this.people_can_vote_posts = people_can_vote_posts;
    }

    public int getComments_notifications() {
        return comments_notifications;
    }

    public void setComments_notifications(int comments_notifications) {
        this.comments_notifications = comments_notifications;
    }

    public int getVotes_notifications() {
        return votes_notifications;
    }

    public void setVotes_notifications(int votes_notifications) {
        this.votes_notifications = votes_notifications;
    }

    public String getView_language() {
        return view_language;
    }

    public void setView_language(String view_language) {
        this.view_language = view_language;
    }

    public int getIs_stories_visible() {
        return is_stories_visible;
    }

    public void setIs_stories_visible(int is_stories_visible) {
        this.is_stories_visible = is_stories_visible;
    }
}
