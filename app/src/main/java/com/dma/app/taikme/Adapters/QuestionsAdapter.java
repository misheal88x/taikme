package com.dma.app.taikme.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dma.app.taikme.Activities.QuestionAnswerActivity;
import com.dma.app.taikme.Models.FaqObject;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;

import java.util.List;

/**
 * Created by Misheal on 17/11/2019.
 */

public class QuestionsAdapter extends  RecyclerView.Adapter<QuestionsAdapter.ViewHolder> {
    private Context context;
    private List<FaqObject> list;

    public QuestionsAdapter(Context context,List<FaqObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_text;

        public ViewHolder(View view) {
            super(view);
            tv_text = view.findViewById(R.id.item_question_text);


        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_question, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final FaqObject fo = list.get(position);
        final String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
        if (lan.equals("en")){
            if (fo.getEn_title()!=null){
                holder.tv_text.setText(fo.getEn_title());
            }else {
                if (fo.getAr_title()!=null){
                    holder.tv_text.setText(fo.getAr_title());
                }
            }
        }else if (lan.equals("ar")){
            if (fo.getAr_title()!=null){
                holder.tv_text.setText(fo.getAr_title());
            }else {
                if (fo.getEn_title()!=null){
                    holder.tv_text.setText(fo.getEn_title());
                }
            }
        }
        holder.tv_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,QuestionAnswerActivity.class);
                String answer = "";
                if (lan.equals("en")){
                    if (fo.getEn_content()!=null){
                        answer = fo.getEn_content();
                    }else {
                        if (fo.getAr_content()!=null){
                            answer = fo.getAr_content();
                        }
                    }
                }else if (lan.equals("ar")){
                    if (fo.getAr_content()!=null){
                        answer = fo.getAr_content();
                    }else {
                        if (fo.getEn_content()!=null){
                            answer = fo.getEn_content();
                        }
                    }
                }
                intent.putExtra("answer",answer);
                intent.putExtra("question",holder.tv_text.getText().toString());
                context.startActivity(intent);
            }
        });
    }
}
