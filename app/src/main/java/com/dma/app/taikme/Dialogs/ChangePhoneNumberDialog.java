package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class ChangePhoneNumberDialog extends AlertDialog {
    private Context context;
    private EditText edt_new_phone,edt_code;
    private Button btn_send_code,btn_change_number;
    public ChangePhoneNumberDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_change_phone_number);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //EditText
        edt_new_phone = findViewById(R.id.change_phone_phone_number);
        edt_code = findViewById(R.id.change_phone_code);
        //Button
        btn_send_code = findViewById(R.id.change_phone_send_code);
        btn_change_number = findViewById(R.id.change_phone_change);

    }

    private void init_events(){
        btn_send_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_new_phone.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.change_phone_no_number), Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
        btn_change_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_new_phone.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.change_phone_no_number), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_code.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.change_phone_no_code), Toast.LENGTH_SHORT).show();
                    return;
                }
                dismiss();
            }
        });
    }

    private void init_dialog(){

    }
}
