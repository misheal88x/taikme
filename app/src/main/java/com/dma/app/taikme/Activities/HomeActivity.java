package com.dma.app.taikme.Activities;

import android.Manifest;
import android.content.Intent;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Dialogs.AddEventDialog;
import com.dma.app.taikme.Dialogs.AllCommentsDialog;
import com.dma.app.taikme.Fragments.AdsManagerFragment;
import com.dma.app.taikme.Fragments.CreatePostFragment;
import com.dma.app.taikme.Fragments.CustomAudiencesFragment;
import com.dma.app.taikme.Fragments.EventsFragment;
import com.dma.app.taikme.Fragments.FacilityInfoFragment;
import com.dma.app.taikme.Fragments.HomeFragment;
import com.dma.app.taikme.Fragments.InsightsFragment;
import com.dma.app.taikme.Fragments.NotificationsFragment;
import com.dma.app.taikme.Fragments.PlacesReviewedFragment;
import com.dma.app.taikme.Fragments.PlacesVisitedFragment;
import com.dma.app.taikme.Fragments.ProfileFragment;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Others.App;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Services.MyFirebaseInstanceIDService;
import com.dma.app.taikme.Utils.DatePickerForFragment;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.dma.app.taikme.Utils.TimePickerForFragment;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

public class HomeActivity extends BaseActivity implements DatePickerForFragment.OnDateReceiveCallBack,
        TimePickerForFragment.OnTimeReceiveCallBack{
    private RelativeLayout toolbar;
    private EditText edt_search;
    private RelativeLayout btn_notifications;
    private RelativeLayout btn_create_post;
    private RelativeLayout btn_more;
    private ImageView btn_home;
    private Intent my_intent;
    int numBackPressed = 0;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_home);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        String type = my_intent.getStringExtra("type");
         switch (type){
            case "home":{
                HomeFragment home = new HomeFragment();
                open_fragment(home);
            }break;
            case "notifications":{
                NotificationsFragment home = new NotificationsFragment();
                open_fragment(home);
            }break;
            case "post":{
                CreatePostFragment home = new CreatePostFragment();
                open_fragment(home);
            }break;
            case "places_visited":{
                PlacesVisitedFragment placesVisitedFragment = new PlacesVisitedFragment();
                open_fragment(placesVisitedFragment);
            }break;
            case "places_reviewed":{
                PlacesReviewedFragment placesReviewedFragment = new PlacesReviewedFragment();
                open_fragment(placesReviewedFragment);
            }break;
            case "profile":{
                ProfileFragment profileFragment = new ProfileFragment();
                if (!my_intent.getStringExtra("profile_type").equals("me")){
                    Bundle bundle = new Bundle();
                    bundle.putInt("profile_id",my_intent.getIntExtra("profile_id",0));
                    profileFragment.setArguments(bundle);
                }
                open_fragment(profileFragment);
            }break;
            case "facility_info":{
                FacilityInfoFragment facilityInfoFragment = new FacilityInfoFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("place_id",my_intent.getIntExtra("place_id",0));
                facilityInfoFragment.setArguments(bundle);
                open_fragment(facilityInfoFragment);
            }break;
            case "ads_manager":{
                AdsManagerFragment adsManagerFragment = new AdsManagerFragment();
                open_fragment(adsManagerFragment);
            }break;
            case "events":{
                EventsFragment fragment = new EventsFragment();
                Bundle bundle = new Bundle();
                bundle.putString("type","home");
                fragment.setArguments(bundle);
                open_fragment(fragment);
            }break;

        }


    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = findViewById(R.id.home_toolbar);
        edt_search = toolbar.findViewById(R.id.toolbar_search);
        btn_notifications = toolbar.findViewById(R.id.toolbar_notifications);
        btn_create_post = toolbar.findViewById(R.id.toolbar_create_post);
        btn_more = toolbar.findViewById(R.id.toolbar_more_btn);
        btn_home = toolbar.findViewById(R.id.toolbar_home);
        //Intent
        my_intent = getIntent();
    }

    @Override
    public void init_events() {
        btn_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HomeActivity.this,MoreActivity.class));
            }
        });
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edt_search.getText().toString().equals("")){
                        Toast.makeText(HomeActivity.this, getResources().getString(R.string.search_results_no_text), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    Intent intent = new Intent(HomeActivity.this,SearchResultsActivity.class);
                    intent.putExtra("text",edt_search.getText().toString());
                    startActivity(intent);
                    return true;
                }
                return false;
            }
        });
        btn_notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationsFragment notificationsFragment = new NotificationsFragment();
                open_fragment(notificationsFragment);
            }
        });
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HomeFragment home = new HomeFragment();
                open_fragment(home);
            }
        });
        btn_create_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(HomeActivity.this,new String[]{
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA},
                        1);
            }
        });
    }

    @Override
    public void set_fragment_place() {
        this.fragment_place = findViewById(R.id.container);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED){
                    CreatePostFragment createPostFragment = new CreatePostFragment();
                    open_fragment(createPostFragment);
                }else {
                    Toast.makeText(this, getResources().getString(R.string.no_permissions), Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }


    @Override
    public void onBackPressed() {
        if (current_fragment != null) {
            if (current_fragment instanceof HomeFragment){
                if (App.home_api_selected.equals("hash")){
                    ((HomeFragment)current_fragment).continue_paginate = true;
                    ((HomeFragment)current_fragment).current_page = 1;
                    ((HomeFragment)current_fragment).callHomeAPI(1,0);
                }else {
                    if (numBackPressed==1){
                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }else {
                        Toast.makeText(this, getResources().getString(R.string.exit_app), Toast.LENGTH_SHORT).show();
                        numBackPressed++;
                    }
                }
            }else {
                if (current_fragment.get_previous_fragment()!=null) {
                    open_fragment(current_fragment.get_previous_fragment());
                }else {
                    HomeFragment home = new HomeFragment();
                    open_fragment(home);
                }
            }
        }
        else {
            HomeFragment home = new HomeFragment();
            open_fragment(home);
        }

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDateReceive(int dd, int mm, int yy) {
        try {
            int mm1 = mm+1;
            String new_month = mm1<10?"0"+mm1:mm1+"";
            String new_day = dd<10?"0"+dd:dd+"";
            AddEventDialog.date = yy+"-"+new_month+"-"+new_day;
        }catch (Exception e){}
    }

    @Override
    public void onTimeReceive(int hh, int mm) {
        try {
            String new_hour = hh<10?"0"+hh:hh+"";
            String new_minute = mm<10?"0"+mm:mm+"";
            AddEventDialog.time = new_hour+":"+new_minute+":"+"00";
        }catch (Exception e){}
    }
}
