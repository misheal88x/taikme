package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class NearByPlaceObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("lat") private float lat = 0.0f;
    @SerializedName("lng") private float lng = 0.0f;
    @SerializedName("location_address") private String location_address = "";
    @SerializedName("business_image") private String business_image = "";
    @SerializedName("business_thumb_image") private String business_thumb_image = "";
    @SerializedName("business_location_id") private int business_location_id = 0;
    @SerializedName("business_avg_review") private String business_avg_review = "";


    @SerializedName("is_selected") private boolean is_selected = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }

    public String getLocation_address() {
        return location_address;
    }

    public void setLocation_address(String location_address) {
        this.location_address = location_address;
    }

    public boolean getIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }

    public String getBusiness_image() {
        return business_image;
    }

    public void setBusiness_image(String business_image) {
        this.business_image = business_image;
    }

    public String getBusiness_thumb_image() {
        return business_thumb_image;
    }

    public void setBusiness_thumb_image(String business_thumb_image) {
        this.business_thumb_image = business_thumb_image;
    }

    public int getBusiness_location_id() {
        return business_location_id;
    }

    public void setBusiness_location_id(int business_location_id) {
        this.business_location_id = business_location_id;
    }

    public String getBusiness_avg_review() {
        return business_avg_review;
    }

    public void setBusiness_avg_review(String business_avg_review) {
        this.business_avg_review = business_avg_review;
    }

    public boolean isIs_selected() {
        return is_selected;
    }
}
