package com.dma.app.taikme.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.PeopleToFollowObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Misheal on 18/11/2019.
 */

public class PeopleToFollowAdapter extends  RecyclerView.Adapter<PeopleToFollowAdapter.ViewHolder> {
    private Context context;
    private List<PeopleToFollowObject> list;

    public PeopleToFollowAdapter(Context context,List<PeopleToFollowObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private SimpleDraweeView img_image;
        private RelativeLayout btn_confirm,follow_layout;
        private TextView tv_name;
        private ImageView follow_image;

        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_people_to_follow_image);
            btn_confirm = view.findViewById(R.id.item_people_to_follow_confirm);
            tv_name = view.findViewById(R.id.item_people_to_follow_name);
            follow_layout = view.findViewById(R.id.follow_layout);
            follow_image = view.findViewById(R.id.follow_image);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_people_to_follow, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final PeopleToFollowObject o = list.get(position);

        holder.tv_name.setText(o.getName()!=null?o.getName():"");
        BaseFunctions.setFrescoImage(holder.img_image,o.getImage());
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,HomeActivity.class);
                intent.putExtra("type","profile");
                intent.putExtra("profile_type","other");
                intent.putExtra("profile_id",o.getId());
                context.startActivity(intent);
            }
        });

        if (o.getIs_following().equals("0")){
            holder.follow_layout.setBackgroundResource(R.drawable.primary_circle);
            holder.follow_image.setImageResource(R.drawable.ic_plus);
        }else if (o.getIs_following().equals("1")){
            holder.follow_layout.setBackgroundResource(R.drawable.dark_grey_circle);
            holder.follow_image.setImageResource(R.drawable.ic_minus);
        }

        holder.btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callFollowAPI(holder,position,String.valueOf(o.getId()),o.getIs_following().equals("0")?"yes":"no");
            }
        });
    }

    private void callFollowAPI(final ViewHolder holder,final int position,String id,final String value){
        FollowersAPIsClass.followNew(
                context,
                BaseFunctions.getDeviceId(context),
                "user",
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("yes")) {
                                    Toast.makeText(context, context.getResources().getString(R.string.follow_person_true), Toast.LENGTH_SHORT).show();
                                    holder.follow_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                    holder.follow_image.setImageResource(R.drawable.ic_minus);
                                    PeopleToFollowObject o = list.get(position);
                                    o.setIs_following("1");
                                    list.set(position,o);
                                    notifyItemChanged(position);
                                }else {
                                    Toast.makeText(context, context.getResources().getString(R.string.follow_person_false), Toast.LENGTH_SHORT).show();
                                    holder.follow_layout.setBackgroundResource(R.drawable.primary_circle);
                                    holder.follow_image.setImageResource(R.drawable.ic_plus);
                                    PeopleToFollowObject o = list.get(position);
                                    o.setIs_following("0");
                                    list.set(position,o);
                                    notifyItemChanged(position);
                                }
                            }else {
                                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
