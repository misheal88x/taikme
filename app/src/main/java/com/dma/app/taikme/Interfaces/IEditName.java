package com.dma.app.taikme.Interfaces;

public interface IEditName {
    void onNameWritten(String name);
}
