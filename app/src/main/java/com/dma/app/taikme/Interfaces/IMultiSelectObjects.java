package com.dma.app.taikme.Interfaces;

import com.dma.app.taikme.Models.CategoryObject;

import java.util.List;

public interface IMultiSelectObjects {
    void onSelected(List<CategoryObject> list);
}
