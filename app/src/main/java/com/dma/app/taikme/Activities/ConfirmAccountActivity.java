package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIs.UserAPIs;
import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

public class ConfirmAccountActivity extends BaseActivity {

    private EditText txt1,txt2,txt3,txt4,txt5,txt6;
    private TextView btn_confirm;
    private LinearLayout root;
    private int before_1 = 0;
    private int before_2 = 0;
    private int before_3 = 0;
    private int before_4 = 0;
    private int before_5 = 0;
    private int before_6 = 0;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_confirm_account);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        callSendVerificationCode();
        SharedPrefManager.getInstance(this).setAccountActivated(false);
    }

    @Override
    public void init_views() {
        txt1 = findViewById(R.id.txt1);
        txt2 = findViewById(R.id.txt2);
        txt3 = findViewById(R.id.txt3);
        txt4 = findViewById(R.id.txt4);
        txt5 = findViewById(R.id.txt5);
        txt6 = findViewById(R.id.txt6);
        btn_confirm = findViewById(R.id.add_offer_start_btn);
        root = findViewById(R.id.layout);
    }

    @Override
    public void init_events() {
        txt1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==1){
                    txt2.requestFocus();
                }else if (s.length()>1){
                    txt1.setText(txt1.getText().toString().substring(0,1));
                    txt2.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        txt2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==1){
                    txt3.requestFocus();
                }else if (s.length()>1){
                    txt2.setText(txt2.getText().toString().substring(0,1));
                    txt3.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        txt3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==1){
                    txt4.requestFocus();
                }else if (s.length()>1){
                    txt3.setText(txt3.getText().toString().substring(0,1));
                    txt4.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        txt4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==1){
                    txt5.requestFocus();
                }else if (s.length()>1){
                    txt4.setText(txt4.getText().toString().substring(0,1));
                    txt5.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        txt5.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length()==1){
                    txt6.requestFocus();
                }else if (s.length()>1){
                    txt5.setText(txt5.getText().toString().substring(0,1));
                    txt6.requestFocus();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        txt6.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        txt2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(event.getAction() == KeyEvent.ACTION_DOWN&&keyCode == KeyEvent.KEYCODE_DEL) {
                    txt1.requestFocus();
                }
                return false;
            }
        });
        txt3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(event.getAction() == KeyEvent.ACTION_DOWN&&keyCode == KeyEvent.KEYCODE_DEL) {
                    txt2.requestFocus();
                }
                return false;
            }
        });
        txt4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(event.getAction() == KeyEvent.ACTION_DOWN&&keyCode == KeyEvent.KEYCODE_DEL) {
                    txt3.requestFocus();
                }
                return false;
            }
        });
        txt5.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(event.getAction() == KeyEvent.ACTION_DOWN&&keyCode == KeyEvent.KEYCODE_DEL) {
                    txt4.requestFocus();
                }
                return false;
            }
        });
        txt6.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(event.getAction() == KeyEvent.ACTION_DOWN&&keyCode == KeyEvent.KEYCODE_DEL) {
                    txt5.requestFocus();
                }
                return false;
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (
                        txt1.getText().toString().equals("")||
                        txt2.getText().toString().equals("")||
                        txt3.getText().toString().equals("")||
                        txt4.getText().toString().equals("")||
                        txt5.getText().toString().equals("")||
                        txt6.getText().toString().equals("")){
                    Toast.makeText(ConfirmAccountActivity.this, getResources().getString(R.string.confirm_account_missing), Toast.LENGTH_SHORT).show();
                    return;
                }
                String code = txt1.getText().toString()+
                        txt2.getText().toString()+
                        txt3.getText().toString()+
                        txt4.getText().toString()+
                        txt5.getText().toString()+
                        txt6.getText().toString();
                if (code.equals("000000")){
                    callConfirmAccountAPI();
                }

            }
        });

    }

    @Override
    public void set_fragment_place() {

    }

    private void callSendVerificationCode(){
        UserAPIsClass.sendVerificationCode(
                ConfirmAccountActivity.this,
                BaseFunctions.getDeviceId(ConfirmAccountActivity.this),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        Boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){
                            Toast.makeText(ConfirmAccountActivity.this, getResources().getString(R.string.confirm_account_code_sent), Toast.LENGTH_SHORT).show();
                        }else {
                            Snackbar.make(root, getResources().getString(R.string.error_occurred), Snackbar.LENGTH_INDEFINITE)
                                    .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            callSendVerificationCode();
                                        }
                                    }).setActionTextColor(getResources().getColor(R.color.white)).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callSendVerificationCode();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }

    private void callConfirmAccountAPI(){
        UserAPIsClass.confirmAccount(
                ConfirmAccountActivity.this,
                BaseFunctions.getDeviceId(ConfirmAccountActivity.this),
                "T-" + txt1.getText().toString() +
                        txt2.getText().toString() +
                        txt3.getText().toString() +
                        txt4.getText().toString() +
                        txt5.getText().toString() +
                        txt6.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        Boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){
                            Toast.makeText(ConfirmAccountActivity.this, getResources().getString(R.string.confirm_account_account_activated), Toast.LENGTH_SHORT).show();
                            SharedPrefManager.getInstance(ConfirmAccountActivity.this).setAccountActivated(true);
                            Intent intent = new Intent(ConfirmAccountActivity.this,LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }else {
                            Toast.makeText(ConfirmAccountActivity.this, getResources().getString(R.string.confirm_account_account_not_activated), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callConfirmAccountAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }
}
