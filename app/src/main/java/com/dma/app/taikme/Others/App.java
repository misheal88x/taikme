package com.dma.app.taikme.Others;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.dma.app.taikme.BuildConfig;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.google.firebase.FirebaseApp;

//import com.google.firebase.FirebaseApp;
//import com.onesignal.OSNotificationOpenResult;
//import com.onesignal.OneSignal;

import net.gotev.uploadservice.UploadService;


import org.json.JSONObject;

import androidx.core.app.ActivityCompat;
import androidx.multidex.MultiDex;

/**
 * Created by Misheal on 11/12/2019.
 */

public class App extends Application implements LocationListener {
    public static App instance;
    protected boolean gps_enabled, network_enabled;
    private LocationManager locationManager;
    private String provider;
    public static String categories_json = "";
    public static String home_api_selected = "";

    public App() {
        instance = this;
    }

    public static boolean is_token_updated = false;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        FirebaseApp.initializeApp(getApplicationContext());
        ImagePipelineConfig config = ImagePipelineConfig.newBuilder(this)
                .setDownsampleEnabled(true)
                .build();
        Fresco.initialize(this,config);
        setupLocation();
        UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
        // OneSignal Initialization
        /*
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationOpenedHandler(new NotificationHandler())
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

         */
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "en"));
        MultiDex.install(this);

    }

    public static synchronized App getInstance(){
        return instance;
    }

    private void setupLocation(){
        Log.i("peryes_called", "two :  "+"call location");
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        provider = LocationManager.GPS_PROVIDER;
        // getting GPS status
        gps_enabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);
        // getting network status
        network_enabled = locationManager
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.i("perno_called", "one :  " + "yes");
        }
        Log.i("peryes_called", "two :  "+"yes");
        //Location location = locationManager.getLastKnownLocation(provider);
        try {
            if (gps_enabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, 10000, 1,
                        this);
            }else if (network_enabled) {
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, 10000, 1,
                        this);
            }
        }catch (Exception e){}
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("test_location", "onLocationChanged: "+String.valueOf(location.getLatitude())+
                String.valueOf(location.getLongitude()));
        SharedPrefManager.getInstance(getApplicationContext()).setLat(String.valueOf(location.getLatitude()));
        SharedPrefManager.getInstance(getApplicationContext()).setLng(String.valueOf(location.getLongitude()));
        Log.i("ddfdfdf", "onLocationChanged: "+location.getAltitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /*
    class NotificationHandler implements OneSignal.NotificationOpenedHandler{

        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            JSONObject data = result.notification.payload.additionalData;
            //Suppose that we send the key = name with value of michel by example
            if (data != null && data.has("name")){
                String name = data.optString("name");
            }
        }
    }

     */
}
