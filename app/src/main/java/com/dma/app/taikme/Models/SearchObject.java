package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class SearchObject {
    @SerializedName("users") private UsersResponse users = new UsersResponse();
    @SerializedName("places") private PlacesResponse places = new PlacesResponse();

    public UsersResponse getUsers() {
        return users;
    }

    public void setUsers(UsersResponse users) {
        this.users = users;
    }

    public PlacesResponse getPlaces() {
        return places;
    }

    public void setPlaces(PlacesResponse places) {
        this.places = places;
    }
}
