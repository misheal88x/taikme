package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface OffersAPIs {

    @FormUrlEncoded
    @POST("offers")
    Call<BaseResponse> add_offer(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("business_location_id") int business_location_id,
            @Field("discount") int discount,
            @Field("ar_title") String ar_title,
            @Field("en_title") String en_title,
            @Field("expiry_date") String expiry_date,
            @Field("start_date") String start_date);

    @GET("business/{id}/offers")
    Call<BaseResponse> get_offers(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("id") String id,
            @Query("page") int page
    );
}
