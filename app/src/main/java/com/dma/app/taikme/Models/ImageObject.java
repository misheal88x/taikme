package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class ImageObject {
    @SerializedName("image") private String image = "";
    @SerializedName("thumb_image") private String thumb_image = "";
    @SerializedName("thumbnail") private String thumbnail = "";

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }
}
