package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.EventsAPIsClass;
import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.APIsClass.ShareAPIsClass;
import com.dma.app.taikme.APIsClass.VoteAPIsClass;
import com.dma.app.taikme.Activities.AddCommentActivity;
import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Dialogs.AllCommentsDialog;
import com.dma.app.taikme.Dialogs.AllReviewsDialog;
import com.dma.app.taikme.Dialogs.SharePostDialog;
import com.dma.app.taikme.Dialogs.ViewHomeVideoDialog;
import com.dma.app.taikme.Dialogs.ViewImageDialog;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IPostHashTag;
import com.dma.app.taikme.Interfaces.IPostMore;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.HomeModels.HomePostObject;
import com.dma.app.taikme.Models.HomeModels.HomePostsObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class GeneralHomeAdapter extends  RecyclerView.Adapter<GeneralHomeAdapter.ViewHolder>{
    private Context context;
    private List<HomePostsObject> list;
    private IPostMore iPost;
    private IPostHashTag iHash;

    public GeneralHomeAdapter(Context context, List<HomePostsObject> list, IPostMore iPost,IPostHashTag iHash) {
        this.context = context;
        this.list = list;
        this.iPost = iPost;
        this.iHash = iHash;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout layout,review_layout;
        private SimpleDraweeView profile_image,main_image,reviewed_facility_profile_image,check_facility_profile_image;
        private TextView user_name,title,address,post_time,reviewed_facility_name,review_facility_rate_value,review_facility_text,check_facility_name,
                check_facility_rate_value,offer_percent,offer_text,offer_valid_till,votes_count,post_desc,event_title,
        event_location,event_date,add_comment,see_comment,see_reviews,sponsored;
        private SimpleRatingBar user_rate,review_facility_rate,check_facility_rate;
        private ImageView amplify,three_dots,join_image,video_background,play_video,follow_review_facility,
                follow_check_facility,follow_offer_image,up_vote,down_vote,share;
        private RelativeLayout join_big_layout,join_layout,check_layout,offer_layout,follow_offer_layout;
        private CardView video_layout;
        private FrameLayout media_container;
        public ViewHolder(View v) {
            super(v);
            //LinearLayout
            layout = v.findViewById(R.id.layout);
            review_layout = v.findViewById(R.id.review_layout);
            //SimpleDraweeView
            profile_image = v.findViewById(R.id.item_media_profile_image);
            main_image = v.findViewById(R.id.item_media_image);
            reviewed_facility_profile_image = v.findViewById(R.id.item_home_post_rate_image);
            check_facility_profile_image = v.findViewById(R.id.item_home_post_is_at_image);
            //TextView
            user_name = v.findViewById(R.id.item_media_user_name);
            title = v.findViewById(R.id.item_home_post_rate_title);
            address = v.findViewById(R.id.item_media_address);
            post_time = v.findViewById(R.id.item_media_time);
            reviewed_facility_name = v.findViewById(R.id.item_home_post_rate_name);
            review_facility_rate_value = v.findViewById(R.id.item_home_post_rate_rate_value);
            review_facility_text = v.findViewById(R.id.item_home_post_rate_text);
            check_facility_name = v.findViewById(R.id.item_home_post_is_at_name);
            check_facility_rate_value = v.findViewById(R.id.item_home_post_is_at_rate_value);
            offer_percent = v.findViewById(R.id.item_home_post_offer_percent);
            offer_text = v.findViewById(R.id.item_home_post_offer_name);
            offer_valid_till = v.findViewById(R.id.item_home_post_offer_rate_value);
            votes_count = v.findViewById(R.id.votes_count);
            post_desc = v.findViewById(R.id.item_media_desc);
            event_title = v.findViewById(R.id.item_media_desc_event);
            event_location = v.findViewById(R.id.item_home_event_location);
            event_date = v.findViewById(R.id.item_home_event_date_and_going);
            add_comment = v.findViewById(R.id.item_media_comment_user);
            see_comment = v.findViewById(R.id.item_media_read_comments);
            see_reviews = v.findViewById(R.id.item_media_read_reviews);
            sponsored = v.findViewById(R.id.item_media_sponsored);
            //SimpleRatingBar
            user_rate = v.findViewById(R.id.item_media_rate);
            review_facility_rate = v.findViewById(R.id.item_home_post_rate_rate2);
            check_facility_rate = v.findViewById(R.id.item_home_post_is_at_rate2);
            //ImageView
            amplify = v.findViewById(R.id.item_media_speaker);
            three_dots = v.findViewById(R.id.item_media_three_dots);
            video_background = v.findViewById(R.id.item_media_video);
            play_video = v.findViewById(R.id.play_video);
            follow_review_facility = v.findViewById(R.id.item_home_post_rate_broadcast);
            follow_check_facility = v.findViewById(R.id.item_home_post_is_at_broadcast);
            follow_offer_image = v.findViewById(R.id.item_home_post_offer_add_img);
            down_vote = v.findViewById(R.id.item_media_down_arrow);
            up_vote = v.findViewById(R.id.item_media_top_arrow);
            share = v.findViewById(R.id.item_media_rolling_circle);
            //RelativeLayout
            join_big_layout = v.findViewById(R.id.item_home_event_add_btn);
            join_layout = v.findViewById(R.id.going_layout);
            join_image = v.findViewById(R.id.going_image);
            check_layout = v.findViewById(R.id.check_layout);
            offer_layout = v.findViewById(R.id.offer_layout);
            follow_offer_layout = v.findViewById(R.id.item_home_post_offer_add);
            //CardView
            video_layout = v.findViewById(R.id.video_layout);
            //FrameLayout
            media_container = v.findViewById(R.id.media_container);


        }
    }

    @Override
    public int getItemViewType(int position) {
        HomePostsObject o = list.get(position);

        int result_type = 0;
        int offer_type = 0;
        int event_type = 0;
        int post_type = 0;
        int review_type = 0;
        for (StartupItemObject sio : SharedPrefManager.getInstance(context).getStartUp().getHome_flags_types()){
            switch (sio.getName()){
                case "FLAG_OFFER":{
                    offer_type = Integer.valueOf(sio.getId());
                }break;
                case "FLAG_EVENT":{
                    event_type = Integer.valueOf(sio.getId());
                }break;
                case "FLAG_POST":{
                    post_type = Integer.valueOf(sio.getId());
                }break;
                case "FLAG_RATED_POST":{
                    review_type = Integer.valueOf(sio.getId());
                }break;
            }
            if (o.getMetadata().isIs_sponsored()){
                //Share Ad
                if (o.getMetadata().getIs_shared()) {
                    result_type = 15;
                }
                //Ad
                else {
                    result_type = 7;
                }
            }else {
                //Offer
                if (o.getMetadata().getObject_type() == offer_type) {
                    //Share offer
                    if (o.getMetadata().getIs_shared()) {
                        result_type = 16;
                    }
                    //Offer
                    else {
                        result_type = 8;
                    }
                }
                //Event
                else if (o.getMetadata().getObject_type() == event_type) {
                    //Share event
                    if (o.getMetadata().getIs_shared()) {
                        result_type = 14;
                    }
                    //Event
                    else {
                        result_type = 3;
                    }
                }
                //Rate
                else if (o.getMetadata().getObject_type() == review_type) {
                    //Share Review
                    if (o.getMetadata().getIs_shared()) {
                        result_type = 13;
                    }
                    //Review
                    else {
                        result_type = 5;
                    }
                }
                //Post
                else if (o.getMetadata().getObject_type() == post_type) {
                    int image_post = 0;
                    int video_post = 0;
                    int check_in_post = 0;
                    for (StartupItemObject s : SharedPrefManager.getInstance(context).getStartUp().getPosts_types()) {
                        switch (s.getName()) {
                            case "PHOTO": {
                                image_post = Integer.valueOf(s.getId());
                            }
                            break;
                            case "VIDEO": {
                                video_post = Integer.valueOf(s.getId());
                            }
                            break;
                            case "CHECK_IN": {
                                check_in_post = Integer.valueOf(s.getId());
                            }
                            break;
                        }
                    }
                    //Image
                    if (o.getPost_object().getPost_type() == image_post) {
                        //Share image
                        if (o.getMetadata().getIs_shared()) {
                            result_type = 9;
                        }
                        //Image
                        else {
                            result_type = 1;
                        }
                    }
                    //Video
                    else if (o.getPost_object().getPost_type() == video_post) {
                        //Share video
                        if (o.getMetadata().getIs_shared()) {
                            result_type = 10;
                        }
                        //Video
                        else {
                            result_type = 2;
                        }
                    } else if (o.getPost_object().getPost_type() == check_in_post) {
                        //Share check in
                        if (o.getMetadata().getIs_shared()) {
                            result_type = 12;
                        }
                        //Check in
                        else {
                            result_type = 6;
                        }
                    }
                }
            }
        }

        return result_type;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_general_post, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HomePostsObject dpo = list.get(holder.getAdapterPosition());
        final int type = holder.getItemViewType();
        final String type_text = typeConverter(type);
        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();

        //UserRate
        if (dpo.getPost_object().getBusiness_avg_review()!=null){
            try {
                holder.user_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
            }catch (Exception e){}
        }
        //Time
        if (dpo.getPost_object().getCreated_at()!=null){
            holder.post_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
        }
        switch (type){
            //Image
            case 1:{
                //Username
                if (dpo.getPost_object().getUser_name()!=null){
                    holder.user_name.setText(dpo.getPost_object().getUser_name());
                }
                //Profile image
                if (dpo.getPost_object().getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(holder.profile_image,dpo.getPost_object().getUser_thumb_image());
                }
                //Address
                holder.address.setText(dpo.getPost_object().getUser_city_name()!=null?dpo.getPost_object().getUser_city_name():"");
                //Main image
                if (dpo.getPost_object().getContent()!=null){
                    BaseFunctions.setFrescoImage(holder.main_image,dpo.getPost_object().getContent());
                }
                holder.main_image.setVisibility(View.VISIBLE);
                //Description
                holder.post_desc.setVisibility(View.VISIBLE);
                if (dpo.getPost_object().getCaption()!=null){
                    setTags(holder.post_desc,dpo.getPost_object().getCaption());
                }
                //On click on Image
                holder.main_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            try{
                                ViewImageDialog dialog = new ViewImageDialog(context,dpo.getPost_object().getContent());
                                dialog.show();
                            }catch (Exception e){}

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                //On click on profile image
                holder.profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                holder.user_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Video
            case 2:{
                //Username
                holder.user_name.setText(dpo.getPost_object().getUser_name());
                //Profile image
                BaseFunctions.setFrescoImage(holder.profile_image,dpo.getPost_object().getUser_thumb_image());
                //Address
                holder.address.setText(dpo.getPost_object().getUser_city_name()!=null?dpo.getPost_object().getUser_city_name():"");
                //Caption
                holder.post_desc.setVisibility(View.VISIBLE);
                setTags(holder.post_desc,dpo.getPost_object().getCaption());
                holder.video_layout.setVisibility(View.VISIBLE);
                //On click on play
                holder.play_video.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(context,dpo.getPost_object().getContent());
                        dialog.show();
                    }
                });
                //On click on play
                holder.video_background.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(context,dpo.getPost_object().getContent());
                        dialog.show();
                    }
                });
                holder.profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        context.startActivity(intent);
                    }
                });
                holder.user_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        context.startActivity(intent);
                    }
                });
            }break;
            //Event
            case 3:{
                //Username
                if (dpo.getPost_object().getBusiness_name()!=null){
                    holder.user_name.setText(dpo.getPost_object().getBusiness_name());
                }
                //Profile image
                if (dpo.getPost_object().getBusiness_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(holder.profile_image,dpo.getPost_object().getBusiness_thumb_image());
                    //BaseFunctions.setGlideImage(context,eventViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                holder.address.setText(dpo.getPost_object().getBusiness_city_name());
                //Going
                holder.join_big_layout.setVisibility(View.VISIBLE);
                if (dpo.getPost_object().getBusiness_id()!=SharedPrefManager.getInstance(context).getUser().getId()){
                    if (dpo.getPost_object().getIs_going() == 0){
                        holder.join_layout.setBackgroundResource(R.drawable.primary_circle);
                        holder.join_image.setImageResource(R.drawable.ic_plus);
                    }else if (dpo.getPost_object().getIs_going() == 1){
                        holder.join_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                        holder.join_image.setImageResource(R.drawable.ic_minus);
                    }else {
                        holder.join_big_layout.setVisibility(View.GONE);
                    }
                }else {
                    holder.join_big_layout.setVisibility(View.GONE);
                }
                //Image
                holder.main_image.setVisibility(View.GONE);
                if (dpo.getPost_object().getImage()!=null){
                    try {
                        BaseFunctions.setFrescoImage(holder.main_image,dpo.getPost_object().getImage());
                    }catch (Exception e){}
                }
//Ev            //Event title
                holder.event_title.setVisibility(View.VISIBLE);
                if (lan.equals("en")){
                    holder.event_title.setText(dpo.getPost_object().getEn_title()!=null?dpo.getPost_object().getEn_title():"");
                }else if (lan.equals("ar")){
                    holder.event_title.setText(dpo.getPost_object().getAr_title()!=null?dpo.getPost_object().getAr_title():dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }
                //Event location
                holder.event_location.setVisibility(View.VISIBLE);
                holder.event_location.setText(dpo.getPost_object().getBusiness_location_address()!=null?dpo.getPost_object().getBusiness_location_address():"");
                //Event date and going count
                try {
                    holder.event_date.setText(BaseFunctions.dateExtractor(dpo.getPost_object().getEvent_date())+"-"+dpo.getPost_object().getGoing_count()+" "+
                            context.getResources().getString(R.string.create_event_going));
                    holder.event_date.setVisibility(View.VISIBLE);
                }catch (Exception e){}
                holder.three_dots.setVisibility(View.GONE);
                //On click on join
                holder.join_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getIs_going() == 0){
                            callJoinAPI(holder,"event",position,dpo.getPost_object().getId(),dpo.getPost_object().getIs_going());
                        }
                    }
                });
                holder.main_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            ViewImageDialog dialog = new ViewImageDialog(context, dpo.getPost_object().getImage());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                holder.profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                holder.user_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Review
            case 5:{
                //Layout
                holder.review_layout.setVisibility(View.VISIBLE);
                //Username
                holder.user_name.setText(dpo.getPost_object().getUser_name());
                //User Profile Image
                BaseFunctions.setFrescoImage(holder.profile_image,dpo.getPost_object().getUser_thumb_image());
                //Title
                holder.title.setVisibility(View.VISIBLE);
                holder.title.setText(context.getResources().getString(R.string.home_review)+
                        " "+String.valueOf(dpo.getPost_object().getUser_business_rating())+" "+context.getResources().getString(R.string.home_out_of)+" 5");
                //Address
                holder.address.setText(dpo.getPost_object().getUser_city_name()!=null?
                        dpo.getPost_object().getUser_city_name():"");
                //Facility Image
                BaseFunctions.setFrescoImage(holder.reviewed_facility_profile_image,dpo.getPost_object().getBusiness_thumb_image());
                //Facility name
                holder.reviewed_facility_name.setText(dpo.getPost_object().getBusiness_name());
                //Facility rate
                holder.review_facility_rate.setRating(dpo.getPost_object().getUser_business_rating());
                //Facility Rate text
                holder.review_facility_rate_value.setText(String.valueOf(dpo.getPost_object().getUser_business_rating())+" "+
                        context.getResources().getString(R.string.home_out_of)+" 5");
                //Review text
                holder.review_facility_text.setText(dpo.getPost_object().getUser_business_review_text());
                //todo just for now because ahmed didn't supported it yet
                holder.down_vote.setVisibility(View.GONE);
                holder.up_vote.setVisibility(View.GONE);
                holder.share.setVisibility(View.GONE);
                holder.votes_count.setVisibility(View.GONE);
                holder.follow_review_facility.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callFollowAPI(String.valueOf(dpo.getPost_object().getBusinesses_id()),"user",
                                dpo.getPost_object().getIs_business_following()==0?"yes":"no",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false),position);
                    }
                });
                holder.reviewed_facility_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusinesses_id());
                        context.startActivity(intent);

                    }
                });
                holder.reviewed_facility_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusinesses_id());
                        context.startActivity(intent);
                    }
                });
                holder.profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        context.startActivity(intent);
                    }
                });
                holder.user_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        context.startActivity(intent);
                    }
                });
            }break;
            //Check in
            case 6:{
                //Layout
                holder.check_layout.setVisibility(View.VISIBLE);
                //Username & Facility name
                holder.user_name.setText(Html.fromHtml("<b>"+dpo.getPost_object().getUser_name()+"</b>"+" "+
                        context.getResources().getString(R.string.home_is_at)+" "+"<b>"+dpo.getPost_object().getBusiness_name()+"</b>"));
                //Profile image
                if (dpo.getPost_object().getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(holder.profile_image,dpo.getPost_object().getUser_thumb_image());
                }
                //Address
                holder.address.setText(dpo.getPost_object().getUser_city_name());
                //Facility icon
                BaseFunctions.setFrescoImage(holder.check_facility_profile_image,dpo.getPost_object().getBusiness_thumb_image());
                //Facility Name
                holder.check_facility_name.setText(dpo.getPost_object().getBusiness_name());
                //Facility Rate
                holder.check_facility_rate.setRating(Float.valueOf(dpo.getPost_object().getCheck_in_business_avg_review()));
                //Caption
                holder.post_desc.setVisibility(View.VISIBLE);
                holder.post_desc.setText(dpo.getPost_object().getCaption());
                //On click on follow
                holder.follow_check_facility.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callFollowAPI(String.valueOf(dpo.getPost_object().getBusiness_id()),"user",
                                dpo.getPost_object().getIs_following() == 0?"yes":"no",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false),position);
                    }
                });
                holder.profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                holder.user_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                holder.check_facility_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                holder.check_facility_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
            }break;
            //Ad
            case 7:{
                //Layout
                holder.layout.setBackgroundColor(context.getResources().getColor(R.color.light_grey));
                //Facility name
                holder.user_name.setText(dpo.getPost_object().getBusiness_name());
                //Facility image
                BaseFunctions.setGlideImage(context, holder.profile_image, dpo.getPost_object().getBusiness_thumb_image());
                //Address
                holder.address.setText(dpo.getPost_object().getBusiness_city_name()!=null?
                        dpo.getPost_object().getBusiness_city_name():"");
                //Sponsored
                holder.sponsored.setVisibility(View.VISIBLE);
                //Image
                try {
                    BaseFunctions.setFrescoImage(holder.main_image,dpo.getPost_object().getContent());
                    holder.main_image.setVisibility(View.VISIBLE);
                }catch (Exception e){}
                //Caption
                holder.post_desc.setVisibility(View.VISIBLE);
                setTags(holder.post_desc, dpo.getPost_object().getCaption());
                holder.main_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            try{
                                ViewImageDialog dialog = new ViewImageDialog(context,dpo.getPost_object().getContent());
                                dialog.show();
                            }catch (Exception e){}

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                holder.profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                holder.user_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
            }break;
            //Offer
            case 8:{
                //Layout
                holder.offer_layout.setVisibility(View.VISIBLE);
                //Title
                holder.user_name.setText(Html.fromHtml("<b>"+dpo.getPost_object().getBusiness_name()+"</b>"+" "+
                        context.getResources().getString(R.string.home_created_offer)));
                //Facility icon
                BaseFunctions.setFrescoImage(holder.profile_image,dpo.getPost_object().getBusiness_thumb_image());
                //Address
                holder.address.setText(dpo.getPost_object().getBusiness_city_name());
                //Percent
                holder.offer_percent.setText(dpo.getPost_object().getDiscount()+"%");
                //Product name
                if (lan.equals("en")) {
                    holder.offer_text.setText(dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }else if (lan.equals("ar")){
                    holder.offer_text.setText(dpo.getPost_object().getAr_title()!=null?
                            dpo.getPost_object().getAr_title():dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }
                //End date
                holder.offer_valid_till.setText(context.getResources().getString(R.string.home_valid_till)+" "+
                        BaseFunctions.dateExtractorOld(dpo.getPost_object().getExpiry_date()));
                //Following offer
                if (dpo.getPost_object().getIs_following() == 0){
                    holder.follow_offer_layout.setBackgroundResource(R.drawable.primary_circle);
                    holder.follow_offer_image.setImageResource(R.drawable.ic_plus);
                }else if (dpo.getPost_object().getIs_following() == 1){
                    holder.follow_offer_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                    holder.follow_offer_image.setImageResource(R.drawable.ic_minus);
                }
                holder.profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);

                    }
                });
                holder.user_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                holder.follow_offer_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getIs_following() == 0){
                            holder.follow_offer_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                            holder.follow_offer_image.setImageResource(R.drawable.ic_minus);
                        }else {
                            holder.follow_offer_layout.setBackgroundResource(R.drawable.primary_circle);
                            holder.follow_offer_image.setImageResource(R.drawable.ic_plus);
                        }
                        callFollowAPI(String.valueOf(dpo.getPost_object().getId()),"offer",
                                dpo.getPost_object().getIs_following() == 0?"yes":"no",
                                context.getResources().getString(R.string.follow_offer_true),
                                context.getResources().getString(R.string.follow_offer_false),position);

                    }
                });
            }break;
        }
        //Votes count
        holder.votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
        //User vote
        if (dpo.getPost_object().getUser_vote()==1){
            holder.up_vote.setImageResource(R.drawable.ic_upvote_tapped);
        }else if (dpo.getPost_object().getUser_vote()==-1){
            holder.down_vote.setImageResource(R.drawable.ic_downvote_tapped);
        }else {
            holder.up_vote.setImageResource(R.drawable.ic_upvote_untapped);
            holder.down_vote.setImageResource(R.drawable.ic_downvote_untapped);
        }
        if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
            holder.add_comment.setVisibility(View.GONE);
        }
        if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
            holder.down_vote.setVisibility(View.GONE);
            holder.up_vote.setVisibility(View.GONE);
        }

        holder.see_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)) {
                    AllCommentsDialog dialog = new AllCommentsDialog(context,type_text,dpo.getPost_object().getId());
                    dialog.show();
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.see_reviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)) {
                    AllReviewsDialog dialog = new AllReviewsDialog(context,type_text,dpo.getPost_object().getId());
                    dialog.show();
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.add_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddCommentActivity.class);
                intent.putExtra("id",dpo.getPost_object().getId());
                intent.putExtra("type",type_text);
                context.startActivity(intent);
            }
        });
        holder.three_dots.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iPost.onMoreClicked(type,position,dpo.getPost_object().getIs_business_following());
            }
        });
        holder.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)){
                    SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                        @Override
                        public void onNameWritten(String name) {
                            callShareAPI(String.valueOf(dpo.getPost_object().getId()),type_text,name);
                        }
                    });
                    dialog.show();

                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.up_vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dpo.getPost_object().getUser_vote()==0){
                    callVoteAPI(holder,position,type_text,String.valueOf(dpo.getPost_object().getId()),
                            "1");
                }else if (dpo.getPost_object().getUser_vote() == 1){
                    callVoteAPI(holder,position,type_text,String.valueOf(dpo.getPost_object().getId()),
                            "0");
                }
            }
        });
        holder.down_vote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dpo.getPost_object().getUser_vote()==0){
                    callVoteAPI(holder,position,type_text,String.valueOf(dpo.getPost_object().getId()),
                            "-1");
                }else if (dpo.getPost_object().getUser_vote() == -1){
                    callVoteAPI(holder,position,type_text,String.valueOf(dpo.getPost_object().getId()),
                            "0");
                }
            }
        });
    }

    private void setTags(TextView pTextView, String pTagString) {
        SpannableString string = new SpannableString(pTagString);

        int start = -1;
        for (int i = 0; i < pTagString.length(); i++) {
            if (pTagString.charAt(i) == '#') {
                start = i;
            } else if (pTagString.charAt(i) == ' ' || (i == pTagString.length() - 1 && start != -1)) {
                if (start != -1) {
                    if (i == pTagString.length() - 1) {
                        i++; // case for if hash is last word and there is no
                        // space after word
                    }

                    final String tag = pTagString.substring(start, i);
                    string.setSpan(new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                            iHash.onHashTagClicked(tag);
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            // link color
                            ds.setColor(Color.parseColor("#F7931D"));
                            ds.setUnderlineText(false);
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    start = -1;
                }
            }
        }

        pTextView.setMovementMethod(LinkMovementMethod.getInstance());
        pTextView.setText(string);
    }

    private void callShareAPI(String id,String type,String sharing_text){
        ShareAPIsClass.share(
                context,
                BaseFunctions.getDeviceId(context),
                id,
                type,
                sharing_text,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(context, context.getResources().getString(R.string.home_share_success), Toast.LENGTH_SHORT).show();
                        BaseFunctions.playMusic(context,R.raw.added);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callVoteAPI(final ViewHolder holder, final  int position, final  String type, final  String id, final  String value){
        if (value.equals("1")){
            holder.up_vote.setImageResource(R.drawable.ic_upvote_tapped);
            holder.down_vote.setImageResource(R.drawable.ic_downvote_untapped);
            HomePostsObject e = list.get(position);
            HomePostObject o = e.getPost_object();
            o.setUser_vote(1);
            o.setUpvoting_count(o.getUpvoting_count()+1);
            holder.votes_count.setText(String.valueOf(o.getUpvoting_count()+o.getDownvote_count()));
            e.setObject(o);
        }else if (value.equals("0")){
            holder.up_vote.setImageResource(R.drawable.ic_upvote_untapped);
            holder.down_vote.setImageResource(R.drawable.ic_downvote_untapped);
            HomePostsObject e = list.get(position);
            HomePostObject o = e.getPost_object();
            if (o.getUser_vote() == 1){
                o.setUpvoting_count(o.getUpvoting_count()-1);
            }else if (o.getUser_vote() == -1){
                o.setDownvote_count(o.getDownvote_count()-1);
            }
            holder.votes_count.setText(String.valueOf(o.getUpvoting_count()+o.getDownvote_count()));
            o.setUser_vote(0);
            e.setObject(o);
            list.set(position,e);
        }else if (value.equals("-1")){
            holder.up_vote.setImageResource(R.drawable.ic_upvote_untapped);
            holder.down_vote.setImageResource(R.drawable.ic_downvote_tapped);
            HomePostsObject e = list.get(position);
            HomePostObject o = e.getPost_object();
            o.setUser_vote(-1);
            o.setDownvote_count(o.getDownvote_count()+1);
            holder.votes_count.setText(String.valueOf(o.getUpvoting_count()+o.getDownvote_count()));
            e.setObject(o);
            list.set(position,e);
        }

        VoteAPIsClass.vote(context,
                BaseFunctions.getDeviceId(context),
                type,
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        Boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void callJoinAPI(final ViewHolder holder,final String type,final int position,final int id,final int going){
        if (type.equals("event")){
            if (going == 0){
                holder.join_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                holder.join_image.setImageResource(R.drawable.ic_minus);
            }else {
                holder.join_layout.setBackgroundResource(R.drawable.primary_circle);
                holder.join_image.setImageResource(R.drawable.ic_plus);
            }
        }
        /*
        else {
            if (going == 0){
                ((HomePostsAdapter.ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                ((HomePostsAdapter.ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_minus);
            }else {
                ((HomePostsAdapter.ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.primary_circle);
                ((HomePostsAdapter.ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_plus);
            }
        }
         */
        EventsAPIsClass.joinEvent(context,
                BaseFunctions.getDeviceId(context),
                id,
                going,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){
                            Toast.makeText(context, context.getResources().getString(R.string.create_event_going_success), Toast.LENGTH_SHORT).show();
                            HomePostsObject hpo = list.get(position);
                            HomePostObject sub_hpo = hpo.getPost_object();
                            if (going == 0) {
                                sub_hpo.setIs_going(1);
                                sub_hpo.setGoing_count(sub_hpo.getGoing_count()+1);
                                try {
                                    holder.event_date.setText(BaseFunctions.dateExtractor(sub_hpo.getEvent_date())+"-"+sub_hpo.getGoing_count()+" "+
                                            context.getResources().getString(R.string.create_event_going));
                                }catch (Exception e){}
                            }else {
                                sub_hpo.setIs_going(0);
                                sub_hpo.setGoing_count(sub_hpo.getGoing_count()-1);
                                try {
                                    holder.event_date.setText(BaseFunctions.dateExtractor(sub_hpo.getEvent_date())+"-"+sub_hpo.getGoing_count()+" "+
                                            context.getResources().getString(R.string.create_event_going));
                                }catch (Exception e){}
                            }
                            hpo.setObject(sub_hpo);
                            list.set(position,hpo);
                            notifyItemChanged(position);
                        }else {
                            if (type.equals("event")){
                                if (going == 1){
                                    holder.join_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                    holder.join_image.setImageResource(R.drawable.ic_minus);
                                }else {
                                    holder.join_layout.setBackgroundResource(R.drawable.primary_circle);
                                    holder.join_image.setImageResource(R.drawable.ic_plus);
                                }
                            }
                            /*
                            else {
                                if (going == 1){
                                    ((HomePostsAdapter.ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                    ((HomePostsAdapter.ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_minus);
                                }else {
                                    ((HomePostsAdapter.ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.primary_circle);
                                    ((HomePostsAdapter.ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_plus);
                                }
                            }
                             */
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        if (type.equals("event")){
                            if (going == 1){
                                holder.join_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                holder.join_image.setImageResource(R.drawable.ic_minus);
                            }else {
                                holder.join_layout.setBackgroundResource(R.drawable.primary_circle);
                                holder.join_image.setImageResource(R.drawable.ic_plus);
                            }
                        }
                        /*
                        else {
                            if (going == 1){
                                ((HomePostsAdapter.ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                ((HomePostsAdapter.ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_minus);
                            }else {
                                ((HomePostsAdapter.ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.primary_circle);
                                ((HomePostsAdapter.ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_plus);
                            }
                        }
                         */
                    }
                });

    }

    private void callFollowAPI(String id,
                               final String type,
                               final String value,
                               final String follow_message,
                               final String unfollow_message,
                               final int position){
        FollowersAPIsClass.followNew(
                context,
                BaseFunctions.getDeviceId(context),
                type,
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("yes")){
                                    Toast.makeText(context, follow_message, Toast.LENGTH_SHORT).show();
                                    HomePostsObject o = list.get(position);
                                    HomePostObject oo = o.getPost_object();
                                    oo.setIs_following(1);
                                    o.setObject(oo);
                                    list.set(position,o);
                                    if (!type.equals("offer")) {
                                        notifyItemChanged(position);
                                    }
                                }else {
                                    Toast.makeText(context, unfollow_message, Toast.LENGTH_SHORT).show();
                                    HomePostsObject o = list.get(position);
                                    HomePostObject oo = o.getPost_object();
                                    oo.setIs_following(0);
                                    o.setObject(oo);
                                    list.set(position,o);
                                    if (!type.equals("offer")){
                                        notifyItemChanged(position);
                                    }
                                }
                            }else {
                                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private String typeConverter(int type){
        String out = "";
        switch (type){
            //Image
            //Video
            //Check in
            //Ad
            case 1:
            case 2:
            case 6:
            case 7:{
                out = "post";
            }break;
            //Event
            case 3:{
                out = "event";
            }break;
            //Offer
            case 8:{
                out = "offer";
            }break;
        }
        return out;
    }
}
