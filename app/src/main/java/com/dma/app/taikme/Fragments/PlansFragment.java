package com.dma.app.taikme.Fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dma.app.taikme.Adapters.PlansAdapter;
import com.dma.app.taikme.Models.DumpPlansObject;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;

import java.util.ArrayList;
import java.util.List;

public class PlansFragment extends BaseFragment {

    private RecyclerView rv_plans;
    private List<DumpPlansObject> listOfPlans;
    private PlansAdapter adapter;
    private LinearLayoutManager layoutManager;
    private Button btn_cancel_1,btn_cancel_2;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_plans,container,false);
    }

    @Override
    public void init_views() {
        super.init_views();
        //RecyclerView
        rv_plans = base.findViewById(R.id.plans_recycler);
        //Button
        btn_cancel_1 = base.findViewById(R.id.plans_cancel_1);
        btn_cancel_2 = base.findViewById(R.id.plans_cancel_2);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        if (listOfPlans.size() > 2){
            btn_cancel_1.setVisibility(View.VISIBLE);
            btn_cancel_2.setVisibility(View.GONE);
        }else {
            btn_cancel_2.setVisibility(View.VISIBLE);
            btn_cancel_1.setVisibility(View.GONE);
        }
    }

    private void init_recycler(){
        listOfPlans = new ArrayList<>();

        DumpPlansObject p1 = new DumpPlansObject();
        p1.setTitle("Make sure you arrive here by");
        p1.setTime("11:00 AM");
        p1.setFacility_name("Tyre Beach");
        p1.setFacility_rate(4);

        DumpPlansObject p2 = new DumpPlansObject();
        p2.setTitle("Try to be here by");
        p2.setTime("03:00 PM");
        p2.setFacility_name("Tyre Beach");
        p2.setFacility_rate(4);

        DumpPlansObject p3 = new DumpPlansObject();
        p3.setTitle("You can have dinner here at");
        p3.setTime("06:00 PM");
        p3.setFacility_name("Tyre Beach");
        p3.setFacility_rate(4);

        DumpPlansObject p4 = new DumpPlansObject();
        p4.setTitle("You can enjoy soft music and some drinks here at");
        p4.setTime("09:00 PM");
        p4.setFacility_name("Tyre Beach");
        p4.setFacility_rate(4);

        DumpPlansObject p5 = new DumpPlansObject();
        p5.setTitle("You can enjoy resting for the night here at");
        p5.setTime("12:00 AM");
        p5.setFacility_name("Tyre Beach");
        p5.setFacility_rate(4);

        listOfPlans.add(p1);
        listOfPlans.add(p2);
        listOfPlans.add(p3);
        listOfPlans.add(p4);
        listOfPlans.add(p5);

        adapter = new PlansAdapter(base,listOfPlans);
        layoutManager = new LinearLayoutManager(base,LinearLayoutManager.VERTICAL,false);
        rv_plans.setLayoutManager(layoutManager);
        BaseFunctions.runAnimation(rv_plans,0,adapter);
        rv_plans.setAdapter(adapter);
    }
}
