package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class ChangePasswordDialog extends AlertDialog {
    private Context context;
    private EditText edt_old,edt_new,edt_confirm;
    private Button btn_save;
    public ChangePasswordDialog(@NonNull Context context) {
        super(context);
        this.context = context;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) { ;
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_change_password);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //EditText
        edt_old = findViewById(R.id.change_password_old);
        edt_new = findViewById(R.id.change_password_new);
        edt_confirm = findViewById(R.id.change_password_confirm);
        //Button
        btn_save = findViewById(R.id.change_password_save);

    }

    private void init_events(){
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (edt_old.getText().toString().equals("")){
                //    Toast.makeText(context, context.getResources().getString(R.string.change_password_no_old_password), Toast.LENGTH_SHORT).show();
                //    return;
                //}
                if (edt_new.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.change_password_no_new), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_confirm.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.change_password_no_confirm), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!edt_new.getText().toString().equals(edt_confirm.getText().toString())){
                    Toast.makeText(context, context.getResources().getString(R.string.change_password_not_match), Toast.LENGTH_SHORT).show();
                    return;
                }
                callChangePasswordAPI();
            }
        });
    }

    private void init_dialog(){

    }

    private void callChangePasswordAPI(){
        UserAPIsClass.changePassword(
                context,
                BaseFunctions.getDeviceId(context),
                edt_new.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        Boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){
                            Toast.makeText(context, context.getResources().getString(R.string.change_password_success), Toast.LENGTH_SHORT).show();
                            cancel();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}
