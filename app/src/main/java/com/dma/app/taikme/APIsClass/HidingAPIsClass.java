package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.HidingAPIs;
import com.dma.app.taikme.APIs.ReviewsAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HidingAPIsClass extends BaseRetrofit {

    private static NewProgressDialog dialog;

    public static void hide(final Context context,
                                 String device_id,
                                 String type,
                                 String value,
                                 int type_owner_id,
                                 int type_id,
                                 IResponse onResponse1,
                                 final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        HidingAPIs api = retrofit.create(HidingAPIs.class);
        Call<BaseResponse> call = api.hide("application/json",
                device_id,
                type,
                value,
                type_owner_id,
                type_id);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
