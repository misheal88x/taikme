package com.dma.app.taikme.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.dma.app.taikme.APIsClass.EventsAPIsClass;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.Activities.CreateStatusActivity;
import com.dma.app.taikme.Adapters.EventsAdapter;
import com.dma.app.taikme.Dialogs.AddEventDialog;
import com.dma.app.taikme.Dialogs.ChooseSelectionDialog;
import com.dma.app.taikme.Interfaces.IAddEvent;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Interfaces.ISelection;
import com.dma.app.taikme.Models.DumpPostObject;
import com.dma.app.taikme.Models.EventObject;
import com.dma.app.taikme.Models.EventsResponse;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.DatePickerForFragment;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.dma.app.taikme.Utils.TimePickerForFragment;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.soundcloud.android.crop.Crop;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import static android.app.Activity.RESULT_OK;

public class EventsFragment extends BaseFragment implements IAddEvent,BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate{

    private RecyclerView rv_events;
    private List<EventObject> listOfEvents;
    private EventsAdapter adapter;
    private RelativeLayout root,btn_add_event;
    private LinearLayout no_data_layout;
    private NestedScrollView scrollView;
    private AVLoadingIndicatorView pb_more;
    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;
    private File imageFile;
    private AddEventDialog addEventDialog;
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";
    private String type = "";
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_events,container,false);
    }

    @Override
    public void init_views() {
        //RecyclerView
        rv_events = base.findViewById(R.id.events_recycler);
        //RelativeLayout
        root = base.findViewById(R.id.events_layout);
        btn_add_event = base.findViewById(R.id.profile_cover_add_btn);
        //ScrollView
        scrollView = base.findViewById(R.id.events_scrollview);
        //LinearLayout
        no_data_layout = base.findViewById(R.id.no_data_layout);
        //LocationIndicator
        pb_more = base.findViewById(R.id.events_more);

    }

    @Override
    public void init_events() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (listOfEvents.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callEventsAPI(currentPage,1,type);
                            }
                        }
                    }
                }
            }
        });
        btn_add_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEventDialog = new AddEventDialog(base,EventsFragment.this);
                addEventDialog.show();
            }
        });
    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        String type = this.getArguments().getString("type");
        if (type.equals("home")){
            btn_add_event.setVisibility(View.GONE);
        }
        init_recycler();
        callEventsAPI(currentPage,0,type);
    }

    private void init_recycler(){

        listOfEvents = new ArrayList<>();
        adapter = new EventsAdapter(base, listOfEvents, new IMove() {
            @Override
            public void move() {

            }

            @Override
            public void move(int position) {

            }
        });
        rv_events.setLayoutManager(new LinearLayoutManager(base,RecyclerView.VERTICAL,false));
        rv_events.setAdapter(adapter);
    }

    private void callEventsAPI(final int page,final int type,final String type2){
        if (type == 1){
            pb_more.smoothToShow();
        }
        EventsAPIsClass.getAllEvents(
                base,
                BaseFunctions.getDeviceId(base),
                page,
                type,
                type2.equals("facility"),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            EventsResponse success = new Gson().fromJson(j,EventsResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (EventObject po : success.getData()){
                                        listOfEvents.add(po);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        BaseFunctions.runAnimation(rv_events,0,adapter);
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callEventsAPI(page,type,type2);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            pb_more.smoothToHide();
        }else {
            no_data_layout.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Uri source_uri = uri;
        Uri destination_uri = Uri.fromFile(getTempFile());
        Crop.of(source_uri,destination_uri).withAspect(16,9).start(base,EventsFragment.this);
    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {
        Glide.with(base).load(imageFile).into(ivImage);
    }

    private void showChooserDialog(){
        ChooseSelectionDialog dialog = new ChooseSelectionDialog(base, "camera", new ISelection() {
            @Override
            public void onTakeCameraClicked() {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                imageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                        "test.jpg");
                Uri tempUri = Uri.fromFile(imageFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,tempUri);
                intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,1);
                startActivityForResult(intent,100);
            }

            @Override
            public void onTakeVideoClicked() {

            }

            @Override
            public void onCameraGalleryClicked() {
                BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.dma.app.taikme.provider")
                        .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                        .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                        .build();
                singleSelectionPicker.show(getChildFragmentManager(),"picker");
            }

            @Override
            public void onVideoGalleryClicked() {

            }
        });
        dialog.show();
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    showChooserDialog();
                } else {
                    Toast.makeText(base, getResources().getString(R.string.no_permissions), Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 100: {
                if (resultCode == RESULT_OK) {
                    if (imageFile.exists()) {
                        Uri source_uri = Uri.fromFile(imageFile);
                        Uri destination_uri = Uri.fromFile(getTempFile());
                        Crop.of(source_uri,destination_uri).withAspect(16,9).start(base,EventsFragment.this);
                    } else {
                        Toast.makeText(base, "Image file does not exists", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            break;
            case Crop.REQUEST_CROP:{
                handle_crop(resultCode,data);
            }break;
        }
    }

    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            base.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String filePath=  Environment.getExternalStorageDirectory()
                            + "/"+"temporary_holder.jpg";
                    if (new File(filePath).exists()){
                        addEventDialog.setImagePath(filePath);
                    }

                }
            });
        }
    }

    @Override
    public void onDateClicked() {
        DialogFragment datePicker = new DatePickerForFragment();
        datePicker.show(getFragmentManager(),"date picker");
    }

    @Override
    public void onTimeClicked() {
        DialogFragment timePicker = new TimePickerForFragment();
        timePicker.show(getFragmentManager(),"date picker");
    }

    @Override
    public void onImageClicked() {
        requestPermissions(new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA},
                1);
    }

    @Override
    public void onEventAdded() {
        currentPage = 1;
        listOfEvents.clear();
        rv_events.setAdapter(new EventsAdapter(base, listOfEvents, new IMove() {
            @Override
            public void move() {

            }

            @Override
            public void move(int position) {

            }
        }));
        callEventsAPI(currentPage,0,type);
    }
}
