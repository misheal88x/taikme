package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HomeObject {
    @SerializedName("stories") private StoriesResponse stories = new StoriesResponse();
    @SerializedName("wall") private List<PostFatherObject> wall = new ArrayList<>();

    public StoriesResponse getStories() {
        return stories;
    }

    public void setStories(StoriesResponse stories) {
        this.stories = stories;
    }

    public List<PostFatherObject> getWall() {
        return wall;
    }

    public void setWall(List<PostFatherObject> wall) {
        this.wall = wall;
    }
}
