package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class CommentObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("comment_text") private String comment_text = "";
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("user_name") private String user_name = "";
    @SerializedName("user_image") private String user_image = "";
    @SerializedName("user_thumb_image") private String user_thumb_image = "";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getComment_text() {
        return comment_text;
    }

    public void setComment_text(String comment_text) {
        this.comment_text = comment_text;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getUser_thumb_image() {
        return user_thumb_image;
    }

    public void setUser_thumb_image(String user_thumb_image) {
        this.user_thumb_image = user_thumb_image;
    }
}
