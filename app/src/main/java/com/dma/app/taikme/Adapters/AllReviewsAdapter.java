package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Models.ReviewNewObject;
import com.dma.app.taikme.Models.ReviewObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class AllReviewsAdapter extends  RecyclerView.Adapter<AllReviewsAdapter.ViewHolder>{
    private Context context;
    private List<ReviewNewObject> list;

    public AllReviewsAdapter(Context context,List<ReviewNewObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView img_profile_image;
        private TextView tv_username,tv_address,tv_time,tv_text;
        private SimpleRatingBar rb_rate;

        public ViewHolder(View view) {
            super(view);
            img_profile_image = view.findViewById(R.id.item_facility_info_reviews_profile_image);
            tv_username = view.findViewById(R.id.item_facility_info_reviews_user_name);
            tv_address = view.findViewById(R.id.item_facility_info_reviews_address);
            tv_time = view.findViewById(R.id.item_facility_info_reviews_time);
            tv_text = view.findViewById(R.id.item_facility_info_reviews_text);
            rb_rate = view.findViewById(R.id.item_facility_info_reviews_rate);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_facility_info_reviews, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        final ReviewNewObject o = list.get(position);

        holder.tv_username.setText(o.getUser_name()!=null?o.getUser_name():"");
        BaseFunctions.setGlideImage(context,holder.img_profile_image,o.getUser_thumb_image());
        holder.tv_time.setText(o.getCreated_at()!=null?BaseFunctions.processDate(context,o.getCreated_at()):"");
        holder.tv_text.setText(o.getReview_text()!=null?o.getReview_text():"");
        holder.tv_address.setText(o.getUser_city_name()==null?o.getUser_city_name():"");
        holder.rb_rate.setRating(o.getRating());
        holder.img_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HomeActivity.class);
                intent.putExtra("type","profile");
                intent.putExtra("profile_type","other");
                intent.putExtra("profile_id",o.getUser_id());
                context.startActivity(intent);
            }
        });

        holder.tv_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,HomeActivity.class);
                intent.putExtra("type","profile");
                intent.putExtra("profile_type","other");
                intent.putExtra("profile_id",o.getUser_id());
                context.startActivity(intent);
            }
        });

    }
}
