package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.dma.app.taikme.R;
import com.dma.app.taikme.Services.UploadStoryService;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.util.ArrayList;
import java.util.List;

public class ChartActivity extends AppCompatActivity {

    private BarChart chart;
    private Button btn_start;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        chart = findViewById(R.id.chart);
        btn_start = findViewById(R.id.start_service);
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChartActivity.this, UploadStoryService.class);
                intent.putExtra("message","Hello");
                startService(intent);
            }
        });
        List<Float> d = new ArrayList<>();
        d.add(Float.valueOf("60"));
        d.add(Float.valueOf("50"));
        d.add(Float.valueOf("70"));
        d.add(Float.valueOf("100"));
        d.add(Float.valueOf("40"));
        d.add(Float.valueOf("50"));
        d.add(Float.valueOf("60"));
        setupChart(chart,d,5000);
    }



    public class MyXAxisValueFormater extends ValueFormatter {
        public String[] mValues;

        public MyXAxisValueFormater(String[] mValues) {
            this.mValues = mValues;
        }

        @Override
        public String getFormattedValue(float value) {
            return mValues[(int)value];
        }
    }

    private void setupChart(BarChart chart, List<Float> data,int maxvalue){
        chart.setDragEnabled(true);
        chart.setScaleEnabled(false);
        ArrayList<BarEntry> yValues = new ArrayList<>();
        int index = 0;
        for (Float f : data){
            yValues.add(new BarEntry(index++,f));
        }
        BarDataSet set1 = new BarDataSet(yValues,"Data set 1");
        set1.setDrawValues(false);
        set1.setColor(getResources().getColor(R.color.colorPrimary));
        chart.getAxisRight().setEnabled(false);
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        BarData barData = new BarData(dataSets);
        barData.setBarWidth(0.2f);
        chart.setData(barData);

        String[] days = new String[]{"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new MyXAxisValueFormater(days));
        xAxis.setGranularity(1);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(getResources().getColor(R.color.dark_grey));
        xAxis.setTextSize(12f);
        xAxis.setAxisLineColor(getResources().getColor(R.color.black));
        xAxis.setAxisLineWidth(1f);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setTextColor(getResources().getColor(R.color.dark_grey));
        yAxis.setTextSize(14f);
        yAxis.setAxisLineColor(getResources().getColor(R.color.black));
        yAxis.setAxisLineWidth(1f);
        chart.setGridBackgroundColor(getResources().getColor(R.color.light_grey));

        Description description = new Description();
        description.setEnabled(false);
        chart.setDescription(description);

        Legend legend = chart.getLegend();
        legend.setEnabled(false);

        chart.animateXY(3000,3000);
    }
}
