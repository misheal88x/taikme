package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.CountDownTimer;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIs.ShareAPIs;
import com.dma.app.taikme.APIsClass.CommentsAPIsClass;
import com.dma.app.taikme.APIsClass.EventsAPIsClass;
import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.APIsClass.ShareAPIsClass;
import com.dma.app.taikme.APIsClass.VoteAPIsClass;
import com.dma.app.taikme.Activities.AddCommentActivity;
import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Activities.StreamActivity;
import com.dma.app.taikme.Activities.ViewOriginalPostActivity;
import com.dma.app.taikme.Dialogs.AddCommentDialog;
import com.dma.app.taikme.Dialogs.AllCommentsDialog;
import com.dma.app.taikme.Dialogs.AllReviewsDialog;
import com.dma.app.taikme.Dialogs.SharePostDialog;
import com.dma.app.taikme.Dialogs.ViewHomeVideoDialog;
import com.dma.app.taikme.Dialogs.ViewImageDialog;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IPostHashTag;
import com.dma.app.taikme.Interfaces.IPostMore;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.ContentTypeObject;
import com.dma.app.taikme.Models.DumpPostObject;
import com.dma.app.taikme.Models.EventObject;
import com.dma.app.taikme.Models.HomeModels.HomePostObject;
import com.dma.app.taikme.Models.HomeModels.HomePostsObject;
import com.dma.app.taikme.Models.ImageObject;
import com.dma.app.taikme.Models.PostFatherObject;
import com.dma.app.taikme.Models.PostObject;
import com.dma.app.taikme.Models.PostTypeObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomePostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<HomePostsObject> list;
    private IPostMore iPost;
    private IPostHashTag iHash;
    private HomePostsObject temp_post;
    public HomePostsAdapter(Context context, List<HomePostsObject> list, IPostMore iPost,IPostHashTag iHash){
        this.context = context;
        this.list = list;
        this.iPost = iPost;
        this.iHash = iHash;
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return Long.valueOf(String.valueOf(list.get(position).getPost_object().getId()));
    }

    class EmptyViewHolder extends RecyclerView.ViewHolder {

        public EmptyViewHolder(View itemView){
            super(itemView);
        }
    }

    class PhotoViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image;
        private TextView tv_username,tv_address,tv_time,tv_desc,tv_see_comments,tv_see_reviews;
        private TextView tv_votes_count;
        private TextView tv_comment_user;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots;
        private SimpleDraweeView img_image;

        public PhotoViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            tv_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_media_time);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_comment_user = itemView.findViewById(R.id.item_media_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_media_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_media_read_reviews);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            img_image = itemView.findViewById(R.id.item_media_image);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);

        }
    }

    public static class VideoViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image;
        private TextView tv_username,tv_address,tv_time,tv_desc,tv_see_comments,tv_see_reviews;
        private TextView tv_votes_count;
        private TextView tv_comment_user;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,thumbnail,play;
        private FrameLayout media_container;


        public VideoViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            tv_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_media_time);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_comment_user = itemView.findViewById(R.id.item_media_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_media_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_media_read_reviews);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            thumbnail = itemView.findViewById(R.id.item_media_video);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            media_container = itemView.findViewById(R.id.media_container);
            play = itemView.findViewById(R.id.play_video);
        }
    }

    class EventViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image;
        private RelativeLayout btn_add;
        private TextView tv_username,tv_address,tv_time,tv_desc,tv_see_comments,tv_see_reviews,tv_location,tv_date_and_going;
        private TextView tv_votes_count;
        private TextView tv_comment_user;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling;
        private SimpleDraweeView img_image;
        private ImageView going_image;
        private RelativeLayout going_layout;

        public EventViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            btn_add = itemView.findViewById(R.id.item_home_event_add_btn);
            tv_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_media_time);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_comment_user = itemView.findViewById(R.id.item_media_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_media_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_media_read_reviews);
            tv_location = itemView.findViewById(R.id.item_home_event_location);
            tv_date_and_going = itemView.findViewById(R.id.item_home_event_date_and_going);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            img_image = itemView.findViewById(R.id.item_media_image);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            going_layout = itemView.findViewById(R.id.going_layout);
            going_image = itemView.findViewById(R.id.going_image);
        }
    }

    class RateViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_image;
        private TextView tv_votes_count;
        private TextView tv_username,tv_title,tv_address,tv_time,tv_name,tv_rate,tv_text;
        private SimpleRatingBar rb_rate,rb_rate2;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,img_broadcast;

        public RateViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_home_post_rate_profile_image);
            img_image = itemView.findViewById(R.id.item_home_post_rate_image);
            tv_username = itemView.findViewById(R.id.item_home_post_rate_user_name);
            tv_title = itemView.findViewById(R.id.item_home_post_rate_title);
            tv_address = itemView.findViewById(R.id.item_home_post_rate_address);
            tv_time = itemView.findViewById(R.id.item_home_post_rate_time);
            tv_name = itemView.findViewById(R.id.item_home_post_rate_name);
            tv_rate = itemView.findViewById(R.id.item_home_post_rate_rate_value);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_home_post_rate_rate);
            rb_rate2 = itemView.findViewById(R.id.item_home_post_rate_rate2);
            tv_text = itemView.findViewById(R.id.item_home_post_rate_text);
            btn_up = itemView.findViewById(R.id.item_home_post_rate_top_arrow);
            btn_down = itemView.findViewById(R.id.item_home_post_rate_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_home_post_rate_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_home_post_rate_three_dots);
            img_broadcast = itemView.findViewById(R.id.item_home_post_rate_broadcast);
        }
    }

    class ReviewViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_image;
        private TextView tv_username,tv_title,tv_address,tv_time,tv_name,tv_rate,tv_text;
        private TextView tv_votes_count;
        private SimpleRatingBar rb_rate,rb_rate2;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,img_broadcast;

        public ReviewViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_home_post_rate_profile_image);
            img_image = itemView.findViewById(R.id.item_home_post_rate_image);
            tv_username = itemView.findViewById(R.id.item_home_post_rate_user_name);
            tv_title = itemView.findViewById(R.id.item_home_post_rate_title);
            tv_address = itemView.findViewById(R.id.item_home_post_rate_address);
            tv_time = itemView.findViewById(R.id.item_home_post_rate_time);
            tv_name = itemView.findViewById(R.id.item_home_post_rate_name);
            tv_rate = itemView.findViewById(R.id.item_home_post_rate_rate_value);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_home_post_rate_rate);
            rb_rate2 = itemView.findViewById(R.id.item_home_post_rate_rate2);
            btn_up = itemView.findViewById(R.id.item_home_post_rate_top_arrow);
            btn_down = itemView.findViewById(R.id.item_home_post_rate_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_home_post_rate_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_home_post_rate_three_dots);
            img_broadcast = itemView.findViewById(R.id.item_home_post_rate_broadcast);
            tv_text = itemView.findViewById(R.id.item_home_post_rate_text);
        }
    }

    class CheckInViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_image;
        private TextView tv_username,tv_address,tv_time,tv_name,tv_rate,tv_caption,tv_see_comments,tv_see_reviews;
        private TextView tv_votes_count;
        private TextView tv_comment_user;
        private SimpleRatingBar rb_rate,rb_rate2;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,img_broadcast;

        public CheckInViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_home_post_is_at_profile_image);
            img_image = itemView.findViewById(R.id.item_home_post_is_at_image);
            tv_username = itemView.findViewById(R.id.item_home_post_is_at_user_name);
            tv_address = itemView.findViewById(R.id.item_home_post_is_at_address);
            tv_time = itemView.findViewById(R.id.item_home_post_is_at_time);
            tv_name = itemView.findViewById(R.id.item_home_post_is_at_name);
            tv_rate = itemView.findViewById(R.id.item_home_post_is_at_rate_value);
            rb_rate = itemView.findViewById(R.id.item_home_post_is_at_rate);
            rb_rate2 = itemView.findViewById(R.id.item_home_post_is_at_rate2);
            tv_caption = itemView.findViewById(R.id.item_home_post_is_at_desc);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            btn_up = itemView.findViewById(R.id.item_home_post_is_at_top_arrow);
            btn_down = itemView.findViewById(R.id.item_home_post_is_at_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_home_post_is_at_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_home_post_is_at_three_dots);
            img_broadcast = itemView.findViewById(R.id.item_home_post_is_at_broadcast);
            tv_comment_user = itemView.findViewById(R.id.item_media_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_media_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_media_read_reviews);
        }
    }

    class AdViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image;
        private TextView tv_username,tv_address,tv_time,tv_sposored,tv_desc,tv_see_comments,tv_see_reviews;
        private TextView tv_votes_count;
        private TextView tv_comment_user;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots;
        private SimpleDraweeView img_image;
        private LinearLayout layout;

        public AdViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            tv_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_media_time);
            tv_sposored = itemView.findViewById(R.id.item_media_sponsored);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_comment_user = itemView.findViewById(R.id.item_media_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_media_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_media_read_reviews);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            img_image = itemView.findViewById(R.id.item_media_image);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            layout = itemView.findViewById(R.id.item_media_layout);

        }
    }

    class CreateOfferViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image;
        private TextView tv_username,tv_address,tv_time,tv_name,tv_rate,tv_percent,tv_till,tv_see_comments,tv_see_reviews;
        private TextView tv_comment_user;
        private TextView tv_votes_count;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,add_img;
        private RelativeLayout img_add;

        public CreateOfferViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_home_post_offer_profile_image);
            tv_username = itemView.findViewById(R.id.item_home_post_offer_user_name);
            tv_address = itemView.findViewById(R.id.item_home_post_offer_address);
            tv_time = itemView.findViewById(R.id.item_home_post_offer_time);
            tv_name = itemView.findViewById(R.id.item_home_post_offer_name);
            tv_rate = itemView.findViewById(R.id.item_home_post_offer_rate_value);
            tv_percent = itemView.findViewById(R.id.item_home_post_offer_percent);
            tv_till = itemView.findViewById(R.id.item_home_post_offer_rate_value);
            tv_comment_user = itemView.findViewById(R.id.item_home_post_offer_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_home_post_offer_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_home_post_offer_read_reviews);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_home_post_offer_rate);
            btn_up = itemView.findViewById(R.id.item_home_post_offer_top_arrow);
            btn_down = itemView.findViewById(R.id.item_home_post_offer_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_home_post_offer_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_home_post_offer_three_dots);
            img_add = itemView.findViewById(R.id.item_home_post_offer_add);
            add_img = itemView.findViewById(R.id.item_home_post_offer_add_img);
        }
    }

    class SharePhotoViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_shared_profile_image;
        private TextView tv_username,tv_shared_username,tv_address,tv_time,tv_shared_time,tv_desc;
        private TextView tv_votes_count,share_desc;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots;
        private SimpleDraweeView img_image;
        private LinearLayout shared_layout;

        public SharePhotoViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_share_image_profile);
            img_shared_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            tv_username = itemView.findViewById(R.id.item_share_image_user_name);
            tv_shared_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_share_image_time);
            tv_shared_time = itemView.findViewById(R.id.item_media_time);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            img_image = itemView.findViewById(R.id.item_media_image);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            share_desc = itemView.findViewById(R.id.share_desc);
            shared_layout = itemView.findViewById(R.id.shared_layout);
        }
    }

    public static class ShareVideoViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_shared_profile_image;
        private TextView tv_username,tv_shared_username,tv_address,tv_time,tv_shared_time,tv_desc;
        private TextView tv_votes_count,share_desc;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,thumbnail;
        private FrameLayout media_container;
        private ImageView play;
        private LinearLayout shared_layout;


        public ShareVideoViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_share_video_profile);
            img_shared_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            tv_username = itemView.findViewById(R.id.item_share_video_user_name);
            tv_shared_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_share_video_time);
            tv_shared_time = itemView.findViewById(R.id.item_media_time);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            thumbnail = itemView.findViewById(R.id.item_media_video);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            media_container = itemView.findViewById(R.id.media_container);
            play = itemView.findViewById(R.id.play_video);
            share_desc = itemView.findViewById(R.id.share_desc);
            shared_layout = itemView.findViewById(R.id.shared_layout);
        }
    }

    class ShareRateViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_shared_profile_image,img_image;
        private TextView tv_username,tv_shared_username,tv_title,tv_address,tv_time,tv_shared_time,tv_name,tv_rate,tv_text;
        private TextView tv_votes_count,share_desc;
        private SimpleRatingBar rb_rate,rb_rate2;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,img_broadcast;
        private LinearLayout shared_layout;

        public ShareRateViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_share_rate_profile);
            img_shared_profile_image = itemView.findViewById(R.id.item_home_post_rate_profile_image);
            img_image = itemView.findViewById(R.id.item_home_post_rate_image);
            tv_username = itemView.findViewById(R.id.item_share_rate_user_name);
            tv_shared_username = itemView.findViewById(R.id.item_home_post_rate_user_name);
            tv_title = itemView.findViewById(R.id.item_home_post_rate_title);
            tv_address = itemView.findViewById(R.id.item_home_post_rate_address);
            tv_time = itemView.findViewById(R.id.item_share_rate_time);
            tv_shared_time = itemView.findViewById(R.id.item_home_post_rate_time);
            tv_name = itemView.findViewById(R.id.item_home_post_rate_name);
            tv_rate = itemView.findViewById(R.id.item_home_post_rate_rate_value);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_home_post_rate_rate);
            rb_rate2 = itemView.findViewById(R.id.item_home_post_rate_rate2);
            tv_text = itemView.findViewById(R.id.item_home_post_rate_text);
            btn_up = itemView.findViewById(R.id.item_home_post_rate_top_arrow);
            btn_down = itemView.findViewById(R.id.item_home_post_rate_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_home_post_rate_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_home_post_rate_three_dots);
            img_broadcast = itemView.findViewById(R.id.item_home_post_rate_broadcast);
            share_desc = itemView.findViewById(R.id.share_desc);
            shared_layout = itemView.findViewById(R.id.shared_layout);
        }
    }

    class ShareCheckInViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_shared_profile_image,img_image;
        private TextView tv_username,tv_shared_username,tv_address,tv_time,tv_shared_time,tv_name,tv_rate,tv_caption;
        private TextView tv_votes_count,share_desc;
        private SimpleRatingBar rb_rate,rb_rate2;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,img_broadcast;
        private LinearLayout shared_layout;

        public ShareCheckInViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_share_check_profile);
            img_shared_profile_image = itemView.findViewById(R.id.item_home_post_is_at_profile_image);
            img_image = itemView.findViewById(R.id.item_home_post_is_at_image);
            tv_username = itemView.findViewById(R.id.item_share_check_user_name);
            tv_shared_username = itemView.findViewById(R.id.item_home_post_is_at_user_name);
            tv_address = itemView.findViewById(R.id.item_home_post_is_at_address);
            tv_time = itemView.findViewById(R.id.item_share_check_time);
            tv_shared_time = itemView.findViewById(R.id.item_home_post_is_at_time);
            tv_name = itemView.findViewById(R.id.item_home_post_is_at_name);
            tv_rate = itemView.findViewById(R.id.item_home_post_is_at_rate_value);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_home_post_is_at_rate);
            rb_rate2 = itemView.findViewById(R.id.item_home_post_is_at_rate2);
            tv_caption = itemView.findViewById(R.id.item_home_post_is_at_desc);
            btn_up = itemView.findViewById(R.id.item_home_post_is_at_top_arrow);
            btn_down = itemView.findViewById(R.id.item_home_post_is_at_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_home_post_is_at_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            img_broadcast = itemView.findViewById(R.id.item_home_post_is_at_broadcast);
            share_desc = itemView.findViewById(R.id.share_desc);
            shared_layout = itemView.findViewById(R.id.shared_layout);
        }
    }

    class ShareReviewViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_shared_profile_image,img_image;
        private TextView tv_username,tv_shared_username,tv_title,tv_address,tv_time,tv_shared_time,tv_name,tv_rate,tv_text;
        private TextView tv_votes_count,share_desc;
        private SimpleRatingBar rb_rate,rb_rate2;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,img_broadcast;
        private LinearLayout shared_layout;


        public ShareReviewViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_share_review_profile);
            img_shared_profile_image = itemView.findViewById(R.id.item_home_post_rate_profile_image);
            img_image = itemView.findViewById(R.id.item_home_post_rate_image);
            tv_username = itemView.findViewById(R.id.item_share_review_user_name);
            tv_shared_username = itemView.findViewById(R.id.item_home_post_rate_user_name);
            tv_title = itemView.findViewById(R.id.item_home_post_rate_title);
            tv_address = itemView.findViewById(R.id.item_home_post_rate_address);
            tv_time = itemView.findViewById(R.id.item_share_review_time);
            tv_shared_time = itemView.findViewById(R.id.item_home_post_rate_time);
            tv_name = itemView.findViewById(R.id.item_home_post_rate_name);
            tv_rate = itemView.findViewById(R.id.item_home_post_rate_rate_value);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_home_post_rate_rate);
            rb_rate2 = itemView.findViewById(R.id.item_home_post_rate_rate2);
            tv_text = itemView.findViewById(R.id.item_home_post_rate_text);
            btn_up = itemView.findViewById(R.id.item_home_post_rate_top_arrow);
            btn_down = itemView.findViewById(R.id.item_home_post_rate_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_home_post_rate_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_home_post_rate_three_dots);
            img_broadcast = itemView.findViewById(R.id.item_home_post_rate_broadcast);
            share_desc = itemView.findViewById(R.id.share_desc);
            shared_layout = itemView.findViewById(R.id.shared_layout);
        }
    }

    class ShareEventViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_shared_profile_image;
        private RelativeLayout btn_add;
        private TextView tv_username,tv_shared_username,tv_address,tv_time,tv_shared_time,tv_desc,tv_location,tv_date_and_going;
        private TextView tv_votes_count,share_desc;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling;
        private SimpleDraweeView img_image;
        private RelativeLayout going_layout;
        private ImageView going_image,btn_three_dots;
        private LinearLayout shared_layout;

        public ShareEventViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_share_event_profile);
            img_shared_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            btn_add = itemView.findViewById(R.id.item_home_event_add_btn);
            tv_username = itemView.findViewById(R.id.item_share_event_user_name);
            tv_shared_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_share_event_time);
            tv_shared_time = itemView.findViewById(R.id.item_media_time);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_location = itemView.findViewById(R.id.item_home_event_location);
            tv_date_and_going = itemView.findViewById(R.id.item_home_event_date_and_going);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            img_image = itemView.findViewById(R.id.item_media_image);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            going_image = itemView.findViewById(R.id.going_image);
            going_layout = itemView.findViewById(R.id.going_layout);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            share_desc = itemView.findViewById(R.id.share_desc);
            shared_layout = itemView.findViewById(R.id.shared_layout);
        }
    }

    class ShareAdViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_shared_profile_image;
        private TextView tv_username,tv_shared_username,tv_address,tv_time,tv_shared_time,tv_sposored,tv_desc;
        private TextView tv_votes_count,share_desc;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots;
        private SimpleDraweeView img_image;
        private RelativeLayout layout;
        private LinearLayout shared_layout;

        public ShareAdViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_share_ad_profile);
            img_shared_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            tv_username = itemView.findViewById(R.id.item_share_ad_user_name);
            tv_shared_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_share_ad_time);
            tv_shared_time = itemView.findViewById(R.id.item_media_time);
            tv_sposored = itemView.findViewById(R.id.item_media_sponsored);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            img_image = itemView.findViewById(R.id.item_media_image);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            layout = itemView.findViewById(R.id.item_media_layout);
            share_desc = itemView.findViewById(R.id.share_desc);
            shared_layout = itemView.findViewById(R.id.shared_layout);

        }
    }

    class ShareOfferViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image,img_shared_profile_image;
        private TextView tv_username,tv_shared_username,tv_address,tv_time,tv_shared_time,tv_name,tv_rate,tv_percent,tv_till;
        private SimpleRatingBar rb_rate;
        private TextView tv_votes_count,share_desc;
        private ImageView btn_up,btn_down,btn_rolling,btn_three_dots,add_img;
        private RelativeLayout img_add;
        private LinearLayout shared_layout;

        public ShareOfferViewHolder(View itemView){
            super(itemView);
            img_profile_image = itemView.findViewById(R.id.item_share_offer_profile);
            img_shared_profile_image = itemView.findViewById(R.id.item_home_post_offer_profile_image);
            tv_username = itemView.findViewById(R.id.item_share_offer_user_name);
            tv_shared_username = itemView.findViewById(R.id.item_home_post_offer_user_name);
            tv_address = itemView.findViewById(R.id.item_home_post_offer_address);
            tv_shared_time = itemView.findViewById(R.id.item_share_offer_time);
            tv_time = itemView.findViewById(R.id.item_home_post_offer_time);
            tv_name = itemView.findViewById(R.id.item_home_post_offer_name);
            tv_rate = itemView.findViewById(R.id.item_home_post_offer_rate_value);
            tv_percent = itemView.findViewById(R.id.item_home_post_offer_percent);
            tv_till = itemView.findViewById(R.id.item_home_post_offer_rate_value);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_home_post_offer_rate);
            btn_up = itemView.findViewById(R.id.item_home_post_offer_top_arrow);
            btn_down = itemView.findViewById(R.id.item_home_post_offer_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_home_post_offer_rolling_circle);
            btn_three_dots = itemView.findViewById(R.id.item_media_three_dots);
            img_add = itemView.findViewById(R.id.item_home_post_offer_add);
            add_img = itemView.findViewById(R.id.item_home_post_offer_add_img);
            share_desc = itemView.findViewById(R.id.share_desc);
            shared_layout = itemView.findViewById(R.id.shared_layout);
        }
    }


    @Override
    public int getItemViewType(int position) {
        HomePostsObject o = list.get(position);

        int result_type = 0;
        int offer_type = 0;
        int event_type = 0;
        int post_type = 0;
        int review_type = 0;
        for (StartupItemObject sio : SharedPrefManager.getInstance(context).getStartUp().getHome_flags_types()){
            switch (sio.getName()){
                case "FLAG_OFFER":{
                    offer_type = Integer.valueOf(sio.getId());
                }break;
                case "FLAG_EVENT":{
                    event_type = Integer.valueOf(sio.getId());
                }break;
                case "FLAG_POST":{
                    post_type = Integer.valueOf(sio.getId());
                }break;
                case "FLAG_RATED_POST":{
                    review_type = Integer.valueOf(sio.getId());
                }break;
            }
            if (o.getMetadata().isIs_sponsored()){
                //Share Ad
                if (o.getMetadata().getIs_shared()) {
                    result_type = 15;
                }
                //Ad
                else {
                    result_type = 7;
                }
            }else {
                //Offer
                if (o.getMetadata().getObject_type() == offer_type) {
                    //Share offer
                    if (o.getMetadata().getIs_shared()) {
                        result_type = 16;
                    }
                    //Offer
                    else {
                        result_type = 8;
                    }
                }
                //Event
                else if (o.getMetadata().getObject_type() == event_type) {
                    //Share event
                    if (o.getMetadata().getIs_shared()) {
                        result_type = 14;
                    }
                    //Event
                    else {
                        result_type = 3;
                    }
                }
                //Rate
                else if (o.getMetadata().getObject_type() == review_type) {
                    //Share Review
                    if (o.getMetadata().getIs_shared()) {
                        result_type = 13;
                    }
                    //Review
                    else {
                        result_type = 5;
                    }
                }
                //Post
                else if (o.getMetadata().getObject_type() == post_type) {
                    int image_post = 0;
                    int video_post = 0;
                    int check_in_post = 0;
                    for (StartupItemObject s : SharedPrefManager.getInstance(context).getStartUp().getPosts_types()) {
                        switch (s.getName()) {
                            case "PHOTO": {
                                image_post = Integer.valueOf(s.getId());
                            }
                            break;
                            case "VIDEO": {
                                video_post = Integer.valueOf(s.getId());
                            }
                            break;
                            case "CHECK_IN": {
                                check_in_post = Integer.valueOf(s.getId());
                            }
                            break;
                        }
                    }
                    //Image
                    if (o.getPost_object().getPost_type() == image_post) {
                        //Share image
                        if (o.getMetadata().getIs_shared()) {
                            result_type = 9;
                        }
                        //Image
                        else {
                            result_type = 1;
                        }
                    }
                    //Video
                    else if (o.getPost_object().getPost_type() == video_post) {
                        //Share video
                        if (o.getMetadata().getIs_shared()) {
                            result_type = 10;
                        }
                        //Video
                        else {
                            result_type = 2;
                        }
                    } else if (o.getPost_object().getPost_type() == check_in_post) {
                        //Share check in
                        if (o.getMetadata().getIs_shared()) {
                            result_type = 12;
                        }
                        //Check in
                        else {
                            result_type = 6;
                        }
                    }
                }
            }
        }

        return result_type;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_empty, parent, false);
                return new EmptyViewHolder(itemView);
            }
            case 1: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_photo, parent, false);
                return new PhotoViewHolder(itemView);
            }
            case 2: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_video, parent, false);
                return new VideoViewHolder(itemView);
            }
            case 3: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_event, parent, false);
                return new EventViewHolder(itemView);
            }
            case 4: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_post_rate, parent, false);
                return new RateViewHolder(itemView);
            }
            case 5: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_post_rate, parent, false);
                return new ReviewViewHolder(itemView);
            }
            case 6: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_post_is_at_location, parent, false);
                return new CheckInViewHolder(itemView);
            }
            case 7: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_media_row, parent, false);
                return new AdViewHolder(itemView);
            }
            case 8: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_post_create_offer, parent, false);
                return new CreateOfferViewHolder(itemView);
            }
            case 9: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_share_image, parent, false);
                return new SharePhotoViewHolder(itemView);
            }
            case 10: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_share_video, parent, false);
                return new ShareVideoViewHolder(itemView);
            }
            case 11: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_share_rate, parent, false);
                return new ShareRateViewHolder(itemView);
            }
            case 12: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_share_check_in, parent, false);
                return new ShareCheckInViewHolder(itemView);
            }
            case 13: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_share_review, parent, false);
                return new ShareReviewViewHolder(itemView);
            }
            case 14: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_share_event, parent, false);
                return new ShareEventViewHolder(itemView);
            }
            case 15: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_share_ad, parent, false);
                return new ShareAdViewHolder(itemView);
            }
            case 16: {
                View itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_home_share_offer, parent, false);
                return new ShareOfferViewHolder(itemView);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final HomePostsObject dpo = list.get(holder.getAdapterPosition());
        Log.i("dfdfrfcxcdf", "onBindViewHolder: "+position);
        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
        switch (holder.getItemViewType()) {
            case 0 : {
                EmptyViewHolder photoViewHolder = (EmptyViewHolder) holder;
            }break;
            //Photo------------------------------------------------------------------------------
            case 1 : {
                final PhotoViewHolder photoViewHolder = (PhotoViewHolder) holder;
                //Username
                if (dpo.getPost_object().getUser_name()!=null){
                    photoViewHolder.tv_username.setText(dpo.getPost_object().getUser_name());
                }
                //Profile image
                if (dpo.getPost_object().getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(photoViewHolder.img_profile_image,dpo.getPost_object().getUser_thumb_image());
                    //BaseFunctions.setGlideImage(context,photoViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                photoViewHolder.tv_address.setText(dpo.getPost_object().getUser_city_name()!=null?dpo.getPost_object().getUser_city_name():"");
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    try {
                        photoViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                    }catch (Exception e){}
                }
                //Time
                if (dpo.getPost_object().getCreated_at()!=null){
                    photoViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                }
                //Image
                if (dpo.getPost_object().getContent()!=null){
                    BaseFunctions.setFrescoImage(photoViewHolder.img_image,dpo.getPost_object().getContent());
                    //BaseFunctions.setGlideImage(context,photoViewHolder.img_image,dpo.getReal_object().getContent().toString());
                }
                //Description
                if (dpo.getPost_object().getCaption()!=null){
                    setTags(photoViewHolder.tv_desc,dpo.getPost_object().getCaption());
                }
                //Votes count
                photoViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    photoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    photoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    photoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    photoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                    if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                        photoViewHolder.tv_comment_user.setVisibility(View.GONE);
                    }
                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        photoViewHolder.btn_down.setVisibility(View.GONE);
                        photoViewHolder.btn_up.setVisibility(View.GONE);
                    }

                photoViewHolder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            AllCommentsDialog dialog = new AllCommentsDialog(context,"post",dpo.getPost_object().getId());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                photoViewHolder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            AllReviewsDialog dialog = new AllReviewsDialog(context,"post",dpo.getPost_object().getId());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                photoViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (photoViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getPost_object().getId(),photoViewHolder.tv_comment_user.getText().toString());
                            photoViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });
                 */
                photoViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            try{
                                ViewImageDialog dialog = new ViewImageDialog(context,dpo.getPost_object().getContent());
                                dialog.show();
                            }catch (Exception e){}

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(1,position,dpo.getPost_object().getIs_business_following());
                    }
                });

                photoViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });
                photoViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
            }break;
            //Video---------------------------------------------------------------------------------
            case 2 : {
                final VideoViewHolder videoViewHolder = (VideoViewHolder) holder;
                //Username
                videoViewHolder.tv_username.setText(dpo.getPost_object().getUser_name());
                //Profile image
                BaseFunctions.setFrescoImage(videoViewHolder.img_profile_image,dpo.getPost_object().getUser_thumb_image());
                //BaseFunctions.setGlideImage(context,videoViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                //Address
                videoViewHolder.tv_address.setText(dpo.getPost_object().getUser_city_name()!=null?dpo.getPost_object().getUser_city_name():"");
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    videoViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                videoViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                //Description
                setTags(videoViewHolder.tv_desc,dpo.getPost_object().getCaption());
                //Votes Count
                videoViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    videoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    videoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    videoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    videoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                /*
                try {
                    MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();

                    mediaMetadataRetriever .setDataSource(dpo.getPost_object().getContent(), new HashMap<String, String>());
                    Bitmap bmFrame = mediaMetadataRetriever.getFrameAtTime(1000); //unit in microsecond
                    videoViewHolder.thumbnail.setImageBitmap(bmFrame);
                }catch (Exception e){}


                 */
                    if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                        videoViewHolder.tv_comment_user.setVisibility(View.GONE);
                    }
                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        videoViewHolder.btn_down.setVisibility(View.GONE);
                        videoViewHolder.btn_up.setVisibility(View.GONE);
                    }

                videoViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,2,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(holder,2,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });

                videoViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(videoViewHolder,2,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(videoViewHolder,2,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
                videoViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                videoViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (videoViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getPost_object().getId(),videoViewHolder.tv_comment_user.getText().toString());
                            videoViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });
                 */
                videoViewHolder.play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(context,dpo.getPost_object().getContent());
                        dialog.show();
                    }
                });
                videoViewHolder.thumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(context,dpo.getPost_object().getContent());
                        dialog.show();
                    }
                });
                videoViewHolder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AllCommentsDialog dialog = new AllCommentsDialog(context,"post",dpo.getPost_object().getId());
                        dialog.show();
                    }
                });

                videoViewHolder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AllReviewsDialog dialog = new AllReviewsDialog(context,"post",dpo.getPost_object().getId());
                        dialog.show();
                    }
                });
                videoViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                videoViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (videoViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getPost_object().getId(),videoViewHolder.tv_comment_user.getText().toString());
                            videoViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });

                 */

                videoViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        context.startActivity(intent);
                    }
                });
                videoViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        context.startActivity(intent);
                    }
                });
                videoViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(2,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                videoViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Event-------------------------------------------------------------------------------------
            case 3: {
                final EventViewHolder eventViewHolder = (EventViewHolder) holder;
                //Username
                if (dpo.getPost_object().getBusiness_name()!=null){
                    eventViewHolder.tv_username.setText(dpo.getPost_object().getBusiness_name());
                }
                //Profile image
                if (dpo.getPost_object().getBusiness_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(eventViewHolder.img_profile_image,dpo.getPost_object().getBusiness_thumb_image());
                    //BaseFunctions.setGlideImage(context,eventViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                eventViewHolder.tv_location.setText(dpo.getPost_object().getBusiness_city_name());

                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    eventViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                if (dpo.getPost_object().getCreated_at()!=null){
                    eventViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                }
                //Going
                if (dpo.getPost_object().getBusiness_id()!=SharedPrefManager.getInstance(context).getUser().getId()){
                    if (dpo.getPost_object().getIs_going() == 0){
                        eventViewHolder.going_layout.setBackgroundResource(R.drawable.primary_circle);
                        eventViewHolder.going_image.setImageResource(R.drawable.ic_plus);
                    }else if (dpo.getPost_object().getIs_going() == 1){
                        eventViewHolder.going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                        eventViewHolder.going_image.setImageResource(R.drawable.ic_minus);
                    }else {
                        eventViewHolder.going_layout.setVisibility(View.GONE);
                    }
                }else {
                    eventViewHolder.going_layout.setVisibility(View.GONE);
                }
                //Image
                if (dpo.getPost_object().getImage()!=null){
                    try {
                        BaseFunctions.setFrescoImage(eventViewHolder.img_image,dpo.getPost_object().getImage());
                    }catch (Exception e){}
                    //BaseFunctions.setGlideImage(context,eventViewHolder.img_image,dpo.getReal_object().getImage());
                }
                //Event title
                if (lan.equals("en")){
                    eventViewHolder.tv_desc.setText(dpo.getPost_object().getEn_title()!=null?dpo.getPost_object().getEn_title():"");
                }else if (lan.equals("ar")){
                    eventViewHolder.tv_desc.setText(dpo.getPost_object().getAr_title()!=null?dpo.getPost_object().getAr_title():dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }
                //Event location
                eventViewHolder.tv_location.setText(dpo.getPost_object().getBusiness_location_address()!=null?dpo.getPost_object().getBusiness_location_address():"");
                //Event date and going count
                try {
                    eventViewHolder.tv_date_and_going.setText(BaseFunctions.dateExtractor(dpo.getPost_object().getEvent_date())+"-"+dpo.getPost_object().getGoing_count()+" "+
                            context.getResources().getString(R.string.create_event_going));
                }catch (Exception e){}
                //Votes count
                eventViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //Voted Value
                if (dpo.getPost_object().getUser_vote()==1){
                    eventViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    eventViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    eventViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    eventViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                    if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                        eventViewHolder.tv_comment_user.setVisibility(View.GONE);
                    }
                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        eventViewHolder.btn_down.setVisibility(View.GONE);
                        eventViewHolder.btn_up.setVisibility(View.GONE);
                    }

                eventViewHolder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            AllCommentsDialog dialog = new AllCommentsDialog(context,"event",dpo.getPost_object().getId());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            AllReviewsDialog dialog = new AllReviewsDialog(context,"event",dpo.getPost_object().getId());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.going_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getIs_going() == 0){
                            callJoinAPI(eventViewHolder,"event",position,dpo.getPost_object().getId(),dpo.getPost_object().getIs_going());
                        }
                    }
                });
                eventViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(eventViewHolder,3,position,"event",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(eventViewHolder,3,position,"event",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });

                eventViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,3,position,"event",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(holder,3,position,"event",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
                eventViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"event",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","event");
                        context.startActivity(intent);
                    }
                });
                /*
                eventViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (eventViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("event",dpo.getPost_object().getId(),eventViewHolder.tv_comment_user.getText().toString());
                            eventViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });
                 */
                eventViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            ViewImageDialog dialog = new ViewImageDialog(context, dpo.getPost_object().getImage());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"event",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Rate---------------------------------------------------------------------------------
            //todo Not implemented yet
            case 4: {
                RateViewHolder rateViewHolder = (RateViewHolder) holder;
                /*
                //Username
                rateViewHolder.tv_username.setText(dpo.getReal_object().getUser_name());
                //User Profile Image
                BaseFunctions.setFrescoImage(rateViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                //BaseFunctions.setGlideImage(context,rateViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                //Title
                //todo Rate value is missing
                //rateViewHolder.tv_title.setText(context.getResources().getString(R.string.home_rate)+
                //        " "+String.valueOf(dpo.getRateValue)+" "+context.getResources().getString(R.string.home_out_of)+" 5");
                //Address
                //todo Address is missing
                //rateViewHolder.tv_address.setText(dpo.getAddress());
                //Rating
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    rateViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                rateViewHolder.tv_time.setText(BaseFunctions.processDateTime(context,dpo.getReal_object().getCreated_at()));
                //Facility Image
                //todo Facility image is missing
                //BaseFunctions.setGlideImage(context,rateViewHolder.img_image,dpo.getFacility_image());
                //Facility name
                //todo Facility name is missing
                //rateViewHolder.tv_name.setText(dpo.getFacilityName());
                //Facility rate
                //todo Facility Rate is missing
                //rateViewHolder.rb_rate2.setRating(dpo.getFacilityRate());
                //Facility Rate text
                //todo Facility Rate is missing
                //rateViewHolder.tv_rate.setText(String.valueOf(dpo.getFacilityRate())+" "+
                //        context.getResources().getString(R.string.home_out_of)+" 5");

                //Text
                //todo Text is missing
                //rateViewHolder.tv_text.setText(dpo.getText());
                //Votes Count
                if (dpo.getReal_object().getVotes_count()!=null) {
                    rateViewHolder.tv_votes_count.setText(String.valueOf(dpo.getReal_object().getVotes_count()));
                }else {
                    rateViewHolder.tv_votes_count.setText("0");
                }
                rateViewHolder.img_broadcast.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //todo missing the id and the following status
                        callFollowAPI("0","user","yes",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false));
                    }
                });
                rateViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //todo Facility id is missing
                        /*
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getFacility_id());
                        context.startActivity(intent);
                        **/
                //    }
           //     });

        /*
                rateViewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //todo Facility id is missing
                        /*
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getFacility_id());
                        context.startActivity(intent);
                        **/
             //       }
             //   });
/*
                rateViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getReal_object().getUser_id());
                        context.startActivity(intent);
                    }
                });
                rateViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getReal_object().getUser_id());
                        context.startActivity(intent);
                    }
                });
                //todo respond to the post type and in home
                rateViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(4,position,dpo.getReal_object().getIs_followed());
                    }
                });
                rateViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post");
                                }
                            });
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                **/
            }break;
            //Review--------------------------------------------------------------------------------
            case 5: {
                ReviewViewHolder reviewViewHolder = (ReviewViewHolder) holder;
                //Username
                reviewViewHolder.tv_username.setText(dpo.getPost_object().getUser_name());

                //User Profile Image
                BaseFunctions.setFrescoImage(reviewViewHolder.img_profile_image,dpo.getPost_object().getUser_thumb_image());

                //Title
                reviewViewHolder.tv_title.setText(context.getResources().getString(R.string.home_review)+
                        " "+String.valueOf(dpo.getPost_object().getUser_business_rating())+" "+context.getResources().getString(R.string.home_out_of)+" 5");

                //Address
                reviewViewHolder.tv_address.setText(dpo.getPost_object().getUser_city_name()!=null?
                        dpo.getPost_object().getUser_city_name():"");

                //Rating
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    reviewViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }

                //Time
                reviewViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));

                //Facility Image
                BaseFunctions.setFrescoImage(reviewViewHolder.img_image,dpo.getPost_object().getBusiness_thumb_image());

                //Facility name
                reviewViewHolder.tv_name.setText(dpo.getPost_object().getBusiness_name());

                //Facility rate
                reviewViewHolder.rb_rate2.setRating(dpo.getPost_object().getUser_business_rating());

                //Facility Rate text
                reviewViewHolder.tv_rate.setText(String.valueOf(dpo.getPost_object().getUser_business_rating())+" "+
                        context.getResources().getString(R.string.home_out_of)+" 5");

                //Review text
                reviewViewHolder.tv_text.setText(dpo.getPost_object().getUser_business_review_text());

                //Votes count
                reviewViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");

                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    reviewViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    reviewViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    reviewViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    reviewViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                    reviewViewHolder.btn_down.setVisibility(View.GONE);
                    reviewViewHolder.btn_up.setVisibility(View.GONE);
                }
                //todo just for now because ahmed didn't supported it yet
                reviewViewHolder.btn_down.setVisibility(View.GONE);
                reviewViewHolder.btn_up.setVisibility(View.GONE);
                reviewViewHolder.btn_rolling.setVisibility(View.GONE);
                reviewViewHolder.tv_votes_count.setVisibility(View.GONE);
                reviewViewHolder.img_broadcast.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callFollowAPI(String.valueOf(dpo.getPost_object().getBusinesses_id()),"user",
                                dpo.getPost_object().getIs_business_following()==0?"yes":"no",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false),position);
                    }
                });

                reviewViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusinesses_id());
                        context.startActivity(intent);

                   }
                });

                reviewViewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusinesses_id());
                        context.startActivity(intent);
                    }
                });

                reviewViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        context.startActivity(intent);
                    }
                });
                reviewViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","profile");
                        intent.putExtra("profile_type","customer");
                        intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                        context.startActivity(intent);
                    }
                });

                reviewViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(5,position,dpo.getPost_object().getIs_following());
                    }
                });

                reviewViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getReview_id()),"post",name);
                                }
                            });
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });


                reviewViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,5,position,"post",String.valueOf(dpo.getPost_object().getReview_id()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(holder,5,position,"post",String.valueOf(dpo.getPost_object().getReview_id()),
                                    "0","1");
                        }
                    }
                });
                reviewViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,5,position,"post",String.valueOf(dpo.getPost_object().getReview_id()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(holder,5,position,"post",String.valueOf(dpo.getPost_object().getReview_id()),
                                    "0","-1");
                        }
                    }
                });


            }break;
            //CheckIn------------------------------------------------------------------------------------
            case 6: {

                final CheckInViewHolder checkInViewHolder = (CheckInViewHolder) holder;

                //Username & Facility name
                checkInViewHolder.tv_username.setText(Html.fromHtml("<b>"+dpo.getPost_object().getUser_name()+"</b>"+" "+
                context.getResources().getString(R.string.home_is_at)+" "+"<b>"+dpo.getPost_object().getBusiness_name()+"</b>"));
                //Profile image
                if (dpo.getPost_object().getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(checkInViewHolder.img_profile_image,dpo.getPost_object().getUser_thumb_image());
                    //BaseFunctions.setGlideImage(context,checkInViewHolder.img_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                checkInViewHolder.tv_address.setText(dpo.getPost_object().getUser_city_name());
                //Rating
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    checkInViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                if (dpo.getPost_object().getCreated_at()!=null){
                    checkInViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                }
                //Facility icon
                BaseFunctions.setFrescoImage(checkInViewHolder.img_image,dpo.getPost_object().getBusiness_thumb_image());
                //Facility Name
                checkInViewHolder.tv_name.setText(dpo.getPost_object().getBusiness_name());
                //Facility Rate
                checkInViewHolder.rb_rate2.setRating(Float.valueOf(dpo.getPost_object().getCheck_in_business_avg_review()));
                //Caption
                checkInViewHolder.tv_caption.setText(dpo.getPost_object().getCaption());
                //Votes count
                checkInViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    checkInViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    checkInViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    checkInViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    checkInViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                    if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                        checkInViewHolder.tv_comment_user.setVisibility(View.GONE);
                    }
                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        checkInViewHolder.btn_down.setVisibility(View.GONE);
                        checkInViewHolder.btn_up.setVisibility(View.GONE);
                    }

                checkInViewHolder.img_broadcast.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callFollowAPI(String.valueOf(dpo.getPost_object().getBusiness_id()),"user",
                                dpo.getPost_object().getIs_following() == 0?"yes":"no",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false),position);
                    }
                });

                checkInViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(checkInViewHolder,6,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(checkInViewHolder,6,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });

                checkInViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(checkInViewHolder,6,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(checkInViewHolder,6,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });

                checkInViewHolder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AllCommentsDialog dialog = new AllCommentsDialog(context,"post",dpo.getPost_object().getId());
                        dialog.show();
                    }
                });

                checkInViewHolder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AllReviewsDialog dialog = new AllReviewsDialog(context,"post",dpo.getPost_object().getId());
                        dialog.show();
                    }
                });
                checkInViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                checkInViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (checkInViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getPost_object().getId(),checkInViewHolder.tv_comment_user.getText().toString());
                            checkInViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });

                 */

                checkInViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                checkInViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                checkInViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                checkInViewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                checkInViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(6,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                checkInViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }break;
            //Ad--------------------------------------------------------------------------------
            case 7: {
                final AdViewHolder adViewHolder = (AdViewHolder) holder;
                //Layout
                adViewHolder.layout.setBackgroundColor(context.getResources().getColor(R.color.light_grey));
                //Facility name
                adViewHolder.tv_username.setText(dpo.getPost_object().getBusiness_name());
                //Facility image
                BaseFunctions.setGlideImage(context, adViewHolder.img_profile_image, dpo.getPost_object().getBusiness_thumb_image());
                //Address
                adViewHolder.tv_address.setText(dpo.getPost_object().getBusiness_city_name()!=null?
                        dpo.getPost_object().getBusiness_city_name():"");
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    adViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                adViewHolder.tv_time.setText(BaseFunctions.processDate(context, dpo.getPost_object().getCreated_at()));
                //Sponsored
                adViewHolder.tv_sposored.setVisibility(View.VISIBLE);
                //Image
                try {
                    BaseFunctions.setFrescoImage(adViewHolder.img_image,dpo.getPost_object().getContent());
                }catch (Exception e){}

                //Sponsored
                adViewHolder.tv_sposored.setVisibility(View.VISIBLE);
                //Description
                setTags(adViewHolder.tv_desc, dpo.getPost_object().getCaption());
                //Votes count
                adViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    adViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    adViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    adViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    adViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                    if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                        adViewHolder.tv_comment_user.setVisibility(View.GONE);
                    }
                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        adViewHolder.btn_down.setVisibility(View.GONE);
                        adViewHolder.btn_up.setVisibility(View.GONE);
                    }

                adViewHolder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            AllCommentsDialog dialog = new AllCommentsDialog(context,"post",dpo.getPost_object().getId());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                adViewHolder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            AllReviewsDialog dialog = new AllReviewsDialog(context,"post",dpo.getPost_object().getId());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                adViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"post",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","post");
                        context.startActivity(intent);
                    }
                });
                /*
                adViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (adViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("post",dpo.getPost_object().getId(),adViewHolder.tv_comment_user.getText().toString());
                            adViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });
                 */
                adViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            try{
                                ViewImageDialog dialog = new ViewImageDialog(context,dpo.getPost_object().getContent());
                                dialog.show();
                            }catch (Exception e){}

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                adViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                adViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                adViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(7,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                adViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                adViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });
                adViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(holder,1,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
            }break;
            //Offer--------------------------------------------------------------------------------
            case 8: {
                final CreateOfferViewHolder createOfferViewHolder = (CreateOfferViewHolder) holder;

                //Title
                createOfferViewHolder.tv_username.setText(Html.fromHtml("<b>"+dpo.getPost_object().getBusiness_name()+"</b>"+" "+
                context.getResources().getString(R.string.home_created_offer)));
                //Facility icon
                BaseFunctions.setFrescoImage(createOfferViewHolder.img_profile_image,dpo.getPost_object().getBusiness_thumb_image());
                //Address
                createOfferViewHolder.tv_address.setText(dpo.getPost_object().getBusiness_city_name());
                //Rating
                createOfferViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                //Time
                createOfferViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                //Percent
                createOfferViewHolder.tv_percent.setText(dpo.getPost_object().getDiscount()+"%");
                //Product name
                if (lan.equals("en")) {
                    createOfferViewHolder.tv_name.setText(dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }else if (lan.equals("ar")){
                    createOfferViewHolder.tv_name.setText(dpo.getPost_object().getAr_title()!=null?
                            dpo.getPost_object().getAr_title():dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }
                //End date
                createOfferViewHolder.tv_till.setText(context.getResources().getString(R.string.home_valid_till)+" "+
                        BaseFunctions.dateExtractorOld(dpo.getPost_object().getExpiry_date()));
                //Votes Count
                createOfferViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    createOfferViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    createOfferViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    createOfferViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    createOfferViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                    if (dpo.getPost_object().getPeople_can_comment_on_posts()==0){
                        createOfferViewHolder.tv_comment_user.setVisibility(View.GONE);
                    }
                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        createOfferViewHolder.btn_down.setVisibility(View.GONE);
                        createOfferViewHolder.btn_up.setVisibility(View.GONE);
                    }

                    if (dpo.getPost_object().getIs_following() == 0){
                        createOfferViewHolder.img_add.setBackgroundResource(R.drawable.primary_circle);
                        createOfferViewHolder.add_img.setImageResource(R.drawable.ic_plus);
                    }else if (dpo.getPost_object().getIs_following() == 1){
                        createOfferViewHolder.img_add.setBackgroundResource(R.drawable.dark_grey_circle);
                        createOfferViewHolder.add_img.setImageResource(R.drawable.ic_minus);
                    }
                createOfferViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);

                    }
                });
                createOfferViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                createOfferViewHolder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AllCommentsDialog dialog = new AllCommentsDialog(context,"offer",dpo.getPost_object().getId());
                        dialog.show();
                    }
                });
                createOfferViewHolder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AllReviewsDialog dialog = new AllReviewsDialog(context,"offer",dpo.getPost_object().getId());
                        dialog.show();
                    }
                });
                createOfferViewHolder.tv_comment_user.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //AddCommentDialog dialog = new AddCommentDialog(context,"offer",dpo.getPost_object().getId());
                        //dialog.show();
                        Intent intent = new Intent(context, AddCommentActivity.class);
                        intent.putExtra("id",dpo.getPost_object().getId());
                        intent.putExtra("type","offer");
                        context.startActivity(intent);
                    }
                });
                /*
                createOfferViewHolder.tv_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            if (createOfferViewHolder.tv_comment_user.getText().toString().equals("")){
                                Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                                return false;
                            }
                            callAddCommentAPI("offer",dpo.getPost_object().getId(),createOfferViewHolder.tv_comment_user.getText().toString());
                            createOfferViewHolder.tv_comment_user.setText("");
                            return true;
                        }
                        return false;
                    }
                });
                 */
                createOfferViewHolder.img_add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getIs_following() == 0){
                            createOfferViewHolder.img_add.setBackgroundResource(R.drawable.dark_grey_circle);
                            createOfferViewHolder.add_img.setImageResource(R.drawable.ic_minus);
                        }else {
                            createOfferViewHolder.img_add.setBackgroundResource(R.drawable.primary_circle);
                            createOfferViewHolder.add_img.setImageResource(R.drawable.ic_plus);
                        }
                        callFollowAPI(String.valueOf(dpo.getPost_object().getId()),"offer",
                                dpo.getPost_object().getIs_following() == 0?"yes":"no",
                                context.getResources().getString(R.string.follow_offer_true),
                                context.getResources().getString(R.string.follow_offer_false),position);
                    }
                });
                createOfferViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(8,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                createOfferViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"offer",name);
                                }
                            });
                            dialog.show();
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                createOfferViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(createOfferViewHolder,8,position,"offer",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(createOfferViewHolder,8,position,"offer",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });

                createOfferViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(createOfferViewHolder,8,position,"offer",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(createOfferViewHolder,8,position,"offer",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
            }break;
            //Share Image----------------------------------------------------------------------------
            case 9 : {
                final SharePhotoViewHolder photoViewHolder = (SharePhotoViewHolder) holder;
                //Username
                if (dpo.getMetadata().getSharer().getName()!=null){
                    photoViewHolder.tv_username.setText(Html.fromHtml("<b>"+dpo.getMetadata().getSharer().getName()+"</b>"+
                            " "+context.getResources().getString(R.string.home_shared_post)));
                }
                //Shared user name
                if (dpo.getPost_object().getUser_name()!=null){
                    photoViewHolder.tv_shared_username.setText(dpo.getPost_object().getUser_name());
                }
                //Profile image
                if (dpo.getMetadata().getSharer().getImage()!=null){
                    BaseFunctions.setFrescoImage(photoViewHolder.img_profile_image,dpo.getMetadata().getSharer().getImage());
                    //BaseFunctions.setGlideImage(context,photoViewHolder.img_profile_image,dpo.getSharer_profile_image());
                }
                //Shared Profile image
                if (dpo.getPost_object().getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(photoViewHolder.img_shared_profile_image,dpo.getPost_object().getUser_thumb_image());
                    //BaseFunctions.setGlideImage(context,photoViewHolder.img_shared_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                photoViewHolder.tv_address.setText(dpo.getPost_object().getUser_city_name());
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    photoViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                if (dpo.getMetadata().getShare_time()!=null&&!dpo.getMetadata().getShare_time().equals("")) {
                    photoViewHolder.tv_time.setText(BaseFunctions.processDate(context, dpo.getMetadata().getShare_time()));
                }
                //Shared time
                if (dpo.getPost_object().getCreated_at()!=null){
                    photoViewHolder.tv_shared_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                }
                //Image
                if (dpo.getPost_object().getContent()!=null){
                    BaseFunctions.setFrescoImage(photoViewHolder.img_image,dpo.getPost_object().getContent());
                    //BaseFunctions.setGlideImage(context,photoViewHolder.img_image,dpo.getReal_object().getContent().toString());
                }
                //Share desc
                photoViewHolder.share_desc.setText(dpo.getMetadata().getSharing_text()!=null?dpo.getMetadata().getSharing_text():"");
                //Votes count
                photoViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //Voted Value
                if (dpo.getPost_object().getUser_vote()==1){
                    photoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    photoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    photoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    photoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        photoViewHolder.btn_down.setVisibility(View.GONE);
                        photoViewHolder.btn_up.setVisibility(View.GONE);
                    }

                photoViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            ViewImageDialog dialog = new ViewImageDialog(context, dpo.getPost_object().getContent());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.img_shared_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.tv_shared_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(9,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                photoViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                photoViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(photoViewHolder,9,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(photoViewHolder,9,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });

                photoViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(photoViewHolder,9,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(photoViewHolder,9,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
                photoViewHolder.shared_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewOriginalPostActivity.class);
                        intent.putExtra("post",new Gson().toJson(dpo));
                        intent.putExtra("type",1);
                        context.startActivity(intent);
                    }
                });
            }break;
            //Shared Video---------------------------------------------------------------------------------
            case 10 : {
                final ShareVideoViewHolder videoViewHolder = (ShareVideoViewHolder) holder;
                //Username
                if (dpo.getMetadata().getSharer().getName()!=null) {
                    videoViewHolder.tv_username.setText(Html.fromHtml("<b>" + dpo.getMetadata().getSharer().getName() + "</b>" +
                            " " + context.getResources().getString(R.string.home_shared_post)));
                }
                //Shared Username
                videoViewHolder.tv_shared_username.setText(dpo.getPost_object().getUser_name());
                //Profile image
                if (dpo.getMetadata().getSharer().getImage()!=null) {
                    BaseFunctions.setFrescoImage(videoViewHolder.img_profile_image,dpo.getMetadata().getSharer().getImage());
                    //BaseFunctions.setGlideImage(context, videoViewHolder.img_profile_image, dpo.getSharer_profile_image());
                }
                //Shared profile image
                BaseFunctions.setFrescoImage(videoViewHolder.img_shared_profile_image,dpo.getPost_object().getUser_thumb_image());
                //BaseFunctions.setGlideImage(context,videoViewHolder.img_shared_profile_image,dpo.getReal_object().getUser_profile_image());
                //Address
                videoViewHolder.tv_address.setText(dpo.getPost_object().getUser_city_name());
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    videoViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                if (dpo.getMetadata().getShare_time()!=null&&!dpo.getMetadata().getShare_time().equals("")) {
                    videoViewHolder.tv_time.setText(BaseFunctions.processDate(context, dpo.getMetadata().getShare_time()));
                }
                //Shared time
                videoViewHolder.tv_shared_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));

                /*
                MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
                mediaMetadataRetriever .setDataSource(dpo.getPost_object().getContent(), new HashMap<String, String>());
                Bitmap bmFrame = mediaMetadataRetriever.getFrameAtTime(1000); //unit in microsecond
                videoViewHolder.thumbnail.setImageBitmap(bmFrame);
                */
                videoViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //Share desc
                videoViewHolder.share_desc.setText(dpo.getMetadata().getSharing_text()!=null?dpo.getMetadata().getSharing_text():"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    videoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    videoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    videoViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    videoViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        videoViewHolder.btn_down.setVisibility(View.GONE);
                        videoViewHolder.btn_up.setVisibility(View.GONE);
                    }

                videoViewHolder.play.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(context,dpo.getPost_object().getContent());
                        dialog.show();
                    }
                });
                videoViewHolder.thumbnail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ViewHomeVideoDialog dialog = new ViewHomeVideoDialog(context,dpo.getPost_object().getContent());
                        dialog.show();
                    }
                });
                videoViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                videoViewHolder.img_shared_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                videoViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                videoViewHolder.tv_shared_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                videoViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(10,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                videoViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                videoViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,10,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(holder,10,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });

                videoViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(videoViewHolder,10,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(videoViewHolder,10,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
                videoViewHolder.shared_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewOriginalPostActivity.class);
                        intent.putExtra("post",new Gson().toJson(dpo));
                        intent.putExtra("type",2);
                        context.startActivity(intent);
                    }
                });
            }break;
            //Share Rate---------------------------------------------------------------------------------
            //todo not implemented yet
            case 11: {
                ShareRateViewHolder rateViewHolder = (ShareRateViewHolder) holder;
                /*
                //Username
                if (dpo.getSharer_name()!=null) {
                    rateViewHolder.tv_username.setText(Html.fromHtml("<b>" + dpo.getSharer_name() + "</b>" +
                            " " + context.getResources().getString(R.string.home_shared_post)));
                }
                //Shared username
                rateViewHolder.tv_shared_username.setText(dpo.getReal_object().getUser_name());
                //User Profile Image
                if (dpo.getSharer_profile_image()!=null){
                    BaseFunctions.setFrescoImage(rateViewHolder.img_profile_image,dpo.getSharer_name());
                    //BaseFunctions.setGlideImage(context,rateViewHolder.img_profile_image,dpo.getSharer_name());
                }
                //Shared user profile image
                BaseFunctions.setFrescoImage(rateViewHolder.img_shared_profile_image,dpo.getReal_object().getUser_profile_image());
                //BaseFunctions.setGlideImage(context,rateViewHolder.img_shared_profile_image,dpo.getReal_object().getUser_profile_image());
                //Title
                //todo Rate value is missing
                //rateViewHolder.tv_title.setText(context.getResources().getString(R.string.home_rate)+
                //        " "+String.valueOf(dpo.getRateValue)+" "+context.getResources().getString(R.string.home_out_of)+" 5");
                //Address
                //todo Address is missing
                //rateViewHolder.tv_address.setText(dpo.getAddress());
                //Rating
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    rateViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                rateViewHolder.tv_time.setText(BaseFunctions.processDateTime(context,dpo.getReal_object().getCreated_at()));
                //Shared time
                //todo shared time is missing
                //rateViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getSahred_created_at()));
                //Facility Image
                //todo Facility image is missing
                //BaseFunctions.setGlideImage(context,rateViewHolder.img_image,dpo.getFacility_image());
                //Facility name
                //todo Facility name is missing
                //rateViewHolder.tv_name.setText(dpo.getFacilityName());
                //Facility rate
                //todo Facility Rate is missing
                //rateViewHolder.rb_rate2.setRating(dpo.getFacilityRate());
                //Facility Rate text
                //todo Facility Rate is missing
                //rateViewHolder.tv_rate.setText(String.valueOf(dpo.getFacilityRate())+" "+
                //        context.getResources().getString(R.string.home_out_of)+" 5");

                //Text
                //todo Text is missing
                //rateViewHolder.tv_text.setText(dpo.getText());
                //Votes Count
                if (dpo.getReal_object().getVotes_count()!=null){
                    rateViewHolder.tv_votes_count.setText(String.valueOf(dpo.getReal_object().getVotes_count()));
                }else {
                    rateViewHolder.tv_votes_count.setText("0");
                }
                rateViewHolder.img_broadcast.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //todo missing the id and the following status
                        callFollowAPI("0","user","yes",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false));
                    }
                });
                rateViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //todo Facility id is missing
                        /*
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getFacility_id());
                        context.startActivity(intent);
                        **/
                /*
                    }
                });
                rateViewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //todo Facility id is missing
                        /*
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getFacility_id());
                        context.startActivity(intent);
                        **/
                /*
                    }
                });
                rateViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            if (dpo.getSharer_id()!=null&&!dpo.getSharer_id().equals("")) {
                                Intent intent = new Intent(context, HomeActivity.class);
                                intent.putExtra("type", "profile");
                                intent.putExtra("profile_type", "customer");
                                intent.putExtra("profile_id", Integer.valueOf(dpo.getSharer_id()));
                                context.startActivity(intent);
                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                rateViewHolder.img_shared_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getReal_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                rateViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            if (dpo.getSharer_id()!=null&&!dpo.getSharer_id().equals("")) {
                                Intent intent = new Intent(context, HomeActivity.class);
                                intent.putExtra("type", "profile");
                                intent.putExtra("profile_type", "customer");
                                intent.putExtra("profile_id", Integer.valueOf(dpo.getSharer_id()));
                                context.startActivity(intent);
                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                rateViewHolder.tv_shared_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","profile");
                            intent.putExtra("profile_type","customer");
                            intent.putExtra("profile_id",dpo.getReal_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                //todo respond to the post type and in home
                rateViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(11,position,dpo.getReal_object().getIs_followed());
                    }
                });
                rateViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post");
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                 */
            }break;
            //Shared CheckIn------------------------------------------------------------------------------------
            case 12: {
                final  ShareCheckInViewHolder checkInViewHolder = (ShareCheckInViewHolder) holder;
                //User name
                if (dpo.getMetadata().getSharer().getName()!=null) {
                    checkInViewHolder.tv_username.setText(Html.fromHtml("<b>" + dpo.getMetadata().getSharer().getName() + "</b>" +
                            " " + context.getResources().getString(R.string.home_shared_post)));
                }
                //Shared Username & Facility name
                checkInViewHolder.tv_shared_username.setText(Html.fromHtml("<b>"+dpo.getPost_object().getUser_name()+"</b>"+" "+
                        context.getResources().getString(R.string.home_is_at)+" "+"<b>"+dpo.getPost_object().getBusiness_name()+"</b>"));
                //Profile image
                if (dpo.getMetadata().getSharer().getImage()!=null) {
                    BaseFunctions.setFrescoImage(checkInViewHolder.img_profile_image,dpo.getMetadata().getSharer().getImage());
                    //BaseFunctions.setGlideImage(context,checkInViewHolder.img_profile_image,dpo.getSharer_profile_image());
                }
                //Shared profile image
                if (dpo.getPost_object().getUser_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(checkInViewHolder.img_shared_profile_image,dpo.getPost_object().getUser_thumb_image());
                    //BaseFunctions.setGlideImage(context,checkInViewHolder.img_shared_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                checkInViewHolder.tv_address.setText(dpo.getPost_object().getUser_city_name());
                //Rating
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    checkInViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                if (dpo.getMetadata().getShare_time()!=null&&!dpo.getMetadata().getShare_time().equals("")) {
                    checkInViewHolder.tv_time.setText(BaseFunctions.processDate(context,
                            dpo.getMetadata().getShare_time()));
                }
                //Shared time
                checkInViewHolder.tv_shared_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                //Facility icon
                BaseFunctions.setFrescoImage(checkInViewHolder.img_image,dpo.getPost_object().getBusiness_thumb_image());
                //Facility Name
                checkInViewHolder.tv_name.setText(dpo.getPost_object().getBusiness_name());
                //Facility Rate
                checkInViewHolder.rb_rate2.setRating(Float.valueOf(dpo.getPost_object().getCheck_in_business_avg_review()));
                //Caption
                checkInViewHolder.tv_caption.setText(dpo.getPost_object().getCaption());
                //Share desc
                checkInViewHolder.share_desc.setText(dpo.getMetadata().getSharing_text()!=null?dpo.getMetadata().getSharing_text():"");
                //Votes count
                checkInViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    checkInViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    checkInViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    checkInViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    checkInViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        checkInViewHolder.btn_down.setVisibility(View.GONE);
                        checkInViewHolder.btn_up.setVisibility(View.GONE);
                    }

                checkInViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(checkInViewHolder,12,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(checkInViewHolder,12,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });

                checkInViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(checkInViewHolder,12,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(checkInViewHolder,12,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
                checkInViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                checkInViewHolder.img_shared_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                checkInViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                checkInViewHolder.tv_shared_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                checkInViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                checkInViewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                        context.startActivity(intent);
                    }
                });
                checkInViewHolder.img_broadcast.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callFollowAPI(String.valueOf(dpo.getPost_object().getBusiness_id()),"user",
                                dpo.getPost_object().getIs_following() == 0?"yes":"no",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false),position);
                    }
                });
                checkInViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(12,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                checkInViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                checkInViewHolder.shared_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewOriginalPostActivity.class);
                        intent.putExtra("post",new Gson().toJson(dpo));
                        intent.putExtra("type",6);
                        context.startActivity(intent);
                    }
                });
            }break;
            //Share Review--------------------------------------------------------------------------------
            case 13: {
                ShareReviewViewHolder reviewViewHolder = (ShareReviewViewHolder) holder;

                //Username
                if (dpo.getMetadata().getSharer().getName()!=null) {
                    reviewViewHolder.tv_username.setText(Html.fromHtml("<b>" + dpo.getMetadata().getSharer().getName() + "</b>" +
                            " " + context.getResources().getString(R.string.home_shared_post)));
                }
                //Shared username
                reviewViewHolder.tv_shared_username.setText(dpo.getPost_object().getUser_name());
                //User Profile Image
                if (dpo.getMetadata().getSharer().getImage()!=null) {
                    BaseFunctions.setFrescoImage(reviewViewHolder.img_profile_image,dpo.getMetadata().getSharer().getImage());
                }
                //Shared user profile image
                BaseFunctions.setFrescoImage(reviewViewHolder.img_shared_profile_image,dpo.getPost_object().getUser_thumb_image());
                //Title
                reviewViewHolder.tv_title.setText(context.getResources().getString(R.string.home_review)+
                        " "+String.valueOf(dpo.getPost_object().getUser_business_rating())+" "+context.getResources().getString(R.string.home_out_of)+" 5");
                //Address
                reviewViewHolder.tv_address.setText(dpo.getPost_object().getUser_city_name()!=null?
                        dpo.getPost_object().getUser_city_name():"");
                //Rating
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    reviewViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                reviewViewHolder.tv_time.setText(BaseFunctions.processDate(context,dpo.getMetadata().getShare_time()));
                //Shared time
                reviewViewHolder.tv_shared_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                //Facility Image
                BaseFunctions.setGlideImage(context,reviewViewHolder.img_image,dpo.getPost_object().getBusiness_thumb_image());
                //Facility name
                reviewViewHolder.tv_name.setText(dpo.getPost_object().getBusiness_name());
                //Facility rate
                reviewViewHolder.rb_rate2.setRating(dpo.getPost_object().getUser_business_rating());
                //Facility Rate text
                reviewViewHolder.tv_rate.setText(String.valueOf(dpo.getPost_object().getUser_business_rating())+" "+
                        context.getResources().getString(R.string.home_out_of)+" 5");
                //Share desc
                reviewViewHolder.share_desc.setText(dpo.getMetadata().getSharing_text()!=null?dpo.getMetadata().getSharing_text():"");
                //Votes count
                reviewViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    reviewViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    reviewViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    reviewViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    reviewViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                //todo just for now because ahmed didn't supported it yet
                reviewViewHolder.btn_down.setVisibility(View.GONE);
                reviewViewHolder.btn_up.setVisibility(View.GONE);
                reviewViewHolder.btn_rolling.setVisibility(View.GONE);
                reviewViewHolder.tv_votes_count.setVisibility(View.GONE);
                //Review text
                reviewViewHolder.tv_text.setText(dpo.getPost_object().getUser_business_review_text());

                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        reviewViewHolder.btn_down.setVisibility(View.GONE);
                        reviewViewHolder.btn_up.setVisibility(View.GONE);
                    }

                reviewViewHolder.img_broadcast.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callFollowAPI(String.valueOf(dpo.getPost_object().getBusinesses_id()),"user",
                                dpo.getPost_object().getIs_business_following()==0?"yes":"no",
                                context.getResources().getString(R.string.follow_place_true),
                                context.getResources().getString(R.string.follow_place_false),position);
                    }
                });
                reviewViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusinesses_id());
                        context.startActivity(intent);
                    }
                });
                reviewViewHolder.tv_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,HomeActivity.class);
                        intent.putExtra("type","facility_info");
                        intent.putExtra("place_id",dpo.getPost_object().getBusinesses_id());
                        context.startActivity(intent);
                    }
                });
                reviewViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            if (dpo.getMetadata().getSharer().getId()!=0) {
                                Intent intent = new Intent(context, HomeActivity.class);
                                intent.putExtra("type", "profile");
                                intent.putExtra("profile_type", "customer");
                                intent.putExtra("profile_id", dpo.getMetadata().getSharer().getId());
                                context.startActivity(intent);
                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                reviewViewHolder.img_shared_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                reviewViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            if (dpo.getMetadata().getSharer().getId()!=0) {
                                Intent intent = new Intent(context, HomeActivity.class);
                                intent.putExtra("type", "profile");
                                intent.putExtra("profile_type", "customer");
                                intent.putExtra("profile_id", dpo.getMetadata().getSharer().getId());
                                context.startActivity(intent);
                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                reviewViewHolder.tv_shared_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getUser_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                reviewViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(13,position,dpo.getPost_object().getIs_following());
                    }
                });
                reviewViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getReview_id()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                reviewViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,13,position,"post",String.valueOf(dpo.getPost_object().getReview_id()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(holder,13,position,"post",String.valueOf(dpo.getPost_object().getReview_id()),
                                    "0","1");
                        }
                    }
                });
                reviewViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,13,position,"post",String.valueOf(dpo.getPost_object().getReview_id()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(holder,13,position,"post",String.valueOf(dpo.getPost_object().getReview_id()),
                                    "0","-1");
                        }
                    }
                });
                reviewViewHolder.shared_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewOriginalPostActivity.class);
                        intent.putExtra("post",new Gson().toJson(dpo));
                        intent.putExtra("type",5);
                        context.startActivity(intent);
                    }
                });
            }break;
            //Share Event-------------------------------------------------------------------------------------
            case 14: {
                final ShareEventViewHolder eventViewHolder = (ShareEventViewHolder) holder;
                //Username
                if (dpo.getMetadata().getSharer().getName()!=null) {
                    eventViewHolder.tv_username.setText(Html.fromHtml("<b>"+dpo.getMetadata().getSharer().getName()+"</b>"+
                          " "+context.getResources().getString(R.string.home_shared_post)));
                }
                //Shared Username
                if (dpo.getPost_object().getBusiness_name()!=null){
                    eventViewHolder.tv_shared_username.setText(dpo.getPost_object().getBusiness_name());
                }
                //Profile image
                if (dpo.getMetadata().getSharer().getImage()!=null) {
                    BaseFunctions.setFrescoImage(eventViewHolder.img_profile_image,dpo.getMetadata().getSharer().getImage());
                    //BaseFunctions.setGlideImage(context,eventViewHolder.img_profile_image,dpo.getSharer_profile_image());
                }
                //Shared Profile image
                if (dpo.getPost_object().getBusiness_thumb_image()!=null){
                    BaseFunctions.setFrescoImage(eventViewHolder.img_shared_profile_image,dpo.getPost_object().getBusiness_thumb_image());
                    //BaseFunctions.setGlideImage(context,eventViewHolder.img_shared_profile_image,dpo.getReal_object().getUser_profile_image());
                }
                //Address
                eventViewHolder.tv_location.setText(dpo.getPost_object().getBusiness_city_name());
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null){
                    eventViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                if (dpo.getMetadata().getShare_time()!=null&&!dpo.getMetadata().getShare_time().equals("")) {
                    eventViewHolder.tv_time.setText(BaseFunctions.processDate(context, dpo.getMetadata().getShare_time()));
                }
                //Shared time
                if (dpo.getPost_object().getCreated_at()!=null){
                    eventViewHolder.tv_shared_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                }
                //Going
                if (dpo.getPost_object().getBusiness_id()!=SharedPrefManager.getInstance(context).getUser().getId()){
                    if (dpo.getPost_object().getIs_going() == 0){
                        eventViewHolder.going_layout.setBackgroundResource(R.drawable.primary_circle);
                        eventViewHolder.going_image.setImageResource(R.drawable.ic_plus);
                    }else if (dpo.getPost_object().getIs_going() == 1){
                        eventViewHolder.going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                        eventViewHolder.going_image.setImageResource(R.drawable.ic_minus);
                    }else {
                        eventViewHolder.going_layout.setVisibility(View.GONE);
                    }
                }
                //Image
                if (dpo.getPost_object().getImage()!=null){
                    try {
                        BaseFunctions.setFrescoImage(eventViewHolder.img_image,dpo.getPost_object().getImage());
                    }catch (Exception e){}
                    //BaseFunctions.setGlideImage(context,eventViewHolder.img_image,dpo.getReal_object().getImage());
                }
                //Event title
                if (lan.equals("en")){
                    eventViewHolder.tv_desc.setText(dpo.getPost_object().getEn_title()!=null?dpo.getPost_object().getEn_title():"");
                }else if (lan.equals("ar")){
                    eventViewHolder.tv_desc.setText(dpo.getPost_object().getAr_title()!=null?dpo.getPost_object().getAr_title():dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }
                //Event location
                eventViewHolder.tv_location.setText(dpo.getPost_object().getBusiness_location_address()!=null?dpo.getPost_object().getBusiness_location_address():"");
                //Event date and going count
                try {
                    eventViewHolder.tv_date_and_going.setText(BaseFunctions.dateExtractor(dpo.getPost_object().getEvent_date())+"-"+dpo.getPost_object().getGoing_count()+" "+
                            context.getResources().getString(R.string.create_event_going));
                }catch (Exception e){}
                //Share desc
                eventViewHolder.share_desc.setText(dpo.getMetadata().getSharing_text()!=null?dpo.getMetadata().getSharing_text():"");
                //Votes count
                eventViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //Voted Value
                if (dpo.getPost_object().getUser_vote()==1){
                    eventViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    eventViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    eventViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    eventViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }
                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        eventViewHolder.btn_down.setVisibility(View.GONE);
                        eventViewHolder.btn_up.setVisibility(View.GONE);
                    }

                eventViewHolder.going_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getIs_going() == 0){
                            callJoinAPI(eventViewHolder,"share_event",position,dpo.getPost_object().getId(),dpo.getPost_object().getIs_going());
                        }
                    }
                });
                eventViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(eventViewHolder,14,position,"event",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(eventViewHolder,14,position,"event",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });

                eventViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,14,position,"event",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(holder,14,position,"event",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
                eventViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            ViewImageDialog dialog = new ViewImageDialog(context, dpo.getPost_object().getImage());
                            dialog.show();
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.img_shared_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.tv_shared_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"event",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                eventViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(14,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                eventViewHolder.btn_three_dots.setVisibility(View.GONE);
                eventViewHolder.shared_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewOriginalPostActivity.class);
                        intent.putExtra("post",new Gson().toJson(dpo));
                        intent.putExtra("type",3);
                        context.startActivity(intent);
                    }
                });
            }break;
            //Share Ad--------------------------------------------------------------------------------
            case 15: {

                ShareAdViewHolder adViewHolder = (ShareAdViewHolder) holder;

                //Username
                if (dpo.getMetadata().getSharer().getName()!=null) {
                    adViewHolder.tv_username.setText(Html.fromHtml("<b>" + dpo.getMetadata().getSharer().getName() + "</b>" +
                            " " + context.getResources().getString(R.string.home_shared_post)));
                }
                //Shared Facility name
                adViewHolder.tv_shared_username.setText(dpo.getPost_object().getBusiness_name());
                //Profile image
                if (dpo.getMetadata().getSharer().getImage()!=null) {
                    BaseFunctions.setFrescoImage(adViewHolder.img_profile_image,dpo.getMetadata().getSharer().getImage());
                }
                //Shared Facility image
                BaseFunctions.setFrescoImage(adViewHolder.img_shared_profile_image,dpo.getPost_object().getBusiness_thumb_image());
                //Address
                adViewHolder.tv_address.setText(dpo.getPost_object().getBusiness_city_name()!=null?
                        dpo.getPost_object().getBusiness_city_name():"");
                //UserRate
                if (dpo.getPost_object().getBusiness_avg_review()!=null) {
                    adViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                }
                //Time
                adViewHolder.tv_time.setText(BaseFunctions.processDate(context, dpo.getMetadata().getShare_time()));
                //Shared Time
                adViewHolder.tv_shared_time.setText(BaseFunctions.processDate(context, dpo.getPost_object().getCreated_at()));
                //Sponsored
                adViewHolder.tv_sposored.setVisibility(View.VISIBLE);
                //Image
                try {
                    BaseFunctions.setFrescoImage(adViewHolder.img_image,dpo.getPost_object().getContent());
                }catch (Exception e){}
                //Sponsored
                adViewHolder.tv_sposored.setVisibility(View.VISIBLE);
                //Description
                setTags(adViewHolder.tv_desc, dpo.getPost_object().getCaption());
                //Share desc
                adViewHolder.share_desc.setText(dpo.getMetadata().getSharing_text()!=null?dpo.getMetadata().getSharing_text():"");
                //Votes
                adViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    adViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    adViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    adViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    adViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        adViewHolder.btn_down.setVisibility(View.GONE);
                        adViewHolder.btn_up.setVisibility(View.GONE);
                    }

                adViewHolder.img_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            try {
                                ViewImageDialog dialog = new ViewImageDialog(context, dpo.getPost_object().getContent());
                                dialog.show();
                            }catch (Exception e){}

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                adViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            if (dpo.getMetadata().getSharer().getId()!=0) {
                                Intent intent = new Intent(context, HomeActivity.class);
                                intent.putExtra("type", "profile");
                                intent.putExtra("profile_type", "customer");
                                intent.putExtra("profile_id", dpo.getMetadata().getSharer().getId());
                                context.startActivity(intent);
                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                adViewHolder.img_shared_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                adViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            if (BaseFunctions.isOnline(context)) {
                                if (dpo.getMetadata().getSharer().getId() != 0) {
                                    Intent intent = new Intent(context, HomeActivity.class);
                                    intent.putExtra("type", "profile");
                                    intent.putExtra("profile_type", "customer");
                                    intent.putExtra("profile_id", dpo.getMetadata().getSharer().getId());
                                    context.startActivity(intent);
                                }
                            } else {
                                Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
                adViewHolder.tv_shared_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                adViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(7,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                adViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"post",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                adViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,15,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(holder,15,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });
                adViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(holder,15,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(holder,15,position,"post",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
                adViewHolder.shared_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewOriginalPostActivity.class);
                        intent.putExtra("post",new Gson().toJson(dpo));
                        intent.putExtra("type",7);
                        context.startActivity(intent);
                    }
                });
            }break;

            //Shared Offer--------------------------------------------------------------------------------
            case 16: {
                final ShareOfferViewHolder createOfferViewHolder = (ShareOfferViewHolder) holder;
                //Username
                if (dpo.getMetadata().getSharer().getName()!=null){
                createOfferViewHolder.tv_username.setText(Html.fromHtml("<b>"+dpo.getMetadata().getSharer().getName()+"</b>"+
                        " "+context.getResources().getString(R.string.home_shared_post)));
                }
                //Shared Title
                createOfferViewHolder.tv_shared_username.setText(Html.fromHtml("<b>"+dpo.getPost_object().getBusiness_name()+"</b>"+" "+
                        context.getResources().getString(R.string.home_created_offer)));
                //Sharer icon
                if (dpo.getMetadata().getSharer().getImage()!=null) {
                    BaseFunctions.setFrescoImage(createOfferViewHolder.img_profile_image,dpo.getMetadata().getSharer().getImage());
                    //BaseFunctions.setGlideImage(context,createOfferViewHolder.img_profile_image,dpo.getSharer_profile_image());
                }
                //Shared Facility icon
                BaseFunctions.setFrescoImage(createOfferViewHolder.img_shared_profile_image,dpo.getPost_object().getBusiness_thumb_image());
                //BaseFunctions.setGlideImage(context,createOfferViewHolder.img_shared_profile_image,dpo.getReal_object().getUser_profile_image());
                //Address
                createOfferViewHolder.tv_address.setText(dpo.getPost_object().getBusiness_city_name());
                //Rating
                createOfferViewHolder.rb_rate.setRating(Float.valueOf(dpo.getPost_object().getBusiness_avg_review()));
                //Time
                if (dpo.getMetadata().getShare_time()!=null&&dpo.getMetadata().getShare_time().equals("")) {
                    createOfferViewHolder.tv_time.setText(BaseFunctions.processDate(context, dpo.getMetadata().getShare_time()));
                }
                //Shared Time
                createOfferViewHolder.tv_shared_time.setText(BaseFunctions.processDate(context,dpo.getPost_object().getCreated_at()));
                //Percent
                createOfferViewHolder.tv_percent.setText(dpo.getPost_object().getDiscount()+"%");
                //Product name
                if (lan.equals("en")) {
                    createOfferViewHolder.tv_name.setText(dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }else if (lan.equals("ar")){
                    createOfferViewHolder.tv_name.setText(dpo.getPost_object().getAr_title()!=null?
                            dpo.getPost_object().getAr_title():dpo.getPost_object().getEn_title()!=null?
                            dpo.getPost_object().getEn_title():"");
                }
                //End date
                createOfferViewHolder.tv_till.setText(context.getResources().getString(R.string.home_valid_till)+" "+
                        BaseFunctions.dateExtractorOld(dpo.getPost_object().getExpiry_date()));
                //Share desc
                createOfferViewHolder.share_desc.setText(dpo.getMetadata().getSharing_text()!=null?dpo.getMetadata().getSharing_text():"");
                //Votes Count
                createOfferViewHolder.tv_votes_count.setText((dpo.getPost_object().getUpvoting_count()+dpo.getPost_object().getDownvote_count())+"");
                //User vote
                if (dpo.getPost_object().getUser_vote()==1){
                    createOfferViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                }else if (dpo.getPost_object().getUser_vote()==-1){
                    createOfferViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                }else {
                    createOfferViewHolder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                    createOfferViewHolder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                }

                    if (dpo.getPost_object().getPeople_can_vote_posts() == 0){
                        createOfferViewHolder.btn_down.setVisibility(View.GONE);
                        createOfferViewHolder.btn_up.setVisibility(View.GONE);
                    }
                if (dpo.getPost_object().getIs_following() == 0){
                    createOfferViewHolder.img_add.setBackgroundResource(R.drawable.primary_circle);
                    createOfferViewHolder.add_img.setImageResource(R.drawable.ic_plus);
                }else if (dpo.getPost_object().getIs_following() == 1){
                    createOfferViewHolder.img_add.setBackgroundResource(R.drawable.dark_grey_circle);
                    createOfferViewHolder.add_img.setImageResource(R.drawable.ic_minus);
                }
                createOfferViewHolder.img_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                createOfferViewHolder.img_shared_profile_image.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                createOfferViewHolder.tv_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context, HomeActivity.class);
                            intent.putExtra("type", "profile");
                            intent.putExtra("profile_type", "customer");
                            intent.putExtra("profile_id", Integer.valueOf(dpo.getMetadata().getSharer().getId()));
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                createOfferViewHolder.tv_shared_username.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)) {
                            Intent intent = new Intent(context,HomeActivity.class);
                            intent.putExtra("type","facility_info");
                            intent.putExtra("place_id",dpo.getPost_object().getBusiness_id());
                            context.startActivity(intent);
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                createOfferViewHolder.img_add.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getIs_following() == 0){
                            createOfferViewHolder.img_add.setBackgroundResource(R.drawable.dark_grey_circle);
                            createOfferViewHolder.add_img.setImageResource(R.drawable.ic_minus);
                        }else {
                            createOfferViewHolder.img_add.setBackgroundResource(R.drawable.primary_circle);
                            createOfferViewHolder.add_img.setImageResource(R.drawable.ic_plus);
                        }
                        callFollowAPI(String.valueOf(dpo.getPost_object().getId()),"offer",
                                dpo.getPost_object().getIs_following() == 0?"yes":"no",
                                context.getResources().getString(R.string.follow_offer_true),
                                context.getResources().getString(R.string.follow_offer_false),position);
                    }
                });
                createOfferViewHolder.btn_three_dots.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        iPost.onMoreClicked(8,position,dpo.getPost_object().getIs_business_following());
                    }
                });
                createOfferViewHolder.btn_rolling.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (BaseFunctions.isOnline(context)){
                            SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getPost_object().getId()),"offer",name);
                                }
                            });
                            dialog.show();

                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                createOfferViewHolder.btn_up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(createOfferViewHolder,16,position,"offer",String.valueOf(dpo.getPost_object().getId()),
                                    "1","0");
                        }else if (dpo.getPost_object().getUser_vote() == 1){
                            callVoteAPI(createOfferViewHolder,16,position,"offer",String.valueOf(dpo.getPost_object().getId()),
                                    "0","1");
                        }
                    }
                });

                createOfferViewHolder.btn_down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (dpo.getPost_object().getUser_vote()==0){
                            callVoteAPI(createOfferViewHolder,16,position,"offer",String.valueOf(dpo.getPost_object().getId()),
                                    "-1","0");
                        }else if (dpo.getPost_object().getUser_vote() == -1){
                            callVoteAPI(createOfferViewHolder,16,position,"offer",String.valueOf(dpo.getPost_object().getId()),
                                    "0","-1");
                        }
                    }
                });
                createOfferViewHolder.shared_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, ViewOriginalPostActivity.class);
                        intent.putExtra("post",new Gson().toJson(dpo));
                        intent.putExtra("type",8);
                        context.startActivity(intent);
                    }
                });
            }break;
        }
    }

    private void setTags(TextView pTextView, String pTagString) {
        SpannableString string = new SpannableString(pTagString);

        int start = -1;
        for (int i = 0; i < pTagString.length(); i++) {
            if (pTagString.charAt(i) == '#') {
                start = i;
            } else if (pTagString.charAt(i) == ' ' || (i == pTagString.length() - 1 && start != -1)) {
                if (start != -1) {
                    if (i == pTagString.length() - 1) {
                        i++; // case for if hash is last word and there is no
                        // space after word
                    }

                    final String tag = pTagString.substring(start, i);
                    string.setSpan(new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                            iHash.onHashTagClicked(tag);
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            // link color
                            ds.setColor(Color.parseColor("#F7931D"));
                            ds.setUnderlineText(false);
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    start = -1;
                }
            }
        }

        pTextView.setMovementMethod(LinkMovementMethod.getInstance());
        pTextView.setText(string);
    }

    private void callFollowAPI(String id,
                               final String type,
                               final String value,
                               final String follow_message,
                               final String unfollow_message,
                               final int position){
        FollowersAPIsClass.followNew(
                context,
                BaseFunctions.getDeviceId(context),
                type,
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("yes")){
                                    Toast.makeText(context, follow_message, Toast.LENGTH_SHORT).show();
                                    HomePostsObject o = list.get(position);
                                    HomePostObject oo = o.getPost_object();
                                    oo.setIs_following(1);
                                    o.setObject(oo);
                                    list.set(position,o);
                                    if (!type.equals("offer")) {
                                        notifyItemChanged(position);
                                    }
                                }else {
                                    Toast.makeText(context, unfollow_message, Toast.LENGTH_SHORT).show();
                                    HomePostsObject o = list.get(position);
                                    HomePostObject oo = o.getPost_object();
                                    oo.setIs_following(0);
                                    o.setObject(oo);
                                    list.set(position,o);
                                    if (!type.equals("offer")){
                                        notifyItemChanged(position);
                                    }
                                }
                            }else {
                                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callFollowOfferAPI(
                                RecyclerView.ViewHolder holder,
                                String offer_type,
                                String id,
                                String type,
                                final String value,
                                final String follow_message,
                                final String unfollow_message,
                                final int position){
        FollowersAPIsClass.followNew(
                context,
                BaseFunctions.getDeviceId(context),
                type,
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("yes")){
                                    Toast.makeText(context, follow_message, Toast.LENGTH_SHORT).show();
                                    HomePostsObject o = list.get(position);
                                    HomePostObject oo = o.getPost_object();
                                    oo.setIs_following(1);
                                    o.setObject(oo);
                                    list.set(position,o);
                                    notifyItemChanged(position);
                                }else {
                                    Toast.makeText(context, unfollow_message, Toast.LENGTH_SHORT).show();
                                    HomePostsObject o = list.get(position);
                                    HomePostObject oo = o.getPost_object();
                                    oo.setIs_following(0);
                                    o.setObject(oo);
                                    list.set(position,o);
                                    notifyItemChanged(position);
                                }
                            }else {
                                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callShareAPI(String id,String type,String sharing_text){
        ShareAPIsClass.share(
                context,
                BaseFunctions.getDeviceId(context),
                id,
                type,
                sharing_text,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(context, context.getResources().getString(R.string.home_share_success), Toast.LENGTH_SHORT).show();
                        BaseFunctions.playMusic(context,R.raw.added);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callVoteAPI(final RecyclerView.ViewHolder holder, final int view_holder_type, final  int position, final  String type, final  String id, final  String value, final String old_value){
        if (value.equals("1")){
            //((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_tapped);
            //((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
            HomePostsObject e = list.get(position);
            HomePostObject o = e.getPost_object();
            o.setUser_vote(1);
            o.setUpvoting_count(o.getUpvoting_count()+1);
            //((PhotoViewHolder)holder).tv_votes_count.setText(String.valueOf(o.getUpvoting_count()+o.getDownvote_count()));
            e.setObject(o);
            list.set(position,e);
            temp_post = list.get(position);
            list.remove(position);
            notifyItemRemoved(position);
            list.add(position,temp_post);
            notifyItemInserted(position);
        }else if (value.equals("0")){
            //((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
            //((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
            HomePostsObject e = list.get(position);
            HomePostObject o = e.getPost_object();
            if (o.getUser_vote() == 1){
                o.setUpvoting_count(o.getUpvoting_count()-1);
            }else if (o.getUser_vote() == -1){
                o.setDownvote_count(o.getDownvote_count()-1);
            }
            //((PhotoViewHolder)holder).tv_votes_count.setText(String.valueOf(o.getUpvoting_count()+o.getDownvote_count()));
            o.setUser_vote(0);
            e.setObject(o);
            list.set(position,e);
            temp_post = list.get(position);
            list.remove(position);
            notifyItemRemoved(position);
            list.add(position,temp_post);
            notifyItemInserted(position);

        }else if (value.equals("-1")){
            //((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
            //((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_tapped);
            HomePostsObject e = list.get(position);
            HomePostObject o = e.getPost_object();
            o.setUser_vote(-1);
            o.setDownvote_count(o.getDownvote_count()+1);
            //((PhotoViewHolder)holder).tv_votes_count.setText(String.valueOf(o.getUpvoting_count()+o.getDownvote_count()));
            e.setObject(o);
            list.set(position,e);

            temp_post = list.get(position);
            list.remove(position);
            notifyItemRemoved(position);
            list.add(position,temp_post);
            notifyItemInserted(position);
        }

                VoteAPIsClass.vote(context,
                        BaseFunctions.getDeviceId(context),
                        type,
                        id,
                        value,
                        new IResponse() {
                            @Override
                            public void onResponse() {

                            }

                            @Override
                            public void onResponse(Object json) {
                                String j = new Gson().toJson(json);
                                Boolean success = new Gson().fromJson(j,Boolean.class);
                                if (success){
                                }else {
                                    Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                                    if (value.equals("1")){
                                        //((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                        //((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                        HomePostsObject e = list.get(position);
                                        HomePostObject o = e.getPost_object();
                                        o.setUser_vote(0);
                                        o.setUpvoting_count(o.getUpvoting_count()-1);
                                        //((PhotoViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()+o.getDownvote_count())+"");
                                        e.setObject(o);
                                        list.set(position,e);

                                        temp_post = list.get(position);
                                        list.remove(position);
                                        notifyItemRemoved(position);
                                        list.add(position,temp_post);
                                        notifyItemInserted(position);
                                        //notifyItemChanged(position);
                                        //Toast.makeText(context, context.getResources().getString(R.string.vote_on), Toast.LENGTH_SHORT).show();
                                    }else if (value.equals("0")){
                                        if (old_value.equals("1")){
                                            //((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                                            //((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                            HomePostsObject e = list.get(position);
                                            HomePostObject o = e.getPost_object();
                                            o.setUpvoting_count(o.getUpvoting_count()+1);
                                            //((PhotoViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()+o.getDownvote_count())+"");
                                            o.setUser_vote(1);
                                            e.setObject(o);
                                            list.set(position,e);

                                            temp_post = list.get(position);
                                            list.remove(position);
                                            notifyItemRemoved(position);
                                            list.add(position,temp_post);
                                            notifyItemInserted(position);
                                        }else if (old_value.equals("-1")){
                                            //((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                            //((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                                            HomePostsObject e = list.get(position);
                                            HomePostObject o = e.getPost_object();
                                            o.setDownvote_count(o.getDownvote_count()+1);
                                            //((PhotoViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()+o.getDownvote_count())+"");
                                            o.setUser_vote(1);
                                            e.setObject(o);
                                            list.set(position,e);

                                            temp_post = list.get(position);
                                            list.remove(position);
                                            notifyItemRemoved(position);
                                            list.add(position,temp_post);
                                            notifyItemInserted(position);
                                        }
                                    }else if (value.equals("-1")){
                                        //((PhotoViewHolder)holder).btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                        //((PhotoViewHolder)holder).btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                        HomePostsObject e = list.get(position);
                                        HomePostObject o = e.getPost_object();
                                        o.setUser_vote(0);
                                        o.setDownvote_count(o.getDownvote_count()-1);
                                        //((PhotoViewHolder)holder).tv_votes_count.setText((o.getUpvoting_count()+o.getDownvote_count())+"");
                                        e.setObject(o);
                                        list.set(position,e);

                                        temp_post = list.get(position);
                                        list.remove(position);
                                        notifyItemRemoved(position);
                                        list.add(position,temp_post);
                                        notifyItemInserted(position);
                                        //notifyItemChanged(position);
                                        //Toast.makeText(context, context.getResources().getString(R.string.vote_off), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                        }, new IFailure() {
                            @Override
                            public void onFailure() {
                                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        });

    }

    private void callJoinAPI(final RecyclerView.ViewHolder holder,final String type,final int position,final int id,final int going){
        if (type.equals("event")){
            if (going == 0){
                ((EventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                ((EventViewHolder)holder).going_image.setImageResource(R.drawable.ic_minus);
            }else {
                ((EventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.primary_circle);
                ((EventViewHolder)holder).going_image.setImageResource(R.drawable.ic_plus);
            }
        }else {
            if (going == 0){
                ((ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                ((ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_minus);
            }else {
                ((ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.primary_circle);
                ((ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_plus);
            }
        }
        EventsAPIsClass.joinEvent(context,
                BaseFunctions.getDeviceId(context),
                id,
                going,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){
                            Toast.makeText(context, context.getResources().getString(R.string.create_event_going_success), Toast.LENGTH_SHORT).show();
                            HomePostsObject hpo = list.get(position);
                            HomePostObject sub_hpo = hpo.getPost_object();
                            if (going == 0) {
                                sub_hpo.setIs_going(1);
                                sub_hpo.setGoing_count(sub_hpo.getGoing_count()+1);
                                try {
                                    ((EventViewHolder)holder).tv_date_and_going.setText(BaseFunctions.dateExtractor(sub_hpo.getEvent_date())+"-"+sub_hpo.getGoing_count()+" "+
                                            context.getResources().getString(R.string.create_event_going));
                                }catch (Exception e){}
                            }else {
                                sub_hpo.setIs_going(0);
                                sub_hpo.setGoing_count(sub_hpo.getGoing_count()-1);
                                try {
                                    ((EventViewHolder)holder).tv_date_and_going.setText(BaseFunctions.dateExtractor(sub_hpo.getEvent_date())+"-"+sub_hpo.getGoing_count()+" "+
                                            context.getResources().getString(R.string.create_event_going));
                                }catch (Exception e){}
                            }
                            hpo.setObject(sub_hpo);
                            list.set(position,hpo);
                            notifyItemChanged(position);
                        }else {
                            if (type.equals("event")){
                                if (going == 1){
                                    ((EventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                    ((EventViewHolder)holder).going_image.setImageResource(R.drawable.ic_minus);
                                }else {
                                    ((EventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.primary_circle);
                                    ((EventViewHolder)holder).going_image.setImageResource(R.drawable.ic_plus);
                                }
                            }else {
                                if (going == 1){
                                    ((ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                    ((ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_minus);
                                }else {
                                    ((ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.primary_circle);
                                    ((ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_plus);
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        if (type.equals("event")){
                            if (going == 1){
                                ((EventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                ((EventViewHolder)holder).going_image.setImageResource(R.drawable.ic_minus);
                            }else {
                                ((EventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.primary_circle);
                                ((EventViewHolder)holder).going_image.setImageResource(R.drawable.ic_plus);
                            }
                        }else {
                            if (going == 1){
                                ((ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                ((ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_minus);
                            }else {
                                ((ShareEventViewHolder)holder).going_layout.setBackgroundResource(R.drawable.primary_circle);
                                ((ShareEventViewHolder)holder).going_image.setImageResource(R.drawable.ic_plus);
                            }
                        }
                    }
                });

    }
}
