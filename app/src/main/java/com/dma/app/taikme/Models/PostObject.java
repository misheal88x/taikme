package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class PostObject {

    public static final String TYPE_PHOTO = "1";
    public static final String TYPE_VIDEO = "2";
    public static final String TYPE_CHECK_IN = "3";

    @SerializedName("id") private int id = 0;
    @SerializedName("post_topic_en_name") private String post_topic_en_name = "";
    @SerializedName("post_topic_ar_name") private String post_topic_ar_name = "";
    @SerializedName("en_title") private String en_title = "";
    @SerializedName("event_date") private String event_date = "";
    @SerializedName("image") private ImageObject image;
    @SerializedName("topic_id") private int topic_id = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("caption") private String caption = "";
    @SerializedName("post_type") private int post_type = 0;
    @SerializedName("content") private Object content = null;
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("user_name") private String user_name = "";
    @SerializedName("user_mobile_number") private String user_mobile_number = "";
    @SerializedName("user_email") private String user_email = "";
    @SerializedName("user_profile_image") private String user_profile_image = "";
    @SerializedName("user_cover_image") private String user_cover_image = "";
    @SerializedName("since_minute") private String since_minute = "";
    @SerializedName("user_rate") private String user_rate = "";
    @SerializedName("votes_count") private String votes_count;
    @SerializedName("CONTENT_TYPE_ID") private int CONTENT_TYPE_ID = 0;
    @SerializedName("voted_value") private int voted_value = 0;
    @SerializedName("is_followed") private int is_followed = 0;
    @SerializedName("is_sponsored") private boolean is_sponsored = false;
    @SerializedName("expiry_date") private String expiry_date = "";
    @SerializedName("en_description") private String en_description = "";
    @SerializedName("discount") private String discount = "";
    @SerializedName("payload") private String payload = "";
    @SerializedName("location_en_name") private String location_en_name = "";
    @SerializedName("location_ar_name") private String location_ar_name = "";
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPost_topic_en_name() {
        return post_topic_en_name;
    }

    public void setPost_topic_en_name(String post_topic_en_name) {
        this.post_topic_en_name = post_topic_en_name;
    }

    public String getPost_topic_ar_name() {
        return post_topic_ar_name;
    }

    public void setPost_topic_ar_name(String post_topic_ar_name) {
        this.post_topic_ar_name = post_topic_ar_name;
    }

    public int getTopic_id() {
        return topic_id;
    }

    public void setTopic_id(int topic_id) {
        this.topic_id = topic_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public int getPost_type() {
        return post_type;
    }

    public void setPost_type(int post_type) {
        this.post_type = post_type;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_profile_image() {
        return user_profile_image;
    }

    public void setUser_profile_image(String user_profile_image) {
        this.user_profile_image = user_profile_image;
    }

    public String getUser_cover_image() {
        return user_cover_image;
    }

    public void setUser_cover_image(String user_cover_image) {
        this.user_cover_image = user_cover_image;
    }

    public String getUser_mobile_number() {
        return user_mobile_number;
    }

    public void setUser_mobile_number(String user_mobile_number) {
        this.user_mobile_number = user_mobile_number;
    }

    public static String getTypePhoto() {
        return TYPE_PHOTO;
    }

    public static String getTypeVideo() {
        return TYPE_VIDEO;
    }

    public static String getTypeCheckIn() {
        return TYPE_CHECK_IN;
    }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
    }

    public String getEvent_date() {
        return event_date;
    }

    public void setEvent_date(String event_date) {
        this.event_date = event_date;
    }

    public ImageObject getImage() {
        return image;
    }

    public void setImage(ImageObject image) {
        this.image = image;
    }

    public String getSince_minute() {
        return since_minute;
    }

    public void setSince_minute(String since_minute) {
        this.since_minute = since_minute;
    }

    public String getUser_rate() {
        return user_rate;
    }

    public void setUser_rate(String user_rate) {
        this.user_rate = user_rate;
    }

    public String getVotes_count() {
        return votes_count;
    }

    public void setVotes_count(String votes_count) {
        this.votes_count = votes_count;
    }

    public int getCONTENT_TYPE_ID() {
        return CONTENT_TYPE_ID;
    }

    public void setCONTENT_TYPE_ID(int CONTENT_TYPE_ID) {
        this.CONTENT_TYPE_ID = CONTENT_TYPE_ID;
    }

    public int getVoted_value() {
        return voted_value;
    }

    public void setVoted_value(int voted_value) {
        this.voted_value = voted_value;
    }

    public int getIs_followed() {
        return is_followed;
    }

    public void setIs_followed(int is_followed) {
        this.is_followed = is_followed;
    }

    public boolean isIs_sponsored() {
        return is_sponsored;
    }

    public void setIs_sponsored(boolean is_sponsored) {
        this.is_sponsored = is_sponsored;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getEn_description() {
        return en_description;
    }

    public void setEn_description(String en_description) {
        this.en_description = en_description;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getLocation_en_name() {
        return location_en_name;
    }

    public void setLocation_en_name(String location_en_name) {
        this.location_en_name = location_en_name;
    }

    public String getLocation_ar_name() {
        return location_ar_name;
    }

    public void setLocation_ar_name(String location_ar_name) {
        this.location_ar_name = location_ar_name;
    }
}
