package com.dma.app.taikme.Services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.UpdateFCMResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.firebase.iid.FirebaseInstanceId;
//import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.gson.Gson;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MyFirebaseInstanceIDService
       // extends FirebaseInstanceIdService

{

    /*
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        try {
            String recent_token = FirebaseInstanceId.getInstance().getToken();
            SharedPrefManager.getInstance(getApplicationContext()).setFcmToken(recent_token);
            Log.i("refreshed_token", "the token: " + recent_token);
            if (SharedPrefManager.getInstance(getApplicationContext()).getUser()!=null){
                callUpdateTokenAPI(recent_token);
            }
        }catch (Exception e){}
    }

     */

    private void callUpdateTokenAPI(String token) {
        UserAPIsClass.updateFCMToken(
                getApplicationContext(),
                BaseFunctions.getDeviceId(getApplicationContext()),
                token,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            UpdateFCMResponse success = new Gson().fromJson(json1,UpdateFCMResponse.class);
                            if (success.getStatus()){
                                Log.i("fcm_token_update", "onResponse: "+"success");
                            }else {
                                Log.i("fcm_token_update", "onResponse: "+"failed");
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Log.i("fcm_token_update", "onResponse: "+"no_internet");
                    }
                }
        );
    }
}
