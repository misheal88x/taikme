package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.BusinessLocationsAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;

public class AddLocationDialog extends AlertDialog {
    private Context context;
    private EditText edt_address;
    //,edt_city;
    //private Spinner district_spinner;
    //private Spinner country_spinner;
    //private CustomSpinnerAdapter district_adapter;
    //private CustomSpinnerAdapter country_adapter;
    //private List<String> district_list_string;
    //private List<String> country_list_string;
    private LinearLayout btn_pick_location;
    private TextView btn_add;
    private IMove iMove;
    private static String lat = "",lng = "";
    public AddLocationDialog(@NonNull Context context, IMove iMove) {
        super(context);
        this.context = context;
        this.iMove = iMove;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_add_location);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //Spinner
        //district_spinner = findViewById(R.id.add_location_district_spinner);
        //country_spinner = findViewById(R.id.add_location_country_spinner);
        //EditText
        edt_address = findViewById(R.id.dialog_add_location_address);
        //edt_city = findViewById(R.id.dialog_add_location_city);
        //TextView
        btn_add = findViewById(R.id.add_location_set_btn);
        //LinearLayout
        btn_pick_location = findViewById(R.id.add_location_location_btn);
    }

    private void init_events(){
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lat.equals("")||lng.equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.create_location_no_candidated), Toast.LENGTH_SHORT).show();
                    return;
                }

                if (edt_address.getText().toString().equals("")){
                    Toast.makeText(context, context.getResources().getString(R.string.create_location_no_address), Toast.LENGTH_SHORT).show();
                    return;
                }
                callAddLocationAPI(lat,lng,edt_address.getText().toString());
            }
        });
        btn_pick_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move();
            }
        });
    }

    private void init_dialog(){
        init_spinners();
    }

    private void init_spinners(){
        //district_list_string = new ArrayList<>();
        //country_list_string = new ArrayList<>();


        //district_list_string.add(context.getResources().getString(R.string.create_event_dates));
        //country_list_string.add(context.getResources().getString(R.string.create_event_hours));


        //BaseFunctions.init_spinner(context,district_spinner,district_adapter,district_list_string);
        //BaseFunctions.init_spinner(context,country_spinner,country_adapter,country_list_string);


    }

    public static void setLatLng(String lat1,String lng1){
        lat = lat1;
        lng = lng1;
    }

    private void callAddLocationAPI(String lat,String lng,String address){
        BusinessLocationsAPIsClass.addLocation(
                context,
                BaseFunctions.getDeviceId(context),
                lat,
                lng,
                address,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(context, context.getResources().getString(R.string.create_location_success), Toast.LENGTH_SHORT).show();
                            AddLocationDialog.this.cancel();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                }
        );
    }
}
