package com.dma.app.taikme.Fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.dma.app.taikme.APIsClass.CustomAudienceAPIsClass;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.Adapters.CustomAudiencesAdapter;
import com.dma.app.taikme.Adapters.PlacesReviewedAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.CustomAudienceObject;
import com.dma.app.taikme.Models.CustomAudiencesResponse;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.EndlessRecyclerViewScrollListener;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class CustomAudiencesFragment extends BaseFragment {

    private RecyclerView rv_recycler;
    private List<CustomAudienceObject> list;
    private CustomAudiencesAdapter adapter;
    private LinearLayoutManager layoutManager;
    private RelativeLayout root;
    private LinearLayout no_data;
    private AVLoadingIndicatorView pb_more;
    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_custom_audiences,container,false);
    }

    @Override
    public void init_views() {
        //RelativeLayout
        root = base.findViewById(R.id.custom_audiences_layout);
        //LinearLayout
        no_data = base.findViewById(R.id.no_data_layout);
        //RecyclerView
        rv_recycler = base.findViewById(R.id.custom_audiences_recycler);
        //ProgressBar
        pb_more = base.findViewById(R.id.custom_audiences_more);
    }

    @Override
    public void init_events() {

    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_recycler();
        callReviewedPlacesAPI(currentPage,0);
    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new CustomAudiencesAdapter(base, list, new IMove() {
            @Override
            public void move() {

            }

            @Override
            public void move(int position) {

            }
        });
        layoutManager = new LinearLayoutManager(base,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_recycler.setLayoutManager(layoutManager);
        rv_recycler.setAdapter(adapter);
        rv_recycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (list.size()>=per_page){
                    if (continue_paginate){
                        currentPage++;
                        callReviewedPlacesAPI(currentPage,1);
                    }
                }
            }
        });
    }

    private void callReviewedPlacesAPI(final int page,final int type){
        if (type == 1){
            pb_more.smoothToShow();
        }
        CustomAudienceAPIsClass.getAll(
                base,
                BaseFunctions.getDeviceId(base),
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            CustomAudiencesResponse success = new Gson().fromJson(j,CustomAudiencesResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (CustomAudienceObject cao : success.getData()){
                                        list.add(cao);
                                        adapter.notifyDataSetChanged();
                                    }
                                    if (type == 0){
                                        BaseFunctions.runAnimation(rv_recycler,0,adapter);
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callReviewedPlacesAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            pb_more.smoothToHide();
        }else {
            no_data.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data.setVisibility(View.GONE);
            rv_recycler.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
