package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface NotificationsAPIs {
    @GET("notifications")
    Call<BaseResponse> get_all(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Query("page") int page
    );
}
