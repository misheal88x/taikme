package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class MultiSelectObject {
    private String name = "";
    private boolean is_checked = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIs_checked() {
        return is_checked;
    }

    public void setIs_checked(boolean is_checked) {
        this.is_checked = is_checked;
    }
}
