package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.dma.app.taikme.Adapters.GeneralHomeAdapter;
import com.dma.app.taikme.R;

public class TestHomeActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private GeneralHomeAdapter adapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_home);
        recyclerView = findViewById(R.id.recycler);
        //adapter = new GeneralHomeAdapter(this);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}
