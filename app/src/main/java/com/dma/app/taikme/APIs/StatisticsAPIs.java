package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface StatisticsAPIs {
    @GET("business/ad-statistics")
    Call<BaseResponse> get_stats(
            @Header("Accept") String accept,
            @Header("device_id") String device_id
    );
}
