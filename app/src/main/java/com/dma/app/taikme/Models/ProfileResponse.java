package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class ProfileResponse {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("mobile_number") private String mobile_number = "";
    @SerializedName("login_type") private int login_type = 0;
    @SerializedName("profile_image") private String profile_image = "";
    @SerializedName("cover_image") private String cover_image = "";
    @SerializedName("number_of_following") private int number_of_following = 0;
    @SerializedName("number_of_followers") private int number_of_followers = 0;
    @SerializedName("is_followed") private boolean is_followed = false;
    @SerializedName("payload") private ProfilePayloadObject payload = new ProfilePayloadObject();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public int getLogin_type() {
        return login_type;
    }

    public void setLogin_type(int login_type) {
        this.login_type = login_type;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

    public int getNumber_of_following() {
        return number_of_following;
    }

    public void setNumber_of_following(int number_of_following) {
        this.number_of_following = number_of_following;
    }

    public int getNumber_of_followers() {
        return number_of_followers;
    }

    public void setNumber_of_followers(int number_of_followers) {
        this.number_of_followers = number_of_followers;
    }

    public ProfilePayloadObject getPayload() {
        return payload;
    }

    public void setPayload(ProfilePayloadObject payload) {
        this.payload = payload;
    }

    public boolean isIs_followed() {
        return is_followed;
    }

    public void setIs_followed(boolean is_followed) {
        this.is_followed = is_followed;
    }
}
