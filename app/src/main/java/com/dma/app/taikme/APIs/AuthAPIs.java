package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Models.FacebookObject;

import org.androidannotations.annotations.rest.Head;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface AuthAPIs {
    @Multipart
    @POST("users/createAccount")
    Call<BaseResponse> normalResgister(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Part("mobile_number") RequestBody mobile_number,
            @Part("password") RequestBody password,
            @Part("name") RequestBody name,
            @Part("email") RequestBody email,
            @Part("user_type") RequestBody user_type,
            @Part("age") RequestBody age,
            @Part("gender") RequestBody gender,
            @Part("birthday") RequestBody birthday,
            @Part("country_name") RequestBody country_name,
            @Part("city_name") RequestBody city_name,
            @Part("first_best_cuisines") RequestBody first_best_cuisines,
            @Part("second_best_cuisines") RequestBody second_best_cuisines,
            @Part("third_best_cuisines") RequestBody third_best_cuisines,
            @Part("prefer") RequestBody prefer,
            @Part("bio") RequestBody bio,
            @Part("website") RequestBody website,
            @Part("atmosphere") RequestBody atmosphere,
            @Part("first_music_genre") RequestBody first_music_genre,
            @Part("second_music_genre") RequestBody second_music_genre,
            @Part("third_music_genre") RequestBody third_music_genre,
            @Part("cuisine") RequestBody cuisine,
            @Part("kids_area") RequestBody kids_area,
            @Part("handicapped_entrance") RequestBody handicapped_entrance,
            @Part("takeaway") RequestBody takeaway,
            @Part("rest_rooms") RequestBody rest_rooms,
            @Part("price_range") RequestBody price_range,
            @Part("better_for") RequestBody better_for,
            @Part("signature") RequestBody signature,
            @Part("seating") RequestBody seating,
            @Part("parking") RequestBody parking,
            @Part("category") RequestBody category,
            @Part("sub_category") RequestBody sub_category,
            @Part("hours") RequestBody hours,
            @Part("going_on") RequestBody going_on,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("users/authenticateAccount")
    Call<BaseResponse> userLogin(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("username") String username,
            @Field("password") String password
    );

    @FormUrlEncoded
    @POST("users/authenticateAccountViaOAuth2")
    Call<BaseResponse> social_register(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("access_token") String access_token,
            @Field("oauth2_provider") String oauth2_provider,
            @Field("email") String email,
            @Field("name") String name,
            @Field("image") String image
    );

    @FormUrlEncoded
    @POST("users/oauth2/verify")
    Call<BaseResponse> social_verify(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("access_token") String access_token,
            @Field("oauth2_provider") String oauth2_provider
    );

    @GET("me")
    Call<FacebookObject> get_infos(
            @Query("fields") String fields,
            @Query("access_token") String access_token
    );

    @FormUrlEncoded
    @POST("users/info/update")
    Call<BaseResponse> update_social_data(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("mobile_number") String mobile_number,
            @Field("password") String password,
            @Field("name") String name,
            @Field("email") String email,
            @Field("user_type") String user_type,
            @Field("age") String age,
            @Field("gender") String gender,
            @Field("birthday") String birthday,
            @Field("country_name") String country_name,
            @Field("city_name") String city_name,
            @Field("first_best_cuisines") String first_best_cuisines,
            @Field("second_best_cuisines") String second_best_cuisines,
            @Field("third_best_cuisines") String third_best_cuisines,
            @Field("prefer") String prefer,
            @Field("bio") String bio,
            @Field("website") String website,
            @Field("atmosphere") String atmosphere,
            @Field("first_music_genre") String first_music_genre,
            @Field("second_music_genre") String second_music_genre,
            @Field("third_music_genre") String third_music_genre,
            @Field("cuisine") String cuisine,
            @Field("kids_area") String kids_area,
            @Field("handicapped_entrance") String handicapped_entrance,
            @Field("takeaway") String takeaway,
            @Field("rest_rooms") String rest_rooms,
            @Field("price_range") String price_range,
            @Field("better_for") String better_for,
            @Field("signature") String signature,
            @Field("seating") String seating,
            @Field("parking") String parking,
            @Field("category") String category,
            @Field("sub_category") String sub_category,
            @Field("hours") String hours,
            @Field("going_on") String going_on
    );
}
