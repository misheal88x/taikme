package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dma.app.taikme.Interfaces.FilterListListener;
import com.dma.app.taikme.R;
import com.facebook.drawee.view.SimpleDraweeView;
import com.zomato.photofilters.utils.ThumbnailItem;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ImageFiltersAdapter extends  RecyclerView.Adapter<ImageFiltersAdapter.ViewHolder> {
    private Context context;
    private List<ThumbnailItem> list;
    private FilterListListener listener;
    private int selectedIndex = 0;

    public ImageFiltersAdapter(Context context,List<ThumbnailItem> list,FilterListListener listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private SimpleDraweeView img_image;

        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_status_image);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_circle_status, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final ThumbnailItem thumbnailItem = list.get(position);
        holder.img_image.setImageBitmap(thumbnailItem.image);
        holder.img_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onFilterSelected(thumbnailItem.filter);
                selectedIndex = position;
                notifyDataSetChanged();
            }
        });

        if (selectedIndex == position){

        }
    }
}
