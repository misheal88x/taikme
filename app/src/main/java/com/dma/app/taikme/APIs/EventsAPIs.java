package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface EventsAPIs {
    @GET("events")
    Call<BaseResponse> getAll(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Query("mine") boolean mine,
            @Query("page") int page
    );

    @Multipart
    @POST("events")
    Call<BaseResponse> addEvent(@Header("Accept") String accept,
                                @Header("device_id") String device_id,
                                @Part("business_location_id") RequestBody business_location_id,
                                @Part("ar_title") RequestBody ar_title,
                                @Part("en_title") RequestBody en_title,
                                @Part("event_date") RequestBody event_date,
                                @Part MultipartBody.Part file_image);
    @FormUrlEncoded
    @POST("events/{id}/join")
    Call<BaseResponse> joinEvent(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("id") String id,
            @Field("cancel") int cancel
    );
}
