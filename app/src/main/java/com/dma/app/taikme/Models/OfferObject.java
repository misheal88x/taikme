package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class OfferObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("business_id") private int business_id = 0;
    @SerializedName("business_location_id") private int business_location_id = 0;
    @SerializedName("en_title") private String en_title = "";
    @SerializedName("ar_title") private String ar_title = "";
    @SerializedName("en_description") private String en_description = "";
    @SerializedName("ar_description") private String ar_description = "";
    @SerializedName("expiry_date") private String expiry_date = "";
    @SerializedName("start_date") private String start_date = "";
    @SerializedName("discount") private int discount = 0;
    @SerializedName("image") private String image = "";
    @SerializedName("created_at") private String created_at = "";
    @SerializedName("updated_at") private String updated_at = "";
    @SerializedName("business_name") private String business_name = "";
    @SerializedName("business_mobile_number") private String business_mobile_number = "";
    @SerializedName("business_email") private String business_email = "";
    @SerializedName("business_image") private String business_image = "";
    @SerializedName("business_thumb_image") private String business_thumb_image = "";
    @SerializedName("business_cover_image") private String business_cover_image = "";
    @SerializedName("business_country_name") private String business_country_name = "";
    @SerializedName("business_city_name") private String business_city_name = "";
    @SerializedName("reviews_count") private int reviews_count = 0;
    @SerializedName("reviews_rating") private String reviews_rating = "";
    @SerializedName("following_count") private int following_count = 0;
    @SerializedName("upvoting_count") private int upvoting_count = 0;
    @SerializedName("downvote_count") private int downvote_count = 0;
    @SerializedName("business_avg_review") private String business_avg_review = "";
    @SerializedName("user_vote") private int user_vote = 0;
    @SerializedName("since_minute") private int since_minute = 0;
    @SerializedName("is_following") private int is_following = 0;
    @SerializedName("is_business_following") private int is_business_following = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBusiness_id() {
        return business_id;
    }

    public void setBusiness_id(int business_id) {
        this.business_id = business_id;
    }

    public String getEn_title() {
        return en_title;
    }

    public void setEn_title(String en_title) {
        this.en_title = en_title;
    }

    public String getAr_title() {
        return ar_title;
    }

    public void setAr_title(String ar_title) {
        this.ar_title = ar_title;
    }

    public String getEn_description() {
        return en_description;
    }

    public void setEn_description(String en_description) {
        this.en_description = en_description;
    }

    public String getAr_description() {
        return ar_description;
    }

    public void setAr_description(String ar_description) {
        this.ar_description = ar_description;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBusiness_name() {
        return business_name;
    }

    public void setBusiness_name(String business_name) {
        this.business_name = business_name;
    }

    public String getBusiness_mobile_number() {
        return business_mobile_number;
    }

    public void setBusiness_mobile_number(String business_mobile_number) {
        this.business_mobile_number = business_mobile_number;
    }

    public String getBusiness_email() {
        return business_email;
    }

    public void setBusiness_email(String business_email) {
        this.business_email = business_email;
    }

    public String getBusiness_image() {
        return business_image;
    }

    public void setBusiness_image(String business_image) {
        this.business_image = business_image;
    }

    public String getBusiness_thumb_image() {
        return business_thumb_image;
    }

    public void setBusiness_thumb_image(String business_thumb_image) {
        this.business_thumb_image = business_thumb_image;
    }

    public String getBusiness_cover_image() {
        return business_cover_image;
    }

    public void setBusiness_cover_image(String business_cover_image) {
        this.business_cover_image = business_cover_image;
    }

    public String getBusiness_country_name() {
        return business_country_name;
    }

    public void setBusiness_country_name(String business_country_name) {
        this.business_country_name = business_country_name;
    }

    public String getBusiness_city_name() {
        return business_city_name;
    }

    public void setBusiness_city_name(String business_city_name) {
        this.business_city_name = business_city_name;
    }

    public int getReviews_count() {
        return reviews_count;
    }

    public void setReviews_count(int reviews_count) {
        this.reviews_count = reviews_count;
    }

    public String getReviews_rating() {
        return reviews_rating;
    }

    public void setReviews_rating(String reviews_rating) {
        this.reviews_rating = reviews_rating;
    }

    public int getFollowing_count() {
        return following_count;
    }

    public void setFollowing_count(int following_count) {
        this.following_count = following_count;
    }

    public int getUpvoting_count() {
        return upvoting_count;
    }

    public void setUpvoting_count(int upvoting_count) {
        this.upvoting_count = upvoting_count;
    }

    public int getDownvote_count() {
        return downvote_count;
    }

    public void setDownvote_count(int downvote_count) {
        this.downvote_count = downvote_count;
    }

    public String getBusiness_avg_review() {
        return business_avg_review;
    }

    public void setBusiness_avg_review(String business_avg_review) {
        this.business_avg_review = business_avg_review;
    }

    public int getUser_vote() {
        return user_vote;
    }

    public void setUser_vote(int user_vote) {
        this.user_vote = user_vote;
    }

    public int getSince_minute() {
        return since_minute;
    }

    public void setSince_minute(int since_minute) {
        this.since_minute = since_minute;
    }

    public int getIs_following() {
        return is_following;
    }

    public void setIs_following(int is_following) {
        this.is_following = is_following;
    }

    public int getIs_business_following() {
        return is_business_following;
    }

    public void setIs_business_following(int is_business_following) {
        this.is_business_following = is_business_following;
    }

    public int getBusiness_location_id() {
        return business_location_id;
    }

    public void setBusiness_location_id(int business_location_id) {
        this.business_location_id = business_location_id;
    }
}
