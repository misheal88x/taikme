package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Models.AdObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdsManagerAdapter extends RecyclerView.Adapter<AdsManagerAdapter.ViewHolder>{

    private Context context;
    private List<AdObject> list;
    private IMove iMove;

    public AdsManagerAdapter(Context context,List<AdObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_name,tv_date;
        private ImageView btn_explore;
        private RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.item_ads_manager_name);
            tv_date = view.findViewById(R.id.item_ads_manager_date);
            btn_explore = view.findViewById(R.id.item_ads_manager_explore);
            layout =view.findViewById(R.id.item_ads_manager_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ads_manager, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        AdObject ao = list.get(position);
        String language = SharedPrefManager.getInstance(context).getSettings().getView_language();
        if (language.equals("ar")) {
            holder.tv_name.setText(ao.getAr_name());
        }else {
            holder.tv_name.setText(ao.getEn_name());
        }
        holder.tv_date.setText(BaseFunctions.dateExtractor(ao.getCreated_at()));
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
        /*
        holder.btn_explore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iMove.move(position);
            }
        });
**/
    }
}
