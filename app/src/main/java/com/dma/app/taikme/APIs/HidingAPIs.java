package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.rest.Head;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface HidingAPIs {

    @FormUrlEncoded
    @POST("hide/{type}/{value}")
    Call<BaseResponse> hide(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("type") String type,
            @Path("value") String value,
            @Field("type_owner_id") int type_owner_id,
            @Field("type_id") int type_id
    );
}
