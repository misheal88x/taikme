package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface HomeAPIs {
    @POST("home")
    Call<BaseResponse> get_home(
            @Header("Accept") String accept,
            @Header("device_id") String device_i,
            @Query("page") int page
    );

    @FormUrlEncoded
    @POST("home")
    Call<BaseResponse> get_hashtag(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("keyword") String keyword,
            @Query("page") int page
    );
}
