package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.APIsClass.PlacesAPIsClass;
import com.dma.app.taikme.Adapters.PeopleToFollowAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.PeopleToFollowObject;
import com.dma.app.taikme.Models.PeopleToFollowResponse;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class PeopleToFollowActivity extends BaseActivity {
    private RelativeLayout toolbar;
    private ImageView img_icon;
    private TextView tv_title;
    private RecyclerView rv_people;
    private PeopleToFollowAdapter adapter;
    private List<PeopleToFollowObject> list;
    private LinearLayoutManager layoutManager;
    private LinearLayout no_data;
    private NestedScrollView scrollView;
    private AVLoadingIndicatorView pb_more;
    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;
    private RelativeLayout root;

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_people_to_follow);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        tv_title.setText(getResources().getString(R.string.people_to_follow_title));
        img_icon.setImageResource(R.drawable.ic_more_people);
        init_recycler();
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = findViewById(R.id.people_to_follow_toolbar);
        img_icon = toolbar.findViewById(R.id.toolbar_icon);
        tv_title = toolbar.findViewById(R.id.toolbar_title);
        //Recycler View
        rv_people = findViewById(R.id.people_to_follow_recycler);
        //LinearLayout
        no_data = findViewById(R.id.no_data_layout);
        //Nested Scroll View
        scrollView = findViewById(R.id.scroll_view);
        //Progress bar
        pb_more = findViewById(R.id.more);
        //RelativeLayout
        root = findViewById(R.id.layout);
    }

    @Override
    public void init_events() {
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (list.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callPeopleAPI(currentPage,1);
                            }
                        }
                    }
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    private void init_recycler(){
        list = new ArrayList<>();
        adapter = new PeopleToFollowAdapter(PeopleToFollowActivity.this,list);
        layoutManager = new LinearLayoutManager(PeopleToFollowActivity.this,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_people.setLayoutManager(layoutManager);
        BaseFunctions.runAnimation(rv_people,0,adapter);
        rv_people.setAdapter(adapter);
        callPeopleAPI(currentPage,0);
    }

    private void callPeopleAPI(final int page,final int type){
        if (type == 1){
            pb_more.smoothToShow();
        }
        FollowersAPIsClass.getPeople(
                PeopleToFollowActivity.this,
                BaseFunctions.getDeviceId(PeopleToFollowActivity.this),
                page,
                type,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            PeopleToFollowResponse success = new Gson().fromJson(j,PeopleToFollowResponse.class);
                            if (success.getData()!=null){
                                if (success.getData().size()>0){
                                    process_data(type);
                                    per_page = success.getPer_page();
                                    for (PeopleToFollowObject po : success.getData()){
                                        list.add(po);
                                        adapter.notifyDataSetChanged();
                                    }
                                }else {
                                    no_data(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callPeopleAPI(page,type);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            pb_more.smoothToHide();
        }else {
            no_data.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }
}
