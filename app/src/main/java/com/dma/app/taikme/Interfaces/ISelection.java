package com.dma.app.taikme.Interfaces;

public interface ISelection {
    void onTakeCameraClicked();
    void onTakeVideoClicked();
    void onCameraGalleryClicked();
    void onVideoGalleryClicked();
}
