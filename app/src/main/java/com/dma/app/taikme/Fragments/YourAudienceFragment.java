package com.dma.app.taikme.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.dma.app.taikme.APIsClass.CustomAudienceAPIsClass;
import com.dma.app.taikme.Activities.RegisterActivity;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Models.CuisineObject;
import com.dma.app.taikme.Models.LebanonCityObject;
import com.dma.app.taikme.Models.LocationObject;
import com.dma.app.taikme.Models.MusicGenderObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Models.StartupObject;
import com.dma.app.taikme.Others.BaseFragment;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class YourAudienceFragment extends BaseFragment {

    private LineChart cht_age,cht_locations,cht_active;
    private LinearLayout root;

    private EditText edt_name;
    private Spinner gender_spinner,locations_spinner,prefer_spinner,character_spinner
            ,usually_spinner,cuisine_spinner,second_cuisine_spinner,third_cuisine_spinner
            ,music_spinner,second_music_spinner,third_music_spinner,follow_spinner;
    private List<String> gender_list_string,locations_list_string,prefer_list_string,character_list_string
            ,usually_list_string,cuisine_list_string,second_cuisine_list_string,third_cuisine_list_string
            ,music_list_string,second_music_list_string,third_music_list_string,follow_list_string;
    private List<StartupItemObject> usually_list;
    private List<StartupItemObject> gender_list;
    private List<LebanonCityObject> locations_list;
    private List<StartupItemObject> prefer_list;
    private List<StartupItemObject> cuisine_list,second_cuisine_list,third_cuisine_list;
    private List<StartupItemObject> music_list,second_music_list,third_music_list;
    private List<StartupItemObject> follow_list;
    private List<StartupItemObject> character_list;
    private CustomSpinnerAdapter gender_adapter,locations_adapter,prefer_adapter,character_adapter
            ,usually_adapter,cuisine_adapter,second_cuisine_adapter,third_cuisine_adapter
            ,music_adapter,second_music_adapter,third_music_adapter,follow_adapter;

    private Button btn_save;
    private CrystalRangeSeekbar rb_range;
    private TextView tv_min,tv_max;

    private String selected_gender = "";
    private int selected_location = -1;
    private int selected_min_age = -1;
    private int selected_max_age = -1;
    private String selected_prefer = "";
    private String selected_character = "";
    private String selected_going_out = "";
    private String selected_cuisine = "";
    private String selected_second_cuisine = "";
    private String selected_third_cuisine = "";
    private String selected_music = "";
    private String selected_second_music = "";
    private String selected_third_music = "";
    private String selected_followers = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_your_audience,container,false);
    }

    @Override
    public void init_views() {
        //LinearLayout
        root = base.findViewById(R.id.your_audience_layout);
        //LineChart
        cht_age = base.findViewById(R.id.your_audience_age_chart);
        cht_locations = base.findViewById(R.id.your_audience_locations_chart);
        cht_active = base.findViewById(R.id.your_audience_hours_chart);
        //EditView
        edt_name = base.findViewById(R.id.your_audience_name);
        //Spinner
        gender_spinner = base.findViewById(R.id.your_audience_gender_spinner);
        locations_spinner = base.findViewById(R.id.your_audience_locations_spinner);
        prefer_spinner = base.findViewById(R.id.your_audience_prefer_spinner);
        character_spinner = base.findViewById(R.id.your_audience_character_spinner);
        usually_spinner = base.findViewById(R.id.your_audience_usually_spinner);
        cuisine_spinner = base.findViewById(R.id.your_audience_cuisine_spinner);
        second_cuisine_spinner = base.findViewById(R.id.your_audience_second_cuisine_spinner);
        third_cuisine_spinner = base.findViewById(R.id.your_audience_third_cuisine_spinner);
        music_spinner = base.findViewById(R.id.your_audience_music_spinner);
        second_music_spinner = base.findViewById(R.id.your_audience_second_music_spinner);
        third_music_spinner = base.findViewById(R.id.your_audience_third_music_spinner);
        follow_spinner = base.findViewById(R.id.your_audience_follow_spinner);
        //Button
        btn_save = base.findViewById(R.id.your_audience_save);
        //CrystalRangeSeek
        rb_range = base.findViewById(R.id.your_audience_age_range);
        //TextView
        tv_min = base.findViewById(R.id.your_audience_min_value);
        tv_max = base.findViewById(R.id.your_audience_max_value);
    }

    @Override
    public void init_events() {

        gender_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent, position);
                if (p > 0){
                    selected_gender = gender_list.get(p).getId();
                }else {
                    selected_gender = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        locations_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent, position);
                if (p > 0) {
                    selected_location = locations_list.get(p).getId();
                } else {
                    selected_location = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        prefer_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent, position);
                if (p == 0) {
                    selected_prefer = "";
                } else {
                    selected_prefer = prefer_list.get(p).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        character_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent, position);
                if (p > 0) {
                    selected_character = character_list.get(position).getId();
                } else {
                    selected_character = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        usually_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent, position);
                if (p > 0) {
                    selected_going_out = usually_list.get(p).getId();
                } else {
                    selected_going_out = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cuisine_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent, p);
                if (position > 0) {
                    if (!selected_third_cuisine.equals(cuisine_list.get(position).getId())&&
                            !selected_second_cuisine.equals(cuisine_list.get(position).getId())) {
                        selected_cuisine = cuisine_list.get(position).getId();
                    }else {
                        Toast.makeText(base, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_cuisine = "";
                        cuisine_spinner.setSelection(0);
                    }
                } else {
                    selected_cuisine = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        second_cuisine_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent, p);
                if (position > 0) {
                    if (!selected_cuisine.equals(second_cuisine_list.get(position).getId())&&
                            !selected_third_cuisine.equals(second_cuisine_list.get(position).getId())) {
                        selected_second_cuisine = second_cuisine_list.get(position).getId();
                    }else {
                        Toast.makeText(base, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_second_cuisine = "";
                        second_cuisine_spinner.setSelection(0);
                    }
                } else {
                    selected_second_cuisine = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        third_cuisine_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent, p);
                if (position > 0) {
                    if (!selected_cuisine.equals(third_cuisine_list.get(position).getId())&&
                            !selected_second_cuisine.equals(third_cuisine_list.get(position).getId())) {
                        selected_third_cuisine = third_cuisine_list.get(position).getId();
                    }else {
                        Toast.makeText(base, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_third_cuisine = "";
                        third_cuisine_spinner.setSelection(0);
                    }
                } else {
                    selected_third_cuisine = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent, p);
                if (position > 0) {
                    if (!selected_third_music.equals(music_list.get(position).getId())&&
                            !selected_second_music.equals(music_list.get(position).getId())) {
                        selected_music = music_list.get(position).getId();
                    }else {
                        Toast.makeText(base, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_music = "";
                        music_spinner.setSelection(0);
                    }
                } else {
                    selected_music = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        second_music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent, p);
                if (position > 0) {
                    if (!selected_music.equals(second_music_list.get(position).getId())&&
                            !selected_third_music.equals(second_music_list.get(position).getId())) {
                        selected_second_music = second_music_list.get(position).getId();
                    }else {
                        Toast.makeText(base, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_second_music = "";
                        second_music_spinner.setSelection(0);
                    }
                } else {
                    selected_second_music = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        third_music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent, p);
                if (position > 0) {
                    if (!selected_music.equals(third_music_list.get(position).getId())&&
                            !selected_second_music.equals(third_music_list.get(position).getId())) {
                        selected_third_music = third_music_list.get(position).getId();
                    }else {
                        Toast.makeText(base, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_third_music = "";
                        third_music_spinner.setSelection(0);
                    }
                } else {
                    selected_third_music = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        follow_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent, position);
                if (p > 0) {
                    selected_followers = follow_list.get(p).getId();
                } else {
                    selected_followers = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        try {
        rb_range.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tv_min.setText(String.valueOf(minValue));
                tv_max.setText(String.valueOf(maxValue));
                selected_min_age = Integer.valueOf(String.valueOf(minValue));
                selected_max_age = Integer.valueOf(String.valueOf(maxValue));
            }
        });
        }catch(Exception e){
            Log.i("eerr", "instance initializer: "+e.getMessage());
        }
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_name.getText().toString().equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_name), Toast.LENGTH_SHORT).show();
                    edt_name.requestFocus();
                    return;
                }
                if (selected_gender.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_gender), Toast.LENGTH_SHORT).show();
                    gender_spinner.requestFocus();
                    return;
                }
                if (selected_location == -1) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_location), Toast.LENGTH_SHORT).show();
                    locations_spinner.requestFocus();
                    return;
                }
                if (selected_min_age == -1) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_min_val), Toast.LENGTH_SHORT).show();
                    rb_range.requestFocus();
                    return;
                }
                if (selected_max_age == -1) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_max_val), Toast.LENGTH_SHORT).show();
                    rb_range.requestFocus();
                    return;
                }
                if (selected_prefer.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_prefer), Toast.LENGTH_SHORT).show();
                    prefer_spinner.requestFocus();
                    return;
                }
                if (selected_character.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_character), Toast.LENGTH_SHORT).show();
                    character_spinner.requestFocus();
                    return;
                }
                if (selected_going_out.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_usually), Toast.LENGTH_SHORT).show();
                    usually_spinner.requestFocus();
                    return;
                }
                if (selected_cuisine.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_cuisine), Toast.LENGTH_SHORT).show();
                    cuisine_spinner.requestFocus();
                    return;
                }
                if (selected_second_cuisine.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_second_cuisine), Toast.LENGTH_SHORT).show();
                    second_cuisine_spinner.requestFocus();
                    return;
                }
                if (selected_third_cuisine.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_third_cuisine), Toast.LENGTH_SHORT).show();
                    third_cuisine_spinner.requestFocus();
                    return;
                }
                if (selected_music.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_music), Toast.LENGTH_SHORT).show();
                    music_spinner.requestFocus();
                    return;
                }
                if (selected_second_music.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_second_music), Toast.LENGTH_SHORT).show();
                    second_music_spinner.requestFocus();
                    return;
                }
                if (selected_third_music.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_third_music), Toast.LENGTH_SHORT).show();
                    third_music_spinner.requestFocus();
                    return;
                }
                if (selected_followers.equals("")) {
                    Toast.makeText(base, getResources().getString(R.string.your_audience_no_follow), Toast.LENGTH_SHORT).show();
                    follow_spinner.requestFocus();
                    return;
                }
                callAddAudienceAPI();
            }
        });

    }

    @Override
    public void init_fragment(Bundle savedInstanceState) {
        init_charts();
        init_spinners();
    }

    private void init_charts(){
        List<Float> up_list = new ArrayList<>();
        up_list.add(50f);
        up_list.add(20f);
        up_list.add(60f);
        up_list.add(30f);
        up_list.add(40f);
        up_list.add(50f);
        up_list.add(60f);
        setupChart(cht_age,up_list,100);
        setupChart(cht_locations,up_list,100);
        setupChart(cht_active,up_list,100);
    }

    private void init_spinners(){
        StartupObject so = SharedPrefManager.getInstance(base).getStartUp();
        String language = SharedPrefManager.getInstance(base).getSettings().getView_language();

        gender_list_string = new ArrayList<>();
        gender_list = new ArrayList<>();
        locations_list_string = new ArrayList<>();
        locations_list = new ArrayList<>();
        prefer_list_string = new ArrayList<>();
        prefer_list = new ArrayList<>();
        character_list_string = new ArrayList<>();
        character_list = new ArrayList<>();
        usually_list_string = new ArrayList<>();
        usually_list = new ArrayList<>();
        cuisine_list_string = new ArrayList<>();
        cuisine_list = new ArrayList<>();
        second_cuisine_list_string = new ArrayList<>();
        second_cuisine_list = new ArrayList<>();
        third_cuisine_list_string = new ArrayList<>();
        third_cuisine_list= new ArrayList<>();
        music_list_string = new ArrayList<>();
        second_music_list_string = new ArrayList<>();
        third_music_list_string = new ArrayList<>();
        music_list = new ArrayList<>();
        second_music_list = new ArrayList<>();
        third_music_list = new ArrayList<>();
        follow_list_string = new ArrayList<>();
        follow_list = new ArrayList<>();

        gender_list_string.add(getResources().getString(R.string.your_audience_gender));
        gender_list.add(new StartupItemObject());
        if (so.getGenders()!=null && so.getGenders().size()>0){
            for (StartupItemObject sio : so.getGenders()){
                gender_list.add(sio);
                gender_list_string.add(sio.getName());
            }
        }

        locations_list_string.add(getResources().getString(R.string.your_audience_locations));
        locations_list.add(new LebanonCityObject());
        if (so.getLebanon_cities()!=null){
            if (so.getLebanon_cities().size()>0){
                for (LebanonCityObject lo : so.getLebanon_cities()){
                    locations_list.add(lo);
                    if (language.equals("en")){
                        locations_list_string.add(lo.getEn_name());
                    }else {
                        locations_list_string.add(lo.getAr_name());
                    }
                }
            }
        }

        prefer_list_string.add(getResources().getString(R.string.your_audience_prefer));
        prefer_list.add(new StartupItemObject());
        if (so.getPrefer()!=null && so.getPrefer().size()>0){
            for (StartupItemObject sio : so.getPrefer()){
                prefer_list.add(sio);
                prefer_list_string.add(sio.getName());
            }
        }

        character_list_string.add(getResources().getString(R.string.your_audience_character));
        character_list.add(new StartupItemObject());
        if (so.getMy_place_better_for()!=null){
            if (so.getMy_place_better_for().size()>0){
                for (StartupItemObject s : so.getMy_place_better_for()){
                    character_list_string.add(s.getName());
                    character_list.add(s);
                }
            }
        }

        usually_list_string.add(getResources().getString(R.string.your_audience_usually));
        usually_list.add(new StartupItemObject());
        if (so.getGoing_on()!=null && so.getGoing_on().size()>0){
            for (StartupItemObject sio : so.getGoing_on()){
                usually_list.add(sio);
                usually_list_string.add(sio.getName());
            }
        }

        cuisine_list_string.add(getResources().getString(R.string.your_audience_cuisine));
        cuisine_list.add(new StartupItemObject());
        if (so.getCuisine_definitions()!=null){
            if (so.getCuisine_definitions().size()>0){
                for (StartupItemObject co : so.getCuisine_definitions()){
                    cuisine_list.add(co);
                    cuisine_list_string.add(co.getName());
                }
            }
        }

        second_cuisine_list_string.add(getResources().getString(R.string.your_audience_second_cuisine));
        second_cuisine_list.add(new StartupItemObject());
        if (so.getCuisine_definitions()!=null){
            if (so.getCuisine_definitions().size()>0){
                for (StartupItemObject co : so.getCuisine_definitions()){
                    second_cuisine_list.add(co);
                    second_cuisine_list_string.add(co.getName());
                }
            }
        }

        third_cuisine_list_string.add(getResources().getString(R.string.your_audience_third_cuisine));
        third_cuisine_list.add(new StartupItemObject());
        if (so.getCuisine_definitions()!=null){
            if (so.getCuisine_definitions().size()>0){
                for (StartupItemObject co : so.getCuisine_definitions()){
                    third_cuisine_list.add(co);
                    third_cuisine_list_string.add(co.getName());
                }
            }
        }

        music_list_string.add(getResources().getString(R.string.your_audience_music));
        music_list.add(new StartupItemObject());
        if (so.getMusic_gender_type()!=null){
            if (so.getMusic_gender_type().size()>0){
                for (StartupItemObject mo : so.getMusic_gender_type()){
                    music_list.add(mo);
                    music_list_string.add(mo.getName());
                }
            }
        }

        second_music_list_string.add(getResources().getString(R.string.your_audience_second_music));
        second_music_list.add(new StartupItemObject());
        if (so.getMusic_gender_type()!=null){
            if (so.getMusic_gender_type().size()>0){
                for (StartupItemObject mo : so.getMusic_gender_type()){
                    second_music_list.add(mo);
                    second_music_list_string.add(mo.getName());
                }
            }
        }

        third_music_list_string.add(getResources().getString(R.string.your_audience_third_music));
        third_music_list.add(new StartupItemObject());
        if (so.getMusic_gender_type()!=null){
            if (so.getMusic_gender_type().size()>0){
                for (StartupItemObject mo : so.getMusic_gender_type()){
                    third_music_list.add(mo);
                    third_music_list_string.add(mo.getName());
                }
            }
        }

        follow_list_string.add(getResources().getString(R.string.your_audience_follow));
        follow_list.add(new StartupItemObject());
        if (so.getFollow_you()!=null && so.getFollow_you().size()>0){
            for (StartupItemObject sio : so.getFollow_you()){
                follow_list.add(sio);
                follow_list_string.add(sio.getName());
            }
        }

        BaseFunctions.init_spinner(base,gender_spinner,gender_adapter,gender_list_string);
        BaseFunctions.init_spinner(base,locations_spinner,locations_adapter,locations_list_string);
        BaseFunctions.init_spinner(base,prefer_spinner,prefer_adapter,prefer_list_string);
        BaseFunctions.init_spinner(base,character_spinner,character_adapter,character_list_string);
        BaseFunctions.init_spinner(base,usually_spinner,usually_adapter,usually_list_string);
        BaseFunctions.init_spinner(base,cuisine_spinner,cuisine_adapter,cuisine_list_string);
        BaseFunctions.init_spinner(base,second_cuisine_spinner,second_cuisine_adapter,second_cuisine_list_string);
        BaseFunctions.init_spinner(base,third_cuisine_spinner,third_cuisine_adapter,third_cuisine_list_string);
        BaseFunctions.init_spinner(base,music_spinner,music_adapter,music_list_string);
        BaseFunctions.init_spinner(base,second_music_spinner,second_music_adapter,second_music_list_string);
        BaseFunctions.init_spinner(base,third_music_spinner,third_music_adapter,third_music_list_string);
        BaseFunctions.init_spinner(base,follow_spinner,follow_adapter,follow_list_string);
    }

    public class MyXAxisValueFormater extends ValueFormatter {
        public String[] mValues;

        public MyXAxisValueFormater(String[] mValues) {
            this.mValues = mValues;
        }

        @Override
        public String getFormattedValue(float value) {
            return mValues[(int)value];
        }
    }

    private void setupChart(LineChart chart, List<Float> data,int maxvalue){
        chart.setDragEnabled(true);
        chart.setScaleEnabled(false);
        ArrayList<Entry> yValues = new ArrayList<>();
        int index = 0;
        for (Float f : data){
            yValues.add(new Entry(index++,f));
        }
        LineDataSet set1 = new LineDataSet(yValues,"Data set 1");
        //Max value of Y Axis
        set1.setFillAlpha(maxvalue);
        set1.setDrawCircles(false);
        set1.setDrawValues(false);
        set1.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        //Line color
        set1.setColor(getResources().getColor(R.color.colorPrimary));
        //Line width
        set1.setLineWidth(1f);
        chart.getAxisRight().setEnabled(false);
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        LineData lineData = new LineData(dataSets);
        chart.setData(lineData);

        String[] days = new String[]{"Mon","Tue","Wed","Thu","Fri","Sat","Sun"};
        XAxis xAxis = chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setValueFormatter(new MyXAxisValueFormater(days));
        xAxis.setGranularity(1);
        xAxis.setDrawGridLines(false);
        xAxis.setTextColor(getResources().getColor(R.color.dark_grey));
        xAxis.setTextSize(12f);
        xAxis.setAxisLineColor(getResources().getColor(R.color.black));
        xAxis.setAxisLineWidth(1f);

        YAxis yAxis = chart.getAxisLeft();
        yAxis.setTextColor(getResources().getColor(R.color.dark_grey));
        yAxis.setTextSize(14f);
        yAxis.setAxisLineColor(getResources().getColor(R.color.black));
        yAxis.setAxisLineWidth(1f);
        chart.setGridBackgroundColor(getResources().getColor(R.color.light_grey));

        Description description = new Description();
        description.setEnabled(false);
        chart.setDescription(description);

        Legend legend = chart.getLegend();
        legend.setEnabled(false);

        chart.animateX(1000);
    }

    private void callAddAudienceAPI(){
        CustomAudienceAPIsClass.create(
                base,
                BaseFunctions.getDeviceId(base),
                edt_name.getText().toString(),
                edt_name.getText().toString(),
                selected_gender,
                selected_location,
                selected_prefer,
                selected_character,
                selected_min_age,
                selected_max_age,
                selected_going_out,
                selected_cuisine,
                selected_second_cuisine,
                selected_third_cuisine,
                selected_music,
                selected_second_music,
                selected_third_music,
                selected_followers,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Snackbar.make(root, getResources().getString(R.string.error_occurred), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAddAudienceAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            Toast.makeText(base, getResources().getString(R.string.your_audience_success), Toast.LENGTH_SHORT).show();
                            base.open_fragment(new CustomAudiencesFragment());
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callAddAudienceAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                });
    }
}
