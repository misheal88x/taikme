package com.dma.app.taikme.Interfaces;

public interface IAddEvent {
    void onDateClicked();
    void onTimeClicked();
    void onImageClicked();
    void onEventAdded();
}
