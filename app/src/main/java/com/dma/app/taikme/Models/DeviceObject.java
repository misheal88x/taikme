package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class DeviceObject {
    @SerializedName("device_id") private String device_id = "";
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("platform") private String platform = "";
    @SerializedName("device_token") private String device_token = "";
    @SerializedName("fcm_token") private String fcm_token = "";
    @SerializedName("need_login") private int need_login = 0;
    @SerializedName("device_status") private int device_status = 0;

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getFcm_token() {
        return fcm_token;
    }

    public void setFcm_token(String fcm_token) {
        this.fcm_token = fcm_token;
    }

    public int getNeed_login() {
        return need_login;
    }

    public void setNeed_login(int need_login) {
        this.need_login = need_login;
    }

    public int getDevice_status() {
        return device_status;
    }

    public void setDevice_status(int device_status) {
        this.device_status = device_status;
    }
}
