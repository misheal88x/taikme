package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dma.app.taikme.Models.CommentObject;
import com.dma.app.taikme.Models.ReviewObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class AllCommentsAdapter extends RecyclerView.Adapter<AllCommentsAdapter.ViewHolder> {
    private Context context;
    private List<CommentObject> list;

    public AllCommentsAdapter(Context context,List<CommentObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView img_profile_image;
        private TextView tv_username,tv_address,tv_time,tv_text;
        private SimpleRatingBar rb_rate;

        public ViewHolder(View view) {
            super(view);
            img_profile_image = view.findViewById(R.id.item_facility_info_reviews_profile_image);
            tv_username = view.findViewById(R.id.item_facility_info_reviews_user_name);
            tv_address = view.findViewById(R.id.item_facility_info_reviews_address);
            tv_time = view.findViewById(R.id.item_facility_info_reviews_time);
            tv_text = view.findViewById(R.id.item_facility_info_reviews_text);
            rb_rate = view.findViewById(R.id.item_facility_info_reviews_rate);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_facility_info_reviews, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {

        CommentObject o = list.get(position);

        BaseFunctions.setGlideImage(context,holder.img_profile_image,o.getUser_thumb_image());
        holder.tv_username.setText(o.getUser_name()!=null?o.getUser_name():"");
        holder.tv_time.setText(o.getCreated_at()!=null?BaseFunctions.processDate(context,o.getCreated_at()):"");
        holder.tv_text.setText(o.getComment_text()!=null?o.getComment_text():"");
        holder.rb_rate.setVisibility(View.GONE);
        holder.tv_address.setVisibility(View.GONE);
    }
}
