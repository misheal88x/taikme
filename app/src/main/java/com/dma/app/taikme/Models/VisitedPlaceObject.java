package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class VisitedPlaceObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("mobile_number") private String mobile_number = "";
    @SerializedName("image") private String image = "";
    @SerializedName("thumb_image") private String thumb_image = "";
    @SerializedName("cover_image") private String cover_image = "";
    @SerializedName("price_range") private float price_range = 0f;
    @SerializedName("business_avg_review") private String business_avg_review = "";
    @SerializedName("avg_review") private String avg_review = "";
    @SerializedName("user_type") private String user_type = "";
    @SerializedName("is_following") private int is_following = 0;
    @SerializedName("business_locations") private List<BusinessLocationObject> business_locations = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumb_image() {
        return thumb_image;
    }

    public void setThumb_image(String thumb_image) {
        this.thumb_image = thumb_image;
    }

    public String getCover_image() {
        return cover_image;
    }

    public void setCover_image(String cover_image) {
        this.cover_image = cover_image;
    }

    public float getPrice_range() {
        return price_range;
    }

    public void setPrice_range(float price_range) {
        this.price_range = price_range;
    }

    public String getBusiness_avg_review() {
        return business_avg_review;
    }

    public void setBusiness_avg_review(String business_avg_review) {
        this.business_avg_review = business_avg_review;
    }

    public List<BusinessLocationObject> getBusiness_locations() {
        return business_locations;
    }

    public void setBusiness_locations(List<BusinessLocationObject> business_locations) {
        this.business_locations = business_locations;
    }

    public String getAvg_review() {
        return avg_review;
    }

    public void setAvg_review(String avg_review) {
        this.avg_review = avg_review;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public int getIs_following() {
        return is_following;
    }

    public void setIs_following(int is_following) {
        this.is_following = is_following;
    }
}
