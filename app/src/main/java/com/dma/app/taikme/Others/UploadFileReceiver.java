package com.dma.app.taikme.Others;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.dma.app.taikme.R;

import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadServiceBroadcastReceiver;

public class UploadFileReceiver extends UploadServiceBroadcastReceiver {
    @Override
    public void onProgress(Context context, UploadInfo uploadInfo) {
        Log.i("upload_status", "onProgress: "+"progress");
    }

    @Override
    public void onError(Context context, UploadInfo uploadInfo, ServerResponse serverResponse, Exception exception) {
        Log.i("upload_status", "onProgress: "+"error");
    }

    @Override
    public void onCompleted(Context context, UploadInfo uploadInfo, ServerResponse serverResponse) {
        Log.i("upload_status", "onProgress: "+"completed");
        try{
            Toast.makeText(context, context.getResources().getString(R.string.add_success), Toast.LENGTH_SHORT).show();
            BaseFunctions.playMusic(context,R.raw.added);
        }catch (Exception e){}

    }

    @Override
    public void onCancelled(Context context, UploadInfo uploadInfo) {
        Log.i("upload_status", "onProgress: "+"canceled");
    }
}
