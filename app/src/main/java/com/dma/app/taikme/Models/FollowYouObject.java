package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class FollowYouObject {
    @SerializedName("followers") private int followers = 0;
    @SerializedName("not_followers") private int not_followers = 0;
    @SerializedName("both") private int both = 0;

    public int getFollowers() {
        return followers;
    }

    public void setFollowers(int followers) {
        this.followers = followers;
    }

    public int getNot_followers() {
        return not_followers;
    }

    public void setNot_followers(int not_followers) {
        this.not_followers = not_followers;
    }

    public int getBoth() {
        return both;
    }

    public void setBoth(int both) {
        this.both = both;
    }
}
