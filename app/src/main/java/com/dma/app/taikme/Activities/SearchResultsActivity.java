package com.dma.app.taikme.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.SearchAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Adapters.SearchResultsAdapter;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.CategoryObject;
import com.dma.app.taikme.Models.CuisineObject;
import com.dma.app.taikme.Models.HomeObject;
import com.dma.app.taikme.Models.MusicGenderObject;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.PlacesResponse;
import com.dma.app.taikme.Models.SearchObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Models.StartupObject;
import com.dma.app.taikme.Models.VisitedPlaceObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsActivity extends BaseActivity implements IMove {

    private RelativeLayout toolbar;
    private ImageView btn_home;
    private EditText edt_search;
    private RecyclerView rv_results;
    private List<VisitedPlaceObject> listOfPlaces;
    private SearchResultsAdapter adapter;
    private LinearLayoutManager layoutManager;
    private NestedScrollView scrollView;
    private RelativeLayout root;
    private LinearLayout no_data_layout;
    private AVLoadingIndicatorView pb_more;
    private int currentPage = 1;
    private boolean continue_paginate = true;
    private int per_page = 20;
    private float clicked_lat = 0f;
    private float clicked_lng = 0f;


    //Filter dialog
    private Animation fadeIn,fadeOut;
    private boolean is_filter_dialog_shown = false;
    private ImageView btn_filter;
    private CardView filter_layout;
    private Spinner category_spinner,subCategory_spinner,seating_spinner,atmosphere_spinner
            ,music_spinner,cuisine_spinner,kids_spinner,parking_spinner
            ,handicapped_spinner,delivery_spinner,rest_spinner,price_spinner;
    private List<String> category_list_string,subCategory_list_string,seating_list_string,atmosphere_list_string
            ,music_list_string,cuisine_list_string,kids_list_string,parking_list_string
            ,handicapped_list_string,delivery_list_string,rest_list_string,price_list_string;
    private List<CategoryObject> category_list,subCategory_list;
    private List<StartupItemObject> seating_list;
    private List<StartupItemObject> atmosphere_list;
    private List<StartupItemObject> music_list;
    private List<StartupItemObject> cuisine_list;


    private CustomSpinnerAdapter category_adapter,subCategory_adapter,seating_adapter,atmosphere_adapter
            ,music_adapter,cuisine_adapter,kids_adapter,parking_adapter
            ,handicapped_adapter,delivery_adapter,rest_adapter,price_adapter;
    private Button btn_perform_filter;

    private int selected_categoty = -1;
    private int selected_sub_category = -1;
    private String selected_seating = "";
    private String selected_atmosphere = "";
    private String selected_music = "";
    private String selected_cuisine = "";
    private String selected_kids = "";
    private String selected_parking = "";
    private String selected_handicapped = "";
    private String selected_delivery = "";
    private String selected_rest = "";
    private String selected_price = "";

    private String is_filter = "";

    @Override
    public void set_layout() {
        setContentView(R.layout.activity_search_results);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        filter_layout.setVisibility(View.GONE);
        init_recycler();
        edt_search.setText(getIntent().getStringExtra("text"));
        init_spinners();

        callSearchAPI(currentPage,0,edt_search.getText().toString(),"0");
        is_filter = "0";
    }

    @Override
    public void init_views() {
        //Toolbar
        toolbar = findViewById(R.id.search_results_toolbar);
        btn_home = findViewById(R.id.toolbar_home);
        edt_search = toolbar.findViewById(R.id.toolbar_search);
        //RecyclerView
        rv_results = findViewById(R.id.search_results_recycler);
        //NestedScrollView
        scrollView = findViewById(R.id.search_results_scroll);
        //RelativeLayout
        root = findViewById(R.id.search_results_layout);
        //LinearLayout
        no_data_layout = findViewById(R.id.no_data_layout);
        //AVLoadingIndicator
        pb_more = findViewById(R.id.search_results_more);
        //ImageView
        btn_filter = findViewById(R.id.search_results_filter_btn);
        //CardView
        filter_layout = findViewById(R.id.search_results_filter_layout);
        //Spinner
        category_spinner = findViewById(R.id.search_results_category_spinner);
        subCategory_spinner = findViewById(R.id.search_results_sub_category_spinner);
        seating_spinner = findViewById(R.id.search_results_seating_spinner);
        atmosphere_spinner = findViewById(R.id.search_results_atmosphere_spinner);
        music_spinner = findViewById(R.id.search_results_music_spinner);
        cuisine_spinner = findViewById(R.id.search_results_cuisine_spinner);
        kids_spinner = findViewById(R.id.search_results_kids_spinner);
        parking_spinner = findViewById(R.id.search_results_parking_spinner);
        handicapped_spinner = findViewById(R.id.search_results_handicapped_spinner);
        delivery_spinner = findViewById(R.id.search_results_delivery_spinner);
        rest_spinner = findViewById(R.id.search_results_rest_spinner);
        price_spinner = findViewById(R.id.search_results_price_spinner);
        //Button
        btn_perform_filter = findViewById(R.id.search_results_perform_filter_btn);
        //Animation
        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade_out);
    }

    @Override
    public void init_events() {
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchResultsActivity.this,HomeActivity.class);
                intent.putExtra("type","home");
                startActivity(intent);
                finish();
            }
        });
        scrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (listOfPlaces.size()>=per_page){
                            if (continue_paginate){
                                currentPage++;
                                callSearchAPI(currentPage,1,edt_search.getText().toString(),is_filter);
                            }
                        }
                    }
                }
            }
        });
        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (is_filter_dialog_shown){
                    filter_layout.setVisibility(View.GONE);
                    filter_layout.startAnimation(fadeOut);
                    is_filter_dialog_shown = false;
                }else {
                    filter_layout.setVisibility(View.VISIBLE);
                    filter_layout.startAnimation(fadeIn);
                    is_filter_dialog_shown = true;
                }
            }
        });
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (edt_search.getText().toString().equals("")){
                        Toast.makeText(SearchResultsActivity.this, getResources().getString(R.string.search_results_no_text), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    currentPage = 0;
                    continue_paginate = true;
                    callSearchAPI(currentPage,0,edt_search.getText().toString(),"0");
                    is_filter = "0";
                    return true;
                }
                return false;
            }
        });

        category_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                String language = SharedPrefManager.getInstance(SearchResultsActivity.this).getSettings().getView_language();
                if (p>0){
                    selected_categoty = category_list.get(p).getId();
                    CategoryObject co = category_list.get(p);
                    subCategory_list.clear();
                    subCategory_list_string.clear();
                    subCategory_list_string.add(getResources().getString(R.string.search_filter_category));
                    subCategory_list.add(new CategoryObject());
                    subCategory_adapter = new CustomSpinnerAdapter(SearchResultsActivity.this,R.layout.spinner_item,subCategory_list_string);
                    subCategory_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    subCategory_spinner.setAdapter(subCategory_adapter);
                    //BaseFunctions.init_spinner(SearchResultsActivity.this,subCategory_spinner,subCategory_adapter,subCategory_list_string);
                    if (co.getSub_categories()!=null){
                        if (co.getSub_categories().size()>0){
                            for(CategoryObject coo : co.getSub_categories()){
                                subCategory_list.add(coo);
                                if (language.equals("en")){
                                    subCategory_list_string.add(coo.getEn_name());
                                }else {
                                    subCategory_list_string.add(coo.getAr_name());
                                }
                                subCategory_adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }else {
                    selected_categoty = -1;
                    subCategory_list.clear();
                    subCategory_list_string.clear();
                    subCategory_list_string.add(getResources().getString(R.string.search_filter_sub_cat));
                    subCategory_list.add(new CategoryObject());
                    BaseFunctions.init_spinner(SearchResultsActivity.this,subCategory_spinner,subCategory_adapter,subCategory_list_string);
                    subCategory_spinner.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        subCategory_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p > 0){
                    selected_sub_category = subCategory_list.get(p).getId();
                }else {
                    selected_sub_category = -1;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        seating_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_seating = seating_list.get(position).getId();
                }else {
                    selected_seating = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        atmosphere_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_atmosphere = atmosphere_list.get(position).getId();
                }else {
                    selected_atmosphere = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p > 0){
                    selected_music = music_list.get(p).getId();
                }else {
                    selected_music = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        cuisine_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p>0){
                    selected_cuisine = cuisine_list.get(p).getId();
                }else {
                    selected_cuisine = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        kids_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p == 0){
                    selected_kids = "";
                }else if (p == 1){
                    selected_kids = "1";
                }else if (p == 2){
                    selected_kids = "2";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        parking_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p == 0){
                    selected_parking = "";
                }else if (p == 1){
                    selected_parking = "1";
                }else if (p == 2){
                    selected_parking = "2";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        handicapped_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p == 0){
                    selected_handicapped = "";
                }else if (p == 1){
                    selected_handicapped = "1";
                }else if (p == 2){
                    selected_handicapped = "2";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        delivery_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p == 0){
                    selected_delivery = "";
                }else if (p == 1){
                    selected_delivery = "1";
                }else if (p == 2){
                    selected_delivery = "2";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        rest_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p == 0){
                    selected_rest = "";
                }else if (p == 1){
                    selected_rest = "1";
                }else if (p == 2){
                    selected_rest = "2";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        price_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int p = BaseFunctions.getSpinnerPosition(parent,position);
                if (p == 0){
                    selected_price = "";
                }else {
                    selected_price = String.valueOf(price_list_string.get(p).length());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btn_perform_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selected_categoty == -1&&
               selected_sub_category == -1&&
                selected_seating.equals("")&&
                selected_atmosphere.equals("")&&
                selected_music.equals("")&&
                selected_cuisine.equals("")&&
                selected_kids.equals("")&&
                selected_parking.equals("")&&
                selected_handicapped.equals("")&&
                selected_delivery.equals("")&&
                selected_rest.equals("")&&
                selected_price.equals("")){
                    Toast.makeText(SearchResultsActivity.this, getResources().getString(R.string.search_results_no_filter), Toast.LENGTH_SHORT).show();
                }else {
                    category_spinner.setSelection(0);
                    subCategory_spinner.setSelection(0);
                    seating_spinner.setSelection(0);
                    atmosphere_spinner.setSelection(0);
                    music_spinner.setSelection(0);
                    cuisine_spinner.setSelection(0);
                    kids_spinner.setSelection(0);
                    parking_spinner.setSelection(0);
                    handicapped_spinner.setSelection(0);
                    delivery_spinner.setSelection(0);
                    rest_spinner.setSelection(0);
                    price_spinner.setSelection(0);
                    filter_layout.setVisibility(View.GONE);
                    filter_layout.startAnimation(fadeOut);
                    currentPage = 1;
                    continue_paginate = true;
                    callSearchAPI(currentPage,0,edt_search.getText().toString(),"1");
                    is_filter = "1";
                }
            }
        });
    }

    @Override
    public void set_fragment_place() {

    }

    @Override
    public void onBackPressed() {
        if (is_filter_dialog_shown){
            is_filter_dialog_shown = false;
            filter_layout.setVisibility(View.GONE);
            filter_layout.startAnimation(fadeOut);
        }
        super.onBackPressed();

    }

    private void init_spinners(){
        StartupObject sa = SharedPrefManager.getInstance(SearchResultsActivity.this).getStartUp();
        String language = SharedPrefManager.getInstance(SearchResultsActivity.this).getSettings().getView_language();

        category_list_string = new ArrayList<>();
        subCategory_list_string = new ArrayList<>();
        category_list = new ArrayList<>();
        subCategory_list = new ArrayList<>();
        seating_list_string = new ArrayList<>();
        seating_list = new ArrayList<>();
        atmosphere_list_string = new ArrayList<>();
        atmosphere_list = new ArrayList<>();
        music_list_string = new ArrayList<>();
        music_list = new ArrayList<>();
        cuisine_list_string = new ArrayList<>();
        cuisine_list = new ArrayList<>();
        kids_list_string = new ArrayList<>();
        parking_list_string = new ArrayList<>();
        handicapped_list_string = new ArrayList<>();
        delivery_list_string = new ArrayList<>();
        rest_list_string = new ArrayList<>();
        price_list_string = new ArrayList<>();

        category_list_string.add(getResources().getString(R.string.search_filter_category));
        category_list.add(new CategoryObject());
        if (sa.getPlaces_categories()!=null){
            if (sa.getPlaces_categories().size()>0){
                for (CategoryObject co : sa.getPlaces_categories()){
                    category_list.add(co);
                    if (language.equals("en")){
                        category_list_string.add(co.getEn_name());
                    }else {
                        category_list_string.add(co.getAr_name());
                    }
                }
            }
        }
        subCategory_list_string.add(getResources().getString(R.string.search_filter_sub_cat));
        subCategory_list.add(new CategoryObject());

        seating_list_string.add(getResources().getString(R.string.search_filter_seating));
        seating_list.add(new StartupItemObject());
        if (sa.getBusiness_seating()!=null){
            if (sa.getBusiness_seating().size()>0){
                for (StartupItemObject sio : sa.getBusiness_seating()){
                    seating_list_string.add(sio.getName());
                    seating_list.add(sio);
                }
            }
        }

        atmosphere_list_string.add(getResources().getString(R.string.search_filter_atmosphere));
        atmosphere_list.add(new StartupItemObject());
        if (sa.getBusiness_atmosphere()!=null){
            if (sa.getBusiness_atmosphere().size()>0){
                for (StartupItemObject s : sa.getBusiness_atmosphere()){
                    atmosphere_list_string.add(s.getName());
                    atmosphere_list.add(s);
                }
            }
        }

        music_list_string.add(getResources().getString(R.string.search_filter_music));
        music_list.add(new StartupItemObject());
        if (sa.getMusic_gender_type()!=null){
            if (sa.getMusic_gender_type().size()>0){
                for (StartupItemObject mgo : sa.getMusic_gender_type()){
                    music_list.add(mgo);
                    music_list_string.add(mgo.getName());
                }
            }
        }

        cuisine_list_string.add(getResources().getString(R.string.search_filter_cuisine));
        cuisine_list.add(new StartupItemObject());
        if (sa.getCuisine_definitions()!=null){
            if (sa.getCuisine_definitions().size()>0){
                for (StartupItemObject co : sa.getCuisine_definitions()){
                    cuisine_list.add(co);
                    cuisine_list_string.add(co.getName());
                }
            }
        }

        kids_list_string.add(getResources().getString(R.string.search_filter_kids));
        kids_list_string.add(getResources().getString(R.string.yes));
        kids_list_string.add(getResources().getString(R.string.no));

        parking_list_string.add(getResources().getString(R.string.search_filter_parking));
        parking_list_string.add(getResources().getString(R.string.yes));
        parking_list_string.add(getResources().getString(R.string.no));

        handicapped_list_string.add(getResources().getString(R.string.search_filter_entrance));
        handicapped_list_string.add(getResources().getString(R.string.yes));
        handicapped_list_string.add(getResources().getString(R.string.no));

        delivery_list_string.add(getResources().getString(R.string.search_filter_delivery));
        delivery_list_string.add(getResources().getString(R.string.yes));
        delivery_list_string.add(getResources().getString(R.string.no));

        rest_list_string.add(getResources().getString(R.string.search_filter_rest));
        rest_list_string.add(getResources().getString(R.string.yes));
        rest_list_string.add(getResources().getString(R.string.no));

        price_list_string.add(getResources().getString(R.string.search_filter_price));
        price_list_string.add("$");
        price_list_string.add("$$");
        price_list_string.add("$$$");
        price_list_string.add("$$$$");
        price_list_string.add("$$$$$");

        BaseFunctions.init_spinner(SearchResultsActivity.this,category_spinner,category_adapter,category_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,subCategory_spinner,subCategory_adapter,subCategory_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,seating_spinner,seating_adapter,seating_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,atmosphere_spinner,atmosphere_adapter,atmosphere_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,music_spinner,music_adapter,music_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,cuisine_spinner,cuisine_adapter,cuisine_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,kids_spinner,kids_adapter,kids_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,parking_spinner,parking_adapter,parking_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,handicapped_spinner,handicapped_adapter,handicapped_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,delivery_spinner,delivery_adapter,delivery_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,rest_spinner,rest_adapter,rest_list_string);
        BaseFunctions.init_spinner(SearchResultsActivity.this,price_spinner,price_adapter,price_list_string);

    }

    private void init_recycler(){
        listOfPlaces = new ArrayList<>();
        adapter = new SearchResultsAdapter(SearchResultsActivity.this,listOfPlaces,this);
        layoutManager = new LinearLayoutManager(SearchResultsActivity.this,LinearLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollHorizontally() {
                return false;
            }

            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        rv_results.setLayoutManager(layoutManager);
        rv_results.setAdapter(adapter);
    }

    private void callSearchAPI(final int page,final int type,final String word,final String is_advanced){
        if (type == 1){
            pb_more.smoothToShow();
        }
        SearchAPIsClass.search(
                SearchResultsActivity.this,
                BaseFunctions.getDeviceId(SearchResultsActivity.this),
                word,
                page,
                type,
                is_advanced,
                selected_categoty!=-1?String.valueOf(selected_categoty):null,
                selected_sub_category!=-1?String.valueOf(selected_sub_category):null,
                selected_seating.equals("")?null:selected_seating,
                selected_atmosphere.equals("")?null:selected_atmosphere,
                selected_music.equals("")?null:selected_music,
                selected_cuisine.equals("")?null:selected_music,
                selected_kids.equals("")?null:selected_kids,
                selected_parking.equals("")?null:selected_parking,
                selected_handicapped.equals("")?null:selected_handicapped,
                selected_delivery.equals("")?null:selected_delivery,
                selected_rest.equals("")?null:selected_rest,
                selected_price.equals("")?null:selected_price,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        error_happend(type);
                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            PlacesResponse success = new Gson().fromJson(j,PlacesResponse.class);
                            if (type == 0){
                                if (listOfPlaces.size()>0){
                                    listOfPlaces = new ArrayList<>();
                                    rv_results.setAdapter(new SearchResultsAdapter(SearchResultsActivity.this,listOfPlaces,SearchResultsActivity.this));
                                }
                            }
                            if (success!=null){
                                if (success.getData()!=null){
                                    if (success.getData().size()>0){
                                        process_data(type);
                                        per_page = success.getPer_page();
                                        for (VisitedPlaceObject po : success.getData()){
                                            listOfPlaces.add(po);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }else {
                                        no_data(type);
                                    }
                                }else {
                                    error_happend(type);
                                }
                            }else {
                                error_happend(type);
                            }
                        }else {
                            error_happend(type);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        error_happend(type);
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callSearchAPI(page,type,word,is_advanced);
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    private void error_happend(int type){
        if (type == 1){
            pb_more.smoothToHide();
        }else {
            no_data_layout.setVisibility(View.VISIBLE);
        }
    }
    private void process_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.GONE);
            scrollView.setVisibility(View.VISIBLE);
        }else {
            pb_more.smoothToHide();
        }
    }
    private void no_data(int type){
        if (type == 0){
            no_data_layout.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            filter_layout.setVisibility(View.GONE);
        }else {
            pb_more.smoothToHide();
            Snackbar.make(root,getString(R.string.no_more),Snackbar.LENGTH_SHORT).show();
            continue_paginate = false;
        }
    }

    @Override
    public void move() {

    }

    @Override
    public void move(int position) {
        VisitedPlaceObject po = listOfPlaces.get(position);
        if (po.getBusiness_locations()!=null&po.getBusiness_locations().size()>0){
            if (BaseFunctions.isOnline(SearchResultsActivity.this)){
                if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if (po.getBusiness_locations()!=null&&po.getBusiness_locations().size()>0) {
                        clicked_lat = po.getBusiness_locations().get(0).getLat();
                        clicked_lng = po.getBusiness_locations().get(0).getLng();
                    }
                    ActivityCompat.requestPermissions(SearchResultsActivity.this,
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                                    Manifest.permission.ACCESS_FINE_LOCATION},
                            1);
                }else {
                    final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
                    if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                        buildAlertMessageNoGps();
                    }else {
                        if (po.getBusiness_locations()!=null&po.getBusiness_locations().size()>0){
                            Intent intent = new Intent(SearchResultsActivity.this,ShowLocationMapActivity.class);
                            intent.putExtra("lat",po.getBusiness_locations().get(0).getLat());
                            intent.putExtra("lng",po.getBusiness_locations().get(0).getLng());
                            startActivity(intent);
                        }
                    }
                }
            }else {
                Toast.makeText(this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            }
        }else {
            Toast.makeText(this, getResources().getString(R.string.search_results_no_location), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1){
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                final LocationManager manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
                if ( !manager.isProviderEnabled( LocationManager.GPS_PROVIDER ) ) {
                    buildAlertMessageNoGps();
                }else {
                    if (clicked_lat!=0f&&clicked_lat==0f) {
                        Intent intent = new Intent(SearchResultsActivity.this, ShowLocationMapActivity.class);
                        intent.putExtra("lat",clicked_lat);
                        intent.putExtra("lng",clicked_lng);
                        startActivity(intent);
                    }
                }
            }else {
                Toast.makeText(this, "يجب عليك منح سماحية الوصول للموقع قبل المتابعة", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void buildAlertMessageNoGps(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(SearchResultsActivity.this);
        builder.setMessage(getResources().getString(R.string.gps_disabled))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.gps_turn_on), new DialogInterface.OnClickListener() {
                    public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
}
