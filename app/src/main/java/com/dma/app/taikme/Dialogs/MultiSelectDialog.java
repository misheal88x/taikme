package com.dma.app.taikme.Dialogs;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Adapters.MultiSelectAdapter;
import com.dma.app.taikme.Interfaces.IMultiSelect;
import com.dma.app.taikme.Models.MultiSelectObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MultiSelectDialog  extends AlertDialog {
    private Context context;
    private RecyclerView rv_list;
    private List<String> source_list;
    private List<Integer> selected_items;
    private List<MultiSelectObject> l;
    private MultiSelectAdapter adapter;
    private LinearLayoutManager layoutManager;
    private TextView tv_ok;
    private IMultiSelect iMultiSelect;

    public MultiSelectDialog(@NonNull Context context,List<String> source_list,List<Integer> selected_items,IMultiSelect iMultiSelect) {
        super(context);
        this.context = context;
        this.source_list = source_list;
        this.iMultiSelect = iMultiSelect;
        this.selected_items = selected_items;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_multi_select);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        getWindow().setLayout(LinearLayoutCompat.LayoutParams.MATCH_PARENT,
                LinearLayoutCompat.LayoutParams.MATCH_PARENT);
        //getWindow().getAttributes().windowAnimations = R.style.FadeAnimation;
        init_views();
        init_events();
        init_dialog();
    }

    private void init_views(){
        //RecyclerView
        rv_list = findViewById(R.id.multi_select_recycler);
        //TextView
        tv_ok = findViewById(R.id.multi_select_ok);
    }

    private void init_events(){
        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adapter.getCheckedItems()!=null){
                    //if (adapter.getCheckedItems().size()==0){
                    //    Toast.makeText(context, context.getResources().getString(R.string.multi_select_no_items), Toast.LENGTH_SHORT).show();
                    //    return;
                    //}else {
                    for (Integer i : adapter.getCheckedItems()){
                        Log.i("nfhjdvb", "onClick: "+i);
                    }
                        iMultiSelect.onSelected(adapter.getCheckedItems());
                        cancel();
                    //}

                }
            }
        });
    }

    private void init_dialog(){
        init_Recycler();
        if (selected_items!=null){
            if (selected_items.size()>0){
                for (Integer i : selected_items){
                    MultiSelectObject o = l.get(i);
                    o.setIs_checked(true);
                    l.set(i,o);
                    adapter.notifyItemChanged(i);
                }
            }
        }
    }

    private void init_Recycler(){
        l = new ArrayList<>();
        for (String s : source_list){
            MultiSelectObject o = new MultiSelectObject();
            o.setIs_checked(false);
            o.setName(s);
            l.add(o);
        }
        adapter = new MultiSelectAdapter(context,l);
        layoutManager = new LinearLayoutManager(context,RecyclerView.VERTICAL,false);
        rv_list.setLayoutManager(layoutManager);
        rv_list.setAdapter(adapter);
    }

}
