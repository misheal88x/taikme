package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.CommentsAPIsClass;
import com.dma.app.taikme.APIsClass.EventsAPIsClass;
import com.dma.app.taikme.APIsClass.ShareAPIsClass;
import com.dma.app.taikme.APIsClass.VoteAPIsClass;
import com.dma.app.taikme.Activities.AddCommentActivity;
import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Dialogs.AddCommentDialog;
import com.dma.app.taikme.Dialogs.AllCommentsDialog;
import com.dma.app.taikme.Dialogs.AllReviewsDialog;
import com.dma.app.taikme.Dialogs.AmplifyDialog;
import com.dma.app.taikme.Dialogs.SharePostDialog;
import com.dma.app.taikme.Dialogs.ViewImageDialog;
import com.dma.app.taikme.Interfaces.IEditName;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMove;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.DumpPostObject;
import com.dma.app.taikme.Models.EventObject;
import com.dma.app.taikme.Models.ImageObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    private Context context;
    private IMove iMove;
    private List<EventObject> list;

    public EventsAdapter(Context context,List<EventObject> list,IMove iMove) {
        this.context = context;
        this.iMove = iMove;
        this.list = list;
    }
    public class ViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView img_profile_image;
        private RelativeLayout btn_add;
        private TextView tv_username,tv_address,tv_time,tv_desc,tv_see_comments,tv_see_reviews,tv_location,tv_date_and_going;
        private TextView tv_votes_count;
        private TextView edt_comment_user;
        private SimpleRatingBar rb_rate;
        private ImageView btn_up,btn_down,btn_rolling;
        private SimpleDraweeView img_image;
        private RelativeLayout going_layout;
        private ImageView going_image,amplify;
        public ViewHolder(View view) {
            super(view);
            img_profile_image = itemView.findViewById(R.id.item_media_profile_image);
            btn_add = itemView.findViewById(R.id.item_home_event_add_btn);
            tv_username = itemView.findViewById(R.id.item_media_user_name);
            tv_address = itemView.findViewById(R.id.item_media_address);
            tv_time = itemView.findViewById(R.id.item_media_time);
            tv_desc = itemView.findViewById(R.id.item_media_desc);
            edt_comment_user = itemView.findViewById(R.id.item_media_comment_user);
            tv_see_comments = itemView.findViewById(R.id.item_media_read_comments);
            tv_see_reviews = itemView.findViewById(R.id.item_media_read_reviews);
            tv_location = itemView.findViewById(R.id.item_home_event_location);
            tv_date_and_going = itemView.findViewById(R.id.item_home_event_date_and_going);
            tv_votes_count = itemView.findViewById(R.id.votes_count);
            rb_rate = itemView.findViewById(R.id.item_media_rate);
            img_image = itemView.findViewById(R.id.item_media_image);
            btn_up = itemView.findViewById(R.id.item_media_top_arrow);
            btn_down = itemView.findViewById(R.id.item_media_down_arrow);
            btn_rolling = itemView.findViewById(R.id.item_media_rolling_circle);
            going_layout = itemView.findViewById(R.id.going_layout);
            going_image = itemView.findViewById(R.id.going_image);
            amplify = itemView.findViewById(R.id.amplify);

        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home_event, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
       final EventObject dpo = list.get(position);
        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
        //Username
        if (dpo.getBusiness_name()!=null){
            holder.tv_username.setText(dpo.getBusiness_name());
        }
        //Profile image
        if (dpo.getBusiness_thumb_image()!=null){
            BaseFunctions.setFrescoImage(holder.img_profile_image,dpo.getBusiness_thumb_image());
        }
        //Address
        if (dpo.getBusiness_location_address()!=null){
            holder.tv_address.setText(dpo.getBusiness_location_address());
        }
        //UserRate
        if (dpo.getBusiness_avg_review()!=null){
            try {
                holder.rb_rate.setRating(Float.valueOf(dpo.getBusiness_avg_review()));
            }catch (Exception e){}
        }
        //Time
        if (dpo.getCreatedAt()!=null){
            holder.tv_time.setText(BaseFunctions.processDate(context,dpo.getCreatedAt()));
        }
        //Is event mine
        if (dpo.getBusiness_id() == SharedPrefManager.getInstance(context).getUser().getId()){
            holder.amplify.setVisibility(View.VISIBLE);
            holder.going_layout.setVisibility(View.GONE);
            holder.amplify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AmplifyDialog dialog = new AmplifyDialog(context,String.valueOf(dpo.getId()),"event");
                    dialog.show();
                }
            });
        }
        //Event not mine
        else {
            holder.going_layout.setVisibility(View.VISIBLE);
            holder.amplify.setVisibility(View.GONE);
            //Going
            if (dpo.getIs_going() == 0){
                holder.going_layout.setBackgroundResource(R.drawable.primary_circle);
                holder.going_image.setImageResource(R.drawable.ic_plus);
            }else {
                holder.going_layout.setBackgroundResource(R.drawable.grey_circle);
                holder.going_image.setImageResource(R.drawable.ic_minus);
            }
            if (dpo.getDevice_setting()!=null){
                if (dpo.getDevice_setting().getPeople_can_comment_on_posts()==0){
                    holder.edt_comment_user.setVisibility(View.GONE);
                }
                if (dpo.getDevice_setting().getPeople_can_vote_posts() == 0){
                    holder.btn_down.setVisibility(View.GONE);
                    holder.btn_up.setVisibility(View.GONE);
                }
            }
        }

        //Image
        if (dpo.getImage()!=null){
            try {
                BaseFunctions.setFrescoImage(holder.img_image,dpo.getImage());
            }catch (Exception e){}

        }
        //Event title
        if (lan.equals("en")){
            if (dpo.getEn_title()!=null){
                holder.tv_desc.setText(dpo.getEn_title());
            }
        }else if (lan.equals("ar")){
            if (dpo.getAr_title()!=null){
                holder.tv_desc.setText(dpo.getAr_title());
            }else {
                holder.tv_desc.setText(dpo.getEn_title());
            }
        }
        //Event location
        if (dpo.getBusiness_location_address()!=null){
            holder.tv_location.setText(dpo.getBusiness_location_address());
        }
        //Event date and going count
        try {
            holder.tv_date_and_going.setText(BaseFunctions.dateExtractor(dpo.getEventDate())+"-"+dpo.getGoing_count()+" "+
                    context.getResources().getString(R.string.create_event_going));
        }catch (Exception e){}
        //Votes count
        holder.tv_votes_count.setText((dpo.getUpvoting_count()+dpo.getDownvote_count())+"");
        //Voted Value

        if (dpo.getUser_vote()==1){
            holder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
        }else if (dpo.getUser_vote()==-1){
            holder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
        }else {
            holder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
            holder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
        }
        holder.tv_see_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)){
                    AllCommentsDialog dialog = new AllCommentsDialog(context,"event",dpo.getId());
                    dialog.show();
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.tv_see_reviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)){
                    AllReviewsDialog dialog = new AllReviewsDialog(context,"event",dpo.getId());
                    dialog.show();
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.img_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)) {
                    ViewImageDialog dialog = new ViewImageDialog(context, dpo.getImage());
                    dialog.show();
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.img_profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)) {
                    Intent intent = new Intent(context,HomeActivity.class);
                    intent.putExtra("type","facility_info");
                    intent.putExtra("place_id",dpo.getBusiness_id());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.tv_username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)) {
                    Intent intent = new Intent(context,HomeActivity.class);
                    intent.putExtra("type","facility_info");
                    intent.putExtra("place_id",dpo.getBusiness_id());
                    context.startActivity(intent);
                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holder.btn_rolling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseFunctions.isOnline(context)){
                    SharePostDialog dialog = new SharePostDialog(context, new IEditName() {
                        @Override
                        public void onNameWritten(String name) {
                            new SharePostDialog(context, new IEditName() {
                                @Override
                                public void onNameWritten(String name) {
                                    callShareAPI(String.valueOf(dpo.getId()),name);
                                }
                            }).show();

                        }
                    });
                    dialog.show();

                }else {
                    Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.going_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dpo.getIs_going() == 0){
                    callJoinAPI(holder,position,dpo.getId(),dpo.getIs_going());
                }
            }
        });
        holder.edt_comment_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddCommentActivity.class);
                intent.putExtra("id",dpo.getId());
                intent.putExtra("type","event");
                context.startActivity(intent);
                //AddCommentDialog dialog = new AddCommentDialog(context,"event",dpo.getId());
                //dialog.show();
            }
        });
        /*
        holder.edt_comment_user.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (holder.edt_comment_user.getText().toString().equals("")){
                        Toast.makeText(context, context.getResources().getString(R.string.facility_info_add_review_no_comment), Toast.LENGTH_SHORT).show();
                        return false;
                    }
                    callAddCommentAPI(holder,dpo.getId(),holder.edt_comment_user.getText().toString());
                    return true;
                }
                return false;
            }
        });
         */

        holder.btn_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dpo.getUser_vote()==0){
                    callVoteAPI(holder,position,"event",String.valueOf(dpo.getId()),
                            "1");
                }else if (dpo.getUser_vote() == 1){
                    callVoteAPI(holder,position,"event",String.valueOf(dpo.getId()),
                            "0");
                }
            }
        });

        holder.btn_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dpo.getUser_vote()==0){
                    callVoteAPI(holder,position,"event",String.valueOf(dpo.getId()),
                            "-1");
                }else if (dpo.getUser_vote() == -1){
                    callVoteAPI(holder,position,"event",String.valueOf(dpo.getId()),
                            "0");
                }
            }
        });
    }

    private void setTags(TextView pTextView, String pTagString) {
        SpannableString string = new SpannableString(pTagString);

        int start = -1;
        for (int i = 0; i < pTagString.length(); i++) {
            if (pTagString.charAt(i) == '#') {
                start = i;
            } else if (pTagString.charAt(i) == ' ' || (i == pTagString.length() - 1 && start != -1)) {
                if (start != -1) {
                    if (i == pTagString.length() - 1) {
                        i++; // case for if hash is last word and there is no
                        // space after word
                    }

                    final String tag = pTagString.substring(start, i);
                    string.setSpan(new ClickableSpan() {

                        @Override
                        public void onClick(View widget) {
                        }

                        @Override
                        public void updateDrawState(TextPaint ds) {
                            // link color
                            ds.setColor(Color.parseColor("#F7931D"));
                            ds.setUnderlineText(false);
                        }
                    }, start, i, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    start = -1;
                }
            }
        }

        pTextView.setMovementMethod(LinkMovementMethod.getInstance());
        pTextView.setText(string);
    }

    private void callShareAPI(String id,String sharing_text){
        ShareAPIsClass.share(
                context,
                BaseFunctions.getDeviceId(context),
                id,
                "event",
                sharing_text,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(Object json) {
                        Toast.makeText(context, context.getResources().getString(R.string.home_share_success), Toast.LENGTH_SHORT).show();
                        BaseFunctions.playMusic(context,R.raw.added);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callJoinAPI(final ViewHolder holder,final int position,final int id,final int going){
        if (going == 0){
            holder.going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
            holder.going_image.setImageResource(R.drawable.ic_minus);
        }else {
            holder.going_layout.setBackgroundResource(R.drawable.primary_circle);
            holder.going_image.setImageResource(R.drawable.ic_plus);
        }
        EventsAPIsClass.joinEvent(context,
                BaseFunctions.getDeviceId(context),
                id,
                going,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){
                            Toast.makeText(context, context.getResources().getString(R.string.create_event_going_success), Toast.LENGTH_SHORT).show();
                            EventObject o = list.get(position);
                            if (going == 0) {
                                o.setIs_going(1);
                                o.setGoing_count(o.getGoing_count()+1);
                                try {
                                    holder.tv_date_and_going.setText(BaseFunctions.dateExtractor(o.getEventDate())+"-"+o.getGoing_count()+" "+
                                            context.getResources().getString(R.string.create_event_going));
                                }catch (Exception e){}
                            }else {
                                o.setIs_going(0);
                                o.setGoing_count(o.getGoing_count()-1);
                                try {
                                    holder.tv_date_and_going.setText(BaseFunctions.dateExtractor(o.getEventDate())+"-"+o.getGoing_count()+" "+
                                            context.getResources().getString(R.string.create_event_going));
                                }catch (Exception e){}
                            }
                            list.set(position,o);
                            notifyItemChanged(position);
                        }else {
                            if (going == 1){
                                holder.going_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                holder.going_image.setImageResource(R.drawable.ic_minus);
                            }else {
                                holder.going_layout.setBackgroundResource(R.drawable.primary_circle);
                                holder.going_image.setImageResource(R.drawable.ic_plus);
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        if (going == 1){
                            holder.going_layout.setBackgroundResource(R.drawable.grey_circle);
                            holder.going_image.setImageResource(R.drawable.ic_minus);
                        }else {
                            holder.going_layout.setBackgroundResource(R.drawable.primary_circle);
                            holder.going_image.setImageResource(R.drawable.ic_plus);
                        }
                    }
                });

    }

    private void callVoteAPI(final ViewHolder holder,final int position,final String type,final String id,final String value){
        VoteAPIsClass.vote(context,
                BaseFunctions.getDeviceId(context),
                type,
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        String j = new Gson().toJson(json);
                        Boolean success = new Gson().fromJson(j,Boolean.class);
                        if (success){
                            if (value.equals("1")){
                                holder.btn_up.setImageResource(R.drawable.ic_upvote_tapped);
                                holder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                EventObject e = list.get(position);
                                e.setUser_vote(1);
                                e.setUpvoting_count(e.getUpvoting_count()+1);
                                holder.tv_votes_count.setText((e.getUpvoting_count()+e.getDownvote_count())+"");
                                list.set(position,e);
                                notifyItemChanged(position);
                                Toast.makeText(context, context.getResources().getString(R.string.vote_on), Toast.LENGTH_SHORT).show();
                            }else if (value.equals("0")){
                                holder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                holder.btn_down.setImageResource(R.drawable.ic_downvote_untapped);
                                EventObject e = list.get(position);
                                if (e.getUser_vote() == 1){
                                    e.setUpvoting_count(e.getUpvoting_count()-1);
                                }else if (e.getUser_vote() == -1){
                                    e.setDownvote_count(e.getDownvote_count()-1);
                                }
                                holder.tv_votes_count.setText((e.getUpvoting_count()+e.getDownvote_count())+"");
                                e.setUser_vote(0);
                                list.set(position,e);
                                notifyItemChanged(position);
                                Toast.makeText(context, context.getResources().getString(R.string.vote_removed), Toast.LENGTH_SHORT).show();
                            }else if (value.equals("-1")){
                                holder.btn_up.setImageResource(R.drawable.ic_upvote_untapped);
                                holder.btn_down.setImageResource(R.drawable.ic_downvote_tapped);
                                EventObject e = list.get(position);
                                e.setUser_vote(-1);
                                e.setDownvote_count(e.getDownvote_count()+1);
                                holder.tv_votes_count.setText((e.getUpvoting_count()+e.getDownvote_count())+"");
                                list.set(position,e);
                                notifyItemChanged(position);
                                Toast.makeText(context, context.getResources().getString(R.string.vote_off), Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
