package com.dma.app.taikme.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.dma.app.taikme.R;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.wang.avi.AVLoadingIndicatorView;

public class StreamActivity extends AppCompatActivity implements ExoPlayer.EventListener{

    private PlayerView playerView;
    private SimpleExoPlayer simpleExoPlayer;
    private static final String VIDEO_URL = "http://techslides.com/demos/sample-videos/small.mp4";
    private AVLoadingIndicatorView loading;
    private Intent myIntent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stream);
        myIntent = getIntent();
        init_player();
        loading = findViewById(R.id.stream_indicator);
    }

    private void init_player() {
        playerView = findViewById(R.id.stream_player);
        simpleExoPlayer = ExoPlayerFactory.newSimpleInstance(this);
        playerView.setPlayer(simpleExoPlayer);
        DataSource.Factory dataSourceFactory = new DefaultDataSourceFactory(this,
                Util.getUserAgent(this,"appname"));
        MediaSource videoSource = new ExtractorMediaSource.Factory(dataSourceFactory).
                createMediaSource(Uri.parse(myIntent.getStringExtra("url")));
        simpleExoPlayer.prepare(videoSource);
        simpleExoPlayer.setPlayWhenReady(true);
        simpleExoPlayer.addListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        simpleExoPlayer.release();
    }

    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
        switch (playbackState){
            case ExoPlayer.STATE_READY:{
                loading.smoothToHide();
                Log.i("video_state", "onPlayerStateChanged: "+"Ready");
            }break;
            case ExoPlayer.STATE_BUFFERING:{
                loading.smoothToShow();
                Log.i("video_state", "onPlayerStateChanged: "+"Buffering");
            }break;
            case ExoPlayer.STATE_ENDED:{
                loading.smoothToHide();
                Log.i("video_state", "onPlayerStateChanged: "+"Ended");
            }break;
        }
    }
}
