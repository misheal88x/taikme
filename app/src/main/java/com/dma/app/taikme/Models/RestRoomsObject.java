package com.dma.app.taikme.Models;

import com.google.gson.annotations.SerializedName;

public class RestRoomsObject {
    @SerializedName("yes") private int yes = 0;
    @SerializedName("no") private int no = 0;

    public int getYes() {
        return yes;
    }

    public void setYes(int yes) {
        this.yes = yes;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }
}
