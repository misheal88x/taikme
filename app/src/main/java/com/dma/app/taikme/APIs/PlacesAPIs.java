package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import org.androidannotations.annotations.rest.Post;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PlacesAPIs {
    @Multipart
    @POST("places")
    Call<BaseResponse> addPlace(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Header("device_token") String device_token,
            @Part("en_name") RequestBody en_name,
            @Part("ar_name") RequestBody ar_name,
            @Part("price") RequestBody price,
            @Part("lng") RequestBody lng,
            @Part("lat") RequestBody lat,
            @Part("en_location") RequestBody en_location,
            @Part("ar_location") RequestBody ar_location,
            @Part("radius") RequestBody radius,
            @Part MultipartBody.Part image,
            @Part("category_id") RequestBody category_id,
            @Part("en_description") RequestBody en_description,
            @Part("ar_description") RequestBody ar_description);

    @GET("places/visited/{id}")
    Call<BaseResponse> get_visited_places(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("id") String id,
            @Query("page") int page);

    @GET("places/reviewed/{id}")
    Call<BaseResponse> get_reviewed_places(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("id") String id,
            @Query("page") int page);
    @GET("places/trending")
    Call<BaseResponse> get_trending_places(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Query("page") int page);

    @Multipart
    @POST("places")
    Call<BaseResponse> updatePlace(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Header("device_token") String device_token,
            @Part("en_name") RequestBody en_name,
            @Part("ar_name") RequestBody ar_name,
            @Part("price") RequestBody price,
            @Part("lng") RequestBody lng,
            @Part("lat") RequestBody lat,
            @Part("en_location") RequestBody en_location,
            @Part("ar_location") RequestBody ar_location,
            @Part("radius") RequestBody radius,
            @Part MultipartBody.Part image,
            @Part("category_id") RequestBody category_id,
            @Part("en_description") RequestBody en_description,
            @Part("ar_description") RequestBody ar_description,
            @Part("place_id") RequestBody place_id);

    @FormUrlEncoded
    @POST("places/near")
    Call<BaseResponse> get_near_places(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("lat") String lat,
            @Field("lng") String lng,
            @Field("distance") int distance
    );

}
