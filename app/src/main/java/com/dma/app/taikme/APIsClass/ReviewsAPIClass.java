package com.dma.app.taikme.APIsClass;

import android.content.Context;

import com.dma.app.taikme.APIs.PlacesAPIs;
import com.dma.app.taikme.APIs.ReviewsAPIs;
import com.dma.app.taikme.Dialogs.NewProgressDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.BaseResponse;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ReviewsAPIClass extends BaseRetrofit {

    private static NewProgressDialog dialog;
    public static void getPlaceReviews(final Context context,
                                       String device_id,
                                       String device_token,
                                       String place_id,
                                       int page,
                                       final int type,
                                       IResponse onResponse1,
                                       final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        if (type == 0) {
            dialog = new NewProgressDialog(context);
            dialog.show();
        }
        Retrofit retrofit = configureRetrofitWithBearer(context);
        ReviewsAPIs api = retrofit.create(ReviewsAPIs.class);
        Call<BaseResponse> call = api.get_place_reviews("application/json",
                device_id,
                device_token,
                place_id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (type == 0) {
                    dialog.cancel();
                }
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                if (type == 0) {
                    dialog.cancel();
                }
                onFailure.onFailure();
            }
        });
    }

    public static void addReview(final Context context,
                                       String device_id,
                                       String device_token,
                                       String place_id,
                                       float rating,
                                       String review,
                                       IResponse onResponse1,
                                       final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        ReviewsAPIs api = retrofit.create(ReviewsAPIs.class);
        Call<BaseResponse> call = api.add_review("application/json",
                device_id,
                device_token,
                place_id,
                rating,
                review);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void updateReview(final Context context,
                                 String device_id,
                                 String device_token,
                                 String place_id,
                                 String review_id,
                                 float rating,
                                 String review,
                                 IResponse onResponse1,
                                 final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();
        Retrofit retrofit = configureRetrofitWithBearer(context);
        ReviewsAPIs api = retrofit.create(ReviewsAPIs.class);
        Call<BaseResponse> call = api.update_review("application/json",
                device_id,
                device_token,
                place_id,
                review_id,
                rating,
                review);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }

    public static void getReviews(final Context context,
                                       String device_id,
                                       String object_type,
                                       String id,
                                       int page,
                                  final int type,
                                       IResponse onResponse1,
                                       final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithBearer(context);
        ReviewsAPIs api = retrofit.create(ReviewsAPIs.class);
        Call<BaseResponse> call = api.getReviews("application/json",
                device_id,
                object_type,
                id,
                page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }

    public static void addReview(final Context context,
                                  String device_id,
                                  String object_type,
                                  String id,
                                  String text,
                                  String rating,
                                  IResponse onResponse1,
                                  final IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        dialog = new NewProgressDialog(context);
        dialog.show();

        Retrofit retrofit = configureRetrofitWithBearer(context);
        ReviewsAPIs api = retrofit.create(ReviewsAPIs.class);
        Call<BaseResponse> call = api.addReview("application/json",
                device_id,
                object_type,
                id,
                text,
                rating);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                dialog.cancel();
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                dialog.cancel();
                onFailure.onFailure();
            }
        });
    }
}
