package com.dma.app.taikme.Models.HomeModels;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HomeNewObject {
    @SerializedName("stories") private HomeStoriesObject stories = new HomeStoriesObject();
    @SerializedName("wall") private List<HomePostsObject> wall = new ArrayList<>();

    public HomeStoriesObject getStories() {
        return stories;
    }

    public void setStories(HomeStoriesObject stories) {
        this.stories = stories;
    }

    public List<HomePostsObject> getWall() {
        return wall;
    }

    public void setWall(List<HomePostsObject> wall) {
        this.wall = wall;
    }
}
