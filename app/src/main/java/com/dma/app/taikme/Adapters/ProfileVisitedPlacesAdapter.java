package com.dma.app.taikme.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.Activities.HomeActivity;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.FollowerObject;
import com.dma.app.taikme.Models.PlaceObject;
import com.dma.app.taikme.Models.ProfilePlaceObject;
import com.dma.app.taikme.Models.VisitedPlaceObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileVisitedPlacesAdapter extends  RecyclerView.Adapter<ProfileVisitedPlacesAdapter.ViewHolder> {
    private Context context;
    private List<VisitedPlaceObject> list;

    public ProfileVisitedPlacesAdapter(Context context,List<VisitedPlaceObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView img_image;
        private TextView tv_name;
        private SimpleRatingBar rb_rate;
        private TextView tv_rate;
        private ImageView btn_broadcast;
        private RelativeLayout layout;

        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_places_visited_image);
            tv_name = view.findViewById(R.id.item_places_visited_name);
            rb_rate = view.findViewById(R.id.item_places_visited_rate);
            tv_rate = view.findViewById(R.id.item_places_visited_rate_value);
            btn_broadcast = view.findViewById(R.id.item_places_visited_broadcast);
            layout = view.findViewById(R.id.item_place_visited_layout);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_places_visited, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final VisitedPlaceObject place = list.get(position);
        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();
        holder.tv_name.setText(place.getName());
        /*
        if (lan.equals("en")){
            if (place.getEn_name()!=null){
                holder.tv_name.setText(place.getEn_name());
            }else {
                if (place.getAr_name()!=null){
                    holder.tv_name.setText(place.getAr_name());
                }
            }
        }else if (lan.equals("ar")){
            if (place.getAr_name()!=null){
                holder.tv_name.setText(place.getAr_name());
            }else {
                if (place.getEn_name()!=null){
                    holder.tv_name.setText(place.getEn_name());
                }
            }
        }

        if (place.getImages()!=null){
            if (place.getImages().getThumb_image()!= null){
                BaseFunctions.setGlideImage(context,holder.img_image,place.getImages().getThumb_image());
            }else if (place.getImages().getImage()!=null){
                BaseFunctions.setGlideImage(context,holder.img_image,place.getImages().getImage());
            }
        }
        **/
        BaseFunctions.setGlideImage(context,holder.img_image,place.getThumb_image());
        try {
            holder.rb_rate.setRating(Float.valueOf(place.getBusiness_avg_review()));
        }catch (Exception e){}
        try {
            holder.tv_rate.setText(String.valueOf(Math.round(Float.valueOf(place.getBusiness_avg_review()))) + " " + context.getResources().getString(R.string.places_visited_out_of) + " 5");
        }catch (Exception e){
            holder.tv_rate.setText("0" + " " + context.getResources().getString(R.string.places_visited_out_of) + " 5");
        }
        holder.img_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,HomeActivity.class);
                intent.putExtra("type","facility_info");
                intent.putExtra("place_id",place.getId());
                context.startActivity(intent);
            }
        });
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,HomeActivity.class);
                intent.putExtra("type","facility_info");
                intent.putExtra("place_id",place.getId());
                context.startActivity(intent);
            }
        });

        holder.btn_broadcast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (place.getIs_following() == 1){
                    callFollowAPI(position,String.valueOf(place.getId()),"no");
                }else {
                    callFollowAPI(position,String.valueOf(place.getId()),"yes");
                }
            }
        });
    }

    private void callFollowAPI (final int position,String id,final String value){
        FollowersAPIsClass.followNew(
                context,
                BaseFunctions.getDeviceId(context),
                "user",
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("yes")){
                                    Toast.makeText(context, context.getResources().getString(R.string.follow_place_true), Toast.LENGTH_SHORT).show();
                                    VisitedPlaceObject o = list.get(position);
                                    o.setIs_following(1);
                                    notifyItemChanged(position);
                                }else {
                                    Toast.makeText(context, context.getResources().getString(R.string.follow_place_false), Toast.LENGTH_SHORT).show();
                                    VisitedPlaceObject o = list.get(position);
                                    o.setIs_following(0);
                                    notifyItemChanged(position);
                                }
                            }else {
                                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
