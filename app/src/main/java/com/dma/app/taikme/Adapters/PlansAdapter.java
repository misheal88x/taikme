package com.dma.app.taikme.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dma.app.taikme.Models.DumpPlansObject;
import com.dma.app.taikme.R;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PlansAdapter extends  RecyclerView.Adapter<PlansAdapter.ViewHolder>{
    private Context context;
    private List<DumpPlansObject> list;

    public PlansAdapter(Context context,List<DumpPlansObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_title;
        private CircleImageView img_image;
        private TextView tv_name;
        private SimpleRatingBar rb_rate;
        private RelativeLayout btn_minus,btn_x;
        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.item_plan_title);
            img_image = view.findViewById(R.id.item_plan_image);
            tv_name = view.findViewById(R.id.item_plan_name);
            rb_rate = view.findViewById(R.id.item_plan_rate);
            btn_minus = view.findViewById(R.id.item_plan_minus);
            btn_x = view.findViewById(R.id.item_plan_x);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_plan, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        DumpPlansObject dpo = list.get(position);

        holder.tv_title.setText(dpo.getTitle()+" "+dpo.getTime());
        holder.tv_name.setText(dpo.getFacility_name());
        holder.rb_rate.setRating(dpo.getFacility_rate());

    }
}
