package com.dma.app.taikme.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.dma.app.taikme.Models.DeviceObject;
import com.dma.app.taikme.Models.DeviceSettingsObject;
import com.dma.app.taikme.Models.HomeObject;
import com.dma.app.taikme.Models.SettingsObject;
import com.dma.app.taikme.Models.StartupObject;
import com.dma.app.taikme.Models.UserObject;
import com.google.gson.Gson;

public class SharedPrefManager {
    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "Main";
    private static final String KEY_USER = "user";
    private static final String KEY_DEVICE = "device";
    private static final String KEY_DEVICE_SETTING = "device_setting";
    private static final String KEY_ACCESS_TOKEN = "access_token";
    private static final String KEY_FCM_TOKEN = "access_token";
    private static final String KEY_SETTINGS = "settings";
    private static final String KEY_STARTUP = "startup";
    private static final String KEY_HOME = "home";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LNG = "lng";
    private static final String KEY_ACCOUNT_ACTIVATED = "account_activated";

    private SharedPrefManager(Context context) {
        mCtx = context;
    }
    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public void setUser(UserObject user){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = new Gson().toJson(user);
        editor.putString(KEY_USER,json);
        editor.commit();
    }

    public UserObject getUser(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(KEY_USER,"");
        if (json.equals("")){
            return null;
        }else {
            UserObject userObject = new Gson().fromJson(json,UserObject.class);
            return userObject;
        }
    }

    public void setStartUp(StartupObject startUp){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = new Gson().toJson(startUp);
        editor.putString(KEY_STARTUP,json);
        editor.commit();
    }

    public StartupObject getStartUp(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(KEY_STARTUP,"");
        if (json.equals("")){
            return new StartupObject();
        }else {
            StartupObject userObject = new Gson().fromJson(json,StartupObject.class);
            return userObject;
        }
    }

    public void setHome(HomeObject home){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = new Gson().toJson(home);
        editor.putString(KEY_HOME,json);
        editor.commit();
    }

    public HomeObject getHome(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(KEY_HOME,"");
        if (json.equals("")){
            return new HomeObject();
        }else {
            HomeObject homeObject = new Gson().fromJson(json,HomeObject.class);
            return homeObject;
        }
    }

    public void setDevice(DeviceObject device){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = new Gson().toJson(device);
        editor.putString(KEY_DEVICE,json);
        editor.commit();
    }

    public DeviceObject getDevice(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(KEY_DEVICE,"");
        if (json.equals("")){
            return new DeviceObject();
        }
        DeviceObject deviceObject = new Gson().fromJson(json,DeviceObject.class);
        return deviceObject;
    }

    public void setLat(String lat){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_LAT,lat);
        editor.commit();
    }

    public String getLat(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String lat = sharedPreferences.getString(KEY_LAT,"");
        if (lat.equals("")){
            return "0.0";
        }
        return sharedPreferences.getString(KEY_LAT,"");
    }

    public void setAccountActivated(boolean value){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(KEY_ACCOUNT_ACTIVATED,value);
        editor.commit();
    }

    public Boolean getAccountActivated(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        Boolean lat = sharedPreferences.getBoolean(KEY_ACCOUNT_ACTIVATED,true);
        return lat;
    }

    public void setLng(String lng){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_LNG,lng);
        editor.commit();
    }

    public String getLng(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(KEY_LNG,"");
        if (json.equals("")){
            return "0.0";
        }
        return sharedPreferences.getString(KEY_LNG,"");
    }

    public void setDeviceSetting(DeviceSettingsObject device_setting){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = new Gson().toJson(device_setting);
        editor.putString(KEY_DEVICE_SETTING,json);
        editor.commit();
    }

    public DeviceSettingsObject getDeviceSetting(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(KEY_DEVICE_SETTING,"");
        if (json.equals("")){
            return new DeviceSettingsObject();
        }
        DeviceSettingsObject deviceSettingsObject = new Gson().fromJson(json,DeviceSettingsObject.class);
        return deviceSettingsObject;
    }

    public void setAccessToken(String access_token){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_ACCESS_TOKEN,access_token);
        editor.commit();
    }

    public String getAccessToken(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String access_token = sharedPreferences.getString(KEY_ACCESS_TOKEN,"");
        return access_token;
    }

    public void setFcmToken(String fcm_token){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_FCM_TOKEN,fcm_token);
        editor.commit();
    }

    public String getFcmToken(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String fcm_token = sharedPreferences.getString(KEY_FCM_TOKEN,"");
        return fcm_token;
    }

    public void setSettings(SettingsObject settings){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String json = new Gson().toJson(settings);
        editor.putString(KEY_SETTINGS,json);
        editor.commit();
    }

    public SettingsObject getSettings(){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(KEY_SETTINGS,"");
        if (json.equals("")){
            return new SettingsObject();
        }else {
            SettingsObject settingsObject = new Gson().fromJson(json,SettingsObject.class);
            return settingsObject;
        }

    }


}
