package com.dma.app.taikme.Activities;

import android.Manifest;
import android.app.TimePickerDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import de.hdodenhof.circleimageview.CircleImageView;

import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Html;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.asksira.bsimagepicker.BSImagePicker;
import com.bumptech.glide.Glide;
import com.dma.app.taikme.APIs.CategoriesAPIs;
import com.dma.app.taikme.APIsClass.CategoriesAPIsClass;
import com.dma.app.taikme.APIsClass.UserAPIsClass;
import com.dma.app.taikme.Adapters.CustomSpinnerAdapter;
import com.dma.app.taikme.Dialogs.ChooseSelectionDialog;
import com.dma.app.taikme.Dialogs.MultiSelectDialog;
import com.dma.app.taikme.Dialogs.MultiSelectObjectsDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IMultiSelect;
import com.dma.app.taikme.Interfaces.IMultiSelectObjects;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Interfaces.ISelection;
import com.dma.app.taikme.Models.CategoryObject;
import com.dma.app.taikme.Models.CuisineObject;
import com.dma.app.taikme.Models.LoginResponse;
import com.dma.app.taikme.Models.MusicGenderObject;
import com.dma.app.taikme.Models.SettingsObject;
import com.dma.app.taikme.Models.StartupItemObject;
import com.dma.app.taikme.Models.StartupObject;
import com.dma.app.taikme.Models.WorkingHourObject;
import com.dma.app.taikme.Others.BaseActivity;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.Others.BaseRetrofit;
import com.dma.app.taikme.Others.LocaleHelper;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.dma.app.taikme.Utils.TimePicketFragment;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.soundcloud.android.crop.Crop;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MyBusinessActivity extends BaseActivity implements BSImagePicker.OnSingleImageSelectedListener,
        BSImagePicker.ImageLoaderDelegate, TimePickerDialog.OnTimeSetListener{

    private EditText edt_name,edt_phone,edt_email,edt_website,edt_signature;

    private Spinner atmosphere_spinner,music_spinner,
    second_music_spinner,third_music_spinner,cuisine_spinner,kids_spinner,parking_spinner,handicapped_spinner,
    delivery_spinner,rest_spinner,price_spinner,best_spinner;

    private TextView seating_spinner,category_spinner,sub_category_spinner;
    private CustomSpinnerAdapter atmosphere_adapter,music_adapter,
            second_music_adapter,third_music_adapter,cuisine_adapter,kids_adapter,parking_adapter,handicapped_adapter,
            delivery_adapter,rest_adapter,price_adapter,best_adapter;

    private List<String> atmosphere_list_string,music_list_string,
            second_music_list_string,third_music_list_string,cuisine_list_string,kids_list_string,parking_list_string,handicapped_list_string,
            delivery_list_string,rest_list_string,price_list_string,best_list_string;

    private List<StartupItemObject> music_list,second_music_list,third_music_list;
    private List<StartupItemObject> cuisine_list;
    private List<CategoryObject> selected_categories_list,selected_sub_categories_list;
    private List<Integer> selected_seating,selected_seating_indexes;
    private List<StartupItemObject> atmosphere_list;
    private List<StartupItemObject>  best_list;
    private Button btn_register;
    private TextView tv_agree;

    private CircleImageView profile_image;
    private TextView profile_image_hint;
    private RelativeLayout profile_image_layout;
    private File imageFile;
    private String imagePath = "";
    private static final String TEMP_PHOTO_FILE = "temporary_holder.jpg";

    private TextView mon_start_time,mon_end_time,
            tue_start_time,tue_end_time,
            wed_start_time,wed_end_time,
            thu_start_time,thu_end_time,
            fri_start_time,fri_end_time,
            sat_start_time,sat_end_time,
            sun_start_time,sun_end_time;
    private CheckBox mon_check,
            tue_check,
            wed_check,
            thu_check,
            fri_check,
            sat_check,
            sun_check;

    private String selected_mon_start_time = "",selected_mon_end_time = "",
            selected_tue_start_time = "",selected_tue_end_time = "",
            selected_wed_start_time = "",selected_wed_end_time = "",
            selected_thu_start_time = "",selected_thu_end_time = "",
            selected_fri_start_time = "",selected_fri_end_time = "",
            selected_sat_start_time = "",selected_sat_end_time = "",
            selected_sun_start_time = "",selected_sun_end_time = "";

    private int checked_day = 0;

    private String selected_atmosphere = "";
    private String selected_first_music = "";
    private String selected_second_music = "";
    private String selected_third_music = "";
    private String selected_cuisine = "";
    private String selected_signature = "";
    private String selected_kids = "";
    private String selected_parking = "";
    private String selected_handicapped = "";
    private String selected_delivery = "";
    private String selected_restrooms = "";
    private String selected_price = "";
    private String selected_best_for = "";
    private RelativeLayout root;

    private TextView tv_english,tv_arabic;
    @Override
    public void set_layout() {
        setContentView(R.layout.activity_my_business);
    }

    @Override
    public void init_activity(Bundle savedInstanceState) {
        init_spinners();
        tv_agree.setText(Html.fromHtml(getResources().getString(R.string.register_agree)+" "+
                "<font color=#F7931D>"+getResources().getString(R.string.register_terms)+"</font>"+" "+
                getResources().getString(R.string.register_this_app)));
    }

    @Override
    public void init_views() {
        //RelativeLayout
        profile_image_layout = findViewById(R.id.profile_image_layout);
        //Spinner
        category_spinner = findViewById(R.id.my_business_category_spinner);
        sub_category_spinner = findViewById(R.id.my_business_sub_category_spinner);
        seating_spinner = findViewById(R.id.my_business_seating_spinner);
        atmosphere_spinner = findViewById(R.id.my_business_atmosphere_spinner);
        music_spinner = findViewById(R.id.my_business_music_spinner);
        second_music_spinner = findViewById(R.id.my_business_second_music_spinner);
        third_music_spinner = findViewById(R.id.my_business_third_music_spinner);
        cuisine_spinner = findViewById(R.id.my_business_cuisine_spinner);
        kids_spinner = findViewById(R.id.my_business_kids_spinner);
        parking_spinner = findViewById(R.id.my_business_parking_spinner);
        handicapped_spinner = findViewById(R.id.my_business_handicapped_spinner);
        delivery_spinner = findViewById(R.id.my_business_delivery_spinner);
        rest_spinner = findViewById(R.id.my_business_rest_spinner);
        price_spinner = findViewById(R.id.my_business_price_spinner);
        best_spinner = findViewById(R.id.my_business_best_for_spinner);
        //Button
        btn_register = findViewById(R.id.my_business_register);
        //TextViews
        tv_agree = findViewById(R.id.my_business_agree_txt);
        profile_image_hint = findViewById(R.id.register_pick_image);
        //Circle Image View
        profile_image = findViewById(R.id.register_image);

        //TextView
        mon_start_time = findViewById(R.id.my_business_mon_start_value);
        mon_end_time = findViewById(R.id.my_business_mon_end_value);
        tue_start_time = findViewById(R.id.my_business_tue_start_value);
        tue_end_time = findViewById(R.id.my_business_tue_end_value);
        wed_start_time = findViewById(R.id.my_business_wed_start_value);
        wed_end_time = findViewById(R.id.my_business_wed_end_value);
        thu_start_time = findViewById(R.id.my_business_thu_start_value);
        thu_end_time = findViewById(R.id.my_business_thu_end_value);
        fri_start_time = findViewById(R.id.my_business_fri_start_value);
        fri_end_time = findViewById(R.id.my_business_fri_end_value);
        sat_start_time = findViewById(R.id.my_business_sat_start_value);
        sat_end_time = findViewById(R.id.my_business_sat_end_value);
        sun_start_time = findViewById(R.id.my_business_sun_start_value);
        sun_end_time = findViewById(R.id.my_business_sun_end_value);
        tv_english = findViewById(R.id.login_english_language);
        tv_arabic = findViewById(R.id.login_arabic_language);
        //Checkbox
        mon_check = findViewById(R.id.my_business_mon_check);
        tue_check = findViewById(R.id.my_business_tue_check);
        wed_check = findViewById(R.id.my_business_wed_check);
        thu_check = findViewById(R.id.my_business_thu_check);
        fri_check = findViewById(R.id.my_business_fri_check);
        sat_check = findViewById(R.id.my_business_sat_check);
        sun_check = findViewById(R.id.my_business_sun_check);
        //EditText
        edt_name = findViewById(R.id.my_business_name);
        edt_email = findViewById(R.id.my_business_email);
        edt_phone = findViewById(R.id.my_business_phone_number);
        edt_signature = findViewById(R.id.my_business_signature);
        edt_website = findViewById(R.id.my_business_website);

        selected_seating_indexes = new ArrayList<>();
    }

    @Override
    public void init_events() {
        root = findViewById(R.id.layout);

        tv_english.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getInstance(MyBusinessActivity.this).getSettings()!=null){
                    if (!SharedPrefManager.getInstance(MyBusinessActivity.this).getSettings().getView_language().equals("en")){
                        SettingsObject o = SharedPrefManager.getInstance(MyBusinessActivity.this).getSettings();
                        o.setView_language("en");
                        SharedPrefManager.getInstance(MyBusinessActivity.this).setSettings(o);
                        LocaleHelper.setLocale(MyBusinessActivity.this, "en");
                        //attachBaseContext(CalligraphyContextWrapper.wrap(LocaleHelper.onAttach(RegisterActivity.this)));
                        Intent intent = new Intent(MyBusinessActivity.this,MyBusinessActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
        tv_arabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SharedPrefManager.getInstance(MyBusinessActivity.this).getSettings()!=null){
                    if (!SharedPrefManager.getInstance(MyBusinessActivity.this).getSettings().getView_language().equals("ar")){
                        SettingsObject o = SharedPrefManager.getInstance(MyBusinessActivity.this).getSettings();
                        o.setView_language("ar");
                        SharedPrefManager.getInstance(MyBusinessActivity.this).setSettings(o);
                        LocaleHelper.setLocale(MyBusinessActivity.this, "ar");
                        //attachBaseContext(CalligraphyContextWrapper.wrap(LocaleHelper.onAttach(RegisterActivity.this)));
                        Intent intent = new Intent(MyBusinessActivity.this,MyBusinessActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
        profile_image_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(MyBusinessActivity.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA},
                        1);
            }
        });
        category_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MultiSelectObjectsDialog dialog = new MultiSelectObjectsDialog(MyBusinessActivity.this,
                        SharedPrefManager.getInstance(MyBusinessActivity.this).getStartUp().getPlaces_categories(),
                        selected_categories_list, new IMultiSelectObjects() {
                    @Override
                    public void onSelected(List<CategoryObject> list) {
                        selected_categories_list.clear();
                        selected_sub_categories_list.clear();
                        if (list.size()>0){
                            String cat_txt = "";
                            for (CategoryObject co : list){
                                selected_categories_list.add(co);
                                cat_txt+=co.getEn_name()+" ";
                            }
                            category_spinner.setText(cat_txt);
                        }else {
                            category_spinner.setText(getResources().getString(R.string.my_business_category));
                        }
                    }
                });
                dialog.show();
            }
        });
        sub_category_spinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<CategoryObject> subs = new ArrayList<>();
                if (selected_categories_list.size()>0){
                    for (CategoryObject coo : selected_categories_list){
                        if (coo.getSub_categories()!=null){
                            if (coo.getSub_categories().size()>0){
                                for (CategoryObject ccc : coo.getSub_categories()){
                                    subs.add(ccc);
                                }
                            }
                        }
                    }
                }
                MultiSelectObjectsDialog dialog = new MultiSelectObjectsDialog(MyBusinessActivity.this,
                        subs,
                        selected_sub_categories_list, new IMultiSelectObjects() {
                    @Override
                    public void onSelected(List<CategoryObject> list) {
                        selected_sub_categories_list.clear();
                        if (list.size()>0){
                            String sub_cat_txt = "";
                            for (CategoryObject co : list){
                                selected_sub_categories_list.add(co);
                                sub_cat_txt+=co.getEn_name()+" ";
                            }
                            sub_category_spinner.setText(sub_cat_txt);
                        }else {
                            sub_category_spinner.setText(getResources().getString(R.string.my_business_sub_category));
                        }
                    }
                });
                dialog.show();
            }
        });
        atmosphere_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_atmosphere = "";
                }else {
                    selected_atmosphere = atmosphere_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_first_music = "";
                }else {
                    if (!selected_third_music.equals(music_list.get(position).getId())&&
                            !selected_second_music.equals(music_list.get(position).getId())) {
                        selected_first_music = music_list.get(position).getId();
                    }else {
                        Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_first_music = "";
                        music_spinner.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        second_music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_second_music = "";
                }else {
                    if (!selected_first_music.equals(second_music_list.get(position).getId())&&
                            !selected_third_music.equals(second_music_list.get(position).getId())) {
                        selected_second_music = second_music_list.get(position).getId();
                    }else {
                        Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_second_music = "";
                        second_music_spinner.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        third_music_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_third_music = "";
                }else {
                    if (!selected_first_music.equals(third_music_list.get(position).getId())&&
                            !selected_second_music.equals(third_music_list.get(position).getId())) {
                        selected_third_music = third_music_list.get(position).getId();
                    }else {
                        Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.already_chosen), Toast.LENGTH_SHORT).show();
                        selected_third_music = "";
                        third_music_spinner.setSelection(0);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        cuisine_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_cuisine = "";
                }else {
                    selected_cuisine = cuisine_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        kids_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_kids = "";
                }else if (position == 1){
                    selected_kids = "1";
                }else {
                    selected_kids = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        parking_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_parking = "";
                }else if (position == 1){
                    selected_parking = "1";
                }else {
                    selected_parking = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        handicapped_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_handicapped = "";
                }else if (position == 1){
                    selected_handicapped = "1";
                }else {
                    selected_handicapped = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        delivery_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_delivery = "";
                }else if (position == 1){
                    selected_delivery = "1";
                }else {
                    selected_delivery = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rest_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_restrooms = "";
                }else if (position == 1){
                    selected_restrooms = "1";
                }else {
                    selected_restrooms = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        price_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_price = "";
                }else {
                    selected_price = String.valueOf(price_list_string.get(position).length());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        best_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int p, long id) {
                int position = BaseFunctions.getSpinnerPosition(parent,p);
                if (position == 0){
                    selected_best_for = "";
                }else {
                    selected_best_for = best_list.get(position).getId();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mon_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked_day = 1;
                    DialogFragment datePicker = new TimePicketFragment();
                    datePicker.show(getSupportFragmentManager(),"time picker");
                }else {
                    selected_mon_start_time = "";
                    selected_mon_end_time = "";
                    mon_start_time.setText("  --  ");
                    mon_end_time.setText("  --  ");
                }
            }
        });
        tue_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked_day = 2;
                    DialogFragment datePicker = new TimePicketFragment();
                    datePicker.show(getSupportFragmentManager(),"time picker");
                }else {
                    selected_tue_start_time = "";
                    selected_tue_end_time = "";
                    tue_start_time.setText("  --  ");
                    tue_end_time.setText("  --  ");
                }
            }
        });
        wed_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked_day = 3;
                    DialogFragment datePicker = new TimePicketFragment();
                    datePicker.show(getSupportFragmentManager(),"time picker");
                }else {
                    selected_wed_start_time = "";
                    selected_wed_end_time = "";
                    wed_start_time.setText("  --  ");
                    wed_end_time.setText("  --  ");
                }
            }
        });
        thu_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked_day = 4;
                    DialogFragment datePicker = new TimePicketFragment();
                    datePicker.show(getSupportFragmentManager(),"time picker");
                }else {
                    selected_thu_start_time = "";
                    selected_thu_end_time = "";
                    thu_start_time.setText("  --  ");
                    thu_end_time.setText("  --  ");
                }
            }
        });
        fri_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked_day = 5;
                    DialogFragment datePicker = new TimePicketFragment();
                    datePicker.show(getSupportFragmentManager(),"time picker");
                }else {
                    selected_fri_start_time = "";
                    selected_fri_end_time = "";
                    fri_start_time.setText("  --  ");
                    fri_end_time.setText("  --  ");
                }
            }
        });
        sat_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked_day = 6;
                    DialogFragment datePicker = new TimePicketFragment();
                    datePicker.show(getSupportFragmentManager(),"time picker");
                }else {
                    selected_sat_start_time = "";
                    selected_sat_end_time = "";
                    sat_start_time.setText("  --  ");
                    sat_end_time.setText("  --  ");
                }
            }
        });
        sun_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    checked_day = 7;
                    DialogFragment datePicker = new TimePicketFragment();
                    datePicker.show(getSupportFragmentManager(),"time picker");
                }else {
                    selected_sun_start_time = "";
                    selected_sun_end_time = "";
                    sun_start_time.setText("  --  ");
                    sun_end_time.setText("  --  ");
                }
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagePath.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_image), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_name.getText().toString().equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_name), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_phone.getText().toString().equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_phone), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_email.getText().toString().equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_email), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_website.getText().toString().equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_website), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_categories_list.size() == 0){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_category), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_sub_categories_list.size() == 0){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_sub_category), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_seating.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_seating), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_atmosphere.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_atmosphere), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_first_music.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_first_music), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_second_music.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_second_music_genre), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_third_music.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_third_music_genre), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_cuisine.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_cuisine), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_signature.getText().toString().equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_signature), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_kids.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_kids), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_parking.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_parking), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_handicapped.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_handicapped), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_delivery.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_delivery), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_restrooms.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_restrooms), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_price.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_price), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_best_for.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_best_for), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (selected_mon_end_time.equals("")&&
                        selected_tue_end_time.equals("")&&
                        selected_wed_end_time.equals("")&&
                        selected_thu_end_time.equals("")&&
                        selected_fri_end_time.equals("")&&
                        selected_sat_end_time.equals("")&&
                        selected_sun_end_time.equals("")){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_hours), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!((CheckBox)findViewById(R.id.my_business_agree)).isChecked()){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_agree), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!((CheckBox)findViewById(R.id.my_business_understand)).isChecked()){
                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.my_business_no_understand), Toast.LENGTH_SHORT).show();
                    return;
                }
                callUpdateAccountAPI();
            }
        });


    }

    @Override
    public void set_fragment_place() {

    }

    private void init_spinners(){
        final StartupObject so = SharedPrefManager.getInstance(MyBusinessActivity.this).getStartUp();
        String language = SharedPrefManager.getInstance(MyBusinessActivity.this).getSettings().getView_language();

        selected_categories_list = new ArrayList<>();
        selected_sub_categories_list = new ArrayList<>();
        atmosphere_list_string = new ArrayList<>();
        atmosphere_list = new ArrayList<>();
        music_list_string = new ArrayList<>();
        music_list = new ArrayList<>();
        second_music_list_string = new ArrayList<>();
        third_music_list_string = new ArrayList<>();
        second_music_list = new ArrayList<>();
        third_music_list = new ArrayList<>();
        cuisine_list_string = new ArrayList<>();
        cuisine_list = new ArrayList<>();
        kids_list_string = new ArrayList<>();
        parking_list_string = new ArrayList<>();
        handicapped_list_string = new ArrayList<>();
        delivery_list_string = new ArrayList<>();
        rest_list_string = new ArrayList<>();
        price_list_string = new ArrayList<>();
        best_list_string = new ArrayList<>();
        best_list = new ArrayList<>();
        selected_seating = new ArrayList<>();

        if (so.getBusiness_seating()!=null){
            if (so.getBusiness_seating().size()>0){
                seating_spinner.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        List<String> source_names = new ArrayList<>();
                        for (StartupItemObject sio : so.getBusiness_seating()){
                            source_names.add(sio.getName());
                        }
                        MultiSelectDialog dialog = new MultiSelectDialog(MyBusinessActivity.this,
                                source_names,selected_seating_indexes, new IMultiSelect() {
                            @Override
                            public void onSelected(List<Integer> list) {
                                selected_seating.clear();
                                selected_seating_indexes.clear();
                                if (list.size()>0){
                                    String seating_txt = "";
                                    for (Integer i : list){
                                        selected_seating_indexes.add(i);
                                        selected_seating.add(Integer.valueOf(so.getBusiness_seating().get(i).getId()));
                                        seating_txt+=so.getBusiness_seating().get(i).getName()+" ";
                                        seating_spinner.setText(seating_txt);
                                    }
                                }else {
                                    seating_spinner.setText(getResources().getString(R.string.my_business_seating));
                                }
                            }
                        });
                        dialog.show();
                    }
                });
            }
        }

        atmosphere_list_string.add(getResources().getString(R.string.my_business_atmosphere));
        atmosphere_list.add(new StartupItemObject());
        if (so.getBusiness_atmosphere()!=null && so.getBusiness_atmosphere().size()>0){
            if (so.getBusiness_atmosphere()!=null){
                if (so.getBusiness_atmosphere().size()>0){
                    for (StartupItemObject s : so.getBusiness_atmosphere()){
                        atmosphere_list_string.add(s.getName());
                        atmosphere_list.add(s);
                    }
                }
            }
        }

        music_list_string.add(getResources().getString(R.string.my_business_music));
        music_list.add(new StartupItemObject());
        if (so.getMusic_gender_type()!=null){
            if (so.getMusic_gender_type().size()>0){
                for (StartupItemObject mo : so.getMusic_gender_type()){
                    music_list.add(mo);
                    music_list_string.add(mo.getName());
                }
            }
        }

        second_music_list_string.add(getResources().getString(R.string.my_business_second_music));
        second_music_list.add(new StartupItemObject());
        if (so.getMusic_gender_type()!=null){
            if (so.getMusic_gender_type().size()>0){
                for (StartupItemObject mo : so.getMusic_gender_type()){
                    second_music_list.add(mo);
                    second_music_list_string.add(mo.getName());
                }
            }
        }

        third_music_list_string.add(getResources().getString(R.string.my_business_third_music));
        third_music_list.add(new StartupItemObject());
        if (so.getMusic_gender_type()!=null){
            if (so.getMusic_gender_type().size()>0){
                for (StartupItemObject mo : so.getMusic_gender_type()){
                    third_music_list.add(mo);
                    third_music_list_string.add(mo.getName());
                }
            }
        }

        cuisine_list_string.add(getResources().getString(R.string.my_business_cuisine));
        cuisine_list.add(new StartupItemObject());
        if (so.getCuisine_definitions()!=null){
            if (so.getCuisine_definitions().size()>0){
                for (StartupItemObject co : so.getCuisine_definitions()){
                    cuisine_list.add(co);
                    cuisine_list_string.add(co.getName());
                }
            }
        }

        kids_list_string.add(getResources().getString(R.string.my_business_kids));
        kids_list_string.add(getResources().getString(R.string.yes));
        kids_list_string.add(getResources().getString(R.string.no));

        parking_list_string.add(getResources().getString(R.string.my_business_parking));
        parking_list_string.add(getResources().getString(R.string.yes));
        parking_list_string.add(getResources().getString(R.string.no));

        handicapped_list_string.add(getResources().getString(R.string.my_business_handicapped));
        handicapped_list_string.add(getResources().getString(R.string.yes));
        handicapped_list_string.add(getResources().getString(R.string.no));

        delivery_list_string.add(getResources().getString(R.string.my_business_delivery));
        delivery_list_string.add(getResources().getString(R.string.yes));
        delivery_list_string.add(getResources().getString(R.string.no));

        rest_list_string.add(getResources().getString(R.string.my_business_rest));
        rest_list_string.add(getResources().getString(R.string.yes));
        rest_list_string.add(getResources().getString(R.string.no));

        price_list_string.add(getResources().getString(R.string.my_business_price_range));
        price_list_string.add("$");
        price_list_string.add("$$");
        price_list_string.add("$$$");
        price_list_string.add("$$$$");
        price_list_string.add("$$$$$");

        best_list_string.add(getResources().getString(R.string.my_business_best_for));
        best_list.add(new StartupItemObject());
        if (so.getMy_place_better_for()!=null){
            if (so.getMy_place_better_for().size()>0){
                for (StartupItemObject s : so.getMy_place_better_for()){
                    best_list_string.add(s.getName());
                    best_list.add(s);
                }
            }
        }

        BaseFunctions.init_spinner(MyBusinessActivity.this,atmosphere_spinner,atmosphere_adapter,atmosphere_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,music_spinner,music_adapter,music_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,second_music_spinner,second_music_adapter,second_music_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,third_music_spinner,third_music_adapter,third_music_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,cuisine_spinner,cuisine_adapter,cuisine_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,kids_spinner,kids_adapter,kids_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,parking_spinner,parking_adapter,parking_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,handicapped_spinner,handicapped_adapter,handicapped_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,delivery_spinner,delivery_adapter,delivery_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,rest_spinner,rest_adapter,rest_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,price_spinner,price_adapter,price_list_string);
        BaseFunctions.init_spinner(MyBusinessActivity.this,best_spinner,best_adapter,best_list_string);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        switch (requestCode){
            case 100:{
                if (resultCode == RESULT_OK) {
                    if (imageFile.exists()){
                        //Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getPath());
                        //profile_image.setImageBitmap(bitmap);
                        //profile_image_hint.setVisibility(View.GONE);
                        //imagePath = imageFile.getPath();
                        Uri source_uri = Uri.fromFile(imageFile);
                        Uri destination_uri = Uri.fromFile(getTempFile());
                        Crop.of(source_uri,destination_uri).asSquare().start( MyBusinessActivity.this);
                    }else {
                        Toast.makeText(MyBusinessActivity.this, "Image file does not exists", Toast.LENGTH_SHORT).show();
                    }
                }
            }break;

            case Crop.REQUEST_CROP:{
                if (resultCode == RESULT_OK) {
                    handle_crop(resultCode,data);
                }
            }break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
                        && grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    ChooseSelectionDialog dialog = new ChooseSelectionDialog(MyBusinessActivity.this, "camera", new ISelection() {
                        @Override
                        public void onTakeCameraClicked() {
                            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                            StrictMode.setVmPolicy(builder.build());
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            imageFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                                    "test.jpg");
                            Uri tempUri = Uri.fromFile(imageFile);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT,tempUri);
                            intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,1);
                            startActivityForResult(intent,100);
                        }

                        @Override
                        public void onTakeVideoClicked() {

                        }

                        @Override
                        public void onCameraGalleryClicked() {
                            BSImagePicker singleSelectionPicker = new BSImagePicker.Builder("com.dma.app.taikme.provider")
                                    .hideCameraTile() //Default: show. Set this if you don't want user to take photo.
                                    .hideGalleryTile() //Default: show. Set this if you don't want to further let user select from a gallery app. In such case, I suggest you to set maximum displaying images to Integer.MAX_VALUE.
                                    .build();
                            singleSelectionPicker.show(getSupportFragmentManager(),"picker");
                        }

                        @Override
                        public void onVideoGalleryClicked() {

                        }
                    });
                    dialog.show();
                } else {

                    Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.no_permissions), Toast.LENGTH_SHORT).show();
                }
            }
            break;
        }
    }

    @Override
    public void loadImage(File imageFile, ImageView ivImage) {
        Glide.with(MyBusinessActivity.this).load(imageFile).into(ivImage);
    }

    @Override
    public void onSingleImageSelected(Uri uri, String tag) {
        Uri source_uri = uri;
        Uri destination_uri = Uri.fromFile(getTempFile());
        Crop.of(source_uri,destination_uri).asSquare().start( MyBusinessActivity.this);
    }

    private File getTempFile() {
        if (isSDCARDMounted()) {

            File f = new File(Environment.getExternalStorageDirectory(),TEMP_PHOTO_FILE);
            try {
                f.createNewFile();
            } catch (IOException e) {

            }
            return f;
        } else {
            return null;
        }
    }

    private boolean isSDCARDMounted(){
        String status = Environment.getExternalStorageState();
        if (status.equals(Environment.MEDIA_MOUNTED))
            return true;
        return false;
    }

    private void handle_crop(int code, final Intent data){
        if (code == RESULT_OK){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    profile_image.setImageDrawable(null);
                    profile_image.setImageURI(Crop.getOutput(data));
                    profile_image_hint.setVisibility(View.GONE);
                    String filePath=  Environment.getExternalStorageDirectory()
                            + "/"+"temporary_holder.jpg";
                    imagePath = filePath;
                }
            });

        }else if (code == Crop.RESULT_ERROR){
            Toast.makeText(MyBusinessActivity.this, getResources().getString(R.string.error_cropping), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute) {
        String hour_str = new String();
        String minute_str = new String();
        if (hour<10){
            hour_str = "0"+String.valueOf(hour);
        }else {
            hour_str = String.valueOf(hour);
        }
        if (minute<10){
            minute_str = "0"+String.valueOf(minute);
        }else {
            minute_str = String.valueOf(minute);
        }
        String selected_time = hour_str+":"+minute_str;
        switch (checked_day){
            //Mon start
            case 1 :{
                mon_start_time.setText(selected_time);
                selected_mon_start_time = selected_time;
                checked_day = 8;
                DialogFragment datePicker = new TimePicketFragment();
                datePicker.show(getSupportFragmentManager(),"time picker");
            }break;
            //Tue start
            case 2 :{
                tue_start_time.setText(selected_time);
                selected_tue_start_time = selected_time;
                checked_day = 9;
                DialogFragment datePicker = new TimePicketFragment();
                datePicker.show(getSupportFragmentManager(),"time picker");
            }break;
            //Wed start
            case 3 :{
                wed_start_time.setText(selected_time);
                selected_wed_start_time = selected_time;
                checked_day = 10;
                DialogFragment datePicker = new TimePicketFragment();
                datePicker.show(getSupportFragmentManager(),"time picker");
            }break;
            //Thu start
            case 4 :{
                thu_start_time.setText(selected_time);
                selected_thu_start_time = selected_time;
                checked_day = 11;
                DialogFragment datePicker = new TimePicketFragment();
                datePicker.show(getSupportFragmentManager(),"time picker");
            }break;
            //Fri start
            case 5 :{
                fri_start_time.setText(selected_time);
                selected_fri_start_time = selected_time;
                checked_day = 12;
                DialogFragment datePicker = new TimePicketFragment();
                datePicker.show(getSupportFragmentManager(),"time picker");
            }break;
            //Sat start
            case 6 :{
                sat_start_time.setText(selected_time);
                selected_sat_start_time = selected_time;
                checked_day = 13;
                DialogFragment datePicker = new TimePicketFragment();
                datePicker.show(getSupportFragmentManager(),"time picker");
            }break;
            //Sun start
            case 7 :{
                sun_start_time.setText(selected_time);
                selected_sun_start_time = selected_time;
                checked_day = 14;
                DialogFragment datePicker = new TimePicketFragment();
                datePicker.show(getSupportFragmentManager(),"time picker");
            }break;
            //Mon end
            case 8 :{
                mon_end_time.setText(selected_time);
                selected_mon_end_time = selected_time;
            }break;
            //Tue end
            case 9 :{
                tue_end_time.setText(selected_time);
                selected_tue_end_time = selected_time;
            }break;
            //Wed end
            case 10 :{
                wed_end_time.setText(selected_time);
                selected_wed_end_time = selected_time;
            }break;
            //Thu end
            case 11 :{
                thu_end_time.setText(selected_time);
                selected_thu_end_time = selected_time;}break;
            //Fri end
            case 12 :{
                fri_end_time.setText(selected_time);
                selected_fri_end_time = selected_time;
            }break;
            //Sat end
            case 13 :{
                sat_end_time.setText(selected_time);
                selected_sat_end_time = selected_time;
            }break;
            //Sun end
            case 14 :{
                sun_end_time.setText(selected_time);
                selected_sun_end_time = selected_time;
            }break;
        }
    }

    private void callUpdateAccountAPI(){
        List<WorkingHourObject> wh = new ArrayList<>();
        if (!selected_mon_start_time.equals("")&&!selected_mon_end_time.equals("")){
            WorkingHourObject o = new WorkingHourObject();
            o.setDay(0);
            o.setFrom(selected_mon_start_time);
            o.setTo(selected_mon_end_time);
            wh.add(o);
        }
        if (!selected_tue_start_time.equals("")&&!selected_tue_end_time.equals("")){
            WorkingHourObject o = new WorkingHourObject();
            o.setDay(1);
            o.setFrom(selected_tue_start_time);
            o.setTo(selected_tue_end_time);
            wh.add(o);
        }
        if (!selected_wed_start_time.equals("")&&!selected_wed_end_time.equals("")){
            WorkingHourObject o = new WorkingHourObject();
            o.setDay(2);
            o.setFrom(selected_wed_start_time);
            o.setTo(selected_wed_end_time);
            wh.add(o);
        }
        if (!selected_thu_start_time.equals("")&&!selected_thu_end_time.equals("")){
            WorkingHourObject o = new WorkingHourObject();
            o.setDay(3);
            o.setFrom(selected_thu_start_time);
            o.setTo(selected_thu_end_time);
            wh.add(o);
        }
        if (!selected_fri_start_time.equals("")&&!selected_fri_end_time.equals("")){
            WorkingHourObject o = new WorkingHourObject();
            o.setDay(4);
            o.setFrom(selected_fri_start_time);
            o.setTo(selected_fri_end_time);
            wh.add(o);
        }
        if (!selected_sat_start_time.equals("")&&!selected_sat_end_time.equals("")){
            WorkingHourObject o = new WorkingHourObject();
            o.setDay(5);
            o.setFrom(selected_sat_start_time);
            o.setTo(selected_sat_end_time);
            wh.add(o);
        }
        if (!selected_sun_start_time.equals("")&&!selected_sun_end_time.equals("")){
            WorkingHourObject o = new WorkingHourObject();
            o.setDay(6);
            o.setFrom(selected_sun_start_time);
            o.setTo(selected_sun_end_time);
            wh.add(o);
        }
        List<Integer> selected_cats = new ArrayList<>();
        List<Integer> selected_sub_cats = new ArrayList<>();
        if (selected_categories_list.size()>0){
            for (CategoryObject c : selected_categories_list){
                selected_cats.add(c.getId());
            }
        }
        if (selected_sub_categories_list.size()>0){
            for (CategoryObject c : selected_sub_categories_list){
                selected_sub_cats.add(c.getId());
            }
        }

        UserAPIsClass.updateAccount(
                MyBusinessActivity.this,
                BaseFunctions.getDeviceId(MyBusinessActivity.this),
                "BUSINESS",
                edt_name.getText().toString(),
                edt_phone.getText().toString(),
                edt_email.getText().toString(),
                edt_website.getText().toString(),
                new Gson().toJson(selected_cats),
                new Gson().toJson(selected_sub_cats),
                new Gson().toJson(selected_seating),
                selected_atmosphere,
                selected_first_music,
                selected_second_music,
                selected_third_music,
                selected_cuisine,
                edt_signature.getText().toString(),
                selected_kids,
                selected_parking,
                selected_handicapped,
                selected_delivery,
                selected_restrooms,
                selected_price,
                selected_best_for,
                new Gson().toJson(wh),
                imagePath,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String json1 = new Gson().toJson(json);
                            LoginResponse success = new Gson().fromJson(json1,LoginResponse.class);
                            if (success.getProfile()!=null){
                                if (success.getProfile().getUser_basic_info()!=null){
                                    SharedPrefManager.getInstance(MyBusinessActivity.this).setUser(success.getProfile().getUser_basic_info());
                                    SettingsObject so = new SettingsObject();
                                    so.setVisible_to_search(success.getProfile().getDevice_setting().getVisible_to_search());
                                    so.setAllow_people_to_follow(success.getProfile().getDevice_setting().getAllow_people_to_follow());
                                    so.setIs_profile_public(success.getProfile().getDevice_setting().getIs_profile_public());
                                    so.setPeople_can_comment_on_posts(success.getProfile().getDevice_setting().getPeople_can_comment_on_posts());
                                    so.setPeople_can_vote_posts(success.getProfile().getDevice_setting().getPeople_can_vote_posts());
                                    so.setComments_notifications(success.getProfile().getDevice_setting().getComments_notifications());
                                    so.setVotes_notifications(success.getProfile().getDevice_setting().getVotes_notifications());
                                    so.setView_language(success.getProfile().getDevice_setting().getView_language());
                                    SharedPrefManager.getInstance(MyBusinessActivity.this).setSettings(so);
                                }
                                if (success.getProfile().getDevice()!=null){
                                    SharedPrefManager.getInstance(MyBusinessActivity.this).setDevice(success.getProfile().getDevice());
                                }
                                if (success.getProfile().getDevice_setting()!=null){
                                    SharedPrefManager.getInstance(MyBusinessActivity.this).setDeviceSetting(success.getProfile().getDevice_setting());
                                }
                                if (success.getProfile().getTokens()!=null){
                                    SharedPrefManager.getInstance(MyBusinessActivity.this).setAccessToken(success.getProfile().getTokens().getAccess_token());
                                }
                                Intent intent = new Intent(MyBusinessActivity.this,SplashActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Snackbar.make(root, getResources().getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        callUpdateAccountAPI();
                                    }
                                }).setActionTextColor(getResources().getColor(R.color.white)).show();
                    }
                }
        );
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(MyBusinessActivity.this,MoreActivity.class);
        startActivity(intent);
        finish();
    }
}
