package com.dma.app.taikme.Adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dma.app.taikme.APIsClass.FollowersAPIsClass;
import com.dma.app.taikme.Dialogs.AllCommentsDialog;
import com.dma.app.taikme.Dialogs.AllReviewsDialog;
import com.dma.app.taikme.Dialogs.AmplifyDialog;
import com.dma.app.taikme.Interfaces.IFailure;
import com.dma.app.taikme.Interfaces.IResponse;
import com.dma.app.taikme.Models.DumpPostObject;
import com.dma.app.taikme.Models.OfferObject;
import com.dma.app.taikme.Models.PeopleToFollowObject;
import com.dma.app.taikme.Others.BaseFunctions;
import com.dma.app.taikme.R;
import com.dma.app.taikme.Utils.SharedPrefManager;
import com.facebook.drawee.view.SimpleDraweeView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class FacilityInfoOffersAdapter extends  RecyclerView.Adapter<FacilityInfoOffersAdapter.ViewHolder>{
    private Context context;
    private List<OfferObject> list;

    public FacilityInfoOffersAdapter(Context context,List<OfferObject> list) {
        this.context = context;
        this.list = list;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        private TextView tv_name,tv_percent,tv_till;

        private RelativeLayout img_add,follow_layout;
        private ImageView follow_image;


        public ViewHolder(View view) {
            super(view);
            tv_name = view.findViewById(R.id.item_home_post_offer_name);
            tv_percent = view.findViewById(R.id.item_home_post_offer_percent);
            tv_till = view.findViewById(R.id.item_home_post_offer_rate_value);
            img_add = view.findViewById(R.id.item_home_post_offer_add);
            follow_layout = view.findViewById(R.id.follow_layout);
            follow_image = view.findViewById(R.id.follow_image);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_facility_offer, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final OfferObject dpo = list.get(position);
        String lan = SharedPrefManager.getInstance(context).getDeviceSetting().getView_language();

        holder.tv_percent.setText(dpo.getDiscount()+"%");

        holder.tv_name.setText(lan.equals("en")?dpo.getEn_title():dpo.getAr_title());
        holder.tv_till.setText(context.getResources().getString(R.string.home_valid_till)+" "+BaseFunctions.dateExtractorOld(dpo.getExpiry_date()));

        if (dpo.getIs_following()==0){
            holder.follow_layout.setBackgroundResource(R.drawable.primary_circle);
            holder.follow_image.setImageResource(R.drawable.ic_plus);
        }else if (dpo.getIs_following()==1){
            holder.follow_layout.setBackgroundResource(R.drawable.dark_grey_circle);
            holder.follow_image.setImageResource(R.drawable.ic_minus);
        }
        holder.img_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callFollowAPI(holder,position,String.valueOf(dpo.getId()),dpo.getIs_following()==0?"1":"0");
            }
        });

    }

    private void callFollowAPI(final ViewHolder holder,final  int position, final String id,final  String value){
        FollowersAPIsClass.followNew(
                context,
                BaseFunctions.getDeviceId(context),
                "offer",
                id,
                value,
                new IResponse() {
                    @Override
                    public void onResponse() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            Boolean success = new Gson().fromJson(j,Boolean.class);
                            if (success){
                                if (value.equals("1")) {
                                    Toast.makeText(context, context.getResources().getString(R.string.follow_person_true), Toast.LENGTH_SHORT).show();
                                    holder.follow_layout.setBackgroundResource(R.drawable.dark_grey_circle);
                                    holder.follow_image.setImageResource(R.drawable.ic_minus);
                                    OfferObject o = list.get(position);
                                    o.setIs_following(1);
                                    list.set(position,o);
                                    notifyItemChanged(position);
                                }else {
                                    Toast.makeText(context, context.getResources().getString(R.string.follow_person_false), Toast.LENGTH_SHORT).show();
                                    holder.follow_layout.setBackgroundResource(R.drawable.primary_circle);
                                    holder.follow_image.setImageResource(R.drawable.ic_plus);
                                    OfferObject o = list.get(position);
                                    o.setIs_following(0);
                                    list.set(position,o);
                                    notifyItemChanged(position);
                                }
                            }else {
                                Toast.makeText(context, context.getResources().getString(R.string.error_occurred), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(context, context.getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
