package com.dma.app.taikme.APIs;

import com.dma.app.taikme.Models.BaseResponse;

import org.androidannotations.annotations.rest.Post;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserAPIs {
    @Multipart
    @POST("users/images/cover")
    Call<BaseResponse> update_cover(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Part MultipartBody.Part image
    );
    @Multipart
    @POST("users/images/profile")
    Call<BaseResponse> update_profile_image(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Part MultipartBody.Part image
    );
    @FormUrlEncoded
    @POST("users/tokens/fcm")
    Call<BaseResponse> update_fcm(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("token") String token
    );
    @FormUrlEncoded
    @POST("users/settings")
    Call<BaseResponse> update_settings(@Header("Accept") String accept,
                                       @Header("device_id") String device_id,
                                       @Field("visible_to_search") int visible_to_search,
                                       @Field("allow_people_to_follow") int allow_people_to_follow,
                                       @Field("is_profile_public") int is_profile_public,
                                       @Field("is_stories_visible") int is_stories_visible,
                                       @Field("people_can_comment_on_posts") int people_can_comment_on_posts,
                                       @Field("people_can_vote_posts") int people_can_vote_posts,
                                       @Field("comments_notifications") int comments_notifications,
                                       @Field("votes_notifications") int votes_notifications,
                                       @Field("view_language") String view_language);
    @GET("users/{user_id}/profile")
    Call<BaseResponse> get_profile(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("user_id") String user_id
    );

    @POST("users/removeProfileImage")
    Call<BaseResponse> remove_profile_image(
            @Header("Accept") String accept,
            @Header("device_id") String device_id
    );

    @FormUrlEncoded
    @POST("users/updateName")
    Call<BaseResponse> update_name(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("name") String name
    );


    @POST("users/sendVerificationCode")
    Call<BaseResponse> send_verification_code(
            @Header("Accept") String accept,
            @Header("device_id") String device_id
    );

    @FormUrlEncoded
    @POST("users/verifyAccount")
    Call<BaseResponse> confirmAccount(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("code") String code
    );

    @Multipart
    @POST("users/info/update")
    Call<BaseResponse> update_account(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Part("user_type") RequestBody user_type,
            @Part("name") RequestBody name,
            @Part("mobile_number") RequestBody mobile_number,
            @Part("email") RequestBody email,
            @Part("website") RequestBody website,
            @Part("category") RequestBody category,
            @Part("sub_category") RequestBody sub_category,
            @Part("seating") RequestBody seating,
            @Part("atmosphere") RequestBody atmosphere,
            @Part("first_music_genre") RequestBody first_music_genre,
            @Part("second_music_genre") RequestBody second_music_genre,
            @Part("third_music_genre") RequestBody third_music_genre,
            @Part("cuisine") RequestBody cuisine,
            @Part("signature") RequestBody signature,
            @Part("kids_area") RequestBody kids_area,
            @Part("parking") RequestBody parking,
            @Part("handicapped_entrance") RequestBody handicapped_entrance,
            @Part("takeaway") RequestBody takeaway,
            @Part("rest_rooms") RequestBody rest_rooms,
            @Part("price_range") RequestBody price_range,
            @Part("better_for") RequestBody better_for,
            @Part("hours") RequestBody hours,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("users/updateMobileNumber")
    Call<BaseResponse> change_mobile_number(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("mobile_number") String mobile_number
    );
    @FormUrlEncoded
    @POST("users/updatePassword")
    Call<BaseResponse> change_password(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Field("password") String mobile_number
    );

    @GET("business/{id}/media")
    Call<BaseResponse> profile_media(
            @Header("Accept") String accept,
            @Header("device_id") String device_id,
            @Path("id") String id,
            @Query("page") int page
    );
}
